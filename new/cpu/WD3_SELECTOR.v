module WD3_SELECTOR(
    input [31:0] new_data,
    input [4:0] data_source,
    input [31:0] wd3_now,
    input [1:0] wd3_sel,
    output [31:0] wd3_new
    );

    assign wd3_new = (data_source == wd3_sel)? new_data : wd3_now; 

endmodule