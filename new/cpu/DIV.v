`timescale 1ns / 1ps

module div(
    input           clk,
    input           reset,
    input           sig_signed,
    input           start,
    input   [31:0]  src_A,
    input   [31:0]  src_B,
    output  reg [31:0]  quotient,
    output  reg [31:0]  remainder,
    output          busy_out

    );
    wire    [31:0]  A   =   (src_A[31] & sig_signed)    ?   -src_A  :   src_A;
    wire    [31:0]  B   =   (src_B[31] & sig_signed)    ?   -src_B  :   src_B;
    wire    sig_A   =   src_A[31] & sig_signed;
    wire    sig_B   =   src_B[31] & sig_signed;
    reg     sig_A_st;
    reg     sig_B_st;
    reg     busy;
    reg     [31:0]  mark;
    wire undone;
    assign undone = mark[0];
    // reg     [66:0]  A_ext,B_ext1,B_ext2,B_ext3;
    // wire    [66:0]  sub_1   =   (A_ext << 2) - B_ext1,
    //                 sub_2   =   (A_ext << 2) - B_ext2,
    //                 sub_3   =   (A_ext << 2) - B_ext3;
    reg     [66:0]  A_ext;
    // wire    [66:0]  A_shift2 = A_ext << 2;
    // wire    [34:0]  A_cal = A_shift2[66:32];
    wire    [34:0]  A_cal = A_ext[64:30];
    reg     [34:0]  B_ext1,B_ext2,B_ext3;
    wire    [34:0]  sub_1   =   A_cal - B_ext1,
                    sub_2   =   A_cal - B_ext2,
                    sub_3   =   A_cal - B_ext3;
    // wire    [34:0]  sub_1   =   A_shift2[66:32] - B_ext1,
    //                 sub_2   =   A_shift2[66:32] - B_ext2,
    //                 sub_3   =   A_shift2[66:32] - B_ext3;
    wire    [31:0]  quotient_cal    =   (sig_A_st ^ sig_B_st)   ?   (-A_ext[31:0])  :   A_ext[31:0],
                    remainder_cal   =   (sig_A_st)              ?   (-A_ext[63:32]) :   A_ext[63:32];
    always @ (posedge clk) begin
        if(reset) begin
            mark    <=  32'h0;
            sig_A_st    <=  1'b0;
            sig_B_st    <=  1'b0;
            A_ext   <=  67'b0;
            B_ext1  <=  {2'b0,B};
            B_ext2  <=  {2'b0,B,1'b0};
            B_ext3  <=  {3'b0,B} + {2'b0,B,1'b0};
            quotient    <=  32'h0;
            remainder   <=  32'h0;
            busy    <=  1'b0;
            
        end
        else if(start) begin
            mark    <=  32'hffffffff;
            sig_A_st    <=  sig_A;
            sig_B_st    <=  sig_B;
            A_ext   <=  {35'b0,A};
            // B_ext1  <=  {B,32'b0};
            // B_ext2  <=  {B,33'b0};
            // B_ext3  <=  {B,32'b0}   +   {B,33'b0};
            // A_cal   <=  {3'b0,A};
            B_ext1  <=  {2'b0,B};
            B_ext2  <=  {2'b0,B,1'b0};
            B_ext3  <=  {3'b0,B} + {2'b0,B,1'b0};
            
        end
        else if (mark[15] && (A_ext[47:16] == 32'b0)) begin
            mark    <=  mark >> 16;
            A_ext   <=  A_ext << 16;
        end
        else if(mark[7] && (A_ext[55:24] == 32'b0))begin
            mark    <=  mark >> 8;
            A_ext   <=  A_ext << 8;
        end
        else if(mark[3] && (A_ext[59:28]==32'b0))begin
            mark    <=  mark >> 4;
            A_ext   <=  A_ext << 4;
        end
        else if(mark[0])begin
            mark    <=  mark >> 2;
            A_ext   <=  (~sub_3[34])    ?   {sub_3[32:0],A_ext[29:0],2'b11} :
                        (~sub_2[34])    ?   {sub_2[32:0],A_ext[29:0],2'b10} :
                        (~sub_1[34])    ?   {sub_1[32:0],A_ext[29:0],2'b01} :
                                            {A_ext[64:0],2'b00};
        end
        if(!reset)begin 
            busy    <=  undone;
            {remainder,quotient}    <=  (busy && ~undone)   ?   {remainder_cal,quotient_cal} :
                                                                {remainder,quotient};

        end    
    end
    assign busy_out = undone | busy;
endmodule
