`timescale 1ns / 1ps
`include "global.h"

module LD_EXT(
    input [1:0]         addr10,
    input [31:0]        src_rdata_dm,
    input [2:0]         sig_ld_opt,
    output reg [31:0]   rdata
    );

    always@(*) begin
		case(sig_ld_opt)
			`LW: rdata = src_rdata_dm;
			`LBU: begin	
				case(addr10)
					2'b00: rdata = {24'b0, src_rdata_dm[7:0]}; 
					2'b01: rdata = {24'b0, src_rdata_dm[15:8]};
					2'b10: rdata = {24'b0, src_rdata_dm[23:16]};
					2'b11: rdata = {24'b0, src_rdata_dm[31:24]};
					default: rdata = rdata;
				endcase
			end
			`LB: begin
				case(addr10)
					2'b00: rdata = {{24{src_rdata_dm[7]}}, src_rdata_dm[7:0]}; 
					2'b01: rdata = {{24{src_rdata_dm[15]}}, src_rdata_dm[15:8]};
					2'b10: rdata = {{24{src_rdata_dm[23]}}, src_rdata_dm[23:16]};
					2'b11: rdata = {{24{src_rdata_dm[31]}}, src_rdata_dm[31:24]};
					default: rdata = rdata;
				endcase
			end 
			`LHU: begin
				case(addr10[1])
					1'b0: rdata = {16'b0, src_rdata_dm[15:0]};
					1'b1: rdata = {16'b0, src_rdata_dm[31:16]};
					default: rdata = rdata;
				endcase
			end
			`LH: begin
				case(addr10[1])
					1'b0: rdata = {{16{src_rdata_dm[15]}}, src_rdata_dm[15:0]};
					1'b1: rdata = {{16{src_rdata_dm[31]}}, src_rdata_dm[31:16]};
					default: rdata = rdata;
				endcase
			end
			default: rdata = rdata;
		endcase
	end

endmodule
