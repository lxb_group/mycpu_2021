`timescale 1ns / 1ps
`include "global.h"

module BE(
    input sig_wen,
    input [1:0] src_addr,
    input [31:0] sig_mf_rt_m,
    input [1:0] sig_s_sel,
    output reg [31:0] src_wdata,
    output reg [3:0] sig_be
    );

    always@(*) begin
        if (!sig_wen) begin
            src_wdata = 0;
            sig_be = 0;
        end
        else begin
            case(sig_s_sel)
                `WORD:
                    begin
                        sig_be = 4'b1111;
                        src_wdata = sig_mf_rt_m;
                    end
                `HALF:	
                    begin
                        if(src_addr[1]) begin
                            sig_be = 4'b1100;	
                            src_wdata [31:16] = sig_mf_rt_m[15:0];
                        end
                        else begin
                            sig_be = 4'b0011;
                            src_wdata [15:0] = sig_mf_rt_m[15:0];
                        end
                    end
                `BYTE:
                    begin
                        case(src_addr[1:0])
                            2'b00: begin
                                sig_be = 4'b0001;           
                                src_wdata[7:0] = sig_mf_rt_m[7:0];
                            end
                            2'b01: begin
                                sig_be = 4'b0010;
                                src_wdata[15:8] = sig_mf_rt_m[7:0];
                            end
                            2'b10: begin
                                sig_be = 4'b0100;
                                src_wdata[23:16] = sig_mf_rt_m[7:0];
                            end
                            2'b11: begin
                                sig_be = 4'b1000;
                                src_wdata[31:24] = sig_mf_rt_m[7:0];
                            end
                            default: begin
                                sig_be = sig_be;
                                src_wdata = src_wdata;
                            end
                        endcase
                    end
                default:
                    begin
                        sig_be = sig_be;
                        src_wdata = src_wdata;
                    end
            endcase
        end
    end

endmodule
