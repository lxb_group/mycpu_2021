`timescale 1ns / 1ps
`include "global.h"
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/07/03 20:04:07
// Design Name: 
// Module Name: CHECK
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module CHECK(
    input [31:0] src_pc_m,
    input [1:0] src_s_sel_m,
    input [31:0] src_sl_addr,
    input sig_dm_ren_m,
    input sig_dm_wen_m,
    input sig_ri,               //from ctr
    input break,                //from ctr
    input syscall,              //from ctr
    input sig_overable,         //from ctr
    input over,                 //from alu
    output sig_exc,
    output [4:0] src_exccode,
    output [31:0] src_badaddr
    );

    wire adel, ades, ov;

    assign adel = (src_pc_m[1:0] != 2'b00) || (sig_dm_ren_m && (((src_s_sel_m == `WORD) && (src_sl_addr[1:0] != 2'b00)) || ((src_s_sel_m == `HALF) && (src_sl_addr[0] != 1'b0))));
    assign ades = (sig_dm_wen_m && ((src_s_sel_m == `WORD) && (src_sl_addr[1:0] != 2'b00)) || ((src_s_sel_m == `HALF) && (src_sl_addr[0] != 1'b0)));
    assign ov = sig_overable & over;
    
    assign src_badaddr = (src_pc_m[1:0] != 2'b00)? src_pc_m : src_sl_addr;

    wire [4:0] src_exccode_temp;
    //待优化
    assign src_exccode = (src_pc_m[1:0] != 2'b00)? `ADEL:
                                        sig_ri   ? `RI:         
                                        ov       ? `OVER:
                                        syscall  ? `SYSCALL:src_exccode_temp;
                                        
    assign src_exccode_temp =                                   
                                        break    ? `BREAK:      
                                        adel     ? `ADEL:       
                                        ades     ? `ADES:       
                                                    5'h00;

    assign sig_exc = src_exccode != 5'h00;

endmodule
