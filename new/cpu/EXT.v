`timescale 1ns / 1ps
`include "global.h"

module EXT(
    input [15:0] src_imm,
    input [1:0]  sig_ext_opt,
    output reg [31:0] imm_ext
    );

    always @(*) begin
        case (sig_ext_opt)
            `RZERO_EXTEND:  imm_ext = {src_imm, 16'b0};          
            `ZERO_EXTEND:   imm_ext = {16'b0, src_imm};
            default:        imm_ext = {{16{src_imm[15]}}, src_imm};
        endcase
    end
    
endmodule

