`timescale 1ns / 1ps
`include "global.h"

//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/07/05 19:10:20
// Design Name: 
// Module Name: nom_pc
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module PC(
    input clk,
    input reset,
    input stall,
    input [31:0] pc_f,
    output reg [31:0] nom_pc
    );

    always @(posedge clk)
        if(reset)
            nom_pc <= 32'hBFC0_0000;
        else if (!stall) begin
            nom_pc <= pc_f + 4;
        end
        else begin 
            nom_pc <= pc_f;
        end

endmodule
