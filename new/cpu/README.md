# 2020龙芯杯

## 命名规范

+ 模块内端口名称采用 **类型_标签** 格式，其中类型可为src或者sig(src:source,sig:signal)，标签采用**显而易见的命名**即可。如ALU内的数据输入输出端口可命名为：src_in0, src_in1, src_out；如控制信号可命名为：sig_opt

+ 模块外部连接线名称采用 **连接模块_类型_标签** 格式，如连接ALU的数据线可命名为：ALU_src_in0,ALU_src_in1,ALU_src_out,ALU_sig_opt

+ 模块名称全部大写

+ 模块内部的wire以及reg都用小写即可

+ 双发射同样的部分会后缀一个_A和_B

+ parameter以及宏定义名称都是大写。

+ 总结：C语言一般的命名规范。

+ 如果有疑惑可以直接参考清华代码。

参考代码

```verilog
module GRF(
    input clk,
    input reset,
    //A read grf
    input [4:0] src_raddr1_A,
    input [4:0] src_raddr2_A,
    output [31:0] src_rdata1_A,
    output [31:0] src_rdata2_A,
    //B read grf
    input [4:0] src_raddr1_B,
    input [4:0] src_raddr2_B,
    output [31:0] src_rdata1_B,
    output [31:0] src_rdata2_B,
    //A write grf
    input sig_we_A,
    input [4:0] src_waddr_A,
    input [31:0] src_wdata_A,
    //B write  grf
    input sig_we_B,
    input [4:0] src_waddr_B,
    input [31:0] src_wdata_B
    );

    reg [31:0] grf[31:0];
    integer i;

    assign src_rdata1_A = ((src_raddr1_A != 0) && src_raddr1_A == src_waddr_A && sig_we_A)? src_wdata_A : grf[src_raddr1_A];
    assign src_rdata2_A = ((src_raddr2_A != 0) && src_raddr2_A == src_waddr_A && sig_we_A)? src_wdata_A : grf[src_raddr2_A];
    assign src_rdata1_B = ((src_raddr1_B != 0) && src_raddr1_B == src_waddr_B && sig_we_B)? src_wdata_B : grf[src_raddr1_B];
    assign src_rdata2_B = ((src_raddr2_B != 0) && src_raddr2_B == src_waddr_B && sig_we_B)? src_wdata_B : grf[src_raddr2_B];

    always @(posedge clk)
        if(reset) 
            for (i = 0; i<=31 ; i = i+1) 
                grf[i] <= 0;
        else begin
            if(sig_we_A) 
                grf[src_waddr_A] <= src_wdata_A;
            if(sig_we_B) 
                grf[src_waddr_B] <= src_wdata_B;
        end
endmodule
```

## 关于电路优化
+ 多选的话用case实现综合时会自动最优

## 注释
+ 请在复杂的模块前注释阐述模块运行机理
+ 请在位数比较多的控制信号前阐述信号与功能的一一对应关系
+ 注释请写在文件开头

