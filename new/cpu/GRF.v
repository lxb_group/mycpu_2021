 `timescale 1ns / 1ps
`include "global.h"

//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/07/03 10:31:25
// Design Name: 
// Module Name: GRF
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module GRF(
    input clk,
    input reset,
    input [31:0] PC,
	input [31:0] PC_dt,
    input [4:0] A1,
    input [4:0] A2,
    input [4:0] A3,
    input [4:0] A4,
    input [31:0] WD3,
    input [31:0] WD4,
    input WE3,
    input WE4,
    output [31:0] READ1,
    output [31:0] READ2
    );

	reg[31:0] GRF[0:31];
	integer i;
	
	initial begin
		for(i = 0; i<32 ; i = i + 1)
			GRF[i] = 0;
	end


	assign READ1 = ((A3 != 0) & (A3 == A1) & WE3)? WD3:
				   ((A4 != 0) & (A4 == A1) & WE4)? WD4: GRF[A1];

	assign READ2 = ((A3 != 0) & (A3 == A2) & WE3)? WD3: 
				   ((A4 != 0) & (A4 == A2) & WE4)? WD4: GRF[A2];
	
	always@(posedge clk)
		if(reset) 
			for(i = 0; i<32 ; i = i + 1)
				GRF[i] <= 0;
		else if (A3 && A3 == A4 && WE3 && WE4) begin
			GRF[A3] <= WD3;
			$display("%d@%h: $%d <= %h", $time, PC, A3, WD3);
		end
		else begin
			if (WE3 && A3) begin
				GRF[A3] <= WD3;
				$display("%d@%h: $%d <= %h", $time, PC, A3, WD3);
			end
			if (WE4 && A4) begin
				GRF[A4] <= WD4;
				$display("%d@%h: $%d <= %h", $time, PC_dt, A4, WD4);
			end
		end
		
endmodule
