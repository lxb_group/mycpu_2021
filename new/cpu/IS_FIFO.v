`timescale 1ns / 1ps
`include "global.h"
`define NORMAL  1
`define INTREQ  2
`define BJ      3


module IS_FIFO#(
    parameter FIFO_SIZE = 8
)(
    input clk,
    input reset,
    input clr,
    input valid_in,
    input stall_out,
    input bj_e,
    input intreq_eret,

    input [31:0] addr_in,
    input [31:0] data_in,
    output fifo_not_empty,
    output valid_out,
    output [31:0] addr_out,
    output [31:0] data_out,
    output full
);

    reg [63:0] fifo[0:FIFO_SIZE - 1];
    reg [$clog2(FIFO_SIZE)-1:0] head, tail;
    integer number;
    
    assign valid_out = !stall_out && (number > 0);
    assign full = (number >= FIFO_SIZE - 2) && !bj_e && !intreq_eret;
    assign fifo_not_empty = number > 0;
    assign {addr_out, data_out} = valid_out? fifo[head] : 0;

    integer i;
    always @(posedge clk) begin
        if (reset) begin
            for(i = 0; i<FIFO_SIZE ; i = i+1)
                fifo[i] = 0;
            head <= 0;
            tail <= 0;
            number <= 0;
        end
        else if (clr) begin
            head <= 0;
            tail <= 0;
            number <= 0;
        end
        else if (bj_e && number > 0) begin
            number <= 1 - valid_out;
            head <= head + valid_out;
            tail <= head + 1;
        end
        else begin
            head <= head + valid_out;
            number <= number - valid_out + valid_in;
            tail <= tail + valid_in;
            if (valid_in) begin
                fifo[tail] <= {addr_in, data_in};
            end
        end
    end

endmodule