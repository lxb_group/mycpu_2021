`timescale 1ns / 1ps
`include "global.h"

module CMP(
    input [31:0] src_data1,
    input [31:0] src_data2,
    input [2:0] sig_cmp_opt,
    output reg sig_res_cmp
    );

    wire eq, lez, gtz, ltz, gez;
    assign eq = (src_data1 == src_data2);
	assign gez = ($signed(src_data1) >= 0);
	assign gtz = ($signed(src_data1) > 0);
	assign lez = ($signed(src_data1) <= 0);
	assign ltz = ($signed(src_data1) < 0);

    always @(*) begin
        case (sig_cmp_opt)
            `EQ : sig_res_cmp = eq;
            `NEQ: sig_res_cmp = ~eq;
            `GEZ: sig_res_cmp = gez;
            `GTZ: sig_res_cmp = gtz;
            `LEZ: sig_res_cmp = lez;
            `LTZ: sig_res_cmp = ltz;
            default:
                sig_res_cmp = 1'b0;
        endcase
    end

endmodule
