`timescale 1ns / 1ps
`include "global.h"

module STALL_UNIT (
    input is_full,
    input bj_e,
    input icache_busy,
    input fifo_not_empty,

    input data_full,
    input hazard_stall,
    input intreq,
    input eret_m,
    input dcache_busy,
    input use_dm,
    input [1:0] wd3_sel_m,

    output stall_pc,
    output stall_f2,
    output stall_f3,
    output stall_fifo,
    output stall_e,
    output stall_m,
    output stall_md,

    output clr_f2,
    output clr_f3,
    output clr_fifo,
    output clr_e,
    output clr_m,
    output clr_w
);

    wire bj_need_db = bj_e && !fifo_not_empty;
    wire bj_not_need_db = bj_e && fifo_not_empty;

    assign stall_pc = (is_full && !bj_e) || stall_f2;
    assign stall_f2 = icache_busy && !bj_e && !(intreq || eret_m);
    assign stall_f3 = icache_busy;
    assign stall_fifo = stall_e;

    assign stall_e = hazard_stall || stall_m;
    assign stall_m = (data_full && wd3_sel_m == `DR) || (dcache_busy && use_dm);
    assign stall_md = stall_e || intreq || eret_m;

    assign clr_f2 = is_full && !stall_f2;
    assign clr_f3 = (bj_not_need_db || intreq || eret_m) || (bj_need_db && !icache_busy);
    assign clr_fifo = intreq || eret_m;
    assign clr_e = intreq || eret_m;
    assign clr_m = intreq || eret_m || (hazard_stall && !stall_m);
    assign clr_w = intreq;

endmodule