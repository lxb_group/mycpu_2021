`timescale 1ns / 1ps
`include "global.h"

module DECODER(
    input [31:0] instr,
    output [1:0] ext_opt,
    output alu_srcb_sel,
    output [4:0] alu_opt,
    output dm_ren,
    output dm_wen,
    output [1:0] a3_sel,
    output [1:0] wd3_sel,
    output reg_wen,
    output [2:0] cmp_opt,
    output [44:0] data_hazard,
    output [1:0] s_sel,
    output [2:0] ld_ctr,
    output [2:0] md_op,
    output start,
    output MD,          
    output AO_SEL,      //AO or HILO
    // P7
    output BRANCH,
    output jump,
    output BJ,          //branch or  jump
    output eret,
    output break,
    output syscall,
    output cp0_wen,
    output cp0_ren,
    output ri,
    output overable,
    output JR,           //jr or jalr
    output st_ld
    // output TLB_INSTR,
    // output tlbwi,
    // output tlbr,
    // output tlbp    
    //dual_launch
    );

    wire [5:0] op, func;
    wire [4:0] rs, rt, rd, shamt;
    wire [34:0] DATA_HAZARD;
    wire [34:0] DATA_HAZARD_TEMP1;
    wire [34:0] DATA_HAZARD_TEMP2;
    wire [34:0] DATA_HAZARD_TEMP3;
    assign {op, rs, rt, rd, shamt, func} = instr;

    wire addu, subu, lui, ori, lw, sw, beq, j, jal, jr, jalr;
    wire add, addi, addiu, sub, slt, slti, sltu, sltiu, And, andi, Nor,	Or,	Xor, xori, sllv, sll, srav, sra, srlv, srl;
    wire bne, bgez, bgtz, blez, bltz, bgezal, bltzal;
    wire lb, lbu, lh, lhu, sb, sh;
    wire mult, multu, div, divu, mthi, mtlo, mfhi, mflo;
    wire CAL_R, CAL_I, J, SHIFT, ST, LD;
    wire mtc0, mfc0;
    wire mul;
    // wire tlbwi, tlbr, tlbp;
    
    assign CAL_R = addu || add || sub || subu || slt || sltu || And || Nor || Or || Xor;
    assign CAL_I = lui || ori || addi || addiu || slti || sltiu || andi || xori;
    assign J = j || jal || jalr || jr;
    assign SHIFT = sllv || sll || srav || sra || srlv || srl;
    assign BRANCH = beq || bne || bgez || bgtz || blez || bltz || bgezal || bltzal;
    assign jump = j || jal;
    assign ST = sb || sh || sw;
    assign LD = lb || lbu || lh || lhu || lw;
    assign MD = mult || multu || div || divu || mthi || mtlo || mfhi || mflo || mul;
    assign BJ = J || BRANCH;
    assign CP0_INSTR = mfc0 || mtc0;
    // assign TLB_INSTR = tlbp || tlbwi || tlbr;

    assign ri = !(CAL_R || CAL_I || J || SHIFT || BRANCH || ST || LD || MD || CP0_INSTR || eret || syscall || break); 
    assign overable = add || addi || sub;

    assign addu = (op == 0) && (func == 6'b100001);
	assign subu = (op == 0) && (func == 6'b100011);
	assign lui = (op == 6'b001111);
	assign ori = (op == 6'b001101);
    assign lw = (op == 6'b100011);
	assign sw = (op == 6'b101011);
	assign beq = (op == 6'b000100);
	assign j = (op == 6'b000010);
	assign jal = (op == 6'b000011);
	assign jr = (op == 0) && (func == 6'b001000);
    assign jalr = (op == 0) && (func == 6'b001001);

    assign add = (op == 0) && (func == 6'b100000);
    assign addi = (op == 6'b001000);
    assign addiu = (op == 6'b001001);
    assign sub = (op == 0) && (func == 6'b100010);
    assign slt = (op == 0) && (func == 6'b101010);
    assign slti = (op == 6'b001010);
    assign sltu = (op == 0) && (func == 6'b101011);
    assign sltiu = (op == 6'b001011);
    assign And = (op == 0) && (func == 6'b100100);
    assign andi = (op == 6'b001100);
    assign Nor = (op == 0) && (func == 6'b100111);
    assign Or = (op == 0) && (func == 6'b100101);
    assign Xor = (op == 0) && (func == 6'b100110);
    assign xori = (op == 6'b001110);
    assign sllv = (op == 0) && (func == 6'b000100);
    assign sll = (op == 0) && (func == 6'b000000);
    assign srav = (op == 0) && (func == 6'b000111);
    assign sra = (op == 0) && (func == 6'b000011);
    assign srlv = (op == 0) && (func == 6'b000110);
    assign srl = (op == 0) && (func == 6'b000010);
    
    assign bne = (op == 6'b000101);
    assign bgez = (op == 6'b000001) && (rt == 5'b00001);
    assign bgtz = (op == 6'b000111) && (rt == 5'b00000);
    assign blez = (op == 6'b000110) && (rt == 5'b00000);
    assign bltz = (op == 6'b000001) && (rt == 5'b00000);
    assign bgezal = (op == 6'b000001) && (rt == 5'b10001);
    assign bltzal = (op == 6'b000001) && (rt == 5'b10000);

    assign lb = (op == 6'b100000);
    assign lbu = (op == 6'b100100);
    assign lh = (op == 6'b100001);
    assign lhu = (op == 6'b100101);
    assign sb = (op == 6'b101000);
    assign sh = (op == 6'b101001);

    assign mul = (op == 6'b011100) && (func == 6'b000010);
    assign mult = (op == 0) && (func == 6'b011000);
	assign multu = (op == 0) && (func == 6'b011001);
	assign div = (op == 0) && (func == 6'b011010);
	assign divu = (op == 0) && (func == 6'b011011);
	assign mthi = (op == 0) && (func == 6'b010001);
	assign mtlo = (op == 0) && (func == 6'b010011);
	assign mfhi = (op == 0) && (func == 6'b010000);
	assign mflo = (op == 0) && (func == 6'b010010);

    assign eret =  (op == 6'b010000) && (func == 6'b011000);
    assign mfc0 = (op == 6'b010000) && (rs == 5'b00000);
    assign mtc0 = (op == 6'b010000) && (rs == 5'b00100); 
    assign break = (op == 0) && (func == 6'b001101);
    assign syscall = (op == 0) && (func == 6'b001100);

    assign tlbwi = (op == 6'b010000) && (instr[25]) && (func == 6'b000010);
    assign tlbr = (op == 6'b010000) && (instr[25]) && (func == 6'b000001);
    assign tlbp = (op == 6'b010000) && (instr[25]) && (func == 6'b001000);
    //
    assign ext_opt = {lui, ST || LD || addi || addiu || slti || sltiu};
    assign alu_srcb_sel = ST || LD || CAL_I;
    assign alu_opt = {1'b0, slt || slti || sltu || sltiu || sllv || sll || srav || sra || srlv || srl, subu || sub || sllv || srav || sra || srlv || Xor || xori, addu || subu || ST || LD || add || addi || addiu || sub || Nor || sll || srav || srlv || srl || lui, ori || sltu || sltiu || Nor || Or || sllv || srav || srl};
    assign dm_ren = LD;
    assign dm_wen = ST;
    assign a3_sel = {jal || bgezal || bltzal, jalr || SHIFT || CAL_R || mfhi || mflo || mul};
    
    assign wd3_sel = {jal || jalr || bgezal || bltzal || mfc0, LD || mfc0};
    assign reg_wen = CAL_R || CAL_I || SHIFT || LD || jal || jalr || bgezal || bltzal || mfhi || mflo || mfc0 || mul; 
    assign cmp_opt = {beq || blez || bltz || bltzal, beq || bgez || bgezal || bgtz, bne || bgtz || bltz || bltzal};
    assign s_sel = {sb || lb || lbu, sh || lh || lhu};
    assign ld_ctr = {lh, lb || lhu, lbu || lhu};
    assign md_op = {mthi || mtlo || mfhi || mflo, div || divu || mfhi || mflo, multu || divu || mtlo || mflo};  
    assign start =  mult || multu || div || divu || mthi || mtlo || mul;
    assign AO_SEL = mfhi || mflo || mul;
    assign JR = jr || jalr;
//  P7
    assign cp0_wen = mtc0;
    assign cp0_ren = mfc0;
    assign st_ld = ST || LD;
//                           {`W_PIPE, `W_PIPE, 5'd0, 5'd0, 5'd0, `D2_PIPE, `None}
//                           {Tuse1, Tuse2, grf1, grf2, grfchange, Tnew, WD3}
    //待优�??
    //
    assign DATA_HAZARD = ST? {`E_PIPE, `M_PIPE, rs, rt, 5'd0, `FIFO_PIPE, `None}:
                         LD? {`E_PIPE, `W_PIPE, rs, 5'd0, rt, `W_PIPE, `DR}:
                         (CAL_R || sllv || srav || srlv)? {`E_PIPE, `E_PIPE, rs, rt, rd, `M_PIPE, `AO}:
                         (sll || sra || srl)? {`E_PIPE, `W_PIPE, rt, 5'd0, rd, `M_PIPE, `AO}:
                         (CAL_I)? {`E_PIPE, `W_PIPE, rs, 5'd0, rt, `M_PIPE, `AO}:
                         (beq || bne)? {`E_PIPE, `E_PIPE, rs, rt, 5'd0, `FIFO_PIPE, `None}:
                         DATA_HAZARD_TEMP1;
                                    
    assign DATA_HAZARD_TEMP1 = 
                         (bgez || bgtz || blez || bltz)? {`E_PIPE, `W_PIPE, rs, 5'd0, 5'd0, `FIFO_PIPE, `None}:
                         (bltzal || bgezal)? {`E_PIPE, `W_PIPE, rs, 5'd0, 5'd31, `E_PIPE, `PC8}:
                         jal? {`W_PIPE, `W_PIPE, 5'd0, 5'd0, 5'd31, `E_PIPE, `PC8}:
                         jr? {`E_PIPE, `W_PIPE, rs, 5'd0, 5'd0, `FIFO_PIPE, `None}:
                         j? {`W_PIPE, `W_PIPE, 5'd0, 5'd0, 5'd0, `FIFO_PIPE, `None}:
                         DATA_HAZARD_TEMP2;

    assign DATA_HAZARD_TEMP2 = jalr? {`E_PIPE, `W_PIPE, rs, 5'd0, rd, `E_PIPE, `PC8}:
                         mul? {`E_PIPE, `E_PIPE, rs, rt, rd, `M_PIPE, `AO}:
                         (div || divu || mult || multu)? {`E_PIPE, `E_PIPE, rs, rt, 5'd0, `FIFO_PIPE, `None}:
                         (mthi || mtlo)? {`E_PIPE, `W_PIPE, rs, 5'd0, 5'd0, `FIFO_PIPE, `None}:
                         DATA_HAZARD_TEMP3;
    
    assign DATA_HAZARD_TEMP3 =
                         (mfhi || mflo)? {`W_PIPE, `W_PIPE, 5'd0, 5'd0, rd, `M_PIPE, `AO}:
                         mtc0?           {`M_PIPE, `W_PIPE, rt, 5'd0, 5'd0, `FIFO_PIPE, `None}:
                         mfc0?           {`W_PIPE, `W_PIPE, 5'd0, 5'd0, rt, `W_PIPE, `CP0}:
                                         {`W_PIPE, `W_PIPE, 5'd0, 5'd0, 5'd0, `FIFO_PIPE, `None};

    assign data_hazard = {rs, rt, DATA_HAZARD};


endmodule