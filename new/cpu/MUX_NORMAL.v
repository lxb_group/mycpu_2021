`timescale 1ns / 1ps
`include "global.h"

module MUX_NORMAL(
    input [31:0] instr_fifo,
    input [1:0] grf_waddr_sel_fifo,
    output reg [4:0] grf_waddr_fifo,

    input [31:0] rf_rt_e,
    input [31:0] imm_ext_e,
    input alub_sel_e,
    output [31:0] alub_e,

    input [31:0] alu_res_e,
    input [31:0] hilo_e,
    input ao_sel_e,
    output [31:0] ao_e
    );

    wire [4:0] rt_d1, rd_d1;
    assign {rt_d1, rd_d1} = instr_fifo[20:11];

    always @(*) begin
        case (grf_waddr_sel_fifo)
            2'b01: grf_waddr_fifo = rd_d1;
            2'b10: grf_waddr_fifo = 5'h1f;
            default: 
                grf_waddr_fifo = rt_d1;
        endcase
    end

    assign alub_e = alub_sel_e? imm_ext_e : rf_rt_e;
    assign ao_e = ao_sel_e? hilo_e : alu_res_e;

endmodule
