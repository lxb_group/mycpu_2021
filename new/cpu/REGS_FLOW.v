`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/07/10 08:55:39
// Design Name: 
// Module Name: REGS_FLOW
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module REGS_FLOW(
    input clk,
    input reset,
    input stall,
    input clr,
    //tlb
    // input CP0_INSTR_I,
    // input TLB_INSTR_I,
    // input tlbwi_I,
    // input tlbr_I,
    // input tlbp_I,

    // input mul_I,
    //bpu
    input branch_I,
    input jump_I,
    input [31:0] pc_I,
    input [31:0] instr_I,
    input [31:0] rs_value_I,
    input [31:0] rt_value_I,
    input [31:0] imm_ext_I,
    input [31:0] ao_I,
    //ctr
    input [1:0] ext_opt_I,
    input alub_sel_I,
    input [4:0] alu_opt_I,
    input dm_ren_I,
    input dm_wen_I,
    input [1:0] wd3_sel_I,
    input [31:0] wd3_I,
    input reg_wen_I,
    input [44:0] data_hazard_I,
    input [4:0] grf_waddr_I,
    input [2:0] cmp_opt_I,
    input [1:0] s_sel_I,
    input [2:0] ld_ctr_I,
    input md_I,
    input [2:0] md_op_I,
    input start_I,
    input ao_sel_I,
    input bj_I,
    input eret_I,
    input break_I,
    input syscall_I,
    input cp0_ren_I,
    input cp0_wen_I,
    input ri_I,
    input overable_I,
    input over_I,
    input isir_I,
    input jr_I,
    input [31:0] cp0_grf_I,
    
    //tlb
    // output reg CP0_INSTR_O,
    // output reg TLB_INSTR_O,
    // output reg tlbwi_O,
    // output reg tlbr_O,
    // output reg tlbp_O,

    // output reg mul_O,
    //bpu
    output reg branch_O,
    output reg jump_O,
    output reg [31:0] pc_O,
    output reg [31:0] instr_O,
    output reg [31:0] rs_value_O,
    output reg [31:0] rt_value_O,
    output reg [31:0] imm_ext_O,
    output reg [31:0] ao_O,
    //ctr
    output reg [1:0] ext_opt_O,
    output reg alub_sel_O,
    output reg [4:0] alu_opt_O,
    output reg dm_ren_O,
    output reg dm_wen_O,
    output reg [1:0] wd3_sel_O,
    output reg [31:0] wd3_O,
    output reg reg_wen_O,
    output reg [44:0] data_hazard_O,
    output reg [4:0] grf_waddr_O,
    output reg [2:0] cmp_opt_O,
    output reg [1:0] s_sel_O,
    output reg [2:0] ld_ctr_O,
    output reg md_O,
    output reg [2:0] md_op_O,
    output reg start_O,
    output reg ao_sel_O,
    output reg bj_O,
    output reg eret_O,
    output reg break_O,
    output reg syscall_O,
    output reg cp0_ren_O,
    output reg cp0_wen_O,
    output reg ri_O,
    output reg overable_O,
    output reg over_O,
    output reg isir_O,
    output reg jr_O,
    output reg [31:0] cp0_grf_O
    );

     always @(posedge clk)
        if(reset || clr) begin
            branch_O <= 0;
            jump_O <= 0;
            pc_O <= 32'hBFC0_0000;
            instr_O <= 32'd0;
            rs_value_O <= 32'd0;
            rt_value_O <= 32'd0;
            imm_ext_O <= 32'd0;
            ao_O <= 32'd0;
            alub_sel_O <= 0;  
            alu_opt_O <= 0;  
            dm_ren_O <= 0;    
            dm_wen_O <= 0;    
            wd3_sel_O <= 0;  
            wd3_O <= 0;  
            reg_wen_O <= 0;  
            data_hazard_O <= 45'd0; 
            grf_waddr_O <= 5'd0;
            cmp_opt_O <= 3'd0;
            s_sel_O <= 2'd0;
            ld_ctr_O <= 3'd0;
            md_O <= 0;
            md_op_O <= 3'd0;
            start_O <= 1'd0;
            ao_sel_O <= 1'd0;
            bj_O <= 1'b0;
            eret_O <= 1'b0;
            break_O <= 1'b0;
            syscall_O <= 1'b0;
            cp0_ren_O <= 1'b0;
            cp0_wen_O <= 1'b0;
            ri_O <= 1'b0;
            overable_O <= 1'b0;
            over_O <= 1'b0;     
            isir_O <= 1'b0; 
            jr_O <= 1'b0;  
            cp0_grf_O <= 0;
            ext_opt_O <= 0;
        end
        else if(!stall) begin
            branch_O <= branch_I;
            jump_O <= jump_I;
            pc_O <= pc_I;
            instr_O <= instr_I;
            rs_value_O <= rs_value_I;
            rt_value_O <= rt_value_I;
            imm_ext_O <= imm_ext_I;
            ao_O <= ao_I;
            alub_sel_O <= alub_sel_I;  
            alu_opt_O <= alu_opt_I;  
            dm_ren_O <= dm_ren_I;    
            dm_wen_O <= dm_wen_I;    
            wd3_sel_O <= wd3_sel_I;
            wd3_O <= wd3_I;
            reg_wen_O <= reg_wen_I;  
            data_hazard_O <=data_hazard_I; 
            grf_waddr_O <= grf_waddr_I;   
            cmp_opt_O <= cmp_opt_I;
            s_sel_O <= s_sel_I;
            ld_ctr_O <= ld_ctr_I;
            md_O <= md_I;
            md_op_O <= md_op_I;
            start_O <= start_I;
            ao_sel_O <= ao_sel_I;
            bj_O <= bj_I;
            eret_O <= eret_I;
            break_O <= break_I;
            syscall_O <= syscall_I;
            cp0_ren_O <= cp0_ren_I;
            cp0_wen_O <= cp0_wen_I;
            ri_O <= ri_I;
            overable_O <= overable_I;
            over_O <= over_I;     
            isir_O <= isir_I;   
            jr_O <= jr_I;
            cp0_grf_O <= cp0_grf_I;
            ext_opt_O <= ext_opt_I;
        end

endmodule
