`timescale 1ns / 1ps
`include "global.h"

module DT_FIFO#(
    parameter FIFO_SIZE = 8
)(
    input clk,
    input reset,
    input valid_cache,
    input valid_addr,
    input [4:0] addr_in,
    input [1:0] addr10,
    input [2:0] ld_opt,
    input [31:0] data_in,
    input [31:0] pc_m,

    input [4:0] grf1_E,
    input [4:0] grf2_E,
    output stall_dt_fifo,
    
    input [4:0] grf_waddr_w,
    input grf_wen_w,

    output valid_out,
    output valid_wen,
    output [4:0] addr_out,
    output reg [31:0] data_out,
    output [31:0] pc_dt,
    output full,
    output reg [4:0] debug_overwrite_num
);

    reg [42:0] fifo[0:FIFO_SIZE - 1];
    reg [$clog2(FIFO_SIZE)-1:0] head, tail_addr, tail_data, number_addr, number_data;

    assign valid_out = number_data > 0;
    
    assign full = (number_addr >= FIFO_SIZE - 1);
    assign {pc_dt, valid_wen, addr_out} = {fifo[head][42:10], fifo[head][9:5]};

    integer i;
    wire [31:0] data_out_tail;

    LD_EXT ld_ext(
        .addr10(fifo[tail_data][4:3]),
        .src_rdata_dm(data_in),
        .sig_ld_opt(fifo[tail_data][2:0]),
        .rdata(data_out_tail)
    );

    always @(posedge clk) begin
        if (reset) begin
            for(i = 0; i<FIFO_SIZE ; i = i+1)
                fifo[i] = 0;
            head <= 0;
            tail_addr <= 0;
            tail_data <= 0;
            number_addr <= 0;
            number_data <= 0;
        end
        else begin
            data_out <= data_out_tail;
            number_addr <= number_addr - valid_out + (!full && valid_addr);
            number_data <= number_data - valid_out + valid_cache;
            head <= (head + valid_out);
            tail_addr <= (tail_addr + (!full && valid_addr));
            tail_data <= (tail_data + valid_cache);
            for(i = 0; i<FIFO_SIZE ; i = i+1) begin
                if (i != tail_addr && grf_wen_w && grf_waddr_w == fifo[i][9:5]) begin
                    fifo[i][10] <= 0;
                end else if (i == tail_addr) begin
                    fifo[i][42:11] <= pc_m;
                    fifo[i][10] <= valid_addr;
                    fifo[i][9:5] <= addr_in;
                    fifo[i][4:3] <= addr10;
                    fifo[i][2:0] <= ld_opt;
                end else if (valid_out && i == head) begin
                    fifo[i] <= 0;
                end
            end
        end
    end

    always @(*) begin
        debug_overwrite_num = 0;
        for(i = 0; i<FIFO_SIZE ; i = i+1) begin
            if (i != tail_addr && grf_wen_w && grf_waddr_w == fifo[i][9:5] && fifo[i][10] && !(i == head && number_data > 0)) begin
                debug_overwrite_num = debug_overwrite_num + 1;
            end
        end
    end

    wire [FIFO_SIZE-1:0] stall;
    genvar j;
    generate
        for (j = 0; j < FIFO_SIZE ; j = j + 1) begin
            assign stall[j] = fifo[j][10] && (((fifo[j][9:5] == grf1_E) && (|grf1_E)) || ((fifo[j][9:5] == grf2_E) && (|grf2_E))) && (head != j || (!valid_out));
        end
    endgenerate

    assign stall_dt_fifo = |stall;
    
endmodule