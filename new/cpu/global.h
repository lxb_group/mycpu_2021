`define TESTING  1

`define defaultnettype none

//size
`define WORD    2'b00
`define HALF    2'b01
`define BYTE    2'b10

//exccode type
`define ADEL    5'h04
`define ADES    5'h05
`define SYSCALL 5'h08
`define BREAK   5'h09
`define RI      5'h0a
`define OVER    5'h0c

//cmp type
`define EQ      3'b110
`define NEQ     3'b001
`define GEZ     3'b010
`define GTZ     3'b011
`define LEZ     3'b100
`define LTZ     3'b101

// PIPE_LINE
`define PC_PIPE 5'd0
`define FIFO_PIPE 5'd1
`define E_PIPE 5'd2
`define M_PIPE 5'd3
`define W_PIPE 5'd4

//data source, in W_PIPE
`define AO      5'd0
`define DR      5'd1
`define PC8     5'd2
`define CP0     5'd3
`define None    5'd4

//cp0 regs
`define BADVADDR 5'd8
`define COUNT    5'd9
`define STATUS   5'd12
`define CAUSE    5'd13
`define EPC      5'd14
`define ENTRYHI  5'd10
`define ENTRYLO0 5'd2   
`define ENTRYLO1 5'd3
`define PAGEMASK 5'd5
`define INDEX    5'd0

//ext opt
`define ZERO_EXTEND     2'b00
`define SIGN_EXTEND     2'b01
`define RZERO_EXTEND    2'b10

//ld type
`define LW  3'b000
`define LBU 3'b001
`define LB  3'b010
`define LHU 3'b011
`define LH  3'b100

//FIFO
`define FIFO_SIZE 5'd16

//MD
`define MULT    3'b000
`define MULTU   3'b001
`define DIV     3'b010
`define DIVU    3'b011
`define MTHI    3'b100
`define MTLO    3'b101
`define MFHI    3'b110
`define MFLO    3'b111