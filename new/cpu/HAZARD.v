`timescale 1ns / 1ps
`include "global.h"

module HAZARD (
    input clk,
    input reset,

    input clr_e,
    input stall_e,
    input clr_m,
    input stall_m,

    input [31:0] rs_value_e,
    input [31:0] rt_value_e,
    input [31:0] rt_value_m,

    input [31:0] wd3_m,
    input [31:0] wd3_w,

    input [44:0] data_hazard_fifo,
    input [44:0] data_hazard_e,
    input [44:0] data_hazard_m,
    input [44:0] data_hazard_w,

    output [31:0] mf_rs_value_e,
    output [31:0] mf_rt_value_e,
    output [31:0] mf_rt_value_m,

    input busy_e,
    input MD_e,
    input stall_dt_fifo,

    input grf_wen_dm,
    input [4:0] grf_waddr_dm,
    input [31:0] grf_wdata_dm,

    output hazard_stall
);

    wire stall_m_pipe, stall_md;
    
    wire [32:0] mf_rs_value_e_nom;
    wire [32:0] mf_rt_value_e_nom;
    wire [32:0] mf_rt_value_m_nom;


    wire [4:0] RS_D, RT_D, Tuse1_D, Tuse2_D, grf1_D, grf2_D, grfchange_D, Tnew_D, WD3_D;
    wire [4:0] RS_E, RT_E, Tuse1_E, Tuse2_E, grf1_E, grf2_E, grfchange_E, Tnew_E, WD3_E;
    wire [4:0] RS_M, RT_M, Tuse1_M, Tuse2_M, grf1_M, grf2_M, grfchange_M, Tnew_M, WD3_M;
    wire [4:0] RS_W, RT_W, Tuse1_W, Tuse2_W, grf1_W, grf2_W, grfchange_W, Tnew_W, WD3_W;

    assign {RS_D, RT_D, Tuse1_D, Tuse2_D, grf1_D, grf2_D, grfchange_D, Tnew_D, WD3_D} = data_hazard_fifo;
    assign {RS_E, RT_E, Tuse1_E, Tuse2_E, grf1_E, grf2_E, grfchange_E, Tnew_E, WD3_E} = data_hazard_e;
    assign {RS_M, RT_M, Tuse1_M, Tuse2_M, grf1_M, grf2_M, grfchange_M, Tnew_M, WD3_M} = data_hazard_m;
    assign {RS_W, RT_W, Tuse1_W, Tuse2_W, grf1_W, grf2_W, grfchange_W, Tnew_W, WD3_W} = data_hazard_w;

    assign stall_m_pipe = grfchange_M && ((Tnew_M == `W_PIPE) && ((grfchange_M == grf1_E) || (grfchange_M == grf2_E)));
    assign stall_md = busy_e && MD_e;
    
    reg [31:0] mf_rs_e_stall;
    reg [31:0] mf_rt_e_stall;
    reg [31:0] mf_rt_m_stall;
    reg mf_rs_e_sel;
    reg mf_rt_e_sel;
    reg mf_rt_m_sel;
    always @(posedge clk) begin
         if (reset) begin
            mf_rs_e_stall <= 0;
            mf_rt_e_stall <= 0;
            mf_rt_m_stall <= 0;
            mf_rs_e_sel <= 0;
            mf_rt_e_sel <= 0;
            mf_rt_m_sel <= 0;
        end
        else begin
            if (clr_e || !stall_e) begin
                mf_rs_e_sel <= 0;
                mf_rt_e_sel <= 0;
            end
            else begin
                if (mf_rs_value_e_nom[32]) begin
                    mf_rs_e_stall <= mf_rs_value_e_nom[31:0];
                    mf_rs_e_sel <= 1;
                end
                if (mf_rt_value_e_nom[32]) begin
                    mf_rt_e_stall <= mf_rt_value_e_nom[31:0];
                    mf_rt_e_sel <= 1;
                end 
            end
            if (clr_m || !stall_m) begin
                mf_rt_m_sel <= 0;
            end
            else if (mf_rt_value_m_nom[32]) begin
                mf_rt_m_stall <= mf_rt_value_m_nom[31:0];
                mf_rt_m_sel <= 1;
            end
        end
    end

    assign mf_rs_value_e = mf_rs_e_sel? mf_rs_e_stall : mf_rs_value_e_nom[31:0];
    assign mf_rt_value_e = mf_rt_e_sel? mf_rt_e_stall : mf_rt_value_e_nom[31:0];
    assign mf_rt_value_m = mf_rt_m_sel? mf_rt_m_stall : mf_rt_value_m_nom[31:0];

    assign mf_rs_value_e_nom = (grfchange_M != 0) && (grfchange_M == RS_E) && (WD3_M == `AO)? {1'b1, wd3_m}:
                           (grfchange_W != 0) && (grfchange_W == RS_E)?                  {1'b1, wd3_w}:
                           (grf_wen_dm != 0) && (grf_waddr_dm == RS_E)?                  {1'b1, grf_wdata_dm}:
                                                                                         {1'b0, rs_value_e};

    assign mf_rt_value_e_nom = (grfchange_M != 0) && (grfchange_M == RT_E) && (WD3_M == `AO)? {1'b1, wd3_m}:
                           (grfchange_W != 0) && (grfchange_W == RT_E)?                  {1'b1, wd3_w}:
                           (grf_wen_dm != 0) && (grf_waddr_dm == RT_E)?                  {1'b1, grf_wdata_dm}:
                                                                                         {1'b0, rt_value_e};

    assign mf_rt_value_m_nom = (grfchange_W != 0) && (grfchange_W == RT_M)? wd3_w : rt_value_m;

    assign hazard_stall = stall_m_pipe || stall_md || stall_dt_fifo;

endmodule