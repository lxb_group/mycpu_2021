`timescale 1ns / 1ps
`include "global.h"

module ALU(
    input [4:0] sig_opt,
    input [31:0] src_a,
    input [31:0] src_b,
    input [4:0 ] src_shamt,
    output reg [31:0] src_out,
	output sig_over
    );

    wire[31:0] srav, sra;
	wire[32:0] temp_add, temp_sub;
	assign srav = ($signed(src_b)) >>> src_a[4:0];
	assign sra = ($signed(src_b)) >>> (src_shamt);

	assign temp_add = {src_a[31], src_a} + {src_b[31], src_b};
	assign temp_sub = {src_a[31], src_a} - {src_b[31], src_b};
	assign sig_over = (temp_add[32] != temp_add[31] && sig_opt == 5'b00010) || (temp_sub[32] != temp_sub[31] && sig_opt == 5'b00110);

    always @(*) begin
        case (sig_opt)
            5'b00000: src_out = (src_a & src_b);
            5'b00001: src_out = (src_a | src_b);
            5'b00010: src_out = (src_a + src_b);
            5'b00011: src_out = (~(src_a | src_b));
            5'b00100: src_out = (src_a ^ src_b);
            5'b00110: src_out = (src_a - src_b);
            5'b01001: src_out = (src_a < src_b);
            5'b01000: src_out = ($signed(src_a) < $signed(src_b));
            5'b01010: src_out = (src_b << src_shamt);
            5'b01011: src_out = (src_b >> src_shamt);
            5'b01100: src_out = sra;
            5'b01101: src_out = (src_b << src_a[4:0]);
            5'b01110: src_out = (src_b >> src_a[4:0]);
            5'b01111: src_out = srav;
            default: src_out = (src_a + src_b);
        endcase
    end

endmodule

