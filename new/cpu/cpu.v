`timescale 1ns / 1ps
`include "global.h"

module cpu (
    input clk,
    input resetn,
    input [5:0] hwint,

    input icache_valid,
    input icache_stall,
    input [31:0] inst_sram_rdata,
    output inst_sram_en,
    output [3:0] inst_sram_wen,
    output [31:0] inst_sram_addr,
    output [31:0] inst_sram_wdata,

    input dcache_valid,
    input dcache_busy,
    input [31:0] data_sram_rdata,
    output data_sram_en,
    output [3:0] data_sram_wen,
    output [31:0] data_sram_addr,
    output [31:0] data_sram_wdata,

    output [63:0] debug_wb_pc,
    output [7:0] debug_wb_rf_wen,
    output [9:0] debug_wb_rf_wnum,
    output [63:0] debug_wb_rf_wdata,
    output [4:0] debug_overwrite_num
);
    wire icache_busy;
    wire reset = ~resetn;
    wire isir_f2, isir_f3;
    wire [31:0] instr_fifo, instr_e, instr_m, instr_w;
    wire [31:0] pc_f1, pc_f2, pc_f3, pc_fifo, pc_e, pc_m, pc_dt, pc_w;
    wire [31:0] rs_value_fifo;
    wire [31:0] rt_value_fifo;
    wire [31:0] rs_value_e, mf_rs_value_e;
    wire [31:0] rt_value_e, mf_rt_value_e;
    wire [31:0] rt_value_m, mf_rt_value_m;

    wire full;
    wire stall_pc;
    wire [31:0] nom_pc;
    wire stall_fifo, stall_e, stall_m, stall_f2, stall_f3;

    assign inst_sram_en = isir_f2 && !stall_f2;
    reg has_inst_sram_en;

    always @(posedge clk) begin
        if (reset) begin
            has_inst_sram_en <= 0;
        end
        else begin
            if (icache_valid || !has_inst_sram_en) begin
                has_inst_sram_en <= inst_sram_en;
            end
        end
    end
    assign icache_busy = (!icache_valid && has_inst_sram_en) || icache_stall;

    assign inst_sram_wen = 0;
    assign inst_sram_wdata = 0;
    assign inst_sram_addr = pc_f2;

    PC pc(
        .clk(clk),
        .reset(reset),
        .stall(stall_pc),
        .pc_f(pc_f1),
        .nom_pc(nom_pc)
    );

    wire eret_m;
    wire intreq;
    wire jump_e;
    wire branch_e;
    wire jr_e;
    wire [31:0] epc;

    NPC npc(
        .eret(eret_m),
        .intreq(intreq),
        .jump(jump_e && !stall_e),
        .branch(branch_e && !stall_e),
        .jr(jr_e && !stall_e),
        .instr_e(instr_e),
        .nom_pc(nom_pc),
        .pc4_e(pc_e + 4),
        .rs_value_e(mf_rs_value_e),
        .epc(epc),
        .npc(pc_f1)
    );

    wire clr_f2, clr_f3, clr_fifo;
    wire valid_out, valid_addr;
    
    REGS_FLOW F1_F2(
        .clk(clk),
        .reset(reset),
        .stall(stall_f2),
        .clr(clr_f2),
        .pc_I(pc_f1),
        .isir_I(1'b1),

        .pc_O(pc_f2),
        .isir_O(isir_f2)
    );

    //TLB should be here

    REGS_FLOW F2_F3(
        .clk(clk),
        .reset(reset),
        .stall(stall_f3),
        .clr(clr_f3),
        .pc_I(pc_f2),
        .isir_I(isir_f2),

        .pc_O(pc_f3),
        .isir_O(isir_f3)
    );

    wire fifo_not_empty;
    wire bj_e = (branch_e || jr_e || jump_e) && !stall_e;

    IS_FIFO is_fifo(
        .clk(clk),
        .reset(reset),
        .clr(clr_fifo),
        .valid_in(isir_f3 && icache_valid),
        .stall_out(stall_fifo),
        .bj_e(bj_e),
        .intreq_eret(intreq || eret_m),

        .addr_in(pc_f3),
        .data_in(inst_sram_rdata),
        .fifo_not_empty(fifo_not_empty),
        .valid_out(valid_out),
        .addr_out(pc_fifo),
        .data_out(instr_fifo),
        .full(full)
    );

    wire [4:0] grf_waddr_w;
    wire valid_out_w, valid_wen;
    wire [4:0] grf_mem_waddr;
    wire [31:0] grf_mem_wdata;
    wire [31:0] wd3_w;
    GRF grf(
        .clk(clk),
        .reset(reset),
        .PC(pc_w),
        .PC_dt(pc_dt),
        .A1(instr_fifo[25:21]),
        .A2(instr_fifo[20:16]),
        .A3(grf_waddr_w),
        .A4(grf_mem_waddr),
        .WD3(wd3_w),
        .WD4(grf_mem_wdata),
        .WE3(reg_wen_w),
        .WE4(valid_out_w && valid_wen),
        .READ1(rs_value_fifo),
        .READ2(rt_value_fifo)
    );

    assign debug_wb_pc = {pc_w, pc_dt};
    assign debug_wb_rf_wen = {{4{reg_wen_w}}, {4{valid_out_w && valid_wen}}};
    assign debug_wb_rf_wnum = {grf_waddr_w, grf_mem_waddr};
    assign debug_wb_rf_wdata = {wd3_w, grf_mem_wdata};

    wire [1:0] ext_opt_fifo;
    wire alu_srcb_sel_fifo;
    wire [4:0] alu_opt_fifo;
    wire dm_ren_fifo;
    wire dm_wen_fifo;
    wire [1:0] grf_waddr_sel_fifo;
    wire [1:0] wd3_sel_fifo;
    wire reg_wen_fifo;
    wire [2:0] cmp_opt_fifo;
    wire [44:0] data_hazard_fifo;
    wire [1:0] s_sel_fifo;
    wire [2:0] ld_ctr_fifo;
    wire [2:0] md_op_fifo;
    wire start_fifo;
    wire MD_fifo;
    wire AO_SEL_fifo;
    wire BRANCH_fifo;
    wire jump_fifo;
    wire BJ_fifo;
    wire eret_fifo;
    wire break_fifo;
    wire syscall_fifo;
    wire cp0_wen_fifo;
    wire cp0_ren_fifo;
    wire ri_fifo;
    wire overable_fifo;
    wire jr_fifo;
    wire st_ld_fifo;

    DECODER decoder(
        .instr(instr_fifo),
        .ext_opt(ext_opt_fifo),
        .alu_srcb_sel(alu_srcb_sel_fifo),
        .alu_opt(alu_opt_fifo),
        .dm_ren(dm_ren_fifo),
        .dm_wen(dm_wen_fifo),
        .a3_sel(grf_waddr_sel_fifo),
        .wd3_sel(wd3_sel_fifo),
        .reg_wen(reg_wen_fifo),
        .cmp_opt(cmp_opt_fifo),
        .data_hazard(data_hazard_fifo),
        .s_sel(s_sel_fifo),
        .ld_ctr(ld_ctr_fifo),
        .md_op(md_op_fifo),
        .start(start_fifo),
        .MD(MD_fifo),          
        .AO_SEL(AO_SEL_fifo),      //AO or HILO
        // P7
        .BRANCH(BRANCH_fifo),
        .jump(jump_fifo),
        .BJ(BJ_fifo),          //branch or  jump
        .eret(eret_fifo),
        .break(break_fifo),
        .syscall(syscall_fifo),
        .cp0_wen(cp0_wen_fifo),
        .cp0_ren(cp0_ren_fifo),
        .ri(ri_fifo),
        .overable(overable_fifo),
        .JR(jr_fifo),
        .st_ld(st_ld_fifo)
    );

    wire [31:0] imm_ext_fifo;
    EXT ext(
        .src_imm(instr_fifo[15:0]),
        .sig_ext_opt(ext_opt_fifo),
        .imm_ext(imm_ext_fifo)
    );

    wire [4:0] grf_waddr_fifo;
    wire [31:0] alub_e;


    wire BRANCH_e;
    wire [31:0] imm_ext_e;
    wire alu_srcb_sel_e;
    wire [4:0] alu_opt_e;
    wire dm_ren_e;
    wire dm_wen_e;
    wire [1:0] wd3_sel_e;
    wire reg_wen_e;
    wire [44:0] data_hazard_e;
    wire [4:0] grf_waddr_e;
    wire [2:0] cmp_opt_e;
    wire [1:0] s_sel_e;
    wire [2:0] ld_ctr_e;
    wire MD_e;
    wire [2:0] md_op_e;
    wire start_e;
    wire AO_SEL_e;
    wire BJ_e;
    wire eret_e;
    wire break_e;
    wire syscall_e;
    wire cp0_ren_e;
    wire cp0_wen_e;
    wire ri_e;
    wire overable_e;
    wire isir_e;

    wire [31:0] wd3_new_fifo;
    wire [31:0] wd3_e, wd3_new_e;
    wire [31:0] wd3_m, wd3_new_m;

    WD3_SELECTOR wd3_selector_d2_A(
        .new_data(pc_fifo + 8),
        .data_source(`PC8),

        .wd3_now(0),
        .wd3_sel(wd3_sel_fifo),
        .wd3_new(wd3_new_fifo)
    );

    wire clr_e, clr_m, clr_w;

    REGS_FLOW DE(
        .clk(clk),
        .reset(reset),
        .stall(stall_e),
        .clr(clr_e),
        .branch_I(BRANCH_fifo),
        .jump_I(jump_fifo),
        .pc_I(valid_out? pc_fifo : 32'hBFC0_0000),
        .instr_I(instr_fifo),
        .rs_value_I(rs_value_fifo),
        .rt_value_I(rt_value_fifo),
        .imm_ext_I(imm_ext_fifo),
        .alub_sel_I(alu_srcb_sel_fifo),
        .alu_opt_I(alu_opt_fifo),
        .dm_ren_I(dm_ren_fifo),
        .dm_wen_I(dm_wen_fifo),
        .wd3_sel_I(wd3_sel_fifo),
        .wd3_I(wd3_new_fifo),
        .reg_wen_I(reg_wen_fifo),
        .data_hazard_I(data_hazard_fifo),
        .grf_waddr_I(grf_waddr_fifo),
        .cmp_opt_I(cmp_opt_fifo),
        .s_sel_I(s_sel_fifo),
        .ld_ctr_I(ld_ctr_fifo),
        .md_I(MD_fifo),
        .md_op_I(md_op_fifo),
        .start_I(start_fifo),
        .ao_sel_I(AO_SEL_fifo),
        .bj_I(BJ_fifo),
        .eret_I(eret_fifo),
        .break_I(break_fifo),
        .syscall_I(syscall_fifo),
        .cp0_ren_I(cp0_ren_fifo),
        .cp0_wen_I(cp0_wen_fifo),
        .ri_I(ri_fifo),
        .overable_I(overable_fifo),
        .isir_I(valid_out),
        .jr_I(jr_fifo),
        
        .branch_O(BRANCH_e),
        .jump_O(jump_e),
        .pc_O(pc_e),
        .instr_O(instr_e),
        .rs_value_O(rs_value_e),
        .rt_value_O(rt_value_e),
        .imm_ext_O(imm_ext_e),
        .alub_sel_O(alu_srcb_sel_e),
        .alu_opt_O(alu_opt_e),
        .dm_ren_O(dm_ren_e),
        .dm_wen_O(dm_wen_e),
        .wd3_sel_O(wd3_sel_e),
        .wd3_O(wd3_e),
        .reg_wen_O(reg_wen_e),
        .data_hazard_O(data_hazard_e),
        .grf_waddr_O(grf_waddr_e),
        .cmp_opt_O(cmp_opt_e),
        .s_sel_O(s_sel_e),
        .ld_ctr_O(ld_ctr_e),
        .md_O(MD_e),
        .md_op_O(md_op_e),
        .start_O(start_e),
        .ao_sel_O(AO_SEL_e),
        .bj_O(BJ_e),
        .eret_O(eret_e),
        .break_O(break_e),
        .syscall_O(syscall_e),
        .cp0_ren_O(cp0_ren_e),
        .cp0_wen_O(cp0_wen_e),
        .ri_O(ri_e),
        .overable_O(overable_e),
        .isir_O(isir_e),
        .jr_O(jr_e)
    );

    wire [31:0] alu_res_e;
    wire sig_over_e;

    ALU alu(
        .sig_opt(alu_opt_e),
        .src_a(mf_rs_value_e),
        .src_b(alub_e),
        .src_shamt(instr_e[10:6]),
        .src_out(alu_res_e),
	    .sig_over(sig_over_e)
    );

    wire [31:0] hilo_e;
    wire busy_e;
    wire stall_md;

    MD md(
        .clk(clk),
        .reset(reset),
        .clr(0),
        .sig_stall(stall_md),
        .sig_start(start_e),
        .md_op(md_op_e),
        .opt1(mf_rs_value_e),
        .opt2(mf_rt_value_e),
        .HI_LO(hilo_e),
        .BUSY(busy_e)
    );

    wire res_cmp_e;
    CMP cmp(
        .src_data1(mf_rs_value_e),
        .src_data2(mf_rt_value_e),
        .sig_cmp_opt(cmp_opt_e),
        .sig_res_cmp(res_cmp_e)
    );

    assign branch_e = BRANCH_e && res_cmp_e && !stall_e;
    wire [31:0] ao_e;

    MUX_NORMAL mux_normal(
        .instr_fifo(instr_fifo),
        .grf_waddr_sel_fifo(grf_waddr_sel_fifo),
        .grf_waddr_fifo(grf_waddr_fifo),

        .rf_rt_e(mf_rt_value_e),
        .imm_ext_e(imm_ext_e),
        .alub_sel_e(alu_srcb_sel_e),
        .alub_e(alub_e),

        .alu_res_e(alu_res_e),
        .hilo_e(hilo_e),
        .ao_sel_e(AO_SEL_e),
        .ao_e(ao_e)
    );

    wire [31:0] ao_m;
    wire dm_ren_m;
    wire dm_wen_m;
    wire [1:0] wd3_sel_m;
    wire reg_wen_m;
    wire [44:0] data_hazard_m;
    wire [44:0] data_hazard_w;
    wire [4:0] grf_waddr_m;
    wire [1:0] s_sel_m;
    wire [2:0] ld_ctr_m;
    wire BJ_m;
    wire break_m;
    wire syscall_m;
    wire cp0_ren_m;
    wire cp0_wen_m;
    wire ri_m;
    wire overable_m;
    wire sig_over_m;
    wire isir_m;

    WD3_SELECTOR wd3_selector_e(
        .new_data(ao_e),
        .data_source(`AO),
        .wd3_now(wd3_e),
        .wd3_sel(wd3_sel_e),
        .wd3_new(wd3_new_e)
    );

    REGS_FLOW EM(
        .clk(clk),
        .reset(reset),
        .stall(stall_m),
        .clr(clr_m),

        .pc_I(pc_e),
        .instr_I(instr_e),
        .rt_value_I(mf_rt_value_e),
        .ao_I(ao_e),
        .dm_ren_I(dm_ren_e),
        .dm_wen_I(dm_wen_e),
        .wd3_sel_I(wd3_sel_e),
        .wd3_I(wd3_new_e),
        .reg_wen_I(reg_wen_e),
        .data_hazard_I(data_hazard_e),
        .grf_waddr_I(grf_waddr_e),
        .s_sel_I(s_sel_e),
        .ld_ctr_I(ld_ctr_e),
        .bj_I(BJ_e),
        .eret_I(eret_e),
        .break_I(break_e),
        .syscall_I(syscall_e),
        .cp0_ren_I(cp0_ren_e),
        .cp0_wen_I(cp0_wen_e),
        .ri_I(ri_e),
        .overable_I(overable_e),
        .over_I(sig_over_e),
        .isir_I(isir_e),

        .pc_O(pc_m),
        .instr_O(instr_m),
        .rt_value_O(rt_value_m),
        .ao_O(ao_m),
        .dm_ren_O(dm_ren_m),
        .dm_wen_O(dm_wen_m),
        .wd3_sel_O(wd3_sel_m),
        .wd3_O(wd3_m),
        .reg_wen_O(reg_wen_m),
        .data_hazard_O(data_hazard_m),
        .grf_waddr_O(grf_waddr_m),
        .s_sel_O(s_sel_m),
        .ld_ctr_O(ld_ctr_m),
        .bj_O(BJ_m),
        .eret_O(eret_m),
        .break_O(break_m),
        .syscall_O(syscall_m),
        .cp0_ren_O(cp0_ren_m),
        .cp0_wen_O(cp0_wen_m),
        .ri_O(ri_m),
        .overable_O(overable_m),
        .over_O(sig_over_m),
        .isir_O(isir_m)
    );

    wire sig_exc;
    wire [4:0] src_exccode;
    wire [31:0] src_badaddr;   

    CHECK check(
        .src_pc_m(pc_m),
        .src_s_sel_m(s_sel_m),
        .src_sl_addr(ao_m),
        .sig_dm_ren_m(dm_ren_m),
        .sig_dm_wen_m(dm_wen_m),
        .sig_ri(ri_m),
        .break(break_m),
        .syscall(syscall_m),
        .sig_overable(overable_m),
        .over(sig_over_m),
        .sig_exc(sig_exc),
        .src_exccode(src_exccode),
        .src_badaddr(src_badaddr)
    );

    reg bd_m;

    always @(posedge clk) begin
        if (reset) begin
            bd_m <= 0;
        end
        else if (isir_m) begin
            bd_m <= BJ_m;
        end
    end

    wire [31:0] pc_cp0;
    wire [31:0] temp_pc_cp0;
    assign pc_cp0 = isir_m? pc_m:
                    isir_e? pc_e:
                    valid_out? pc_fifo:temp_pc_cp0;
    assign temp_pc_cp0 = isir_f3? pc_f3:
                         isir_f2? pc_f2:pc_f1;

                    
    wire [31:0] cp0_rdata;
    CP0 cp0(
        .clk(clk),
        .reset(reset),
        .src_pc_cp0(pc_cp0),
        .src_addr(src_badaddr),
        .hwint(hwint),
        .eret(eret_m),
        .sig_BD(bd_m),
        .sig_except(sig_exc),
        .src_exccode(src_exccode),
        .cp0_addr(instr_m[15:11]),
        .sig_wen(cp0_wen_m),
        .src_wdata(mf_rt_value_m),
        .src_rdata(cp0_rdata),
        .src_epc(epc),
        .sig_intreq(intreq)
    );

    wire data_full;
    wire stall_dt_fifo;

    DT_FIFO#(4) dt_fifo(
        .clk(clk),
        .reset(reset),
        .valid_cache(dcache_valid),
        .valid_addr(dm_ren_m && !intreq && !stall_m),
        .addr_in(grf_waddr_m),
        .addr10(ao_m[1:0]),
        .ld_opt(ld_ctr_m),
        .data_in(data_sram_rdata),
        .pc_m(pc_m),

        .grf1_E(data_hazard_e[19:15]),
        .grf2_E(data_hazard_e[24:20]),
        .stall_dt_fifo(stall_dt_fifo),

        .grf_waddr_w(grf_waddr_w),
        .grf_wen_w(reg_wen_w),

        .valid_out(valid_out_w),
        .valid_wen(valid_wen),
        .addr_out(grf_mem_waddr),
        .data_out(grf_mem_wdata),
        .pc_dt(pc_dt),
        .full(data_full),
        .debug_overwrite_num(debug_overwrite_num)
    );

    assign data_sram_en = (dm_ren_m || dm_wen_m) && !stall_m && !intreq;
    assign data_sram_addr = ao_m;

    BE be_A(
        .sig_wen(dm_wen_m && !stall_m && !intreq),
        .src_addr(ao_m[1:0]),
        .sig_mf_rt_m(mf_rt_value_m),
        .sig_s_sel(s_sel_m),
        .src_wdata(data_sram_wdata),
        .sig_be(data_sram_wen)
    );
    integer mem;
    initial begin
        mem = $fopen("../../../../../../../cpu132_gettrace/mem_write.txt", "w");
    end

    always @(posedge clk) begin
        if (|data_sram_wen) begin
            $fdisplay(mem, "%d@%h: *%h <= %h", $time, pc_m, data_sram_addr, data_sram_wdata);
        end
    end

    WD3_SELECTOR wd3_selector_m1_A(
        .new_data(cp0_rdata),
        .data_source(`CP0),
        .wd3_now(wd3_m),
        .wd3_sel(wd3_sel_m),
        .wd3_new(wd3_new_m)
    );

    REGS_FLOW MW(
        .clk(clk),
        .reset(reset),
        .stall(1'b0),
        .clr(clr_w),

        .pc_I(pc_m),
        .wd3_I(wd3_new_m),
        .reg_wen_I(reg_wen_m && !dm_ren_m && wd3_sel_m != `DR),
        .data_hazard_I(((!dm_ren_m && wd3_sel_m != `DR)? data_hazard_m : 0)),
        .grf_waddr_I(grf_waddr_m),
        .instr_I(instr_m),

        .pc_O(pc_w),
        .wd3_O(wd3_w),
        .reg_wen_O(reg_wen_w),
        .data_hazard_O(data_hazard_w),
        .grf_waddr_O(grf_waddr_w),
        .instr_O(instr_w)
    );

    wire hazard_stall;
    HAZARD hazard(
        .clk(clk),
        .reset(reset),
        .clr_e(clr_e),
        .stall_e(stall_e),
        .clr_m(clr_m),
        .stall_m(stall_m),
        .rs_value_e(rs_value_e),
        .rt_value_e(rt_value_e),
        .rt_value_m(rt_value_m),
        .wd3_m(wd3_m),
        .wd3_w(wd3_w),

        .data_hazard_fifo(data_hazard_fifo),
        .data_hazard_e(data_hazard_e),
        .data_hazard_m(data_hazard_m),
        .data_hazard_w(data_hazard_w),
        .mf_rs_value_e(mf_rs_value_e),
        .mf_rt_value_e(mf_rt_value_e),
        .mf_rt_value_m(mf_rt_value_m),
        .busy_e(busy_e),
        .MD_e(MD_e),
        .stall_dt_fifo(stall_dt_fifo),
        .grf_wen_dm(valid_out_w && valid_wen),
        .grf_waddr_dm(grf_mem_waddr),
        .grf_wdata_dm(grf_mem_wdata),
        .hazard_stall(hazard_stall)
    );

    STALL_UNIT stall_unit(
        .is_full(full),
        .bj_e(bj_e),
        .icache_busy(icache_busy),
        .fifo_not_empty(fifo_not_empty),

        .data_full(data_full),
        .hazard_stall(hazard_stall),
        .intreq(intreq),
        .eret_m(eret_m),
        .dcache_busy(dcache_busy),
        .use_dm(dm_ren_m || dm_wen_m),
        .wd3_sel_m(wd3_sel_m),

        .stall_pc(stall_pc),
        .stall_f2(stall_f2),
        .stall_f3(stall_f3),
        .stall_fifo(stall_fifo),
        .stall_e(stall_e),
        .stall_m(stall_m),
        .stall_md(stall_md),

        .clr_f2(clr_f2),
        .clr_f3(clr_f3),
        .clr_fifo(clr_fifo),
        .clr_e(clr_e),
        .clr_m(clr_m),
        .clr_w(clr_w)
    );

endmodule
