`timescale 1ns / 1ps
`include "global.h"

//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/07/05 19:14:45
// Design Name: 
// Module Name: npc
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module NPC(
    input eret,
    input intreq,
    input jump,
    input branch,
    input jr,
    input [31:0] instr_e,
    input [31:0] nom_pc,
    input [31:0] pc4_e,
    input [31:0] rs_value_e,
    input [31:0] epc,
    output [31:0] npc
    );

    wire [31:0] branch_pc, jump_pc;

    reg [31:0] npc_bj;
    wire [1:0] npc_bj_sel;

    assign branch_pc = {pc4_e[31:2] + {{14{instr_e[15]}}, instr_e[15:0]}, pc4_e[1:0]};
    assign jump_pc = {pc4_e[31:28], instr_e[25:0], 2'b00};

    assign npc_bj_sel = {jump || branch, jump || jr};

    always @(*) begin
        case (npc_bj_sel)
            2'b00: npc_bj = nom_pc;
            2'b01: npc_bj = rs_value_e;
            2'b10: npc_bj = branch_pc;
            default: npc_bj = jump_pc;
        endcase
    end
    
    assign npc = intreq? 32'hBFC00380:
                     eret?   epc: npc_bj;
endmodule

//设计好了架构，然后比较通用的模块就用去年双发的改了改，其他的就重写了，现在正在一点点连线然后发现问题
