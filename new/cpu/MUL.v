`timescale 1ns / 1ps

// `define start_1 0
// `define bound_width_1 35

// `define start_2 6
// `define bound_width_2 41

// `define start_3 12
// `define bound_width_3 47

// `define start_4 18
// `define bound_width_4 53

// `define start_5 24
// `define bound_width_5 59
// `define bound_width_6
// `define bound_width_

// `define start_2_1 0
// `define bound_width_2_1 41

// `define start_2_2 7
// `define bound_width_2_2 48

// `define start_2_3 18
// `define bound_width_2_3 59

// `define start_2_4 25
// // `define bound_width_2_3 59
module wallace #(
    parameter integer start = 0, bound_width = 63
)
(
    input   [63:0]  mid_adder_a,
    input   [63:0]  mid_adder_b,
    input   [63:0]  mid_adder_c,
    output  [64:0]  wallace_cout,
    output  [63:0]  wallace_s
);

    assign wallace_cout[0] = 0;
    genvar i;

    generate
        for(i = 0; i < start; i=i+1)begin
            assign wallace_cout[i] = 1'b0;
            assign wallace_s[i] = 1'b0;
        end
    endgenerate
    assign wallace_cout[start] = 1'b0;
    generate
        for(i = start; i <= bound_width; i=i+1)begin
            assign {wallace_cout[i+1], wallace_s[i]} 
                    = mid_adder_a[i] 
                    + mid_adder_b[i] 
                    + mid_adder_c[i];
        end
    endgenerate

    generate
        for(i = bound_width + 2; i <= 64; i=i+1)begin
            assign wallace_cout[i] = wallace_cout[bound_width];
        end
    endgenerate

    generate
        for(i = bound_width + 1; i < 64; i=i+1)begin
            assign wallace_s[i] = wallace_s[bound_width];
        end
    endgenerate
endmodule


module mul(
    input           clk,
    input           reset,
    input           sig_signed,
    input   [31:0]  src_A,
    input   [31:0]  src_B,
    output  [63:0]  res
    );

    wire  [63:0]  A   = {{32{src_A[31] & sig_signed}}, src_A};
    wire  [34:0]  B   = {{2{src_B[31] & sig_signed}}, src_B, 1'b0};
    wire  [63:0]  mid_adder   [16:0];

    wire    [63:0]  A_oppo  =   -A;
    wire [2:0] pro_test[16:0];
    genvar i, j;
    generate

        // for (i=0; i<17; i=i+1) begin
        //     // A << (2*i);
        //     mid_adder[i]    =   (B[(i+1)*2:i*2] == 3'b001 || B[(i+1)*2:i*2] == 3'b010)  ?   (A << (2*i))    :
        //                         (B[(i+1)*2:i*2] == 3'b101 || B[(i+1)*2:i*2] == 3'b110)  ?   ~(A << (2*i))   :
        //                         (B[(i+1)*2:i*2] == 3'b011)  ?   A << (2*i+1)    :
        //                         (B[(i+1)*2:i*2] == 3'b100)  ?   ~(A << (2*i+1)) ;
        //     maintain_res_adder[i+1]  =   (B[(i+1)*2] == 1'b0)  ?   maintain_res_adder[i] : maintain_res_adder[i] + 1;
        // end
        for (i=0; i<17; i=i+1) begin
            // A << (2*i);
            assign pro_test[i] = B[(i+1)*2:i*2];
            assign mid_adder[i]    =    (B[(i+1)*2:i*2] == 3'b001 || B[(i+1)*2:i*2] == 3'b010)  ?   A << (2*i)    :
                                        (B[(i+1)*2:i*2] == 3'b101 || B[(i+1)*2:i*2] == 3'b110)  ?   A_oppo << (2*i)   :
                                        (B[(i+1)*2:i*2] == 3'b011)  ?   A << (2*i+1)    :
                                        (B[(i+1)*2:i*2] == 3'b100)  ?   A_oppo << (2*i+1) : 0;///////////////////////////
        end
    endgenerate

    reg     [63:0]  mid_adder_st    [16:0];
    integer k;
    always @(posedge clk) begin
        for (k=0; k<17; k=k+1) begin
        mid_adder_st[k] <= mid_adder[k];
        end
    end

    
    // wire    [63:0]  wallace_carry_1_1;
    // wire    [63:0]  wallace_sum_1_1;
    // wallace #(
    //     .start(0),
    //     .bound_width(35)
    // ) wallace_1_1 (
    //     .mid_adder_a(mid_adder_st[0]),
    //     .mid_adder_b(mid_adder_st[1]),
    //     .mid_adder_c(mid_adder_st[2]),
    //     .wallace_cout(wallace_carry_1_1),
    //     .wallace_s(wallace_sum_1_1)
    // );



    // wire    [63:0]  wallace_carry_1_2;
    // wire    [63:0]  wallace_sum_1_2;
    // wallace #(
    //     .start(6),
    //     .bound_width(41)
    // ) wallace_1_2 (
    //     .mid_adder_a(mid_adder_st[3]),
    //     .mid_adder_b(mid_adder_st[4]),
    //     .mid_adder_c(mid_adder_st[5]),
    //     .wallace_cout(wallace_carry_1_2),
    //     .wallace_s(wallace_sum_1_2)
    // );


    // wire    [63:0]  wallace_carry_1_3;
    // wire    [63:0]  wallace_sum_1_3;
    // wallace #(
    //     .start(12),
    //     .bound_width(47)
    // ) wallace_1_3 (
    //     .mid_adder_a(mid_adder_st[6]),
    //     .mid_adder_b(mid_adder_st[7]),
    //     .mid_adder_c(mid_adder_st[8]),
    //     .wallace_cout(wallace_carry_1_3),
    //     .wallace_s(wallace_sum_1_3)
    // );



    // wire    [63:0]  wallace_carry_1_4;
    // wire    [63:0]  wallace_sum_1_4;
    // wallace #(
    //     .start(18),
    //     .bound_width(53)
    // ) wallace_1_4 (
    //     .mid_adder_a(mid_adder_st[9]),
    //     .mid_adder_b(mid_adder_st[10]),
    //     .mid_adder_c(mid_adder_st[11]),
    //     .wallace_cout(wallace_carry_1_4),
    //     .wallace_s(wallace_sum_1_4)
    // );


    // wire    [63:0]  wallace_carry_1_5;
    // wire    [63:0]  wallace_sum_1_5;
    // wallace #(
    //     .start(24),
    //     .bound_width(59)
    // ) wallace_1_5 (
    //     .mid_adder_a(mid_adder_st[12]),
    //     .mid_adder_b(mid_adder_st[13]),
    //     .mid_adder_c(mid_adder_st[14]),
    //     .wallace_cout(wallace_carry_1_5),
    //     .wallace_s(wallace_sum_1_5)
    // );


    // wire    [63:0]  wallace_carry_2_1;
    // wire    [63:0]  wallace_sum_2_1;
    // wallace #(
    //     .start(0),
    //     .bound_width(41)
    // ) wallace_2_1 (
    //     .mid_adder_a(wallace_sum_1_1),
    //     .mid_adder_b(wallace_carry_1_1),
    //     .mid_adder_c(wallace_sum_1_2),
    //     .wallace_cout(wallace_carry_2_1),
    //     .wallace_s(wallace_sum_2_1)
    // );


    // wire    [63:0]  wallace_carry_2_2;
    // wire    [63:0]  wallace_sum_2_2;
    // wallace #(
    //     .start(7),
    //     .bound_width(48)
    // ) wallace_2_2 (
    //     .mid_adder_a(wallace_carry_1_2),
    //     .mid_adder_b(wallace_sum_1_3),
    //     .mid_adder_c(wallace_carry_1_3),
    //     .wallace_cout(wallace_carry_2_2),
    //     .wallace_s(wallace_sum_2_2)
    // );


    // wire    [63:0]  wallace_carry_2_3;
    // wire    [63:0]  wallace_sum_2_3;
    // wallace #(
    //     .start(18),
    //     .bound_width(59)
    // ) wallace_2_3 (
    //     .mid_adder_a(wallace_sum_1_4),
    //     .mid_adder_b(wallace_carry_1_4),
    //     .mid_adder_c(wallace_sum_1_5),
    //     .wallace_cout(wallace_carry_2_3),
    //     .wallace_s(wallace_sum_2_3)
    // );


    // wire    [63:0]  wallace_carry_2_4;
    // wire    [63:0]  wallace_sum_2_4;
    // wallace #(
    //     .start(25),
    //     .bound_width(63)
    // ) wallace_2_4 (
    //     .mid_adder_a(wallace_carry_1_5),
    //     .mid_adder_b(mid_adder_st[15]),
    //     .mid_adder_c(mid_adder_st[16]),
    //     .wallace_cout(wallace_carry_2_4),
    //     .wallace_s(wallace_sum_2_4)
    // );



    // wire    [63:0]  wallace_carry_3_1;
    // wire    [63:0]  wallace_sum_3_1;
    // wallace #(
    //     .start(0),
    //     .bound_width(48)
    // ) wallace_3_1 (
    //     .mid_adder_a(wallace_sum_2_1),
    //     .mid_adder_b(wallace_carry_2_1),
    //     .mid_adder_c(wallace_sum_2_2),
    //     .wallace_cout(wallace_carry_3_1),
    //     .wallace_s(wallace_sum_3_1)
    // );

    // wire    [63:0]  wallace_carry_3_2;
    // wire    [63:0]  wallace_sum_3_2;
    // wallace #(
    //     .start(8),
    //     .bound_width(60)
    // ) wallace_3_2 (
    //     .mid_adder_a(wallace_carry_2_2),
    //     .mid_adder_b(wallace_sum_2_3),
    //     .mid_adder_c(wallace_carry_2_3),
    //     .wallace_cout(wallace_carry_3_2),
    //     .wallace_s(wallace_sum_3_2)
    // );




    // wire    [63:0]  wallace_carry_4_1;
    // wire    [63:0]  wallace_sum_4_1;
    // wallace #(
    //     .start(0),
    //     .bound_width(60)
    // ) wallace_4_1 (
    //     .mid_adder_a(wallace_sum_3_1),
    //     .mid_adder_b(wallace_carry_3_1),
    //     .mid_adder_c(wallace_sum_3_2),
    //     .wallace_cout(wallace_carry_4_1),
    //     .wallace_s(wallace_sum_4_1)
    // );

    // wire    [63:0]  wallace_carry_4_2;
    // wire    [63:0]  wallace_sum_4_2;
    // wallace #(
    //     .start(0),
    //     .bound_width(63)
    // ) wallace_4_2 (
    //     .mid_adder_a(wallace_carry_3_2),
    //     .mid_adder_b(wallace_sum_2_4),
    //     .mid_adder_c(wallace_carry_2_4),
    //     .wallace_cout(wallace_carry_4_2),
    //     .wallace_s(wallace_sum_4_2)
    // );
    


    // wire    [63:0]  wallace_carry_5_1;
    // wire    [63:0]  wallace_sum_5_1;
    // wallace #(
    //     .start(0),
    //     .bound_width(63)
    // ) wallace_5_1 (
    //     .mid_adder_a(wallace_sum_4_1),
    //     .mid_adder_b(wallace_carry_4_1),
    //     .mid_adder_c(wallace_sum_4_2),
    //     .wallace_cout(wallace_carry_5_1),
    //     .wallace_s(wallace_sum_5_1)
    // );
    
    wire    [63:0]  wallace_carry_1_1;
    wire    [63:0]  wallace_sum_1_1;
    wallace wallace_1_1 (
        .mid_adder_a(mid_adder_st[0]),
        .mid_adder_b(mid_adder_st[1]),
        .mid_adder_c(mid_adder_st[2]),
        .wallace_cout(wallace_carry_1_1),
        .wallace_s(wallace_sum_1_1)
    );


    wire    [63:0]  wallace_carry_1_2;
    wire    [63:0]  wallace_sum_1_2;
    wallace wallace_1_2 (
        .mid_adder_a(mid_adder_st[3]),
        .mid_adder_b(mid_adder_st[4]),
        .mid_adder_c(mid_adder_st[5]),
        .wallace_cout(wallace_carry_1_2),
        .wallace_s(wallace_sum_1_2)
    );


    wire    [63:0]  wallace_carry_1_3;
    wire    [63:0]  wallace_sum_1_3;
    wallace wallace_1_3 (
        .mid_adder_a(mid_adder_st[6]),
        .mid_adder_b(mid_adder_st[7]),
        .mid_adder_c(mid_adder_st[8]),
        .wallace_cout(wallace_carry_1_3),
        .wallace_s(wallace_sum_1_3)
    );


    wire    [63:0]  wallace_carry_1_4;
    wire    [63:0]  wallace_sum_1_4;
    wallace wallace_1_4 (
        .mid_adder_a(mid_adder_st[9]),
        .mid_adder_b(mid_adder_st[10]),
        .mid_adder_c(mid_adder_st[11]),
        .wallace_cout(wallace_carry_1_4),
        .wallace_s(wallace_sum_1_4)
    );


    wire    [63:0]  wallace_carry_1_5;
    wire    [63:0]  wallace_sum_1_5;
    wallace wallace_1_5 (
        .mid_adder_a(mid_adder_st[12]),
        .mid_adder_b(mid_adder_st[13]),
        .mid_adder_c(mid_adder_st[14]),
        .wallace_cout(wallace_carry_1_5),
        .wallace_s(wallace_sum_1_5)
    );


    wire    [63:0]  wallace_carry_2_1;
    wire    [63:0]  wallace_sum_2_1;
    wallace wallace_2_1 (
        .mid_adder_a(wallace_sum_1_1),
        .mid_adder_b(wallace_carry_1_1),
        .mid_adder_c(wallace_sum_1_2),
        .wallace_cout(wallace_carry_2_1),
        .wallace_s(wallace_sum_2_1)
    );


    wire    [63:0]  wallace_carry_2_2;
    wire    [63:0]  wallace_sum_2_2;
    wallace wallace_2_2 (
        .mid_adder_a(wallace_carry_1_2),
        .mid_adder_b(wallace_sum_1_3),
        .mid_adder_c(wallace_carry_1_3),
        .wallace_cout(wallace_carry_2_2),
        .wallace_s(wallace_sum_2_2)
    );


    wire    [63:0]  wallace_carry_2_3;
    wire    [63:0]  wallace_sum_2_3;
    wallace wallace_2_3 (
        .mid_adder_a(wallace_sum_1_4),
        .mid_adder_b(wallace_carry_1_4),
        .mid_adder_c(wallace_sum_1_5),
        .wallace_cout(wallace_carry_2_3),
        .wallace_s(wallace_sum_2_3)
    );

    reg     [63:0]  cout_3_2;
    reg     [63:0]  sum_3_2;

    wire    [63:0]  wallace_carry_3_2;
    wire    [63:0]  wallace_sum_3_2;
    

    reg     [63:0]  cout_3_1;
    reg     [63:0]  sum_3_1;

    wire    [63:0]  wallace_carry_3_1;
    wire    [63:0]  wallace_sum_3_1;

    reg     [63:0]  cout_2_4;
    reg     [63:0]  sum_2_4;
    wire    [63:0]  wallace_carry_2_4;
    wire    [63:0]  wallace_sum_2_4;

    wire    [63:0]  wallace_carry_4_1;
    wire    [63:0]  wallace_sum_4_1;

    wallace wallace_2_4 (
        .mid_adder_a(wallace_carry_1_5),
        .mid_adder_b(mid_adder_st[15]),
        .mid_adder_c(mid_adder_st[16]),
        .wallace_cout(wallace_carry_2_4),
        .wallace_s(wallace_sum_2_4)
    );
    always @(posedge clk) begin
        cout_2_4 <= wallace_carry_2_4;
        sum_2_4 <= wallace_sum_2_4;

        cout_3_1 <= wallace_carry_3_1;
        sum_3_1 <= wallace_sum_3_1;
        
        cout_3_2 <= wallace_carry_3_2;
        sum_3_2 <= wallace_sum_3_2;

    end


    wallace wallace_3_1 (
        .mid_adder_a(wallace_sum_2_1),
        .mid_adder_b(wallace_carry_2_1),
        .mid_adder_c(wallace_sum_2_2),
        .wallace_cout(wallace_carry_3_1),
        .wallace_s(wallace_sum_3_1)
    );

    wallace wallace_3_2 (
        .mid_adder_a(wallace_carry_2_2),
        .mid_adder_b(wallace_sum_2_3),
        .mid_adder_c(wallace_carry_2_3),
        .wallace_cout(wallace_carry_3_2),
        .wallace_s(wallace_sum_3_2)
    );



    wallace wallace_4_1 (
        .mid_adder_a(sum_3_1),
        .mid_adder_b(cout_3_1),
        .mid_adder_c(sum_3_2),
        .wallace_cout(wallace_carry_4_1),
        .wallace_s(wallace_sum_4_1)
    );

    wire    [63:0]  wallace_carry_4_2;
    wire    [63:0]  wallace_sum_4_2;
    wallace wallace_4_2 (
        .mid_adder_a(cout_3_2),
        .mid_adder_b(sum_2_4),
        .mid_adder_c(cout_2_4),
        .wallace_cout(wallace_carry_4_2),
        .wallace_s(wallace_sum_4_2)
    );
    

    wire    [63:0]  wallace_carry_5_1;
    wire    [63:0]  wallace_sum_5_1;
    wallace wallace_5_1 (
        .mid_adder_a(wallace_sum_4_1),
        .mid_adder_b(wallace_carry_4_1),
        .mid_adder_c(wallace_sum_4_2),
        .wallace_cout(wallace_carry_5_1),
        .wallace_s(wallace_sum_5_1)
    );


    wire    [63:0]  wallace_carry_6_1;
    wire    [63:0]  wallace_sum_6_1;

    // reg [63:0]  cout_5_1;
    // reg [63:0]  cout_4_2;
    // reg [63:0]  sum_5_1;
        wallace #(
            .start(0),
            .bound_width(63)
        ) wallace_6_1 (
            // .mid_adder_a(sum_5_1),
            // .mid_adder_b(cout_5_1),
            // .mid_adder_c(cout_4_2),
            .mid_adder_a(wallace_sum_5_1),
            .mid_adder_b(wallace_carry_5_1),
            .mid_adder_c(wallace_carry_4_2),
            .wallace_cout(wallace_carry_6_1),
            .wallace_s(wallace_sum_6_1)
        );
    assign  res =   wallace_carry_6_1   +   wallace_sum_6_1;


endmodule
