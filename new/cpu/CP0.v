`timescale 1ns / 1ps
`include "global.h"

module CP0(
    input clk,
    input reset,
    input [31:0] src_pc_cp0,
    input [31:0] src_addr,
    input [5:0] hwint,     
    input eret,                         //from ctr
    input sig_BD,                       //from ctr 是否为延迟槽
    input sig_except,                   //from check
    input [4:0] src_exccode,            //from check
    input [4:0] cp0_addr,             
    input sig_wen,                      //from ctr
    input [31:0] src_wdata,
    // tlb
    // input tlbwi,
    // input tlbr,
    // input tlbp,
    // input [31:0] probe_index,
    // input [31:0] read_entry_pagemask,
    // input [31:0] read_entry_hi,
    // input [31:0] read_entry_lo0,
    // input [31:0] read_entry_lo1,

    output reg [31:0] src_rdata,        //mux with AO, AC_SEL
    output reg [31:0] src_epc,
    output sig_intreq
    // output tlb_write_en,
    // output [4:0] tlb_write_idx,
    // output [31:0] src_cp0_EntryHi,
    // output [31:0] src_cp0_PageMask,
    // output [31:0] src_cp0_EntryLo0,
    // output [31:0] src_cp0_EntryLo1
    );

    reg bd, ti, exl, ie;
    reg [4:0] exccode;
    reg [7:0] ip, im;
    reg [31:0] badvaddr;
    reg [32:0] count;
    reg [31:0] EntryHi;                 //10
    reg [31:0] EntryLo0, EntryLo1;      //2,3
    reg [31:0] PageMask;                //5
    reg [31:0] Index;                   //0

    wire shutdown;
    assign shutdown = (|(ip & im)) & ie;
    assign sig_intreq = (shutdown || sig_except) & !exl;

    //TLB
    // assign src_cp0_EntryHi = {EntryHi[31:13], 5'd0, EntryHi[7:0]};
    // assign src_cp0_PageMask = {7'd0, PageMask[24:13], 13'd0};
    // assign src_cp0_EntryLo0 = {6'd0, EntryLo0[25:0]};
    // assign src_cp0_EntryLo1 = {6'd0, EntryLo1[25:0]};

    // assign tlb_write_en = tlbwi;
    // assign tlb_write_idx = Index[4:0];

    always @(*) begin
        case (cp0_addr)
            `BADVADDR : src_rdata = badvaddr;
            `COUNT    : src_rdata = count[32:1];
            `STATUS   : src_rdata = {9'd0, 1'b1, 6'd0, im, 6'd0, exl, ie};
            `CAUSE    : src_rdata = {bd, ti, 14'd0, ip, 1'd0, exccode, 2'd0};
            `EPC      : src_rdata = src_epc;
            // `ENTRYHI  : src_rdata = {EntryHi[31:13], 5'd0, EntryHi[7:0]};
            // `ENTRYLO0 : src_rdata = {6'd0, EntryLo0[25:0]};
            // `ENTRYLO1 : src_rdata = {6'd0, EntryLo1[25:0]};
            // `PAGEMASK : src_rdata = {7'd0, PageMask[24:13], 13'd0};
            // `INDEX    : src_rdata = {Index[31], 26'd0, Index[4:0]};
            default   : src_rdata = 32'd88488848;
        endcase
    end

    always @(posedge clk) begin
        if(reset) begin
            src_epc <= 0;
            bd <= 0;
            ti <= 0;
            exl <= 0;
            ie <= 0;
            exccode <= 0;
            ip <= 0;
            im <= 0;
            count <= 0;
            badvaddr <= 0;
            EntryHi <= 0;
            EntryLo0 <= 0;
            EntryLo1 <= 0;
            PageMask <= 0; 
            Index <= 0;     
        end
        else begin
            count <= count + 1;
            ip[7:2] <= hwint;
            if(sig_intreq) begin
                exl <= 1'b1;
                bd <= sig_BD;
                src_epc <= sig_BD? src_pc_cp0-4 : src_pc_cp0;
                if(shutdown)
                    exccode <= 5'd0;
                else begin
                    exccode <= src_exccode;
                    badvaddr <= src_addr;
                end
            end
            else if(eret) begin
                exl <= 1'b0;
                bd <= 1'b0;
            end
            else begin
                // if (tlbr) begin
                //     PageMask <= read_entry_pagemask;
                //     EntryHi <= read_entry_hi;
                //     EntryLo0 <= read_entry_lo0;
                //     EntryLo1 <= read_entry_lo1;
                // end
                // if (tlbp) begin
                //     Index <= probe_index;
                // end
                if (sig_wen) begin
                    case (cp0_addr)
                        `COUNT : count <= src_wdata;
                        `STATUS: begin
                            im <= src_wdata[15:8];
                            exl <= src_wdata[1];
                            ie <= src_wdata[0];
                        end
                        `CAUSE: ip[1:0] <= src_wdata[9:8];
                        `EPC  : src_epc[31:0] <= src_wdata[31:0];
                        // `ENTRYHI  : EntryHi <= src_wdata;
                        // `ENTRYLO0 : EntryLo0 <= src_wdata;
                        // `ENTRYLO1 : EntryLo1 <= src_wdata;
                        // `PAGEMASK : PageMask <= src_wdata;
                        // `INDEX    : Index[5:0] <= src_wdata[5:0];
                    endcase
                end
            end
        end
    end

endmodule

