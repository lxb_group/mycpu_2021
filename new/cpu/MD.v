`timescale 1ns / 1ps
`include "global.h"
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/07/09 11:29:10
// Design Name: 
// Module Name: MD
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

`define opMult  4'd1
`define opMultu 4'd2
`define opDiv   4'd3
`define opDivu  4'd4

module MD(
    input clk,
    input reset,
    input clr,
	// input mul_e,
    input sig_stall,
    input sig_start,
    input [2:0] md_op,
    input [31:0] opt1,
    input [31:0] opt2,
	// output start_got_md,
    output [31:0] HI_LO,
    output BUSY
    );

	wire start = !clr && !sig_stall && sig_start && !BUSY;
	// assign start_got_md = start;

   	reg [1:0] timeOfCal;
	reg[31:0] H, L, HI, LO;
	
	assign BUSY = (timeOfCal > 0);
    assign HI_LO = (md_op == `MFHI)? HI:
					// (md_op == `MFLO || mul_e)? LO:
					(md_op == `MFLO)? LO:
									0; 
	wire mult = (md_op == `MULT);
	wire multu = (md_op == `MULTU);
	wire div = (md_op == `DIV);
	wire divu = (md_op == `DIVU);
	wire [63:0] mult_res;
	wire [63:0] div_res;
	reg [2:0] op_st;
	reg d;
	reg m;
	mul mul_0(
		.clk(clk),
		.reset(reset),
		.sig_signed(mult),
		.src_A(opt1),
		.src_B(opt2),
		.res(mult_res)
	);
	wire undone;
	div div_0(
		.clk(clk),
		.reset(reset),
		.sig_signed(div),
		.start(start),
		.src_A(opt1),
		.src_B(opt2),
		.quotient(div_res[31:0]),
		.remainder(div_res[63:32]),
		.busy_out(undone)
	);
	always@(posedge clk) begin
		if(reset || clr) begin
			timeOfCal <= 0;
			H <= 0;
			L <= 0;
			HI <= 0;
			LO <= 0;
			d <= 0;
			m <= 0;
			op_st <= 0;
		end
		else if(start) begin
			op_st	<=	(mult)					?	`opMult		:
						(multu)					?	`opMultu	:
						(div	&&opt2!=0)		?	`opDiv		:
						(divu	&&opt2!=0)		?	`opDivu		:
													2'd0		;
			// op_st <= md_op[1:0]; B==0 or when not m/d, md_op != 0
			m <= (mult|multu);
			d <= (div|divu);
			case(md_op)
				`MULT,`MULTU,`DIV,`DIVU: begin
					timeOfCal <= 2;
				end
				`MTHI: 
					HI <= opt1;
				`MTLO:
					LO <= opt1;
			endcase
		end
		else if(timeOfCal > 2'd0) begin
			if(timeOfCal == 2'd1) begin
				timeOfCal <= 2'd0;
				m<=0;
				d<=0;
				case(op_st)
                    `opMult,`opMultu:
                        {HI,LO}	<=	mult_res;
                    `opDiv,`opDivu:
                        {HI,LO}	<=	div_res;
                endcase
			end
			else begin
				timeOfCal <= (d & !undone) || (m)? 2'd1 : 2'd2;
			end
		end
	end

endmodule
