`timescale 1ns / 1ps
`include "signal_defs.svh"
`include "cache_defs.vh"
module CacheTags#(
    parameter WORD_WIDTH = `WORD_WIDTH,
    parameter LINE_WIDTH = `LINE_WIDTH,
    parameter SET_ASSOC = `SET_ASSOC,
    parameter CACHE_SIZE = `CACHE_SIZE,
    parameter LINE_NUM = `LINE_NUM,
    parameter GROUP_NUM = `GROUP_NUM,
    parameter WORD_PER_LINE = `WORD_PER_LINE,
    parameter LINE_WORD_OFFSET = `LINE_WORD_OFFSET,
    parameter INDEX_WIDTH = `INDEX_WIDTH,
    parameter TAG_WIDTH = `TAG_WIDTH
)(
    input clk,
    input reset,
    input [SET_ASSOC-1:0] sig_we,
    input [INDEX_WIDTH-1:0] src_rindex,
    input [INDEX_WIDTH-1:0] src_windex,
    input cache_inf_t src_winf,
    output busy,
    output cache_inf_t src_rinf[SET_ASSOC-1:0]
    );
    cache_inf_t src_rinf_0[SET_ASSOC-1:0],src_rinf_1[SET_ASSOC-1:0];
    logic [SET_ASSOC * 2 - 1:0] _busy;
    assign busy = |_busy;
    genvar i;
    generate
        for(i=0;i<SET_ASSOC;i++) begin:_CacheTagBram
            CacheTagBram _CacheTagBram (
                .clka(clk),            // input wire clka
                .rsta(reset),
                .wea(sig_we[i]),                // input wire [0 : 0] wea
                .addra(src_windex),             // input wire [5 : 0] addra
                .dina(src_winf),                // input wire [31 : 0] dina
                .douta(src_rinf_0[i]),
                .clkb(clk),                     // input wire clkb
                .rstb(reset),                   // input wire rstb
                .web('0),
                .addrb(src_rindex),             // input wire [5 : 0] addrb
                .dinb('0),
                .doutb(src_rinf_1[i]),            // output wire [31 : 0] doutb
                .rsta_busy(_busy[2*i]),         // output wire rsta_busy
                .rstb_busy(_busy[2*i+1])        // output wire rstb_busy
            );
        end
    endgenerate
    logic [INDEX_WIDTH-1:0] _rindex,_windex;
    
    always_ff @(posedge clk) begin
        if(reset) begin
            _rindex <= '0;
            _windex <= '0;
        end
        else begin
            _rindex <= src_rindex;
            _windex <= src_windex;
        end
    end
    
    always_comb begin
        if (_rindex == _windex) begin
            src_rinf[0] = src_rinf_0[0];
            src_rinf[1] = src_rinf_0[1];
            src_rinf[2] = src_rinf_0[2];
            src_rinf[3] = src_rinf_0[3];
        end
        else begin
            src_rinf[0] = src_rinf_1[0];
            src_rinf[1] = src_rinf_1[1];
            src_rinf[2] = src_rinf_1[2];
            src_rinf[3] = src_rinf_1[3];
        end
    end
endmodule
