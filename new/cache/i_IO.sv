`timescale 1ns / 1ps
`include "cache_defs.vh"
`include "signal_defs.svh"

module i_IO#(
    parameter WORD_WIDTH = `WORD_WIDTH,
    parameter BYTE_WIDTH = `BYTE_WIDTH,
    parameter FIFO_DEPTH = `FIFO_DEPTH,
    parameter FIFO_WIDTH = `FIFO_WIDTH
)(
    input clk,
    input reset,
    // signals
    input ren,
    input wen,
    input [WORD_WIDTH/BYTE_WIDTH - 1:0] ben,
    input [WORD_WIDTH - 1:0] addr,
    input [WORD_WIDTH - 1:0] wdata,
    output stall,
    output logic valid,
    output logic [WORD_WIDTH - 1:0] rdata,
    // axi
    input axi_resp_t uncache_axi_resp,
    output axi_req_t uncache_axi_req,
    input axi_resp_t cache_axi_resp,
    output axi_req_t cache_axi_req
    );
    
    logic cache_ren,cache_wen;
    logic [WORD_WIDTH/BYTE_WIDTH - 1:0] cache_ben;
    logic [FIFO_WIDTH - 1:0] cache_idin,cache_idout;
    logic [WORD_WIDTH - 1:0] cache_addr;
    logic [WORD_WIDTH - 1:0] cache_wdata;
    logic cache_busy;
    logic cache_valid;
    logic [WORD_WIDTH - 1:0] cache_rdata;
    
    logic [FIFO_WIDTH - 1:0] f_cache_id;
    logic f_cache_ren,f_cache_wen;
    logic [WORD_WIDTH/BYTE_WIDTH - 1:0] f_cache_ben;
    logic [WORD_WIDTH - 1:0] f_cache_addr;
    logic [WORD_WIDTH - 1:0] f_cache_wdata;
    logic [WORD_WIDTH - 1:0] f_cache_rdata;
    
    logic cache_fifo_full,cache_stall;
    
    
    logic uncache_ren,uncache_wen;
    logic [WORD_WIDTH/BYTE_WIDTH - 1:0] uncache_ben;
    logic [FIFO_WIDTH - 1:0] uncache_idin,uncache_idout;
    logic [WORD_WIDTH - 1:0] uncache_addr;
    logic [WORD_WIDTH - 1:0] uncache_wdata;
    logic uncache_busy;
    logic uncache_valid;
    logic [WORD_WIDTH - 1:0] uncache_rdata;
    
    logic [FIFO_WIDTH - 1:0] f_uncache_id;
    logic f_uncache_ren,f_uncache_wen;
    logic [WORD_WIDTH/BYTE_WIDTH - 1:0] f_uncache_ben;
    logic [WORD_WIDTH - 1:0] f_uncache_addr;
    logic [WORD_WIDTH - 1:0] f_uncache_wdata;
    logic [WORD_WIDTH - 1:0] f_uncache_rdata;
    
    logic uncache_fifo_full,uncache_stall;
    
    integer i;
    
    logic uncache_enable;
    assign uncache_enable = (addr[31:16] == 16'hbfaf) || 
                            (addr[31:14] == {16'hbfe4, 2'b00}) || 
                            (addr[31:23] == 9'b1000_0000_0) || 
                            (addr[31:23] == 9'b1010_0000_0);
    
    assign stall = uncache_fifo_full | cache_fifo_full;
    assign valid = cache_valid | uncache_valid;
    assign rdata = cache_valid ? cache_rdata:uncache_rdata;
    
    always_comb begin
        f_cache_id = '0;
        f_cache_ren = ren & !uncache_enable;
        f_cache_wen = wen & !uncache_enable;
        f_cache_ben = ben;
        f_cache_addr = {3'd0,addr[28:0]};
        f_cache_wdata = wdata;
        
        f_uncache_id = '0;
        f_uncache_ren = ren & uncache_enable;
        f_uncache_wen = wen & uncache_enable;
        f_uncache_ben = ben;
        f_uncache_addr = {3'd0,addr[28:0]};
        f_uncache_wdata = wdata;
    end
    
//    fifo cache_fifo(
//        .clk(clk),
//        .reset(reset),
//        .stall(cache_stall),
//        .full(cache_fifo_full),
//        .id_in(f_cache_id),
//        .ren_in(f_cache_ren),
//        .wen_in(f_cache_wen),
//        .ben_in(f_cache_ben),
//        .addr_in(f_cache_addr),
//        .wdata_in(f_cache_wdata),
//        .id_out(cache_idin),
//        .ren_out(cache_ren),
//        .wen_out(cache_wen),
//        .ben_out(cache_ben),
//        .addr_out(cache_addr),
//        .wdata_out(cache_wdata)
//    );
    always_comb begin
        cache_idin = f_cache_id;
        cache_ren = f_cache_ren & !stall;
        cache_wen = f_cache_wen & !stall;
        cache_ben = f_cache_ben;
        cache_addr = f_cache_addr;
        cache_wdata = f_cache_wdata;
        cache_fifo_full = cache_stall;
        
        uncache_idin = f_uncache_id;
        uncache_ren = f_uncache_ren & !stall;
        uncache_wen = f_uncache_wen & !stall;
        uncache_ben = f_uncache_ben;
        uncache_addr = f_uncache_addr;
        uncache_wdata = f_uncache_wdata;
        uncache_fifo_full = uncache_stall;
    end
    
    Cache _Cache(
        .clk(clk),
        .reset(reset),
        // signals
        .ren(cache_ren),
        .wen(cache_wen),
        .ben(cache_ben),
        .idin(cache_idin),
        .addr(cache_addr),
        .wdata(cache_wdata),
        // fifo
        .stall(cache_stall),
        .idout(cache_idout),
        .valid(cache_valid),
        .rdata(cache_rdata),
        // axi
        .axi_resp(cache_axi_resp),
        .axi_req(cache_axi_req)
    );
    
    Uncache _Uncache(
        .clk(clk),
        .reset(reset),
        // signals
        .ren(uncache_ren),
        .wen(uncache_wen),
        .ben(uncache_ben),
        .idin(uncache_idin),
        .addr(uncache_addr),
        .wdata(uncache_wdata),
        // fifo
        .stall(uncache_stall),
        .idout(uncache_idout),
        .valid(uncache_valid),
        .rdata(uncache_rdata),
        // axi
        .axi_resp(uncache_axi_resp),
        .axi_req(uncache_axi_req)
    );

endmodule
