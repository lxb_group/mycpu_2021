`timescale 1ns / 1ps
`include "signal_defs.svh"
`include "cache_defs.vh"
module CacheValids#(
    parameter WORD_WIDTH = `WORD_WIDTH,
    parameter LINE_WIDTH = `LINE_WIDTH,
    parameter SET_ASSOC = `SET_ASSOC,
    parameter CACHE_SIZE = `CACHE_SIZE,
    parameter LINE_NUM = `LINE_NUM,
    parameter GROUP_NUM = `GROUP_NUM,
    parameter WORD_PER_LINE = `WORD_PER_LINE,
    parameter LINE_WORD_OFFSET = `LINE_WORD_OFFSET,
    parameter INDEX_WIDTH = `INDEX_WIDTH,
    parameter TAG_WIDTH = `TAG_WIDTH
)(
    input clk,
    input reset,
    input [SET_ASSOC - 1:0] we,
    input [SET_ASSOC - 1:0] ce,
    input [INDEX_WIDTH - 1:0] rindex,
    input [INDEX_WIDTH - 1:0] windex,
    input [INDEX_WIDTH - 1:0] cindex,
    output logic [$clog2(WORD_PER_LINE):0] valid_out[SET_ASSOC - 1:0]
    );
    
    genvar i;
    generate
        for(i=0;i<SET_ASSOC;i++) begin:_CacheValidRam
            CacheValidRam _CacheValidRam(
                .clk(clk),
                .reset(reset),
                .we(we[i]),
                .ce(ce[i]),
                .rindex(rindex),
                .windex(windex),
                .cindex(cindex),
                .valid_out(valid_out[i])
            );
        end
    endgenerate 
    
//    always_ff@(posedge clk) begin
//        if (reset) begin
//            _rindex <= '0;
//            _windex <= '0;
//        end
//        else begin
//            _rindex <= rindex;
//            _windex <= windex;
//        end
//    end
    
//    always_comb begin
//        if (_rindex == _windex) begin
//            valid_out0 = _valid_out1;
//        end
//        else begin
//            valid_out0 = _valid_out0;
//        end
//        valid_out1 = _valid_out1;
//    end
endmodule
