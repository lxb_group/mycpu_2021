`timescale 1ns / 1ps
`include "cache_defs.vh"
`include "signal_defs.svh"

module d_IO#(
    parameter WORD_WIDTH = `WORD_WIDTH,
    parameter BYTE_WIDTH = `BYTE_WIDTH,
    parameter FIFO_DEPTH = `FIFO_DEPTH,
    parameter FIFO_WIDTH = `FIFO_WIDTH
)(
    input clk,
    input reset,
    // signals
    input ren,
    input wen,
    input [WORD_WIDTH/BYTE_WIDTH - 1:0] ben,
    input [WORD_WIDTH - 1:0] addr,
    input [WORD_WIDTH - 1:0] wdata,
    output stall,
    output logic valid,
    output logic [WORD_WIDTH - 1:0] rdata,
    // axi
    input axi_resp_t uncache_axi_resp,
    output axi_req_t uncache_axi_req,
    input axi_resp_t cache_axi_resp,
    output axi_req_t cache_axi_req
    );
    
    logic cache_ren,cache_wen;
    logic [WORD_WIDTH/BYTE_WIDTH - 1:0] cache_ben;
    logic [FIFO_WIDTH - 1:0] cache_idin,cache_idout;
    logic [WORD_WIDTH - 1:0] cache_addr;
    logic [WORD_WIDTH - 1:0] cache_wdata;
    logic cache_busy;
    logic cache_valid;
    logic [WORD_WIDTH - 1:0] cache_rdata;
    
    logic [FIFO_WIDTH - 1:0] f_cache_id;
    logic f_cache_ren,f_cache_wen;
    logic [WORD_WIDTH/BYTE_WIDTH - 1:0] f_cache_ben;
    logic [WORD_WIDTH - 1:0] f_cache_addr;
    logic [WORD_WIDTH - 1:0] f_cache_wdata;
    logic [WORD_WIDTH - 1:0] f_cache_rdata;
    
    logic cache_fifo_full,cache_stall;
    
    
    logic uncache_ren,uncache_wen;
    logic [WORD_WIDTH/BYTE_WIDTH - 1:0] uncache_ben;
    logic [FIFO_WIDTH - 1:0] uncache_idin,uncache_idout;
    logic [WORD_WIDTH - 1:0] uncache_addr;
    logic [WORD_WIDTH - 1:0] uncache_wdata;
    logic uncache_busy;
    logic uncache_valid;
    logic [WORD_WIDTH - 1:0] uncache_rdata;
    
    logic [FIFO_WIDTH - 1:0] f_uncache_id;
    logic f_uncache_ren,f_uncache_wen;
    logic [WORD_WIDTH/BYTE_WIDTH - 1:0] f_uncache_ben;
    logic [WORD_WIDTH - 1:0] f_uncache_addr;
    logic [WORD_WIDTH - 1:0] f_uncache_wdata;
    logic [WORD_WIDTH - 1:0] f_uncache_rdata;
    
    logic uncache_fifo_full,uncache_stall;
    
    logic [FIFO_WIDTH - 1:0] p0,p1;
    logic [WORD_WIDTH - 1:0] rdatas[FIFO_DEPTH - 1:0];
    logic [1:0] status[FIFO_DEPTH - 1:0];
    logic [5:0] count;
    integer i;
    
    logic uncache_enable;
    assign uncache_enable = (addr[31:16] == 16'hbfaf) || 
                            (addr[31:14] == {16'hbfe4, 2'b00}) || 
                            (addr[31:23] == 9'b1000_0000_0) || 
                            (addr[31:23] == 9'b1010_0000_0);
    
    always_ff @(posedge clk) begin
        if (reset) begin
            p0 <= '0;
            p1 <= '0;
            count <= '0;
            for (i = 0;i< FIFO_DEPTH;i++ ) begin
                rdatas[i] <= '0;
                status[i] <= '0;
            end
        end
        else begin
            if (f_cache_ren | f_uncache_ren) begin
                status[p0] <= 1;
                p0 <= p0 + 1;
            end
            if (valid) begin
                status[p1] <= 0;
                p1 <= p1 + 1;
            end
            count <= count + (f_cache_ren | f_uncache_ren) - valid;
        end
    end
    
    always_comb begin
        if (status[p1] == 2) begin
            valid = 1'b1;
            rdata = rdatas[p1];
        end
        else if(cache_valid & (cache_idout == p1)) begin
            valid = 1'b1;
            rdata = cache_rdata;
        end
        else if(uncache_valid & (uncache_idout == p1)) begin
            valid = 1'b1;
            rdata = uncache_rdata;
        end
        else begin
            valid = 1'b0;
            rdata = '0;
        end
    end
    
    assign stall = (count == FIFO_DEPTH) | 
                   (uncache_enable & uncache_fifo_full) | 
                   (!uncache_enable & cache_fifo_full);
    
    always_ff @(posedge clk) begin
        if (cache_valid) begin
            if (cache_idout == p1) begin
                rdatas[cache_idout] <= '0;
                status[cache_idout] <= '0;
            end
            else begin
                rdatas[cache_idout] <= cache_rdata;
                status[cache_idout] <= 2;
            end
        end
        if (uncache_valid) begin
            if (uncache_idout == p1) begin
                rdatas[uncache_idout] <= '0;
                status[uncache_idout] <= '0;
            end
            else begin
                rdatas[uncache_idout] <= uncache_rdata;
                status[uncache_idout] <= 2;
            end
        end
    end
    
    always_comb begin
        f_cache_id = p0;
        f_cache_ren = ren & !uncache_enable;
        f_cache_wen = wen & !uncache_enable;
        f_cache_ben = ben;
        f_cache_addr = {3'd0,addr[28:0]};
        f_cache_wdata = wdata;
        
        f_uncache_id = p0;
        f_uncache_ren = ren & uncache_enable;
        f_uncache_wen = wen & uncache_enable;
        f_uncache_ben = ben;
        f_uncache_addr = {3'd0,addr[28:0]};
        f_uncache_wdata = wdata;
    end
    
    fifo cache_fifo(
        .clk(clk),
        .reset(reset),
        .stall(cache_stall),
        .full(cache_fifo_full),
        .id_in(f_cache_id),
        .ren_in(f_cache_ren),
        .wen_in(f_cache_wen),
        .ben_in(f_cache_ben),
        .addr_in(f_cache_addr),
        .wdata_in(f_cache_wdata),
        .id_out(cache_idin),
        .ren_out(cache_ren),
        .wen_out(cache_wen),
        .ben_out(cache_ben),
        .addr_out(cache_addr),
        .wdata_out(cache_wdata)
    );
    
    Cache _Cache(
        .clk(clk),
        .reset(reset),
        // signals
        .ren(cache_ren),
        .wen(cache_wen),
        .ben(cache_ben),
        .idin(cache_idin),
        .addr(cache_addr),
        .wdata(cache_wdata),
        // fifo
        .stall(cache_stall),
        .idout(cache_idout),
        .valid(cache_valid),
        .rdata(cache_rdata),
        // axi
        .axi_resp(cache_axi_resp),
        .axi_req(cache_axi_req)
    );
    
    fifo uncache_fifo(
        .clk(clk),
        .reset(reset),
        .stall(uncache_stall),
        .full(uncache_fifo_full),
        .id_in(f_uncache_id),
        .ren_in(f_uncache_ren),
        .wen_in(f_uncache_wen),
        .ben_in(f_uncache_ben),
        .addr_in(f_uncache_addr),
        .wdata_in(f_uncache_wdata),
        .id_out(uncache_idin),
        .ren_out(uncache_ren),
        .wen_out(uncache_wen),
        .ben_out(uncache_ben),
        .addr_out(uncache_addr),
        .wdata_out(uncache_wdata)
    );
    
    Uncache _Uncache(
        .clk(clk),
        .reset(reset),
        // signals
        .ren(uncache_ren),
        .wen(uncache_wen),
        .ben(uncache_ben),
        .idin(uncache_idin),
        .addr(uncache_addr),
        .wdata(uncache_wdata),
        // fifo
        .stall(uncache_stall),
        .idout(uncache_idout),
        .valid(uncache_valid),
        .rdata(uncache_rdata),
        // axi
        .axi_resp(uncache_axi_resp),
        .axi_req(uncache_axi_req)
    );

endmodule
