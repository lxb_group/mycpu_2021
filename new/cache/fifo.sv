`timescale 1ns / 1ps
`include "cache_defs.vh"
`include "signal_defs.svh"
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/03/07 11:29:25
// Design Name: 
// Module Name: fifo
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module fifo#(
    parameter WORD_WIDTH = `WORD_WIDTH,
    parameter BYTE_WIDTH = `BYTE_WIDTH,
    parameter FIFO_DEPTH = `FIFO_DEPTH,
    parameter FIFO_WIDTH = `FIFO_WIDTH
)(
    input clk,
    input reset,
    input stall,
    input [FIFO_WIDTH - 1:0]id_in,
    input ren_in,
    input wen_in,
    input [WORD_WIDTH/BYTE_WIDTH - 1:0] ben_in,
    input [WORD_WIDTH - 1:0] addr_in,
    input [WORD_WIDTH - 1:0] wdata_in,
    output full,
    output logic [FIFO_WIDTH - 1:0]id_out,
    output logic ren_out,
    output logic wen_out,
    output logic [WORD_WIDTH/BYTE_WIDTH - 1:0] ben_out,
    output logic [WORD_WIDTH - 1:0] addr_out,
    output logic [WORD_WIDTH - 1:0] wdata_out
    );
    
    logic [FIFO_WIDTH:0] count;
    logic [FIFO_WIDTH - 1:0] ids[FIFO_DEPTH - 1:0];
    logic rens[FIFO_DEPTH - 1:0];
    logic wens[FIFO_DEPTH - 1:0];
    logic [WORD_WIDTH/BYTE_WIDTH - 1:0] bens[FIFO_DEPTH - 1:0];
    logic [WORD_WIDTH - 1:0] addrs[FIFO_DEPTH - 1:0];
    logic [WORD_WIDTH - 1:0] wdatas[FIFO_DEPTH - 1:0];
    logic [FIFO_WIDTH - 1:0] p0,p1;
    integer i;
    always_ff@(posedge clk) begin
        if (reset) begin
            for(i = 0;i < FIFO_DEPTH;i++) begin
                ids[i] <= '0;
                rens[i] <= '0;
                wens[i] <= '0;
                bens[i] <= '0;
                addrs[i] <= '0;
                wdatas[i] <= '0;
            end
            p0 <= '0;
            p1 <= '0;
            count <= '0;
        end
        else begin
            if (wen_in | ren_in) begin
                if (stall | (count > 0)) begin
                    ids[p0] <= id_in;
                    rens[p0] <= ren_in;
                    wens[p0] <= wen_in;
                    bens[p0] <= ben_in;
                    addrs[p0] <= addr_in;
                    wdatas[p0] <= wdata_in;
                    p0 <= p0 + 1;
                end
            end
            if (!stall & (count > 0)) begin
                ids[p1] <= '0;
                rens[p1] <= '0;
                wens[p1] <= '0;
                bens[p1] <= '0;
                addrs[p1] <= '0;
                wdatas[p1] <= '0;
                p1 <= p1 + 1;
            end
            count <= count + ((stall | (count>0)) & (wen_in|ren_in)) - (!stall & (count>0));
        end
    end
    
    always_comb begin
        if (count == 0) begin
            id_out = id_in;
            ren_out = ren_in;
            wen_out = wen_in;
            ben_out = ben_in;
            addr_out = addr_in;
            wdata_out = wdata_in;
        end
        else begin
            id_out = ids[p1];
            ren_out = rens[p1];
            wen_out = wens[p1];
            ben_out = bens[p1];
            addr_out = addrs[p1];
            wdata_out = wdatas[p1];
        end
    end
    
    assign full = count == FIFO_DEPTH;
    
endmodule
