`timescale 1ns / 1ps
`include "cache_defs.vh"
`include "signal_defs.svh"

module Cache#(
    parameter WORD_WIDTH = `WORD_WIDTH,
    parameter BYTE_WIDTH = `BYTE_WIDTH,
    parameter FIFO_DEPTH = `FIFO_DEPTH,
    parameter FIFO_WIDTH = `FIFO_WIDTH,
    parameter SET_ASSOC = `SET_ASSOC,
    parameter CACHE_SIZE = `CACHE_SIZE,
    parameter LINE_NUM = `LINE_NUM,
    parameter GROUP_NUM = `GROUP_NUM,
    parameter WORD_PER_LINE = `WORD_PER_LINE,
    parameter LINE_WORD_OFFSET = `LINE_WORD_OFFSET,
    parameter LINE_BYTE_OFFSET = `LINE_BYTE_OFFSET,
    parameter INDEX_WIDTH = `INDEX_WIDTH,
    parameter TAG_WIDTH = `TAG_WIDTH
)(
    input clk,
    input reset,
    // signals
    input ren,
    input wen,
    input [WORD_WIDTH/BYTE_WIDTH - 1:0] ben,
    input [FIFO_WIDTH - 1:0] idin,
    input [WORD_WIDTH - 1:0] addr,
    input [WORD_WIDTH - 1:0] wdata,
    // fifo
    output logic stall,
    output logic valid,
    output logic [FIFO_WIDTH - 1:0] idout,
    output logic [WORD_WIDTH - 1:0] rdata,
    // axi
    input axi_resp_t axi_resp,
    output axi_req_t axi_req
    );
    
    logic miss,tag_miss,tags_busy,datas_busy,data_write_stall;
    logic [SET_ASSOC - 1:0] sig_tag_hit_t1,sig_hit_t1,sig_dirty_t1,sig_empty_t1,sig_replace_t1;

    logic [FIFO_WIDTH - 1:0] id_t1;
    logic ren_t1;
    logic wen_t1;
    logic [WORD_WIDTH/BYTE_WIDTH - 1:0] ben_t1;
    logic [WORD_WIDTH - 1:0] addr_t1;
    logic [WORD_WIDTH - 1:0] wdata_t1;
    logic [INDEX_WIDTH - 1:0] src_index,src_index_t1;
    logic [TAG_WIDTH - 1:0] src_tag_t1;
    
    logic [SET_ASSOC - 1:0] src_tags_we;
    logic [INDEX_WIDTH - 1:0] src_tags_rindex,src_tags_windex;
    cache_inf_t src_tags_winf,src_tags_rinf[SET_ASSOC - 1:0];
    
    logic [SET_ASSOC - 1:0] sig_valids_we,sig_valids_ce;
    logic [INDEX_WIDTH - 1:0] src_valids_rindex,src_valids_windex,src_valids_cindex;
    logic [$clog2(WORD_PER_LINE):0] src_valids_out[SET_ASSOC - 1:0];
    
    logic [SET_ASSOC - 1:0] sig_datas_we0,sig_datas_we1;
    logic [WORD_PER_LINE - 1:0][WORD_WIDTH/BYTE_WIDTH - 1:0] sig_datas_be0,sig_datas_be1;
    logic [INDEX_WIDTH - 1:0] src_datas_index0,src_datas_index1;
    logic [WORD_WIDTH - 1:0] src_datas_wdata0,src_datas_wdata1;
    logic [WORD_PER_LINE - 1:0][WORD_WIDTH - 1:0] src_datas_rdata[SET_ASSOC - 1:0];
    logic [WORD_PER_LINE - 1:0][WORD_WIDTH - 1:0] src_hit_rdata;
    
    axi_rd_state_t rd_state;
    axi_wb_state_t wb_state;
    
    logic [$clog2(WORD_PER_LINE) - 1:0] r_cnt,w_cnt;
    logic [WORD_PER_LINE - 1:0][WORD_WIDTH - 1:0] w_line;
    
    logic [WORD_WIDTH - 1:0] axi_araddr[1:0];
    logic [SET_ASSOC - 1:0] axi_replace[1:0];
    logic [$clog2(WORD_PER_LINE) - 1:0] axi_rcnt[1:0];
    logic axi_rstatus[1:0];
    logic axi_rp0,axi_rp1;
    logic [1:0] axi_rcount;
    
    logic [WORD_WIDTH - 1:0] axi_awaddr[1:0];
    logic axi_wstatus[1:0];
    logic axi_wp0,axi_wp1;
    logic [1:0] axi_wcount;
    
    always_ff @(posedge clk) begin
        if (reset) begin
            id_t1   <= '0;
            ren_t1  <= '0;
            wen_t1  <= '0;
            ben_t1  <= '0;
            addr_t1 <= '0;
            wdata_t1<= '0;
        end
        else begin
            if (!stall) begin
                ren_t1  <= ren;
                wen_t1  <= wen;
                ben_t1  <= ben;
                id_t1   <= idin;
                addr_t1 <= addr;
                wdata_t1<= wdata;
            end
            else begin
                if (data_write_stall) wen_t1  <= '0;
            end
        end
    end
    
    always_comb begin
        src_index = addr[WORD_WIDTH - TAG_WIDTH - 1:WORD_WIDTH - TAG_WIDTH - INDEX_WIDTH];
        src_index_t1 = addr_t1[WORD_WIDTH - TAG_WIDTH - 1:WORD_WIDTH - TAG_WIDTH - INDEX_WIDTH];
        src_tag_t1 = addr_t1[WORD_WIDTH - 1:WORD_WIDTH - TAG_WIDTH];
    end
    
    always_comb begin
        if (miss) begin
            if (tag_miss) begin
                if (axi_resp.rvalid && axi_req.rready && axi_rcnt[axi_rp1] == 0) src_tags_we = sig_replace_t1;
                else src_tags_we = '0;
                src_tags_rindex = src_index_t1;
                src_tags_windex = src_index_t1;
                src_tags_winf = {{WORD_WIDTH - TAG_WIDTH - 2{1'b0}},2'b01,src_tag_t1};
            end
            else begin
                src_tags_rindex = src_index_t1;
                src_tags_we = '0;
                src_tags_windex = '0;
                src_tags_winf = '0;
            end
        end
        else begin
            src_tags_rindex = src_index;
            if (wen_t1) begin
                src_tags_we = sig_hit_t1;
                src_tags_windex = src_index_t1;
                src_tags_winf = {{WORD_WIDTH - TAG_WIDTH - 2{1'b0}},2'b11,src_tag_t1};
            end
            else begin
                src_tags_we = '0;
                src_tags_windex = '0;
                src_tags_winf = '0;
            end
        end
    end
    
    CacheTags _CacheTags(
        .clk(clk),
        .reset(reset),
        .busy(tags_busy),
        .sig_we(src_tags_we),
        .src_rindex(src_tags_rindex),
        .src_windex(src_tags_windex),
        .src_winf(src_tags_winf),
        .src_rinf(src_tags_rinf)
    );
    
    logic [GROUP_NUM - 1:0] i_lru_ram;
    always_ff@ (posedge clk) begin
        if (reset) begin
            i_lru_ram <= '0;
        end
        else begin
            if ((~stall & (ren_t1 | wen_t1)) | data_write_stall) begin
                i_lru_ram[src_index_t1] <= sig_hit_t1[0];
            end
        end
    end
    
    genvar n;
    for(n = 0;n<SET_ASSOC;n++) begin
        assign sig_tag_hit_t1[n] = src_tags_rinf[n].tag == src_tag_t1 & src_tags_rinf[n].valid;
        assign sig_hit_t1[n] = sig_tag_hit_t1[n] && (src_valids_out[n] > addr_t1[LINE_BYTE_OFFSET-1:2]);
        assign sig_dirty_t1[n] = src_tags_rinf[n].dirty;
    end
    
    assign tag_miss = ~|sig_tag_hit_t1 & (wen_t1 | ren_t1);
    assign miss = ~|sig_hit_t1 & (wen_t1 | ren_t1);
    
    always_comb begin
        if(~(src_tags_rinf[0].valid)) sig_empty_t1 = 2'b01;
        else if(~(src_tags_rinf[1].valid)) sig_empty_t1 = 2'b10;
        else sig_empty_t1 = '0;
        if (tag_miss) sig_replace_t1 = |sig_empty_t1?sig_empty_t1:i_lru_ram[src_index_t1]?2'b10:2'b01;
        else sig_replace_t1 = '0;
    end
    
    always_comb begin
        if (axi_req.arvalid & axi_resp.arready) begin
            sig_valids_ce = sig_replace_t1;
            src_valids_cindex = src_index_t1;
        end
        else begin
            sig_valids_ce = '0;
            src_valids_cindex = '0;
        end
        if (axi_resp.rvalid & axi_req.rready) begin
            sig_valids_we = axi_replace[axi_rp1];
            src_valids_windex = axi_araddr[axi_rp1][WORD_WIDTH - TAG_WIDTH - 1:WORD_WIDTH - TAG_WIDTH - INDEX_WIDTH];
        end
        else begin
            sig_valids_we = '0;
            src_valids_windex = '0;
        end
        if (miss) src_valids_rindex = src_index_t1;
        else src_valids_rindex = src_index;
    end
    
    CacheValids _CacheValids(
        .clk(clk),
        .reset(reset),
        .we(sig_valids_we),
        .ce(sig_valids_ce),
        .rindex(src_valids_rindex),
        .windex(src_valids_windex),
        .cindex(src_valids_cindex),
        .valid_out(src_valids_out)
    );
    
    always_comb begin
        // case1 0 for read 1 for write
        // case2 0 for read 1 for missfill
        // case3 0 for write 1 for missfill and stall 1 tick
        sig_datas_be0 = '0;
        sig_datas_be1 = '0;
        if (axi_req.rready & axi_resp.rvalid) begin
            sig_datas_we1 = axi_replace[axi_resp.rid];
            sig_datas_be1[axi_rcnt[axi_rp1]] = 4'b1111;
            src_datas_index1 = axi_araddr[axi_rp1][WORD_WIDTH - TAG_WIDTH - 1:WORD_WIDTH - TAG_WIDTH - INDEX_WIDTH];
            src_datas_wdata1 = axi_resp.rdata;
            if (!miss) begin
                if (wen_t1) begin
                    sig_datas_we0 = sig_hit_t1;
                    sig_datas_be0[addr_t1[LINE_BYTE_OFFSET-1:2]] = ben_t1;
                    src_datas_index0 = src_index_t1;
                    src_datas_wdata0 = wdata_t1;
                end
                else begin
                    sig_datas_we0 = '0;
                    sig_datas_be0 = '0;
                    src_datas_index0 = src_index;
                    src_datas_wdata0 = '0;
                end
            end
            else begin
                sig_datas_we0 = '0;
                sig_datas_be0 = '0;
                src_datas_index0 = src_index_t1;
                src_datas_wdata0 = '0;
            end
        end
        else begin
            sig_datas_we1 = sig_hit_t1;
            sig_datas_be1[addr_t1[LINE_BYTE_OFFSET-1:2]] = ben_t1;
            src_datas_index1 = src_index_t1;
            src_datas_wdata1 = wdata_t1;
            if (!miss) begin
                sig_datas_we0 = '0;
                sig_datas_be0 = '0;
                src_datas_index0 = src_index;
                src_datas_wdata0 = '0;
            end
            else begin
                sig_datas_we0 = '0;
                sig_datas_be0 = '0;
                src_datas_index0 = src_index_t1;
                src_datas_wdata0 = '0;
            end
        end
    end
    
    assign data_write_stall = (axi_req.rready & axi_resp.rvalid) && (!miss && wen_t1);

    CacheDatas _CacheDatas(
        .clk(clk),
        .reset(reset),
        .sig_we0(sig_datas_we0),
        .sig_we1(sig_datas_we1),
        .sig_be0(sig_datas_be0),
        .sig_be1(sig_datas_be1),
        .src_index0(src_datas_index0),
        .src_index1(src_datas_index1),
        .src_wdata0(src_datas_wdata0),
        .src_wdata1(src_datas_wdata1),
        .busy(datas_busy),
        .src_rdata(src_datas_rdata)
    );
    
    always_comb begin
        if (miss) begin
            unique case(sig_replace_t1)
                2'b01:src_hit_rdata = src_datas_rdata[0];
                2'b10:src_hit_rdata = src_datas_rdata[1];
                2'b00:src_hit_rdata = '0;
            endcase
        end
        else begin
            unique case(sig_hit_t1)
                2'b01:src_hit_rdata = src_datas_rdata[0];
                2'b10:src_hit_rdata = src_datas_rdata[1];
                2'b00:src_hit_rdata = '0;
            endcase
        end
    end
    
    
    
    always_ff @(posedge clk) begin
        if (reset) begin
            rd_state <= IDLE;
        end
        else begin
            case(rd_state)
            IDLE: begin
                if(axi_req.arvalid & axi_resp.arready) rd_state <= IDLE;
                else if(axi_req.arvalid) rd_state <= WAIT_ARREADY;
            end
            WAIT_ARREADY: begin
                if(axi_req.arvalid & axi_resp.arready) rd_state <= IDLE;
            end
			endcase
        end
    end
    
    always_comb begin
        //common axi signals
        axi_req.araddr = '0;  
        axi_req.arid = {3'b0,axi_rp0};
        axi_req.arlock = '0;
        axi_req.arcache = '0;
        axi_req.arprot = '0;
        axi_req.arqos = '0;
        axi_req.arlen   = WORD_PER_LINE - 1;
	    axi_req.arsize  = 3'b010; // 4 bytes
	    axi_req.arburst = 2'b01;  // INCR          
	    axi_req.arvalid = 1'b0;
	    axi_req.rready = 1'b1; 
	    case (rd_state)
	       IDLE: begin
	           if (tag_miss) begin
	               axi_req.araddr = {addr_t1[WORD_WIDTH - 1 : LINE_BYTE_OFFSET], {LINE_BYTE_OFFSET{1'b0}}};
	               if (!(axi_req.araddr == axi_araddr[0] && axi_rstatus[0]) & !(axi_req.araddr == axi_araddr[1] && axi_rstatus[1])) axi_req.arvalid = 1'b1;
	           end
	       end
	       WAIT_ARREADY: begin
                axi_req.araddr = {addr_t1[WORD_WIDTH - 1 : LINE_BYTE_OFFSET], {LINE_BYTE_OFFSET{1'b0}}};
	            axi_req.arvalid = 1'b1;
            end
	    endcase
    end
    
    always_ff @(posedge clk) begin
        if (reset) begin
            wb_state <= WB_IDLE;
            w_cnt <= '0;
        end
        case(wb_state)
            WB_IDLE: begin
                if (tag_miss & |(sig_dirty_t1 & sig_replace_t1)) begin
                    w_line <= src_hit_rdata;
                    if (axi_req.awvalid & axi_resp.awready) wb_state <= WB_WRITE;
                    else if(axi_req.awvalid) wb_state <= WB_WAIT_AWREADY;
                end
            end
            WB_WAIT_AWREADY: begin
                if(axi_req.awvalid & axi_resp.awready) wb_state <= WB_WRITE;
            end
		    WB_WRITE: begin
			    if(axi_req.wvalid & axi_resp.wready) begin
			        w_cnt <= w_cnt + 1;
			        if (axi_req.wlast) wb_state <= WB_WAIT_BVALID;
			    end
			end
			WB_WAIT_BVALID: begin
			    if(axi_resp.bvalid) begin
			       wb_state <= WB_IDLE;
                end
            end
	   endcase
    end
    
    always_comb begin
        axi_req.awsize  = 2'b010;
        axi_req.awlen = WORD_PER_LINE - 1;
        axi_req.awburst = 2'b01;
        axi_req.awlock = '0;
        axi_req.awprot = '0;
        axi_req.awqos = '0;
        axi_req.awcache = '0;
        axi_req.awid = '0;
        axi_req.awaddr = '0;
        axi_req.awvalid = 1'b0;
        axi_req.wvalid = 1'b0;
        axi_req.wid = '0;
        axi_req.wdata = '0;
        axi_req.wlast = &w_cnt;
        axi_req.wstrb = 4'b1111;    
        axi_req.bready = 1'b1; // Ignores bresp  
        case(wb_state)
            WB_IDLE: begin
                if (tag_miss & |(sig_dirty_t1 & sig_replace_t1)) begin
                    unique case(sig_replace_t1)
                        2'b01:axi_req.awaddr = {src_tags_rinf[0].tag,src_index_t1,{LINE_BYTE_OFFSET{1'b0}}};
                        2'b10:axi_req.awaddr = {src_tags_rinf[1].tag,src_index_t1,{LINE_BYTE_OFFSET{1'b0}}};
                    endcase
                    axi_req.awvalid = 1'b1;
                end
                else begin
                    axi_req.awaddr = '0;
                    axi_req.awvalid = 1'b0;
                end
            end
            WB_WAIT_AWREADY: begin
                unique case(sig_replace_t1)
                    2'b01:axi_req.awaddr = {src_tags_rinf[0].tag,src_index_t1,{LINE_BYTE_OFFSET{1'b0}}};
                    2'b10:axi_req.awaddr = {src_tags_rinf[1].tag,src_index_t1,{LINE_BYTE_OFFSET{1'b0}}};
                endcase
                axi_req.awvalid = 1'b1;
            end
            WB_WRITE: begin
                axi_req.wdata = w_line[w_cnt];
                axi_req.wvalid = 1'b1;
            end
        endcase
    end

    always_ff @(posedge clk) begin
        if (reset) begin
            axi_rp0 <= '0;
            axi_rp1 <= '0;
            axi_rcount <= '0;
            axi_araddr[0] <= '0;
            axi_araddr[1] <= '0;
            axi_rstatus[0] <= '0;
            axi_rstatus[1] <= '0;
            axi_rcnt[0] <= '0;
            axi_rcnt[1] <= '0;
            axi_replace[0] <= '0;
            axi_replace[1] <= '0;
        end
        else begin
            if (axi_req.arvalid && axi_resp.arready) begin
                axi_araddr[axi_rp0] <= axi_req.araddr;
                axi_rstatus[axi_rp0] <= 1;
                axi_replace[axi_rp0] <= sig_replace_t1;
                axi_rp0 <= axi_rp0 + 1;
            end
            if (axi_resp.rvalid && axi_req.rready) begin
                axi_rcnt[axi_rp1] <= axi_rcnt[axi_rp1] + 1;
                if (axi_resp.rlast) begin
                    axi_araddr[axi_rp1] <= '0;
                    axi_rstatus[axi_rp1] <= '0;
                    axi_replace[axi_rp1] <= '0;
                    axi_rp1 <= axi_rp1 + 1;
                end
            end
            axi_rcount <= axi_rcount + (axi_req.arvalid && axi_resp.arready) - (axi_resp.rvalid && axi_resp.rlast && axi_req.rready);
        end
    end

    assign stall = miss | tags_busy | datas_busy | data_write_stall;
    assign valid = ren_t1 & !miss;
    assign rdata = src_hit_rdata[addr_t1[LINE_BYTE_OFFSET-1:2]];
    assign idout = id_t1;
    
endmodule
