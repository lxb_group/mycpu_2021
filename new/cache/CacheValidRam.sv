`timescale 1ns / 1ps
`include "signal_defs.svh"
`include "cache_defs.vh"
module CacheValidRam#(
    parameter WORD_WIDTH = `WORD_WIDTH,
    parameter LINE_WIDTH = `LINE_WIDTH,
    parameter SET_ASSOC = `SET_ASSOC,
    parameter CACHE_SIZE = `CACHE_SIZE,
    parameter LINE_NUM = `LINE_NUM,
    parameter GROUP_NUM = `GROUP_NUM,
    parameter WORD_PER_LINE = `WORD_PER_LINE,
    parameter LINE_WORD_OFFSET = `LINE_WORD_OFFSET,
    parameter INDEX_WIDTH = `INDEX_WIDTH,
    parameter TAG_WIDTH = `TAG_WIDTH
)(
    input clk,
    input reset,
    input we,
    input ce,
    input [INDEX_WIDTH - 1:0] rindex,
    input [INDEX_WIDTH - 1:0] windex,
    input [INDEX_WIDTH - 1:0] cindex,
    output logic [$clog2(WORD_PER_LINE):0] valid_out
    );
    
    logic [$clog2(WORD_PER_LINE):0] valid_cnt[GROUP_NUM - 1:0];
    integer i,j;
    
    always_ff @(posedge clk) begin
        if (reset) begin
            for (i = 0;i<GROUP_NUM;i++) begin
                valid_cnt[i] <= '0;
            end
        end
        else begin
            if (we) begin
                valid_cnt[windex] <= valid_cnt[windex] + 1;
            end
            if (ce) begin
                valid_cnt[cindex] <= '0;
            end
        end
    end
    
    always_ff @(posedge clk) begin
        if (rindex == windex && we) begin
            valid_out <= valid_cnt[windex] + 1;
        end
        if (rindex == cindex && ce) begin
            valid_out <= '0;
        end
        else begin
            valid_out <= valid_cnt[rindex];
        end
    end
    
//    logic rsta_busy,rstb_busy;
//    assign busy = rsta_busy|rstb_busy;
//    CacheValidBram _CacheValidBram (
//      .clka(clk),            // input wire clka
//      .rsta(reset),            // input wire rsta
//      .wea('0),              // input wire [0 : 0] wea
//      .addra(rindex),          // input wire [5 : 0] addra
//      .dina('0),            // input wire [15 : 0] dina
//      .douta(valid_out0),          // output wire [15 : 0] douta
//      .clkb(clk),            // input wire clkb
//      .rstb(reset),            // input wire rstb
//      .web(we),              // input wire [0 : 0] web
//      .addrb(windex),          // input wire [5 : 0] addrb
//      .dinb(valid_in),            // input wire [15 : 0] dinb
//      .doutb(valid_out1),          // output wire [15 : 0] doutb
//      .rsta_busy(rsta_busy),  // output wire rsta_busy
//      .rstb_busy(rstb_busy)  // output wire rstb_busy
//    );
endmodule
