`timescale 1ns / 1ps
`include "cache_defs.vh"
`include "signal_defs.svh"

module CPUIO#(
    parameter WORD_WIDTH = `WORD_WIDTH,
    parameter BYTE_WIDTH = `BYTE_WIDTH,
    parameter FIFO_DEPTH = `FIFO_DEPTH,
    parameter FIFO_WIDTH = `FIFO_WIDTH,
    parameter SET_ASSOC = `SET_ASSOC,
    parameter CACHE_SIZE = `CACHE_SIZE,
    parameter LINE_NUM = `LINE_NUM,
    parameter GROUP_NUM = `GROUP_NUM,
    parameter WORD_PER_LINE = `WORD_PER_LINE,
    parameter LINE_WORD_OFFSET = `LINE_WORD_OFFSET,
    parameter LINE_BYTE_OFFSET = `LINE_BYTE_OFFSET,
    parameter INDEX_WIDTH = `INDEX_WIDTH,
    parameter TAG_WIDTH = `TAG_WIDTH
)(
    input clk,
    input reset,
    // instruct
    input i_ren,
    input i_wen,
    input [WORD_WIDTH/BYTE_WIDTH - 1:0] i_ben,
    input [WORD_WIDTH - 1:0] i_addr,
    input [WORD_WIDTH - 1:0] i_wdata,
    output i_stall,
    output i_valid,
    output [WORD_WIDTH - 1:0] i_rdata,
    // data
    input d_ren,
    input d_wen,
    input [WORD_WIDTH/BYTE_WIDTH - 1:0] d_ben,
    input [WORD_WIDTH - 1:0] d_addr,
    input [WORD_WIDTH - 1:0] d_wdata,
    output d_stall,
    output d_valid,
    output [WORD_WIDTH - 1:0] d_rdata,
    // axi
     output wire [ 3 : 0] arid,
     output wire [31 : 0] araddr,
     output wire [ 3 : 0] arlen,
     output wire [ 2 : 0] arsize,
     output wire [ 1 : 0] arburst,
     output wire [ 1 : 0] arlock,
     output wire [ 3 : 0] arcache,
     output wire [ 2 : 0] arprot,
     output wire [ 3 : 0] arqos,
     output wire [ 0 : 0] arvalid,
     input  wire [ 0 : 0] arready,
     input  wire [ 3 : 0] rid,
     input  wire [31 : 0] rdata,
     input  wire [ 1 : 0] rresp,
     input  wire [ 0 : 0] rlast,
     input  wire [ 0 : 0] rvalid,
     output wire [ 0 : 0] rready,
     output wire [ 3 : 0] awid,
     output wire [31 : 0] awaddr,
     output wire [ 3 : 0] awlen,
     output wire [ 2 : 0] awsize,
     output wire [ 1 : 0] awburst,
     output wire [ 1 : 0] awlock,
     output wire [ 3 : 0] awcache,
     output wire [ 2 : 0] awprot,
     output wire [ 3 : 0] awqos,
     output wire [ 0 : 0] awvalid,
     input  wire [ 0 : 0] awready,
     output wire [ 3 : 0] wid,
     output wire [31 : 0] wdata,
     output wire [ 3 : 0] wstrb,
     output wire [ 0 : 0] wlast,
     output wire [ 0 : 0] wvalid,
     input  wire [ 0 : 0] wready,
     input  wire [ 3 : 0] bid,
     input  wire [ 1 : 0] bresp,
     input  wire [ 0 : 0] bvalid,
     output wire [ 0 : 0] bready
    );
    
    axi_req_t axi_req0,axi_req1,axi_req2,axi_req3;
    axi_resp_t axi_resp0,axi_resp1,axi_resp2,axi_resp3;
    
    i_IO _i_IO(
        .clk(clk),
        .reset(reset),
        // signals
        .ren(i_ren),
        .wen(i_wen),
        .ben(i_ben),
        .addr(i_addr),
        .wdata(i_wdata),
        // fifo
        .stall(i_stall),
        .valid(i_valid),
        .rdata(i_rdata),
        // axi
        .cache_axi_resp(axi_resp0),
        .cache_axi_req(axi_req0),
        .uncache_axi_resp(axi_resp1),
        .uncache_axi_req(axi_req1)
    );
    
    d_IO _d_IO(
        .clk(clk),
        .reset(reset),
        // signals
        .ren(d_ren),
        .wen(d_wen),
        .ben(d_ben),
        .addr(d_addr),
        .wdata(d_wdata),
        // fifo
        .stall(d_stall),
        .valid(d_valid),
        .rdata(d_rdata),
        // axi
        .cache_axi_resp(axi_resp2),
        .cache_axi_req(axi_req2),
        .uncache_axi_resp(axi_resp3),
        .uncache_axi_req(axi_req3)
    );
    
    Crossbar _Crossbar (
      .aclk(clk),                    // input wire aclk
      .aresetn(~reset),              // input wire aresetn
      
      .s_axi_arid   ({axi_req3.arid,     axi_req2.arid,     axi_req1.arid,     axi_req0.arid    }), // input wire [11 : 0] s_axi_arid
      .s_axi_araddr ({axi_req3.araddr,   axi_req2.araddr,   axi_req1.araddr,   axi_req0.araddr  }), // input wire [127 : 0] s_axi_araddr
      .s_axi_arlen  ({axi_req3.arlen,    axi_req2.arlen,    axi_req1.arlen,    axi_req0.arlen   }), // input wire [15 : 0] s_axi_arlen
      .s_axi_arsize ({axi_req3.arsize,   axi_req2.arsize,   axi_req1.arsize,   axi_req0.arsize  }), // input wire [11 : 0] s_axi_arsize
      .s_axi_arburst({axi_req3.arburst,  axi_req2.arburst,  axi_req1.arburst,  axi_req0.arburst }), // input wire [7 : 0] s_axi_arburst
      .s_axi_arlock ({axi_req3.arlock,   axi_req2.arlock,   axi_req1.arlock,   axi_req0.arlock  }), // input wire [7 : 0] s_axi_arlock
      .s_axi_arcache({axi_req3.arcache,  axi_req2.arcache,  axi_req1.arcache,  axi_req0.arcache }), // input wire [15 : 0] s_axi_arcache
      .s_axi_arprot ({axi_req3.arprot,   axi_req2.arprot,   axi_req1.arprot,   axi_req0.arprot  }), // input wire [11 : 0] s_axi_arprot
      .s_axi_arqos  ({axi_req3.arqos,    axi_req2.arqos,    axi_req1.arqos,    axi_req0.arqos   }), // input wire [15 : 0] s_axi_arqos
      .s_axi_arvalid({axi_req3.arvalid,  axi_req2.arvalid,  axi_req1.arvalid,  axi_req0.arvalid }), // input wire [3 : 0] s_axi_arvalid
      .s_axi_arready({axi_resp3.arready, axi_resp2.arready, axi_resp1.arready, axi_resp0.arready}), // output wire [3 : 0] s_axi_arready
      .s_axi_rid    ({axi_resp3.rid,     axi_resp2.rid,     axi_resp1.rid,     axi_resp0.rid    }), // output wire [11 : 0] s_axi_rid
      .s_axi_rdata  ({axi_resp3.rdata,   axi_resp2.rdata,   axi_resp1.rdata,   axi_resp0.rdata  }), // output wire [127 : 0] s_axi_rdata
      .s_axi_rresp  ({axi_resp3.rresp,   axi_resp2.rresp,   axi_resp1.rresp,   axi_resp0.rresp  }), // output wire [7 : 0] s_axi_rresp
      .s_axi_rlast  ({axi_resp3.rlast,   axi_resp2.rlast,   axi_resp1.rlast,   axi_resp0.rlast  }), // output wire [3 : 0] s_axi_rlast
      .s_axi_rvalid ({axi_resp3.rvalid,  axi_resp2.rvalid,  axi_resp1.rvalid,  axi_resp0.rvalid }), // output wire [3 : 0] s_axi_rvalid
      .s_axi_rready ({axi_req3.rready,   axi_req2.rready,   axi_req1.rready,   axi_req0.rready  }), // input wire [3 : 0] s_axi_rready
      .s_axi_awid   ({axi_req3.awid,     axi_req2.awid,     axi_req1.awid,     axi_req0.awid    }), // input wire [11 : 0] s_axi_awid
      .s_axi_awaddr ({axi_req3.awaddr,   axi_req2.awaddr,   axi_req1.awaddr,   axi_req0.awaddr  }), // input wire [127 : 0] s_axi_awaddr
      .s_axi_awlen  ({axi_req3.awlen,    axi_req2.awlen,    axi_req1.awlen,    axi_req0.awlen   }), // input wire [15 : 0] s_axi_awlen
      .s_axi_awsize ({axi_req3.awsize,   axi_req2.awsize,   axi_req1.awsize,   axi_req0.awsize  }), // input wire [11 : 0] s_axi_awsize
      .s_axi_awburst({axi_req3.awburst,  axi_req2.awburst,  axi_req1.awburst,  axi_req0.awburst }), // input wire [7 : 0] s_axi_awburst
      .s_axi_awlock ({axi_req3.awlock,   axi_req2.awlock,   axi_req1.awlock,   axi_req0.awlock  }), // input wire [7 : 0] s_axi_awlock
      .s_axi_awcache({axi_req3.awcache,  axi_req2.awcache,  axi_req1.awcache,  axi_req0.awcache }), // input wire [15 : 0] s_axi_awcache
      .s_axi_awprot ({axi_req3.awprot,   axi_req2.awprot,   axi_req1.awprot,   axi_req0.awprot  }), // input wire [11 : 0] s_axi_awprot
      .s_axi_awqos  ({axi_req3.awqos,    axi_req2.awqos,    axi_req1.awqos,    axi_req0.awqos   }), // input wire [15 : 0] s_axi_awqos
      .s_axi_awvalid({axi_req3.awvalid,  axi_req2.awvalid,  axi_req1.awvalid,  axi_req0.awvalid }), // input wire [3 : 0] s_axi_awvalid
      .s_axi_awready({axi_resp3.awready, axi_resp2.awready, axi_resp1.awready, axi_resp0.awready}), // output wire [3 : 0] s_axi_awready
      .s_axi_wid    ({axi_req3.wid,      axi_req2.wid,      axi_req1.wid,      axi_req0.wid     }), // input wire [11 : 0] s_axi_wid
      .s_axi_wdata  ({axi_req3.wdata,    axi_req2.wdata,    axi_req1.wdata,    axi_req0.wdata   }), // input wire [127 : 0] s_axi_wdata
      .s_axi_wstrb  ({axi_req3.wstrb,    axi_req2.wstrb,    axi_req1.wstrb,    axi_req0.wstrb   }), // input wire [15 : 0] s_axi_wstrb
      .s_axi_wlast  ({axi_req3.wlast,    axi_req2.wlast,    axi_req1.wlast,    axi_req0.wlast   }), // input wire [3 : 0] s_axi_wlast
      .s_axi_wvalid ({axi_req3.wvalid,   axi_req2.wvalid,   axi_req1.wvalid,   axi_req0.wvalid  }), // input wire [3 : 0] s_axi_wvalid
      .s_axi_wready ({axi_resp3.wready,  axi_resp2.wready,  axi_resp1.wready,  axi_resp0.wready }), // output wire [3 : 0] s_axi_wready
      .s_axi_bid    ({axi_resp3.bid,     axi_resp2.bid,     axi_resp1.bid,     axi_resp0.bid    }), // output wire [11 : 0] s_axi_bid
      .s_axi_bresp  ({axi_resp3.bresp,   axi_resp2.bresp,   axi_resp1.bresp,   axi_resp0.bresp  }), // output wire [7 : 0] s_axi_bresp
      .s_axi_bvalid ({axi_resp3.bvalid,  axi_resp2.bvalid,  axi_resp1.bvalid,  axi_resp0.bvalid }), // output wire [3 : 0] s_axi_bvalid
      .s_axi_bready ({axi_req3.bready,   axi_req2.bready,   axi_req1.bready,   axi_req0.bready  }), // input wire [3 : 0] s_axi_bready
      
      .m_axi_arid   (arid   ), // output wire [2 : 0] m_axi_arid
      .m_axi_araddr (araddr ), // output wire [31 : 0] m_axi_araddr
      .m_axi_arlen  (arlen  ), // output wire [3 : 0] m_axi_arlen
      .m_axi_arsize (arsize ), // output wire [2 : 0] m_axi_arsize
      .m_axi_arburst(arburst), // output wire [1 : 0] m_axi_arburst
      .m_axi_arlock (arlock ), // output wire [1 : 0] m_axi_arlock
      .m_axi_arcache(arcache), // output wire [3 : 0] m_axi_arcache
      .m_axi_arprot (arprot ), // output wire [2 : 0] m_axi_arprot
      .m_axi_arqos  (arqos  ), // output wire [3 : 0] m_axi_arqos
      .m_axi_arvalid(arvalid), // output wire [0 : 0] m_axi_arvalid
      .m_axi_arready(arready), // input wire [0 : 0] m_axi_arready
      .m_axi_rid    (rid    ), // input wire [2 : 0] m_axi_rid
      .m_axi_rdata  (rdata  ), // input wire [31 : 0] m_axi_rdata
      .m_axi_rresp  (rresp  ), // input wire [1 : 0] m_axi_rresp
      .m_axi_rlast  (rlast  ), // input wire [0 : 0] m_axi_rlast
      .m_axi_rvalid (rvalid ), // input wire [0 : 0] m_axi_rvalid
      .m_axi_rready (rready ), // output wire [0 : 0] m_axi_rready
      .m_axi_awid   (awid   ), // output wire [2 : 0] m_axi_awid
      .m_axi_awaddr (awaddr ), // output wire [31 : 0] m_axi_awaddr
      .m_axi_awlen  (awlen  ), // output wire [3 : 0] m_axi_awlen
      .m_axi_awsize (awsize ), // output wire [2 : 0] m_axi_awsize
      .m_axi_awburst(awburst), // output wire [1 : 0] m_axi_awburst
      .m_axi_awlock (awlock ), // output wire [1 : 0] m_axi_awlock
      .m_axi_awcache(awcache), // output wire [3 : 0] m_axi_awcache
      .m_axi_awprot (awprot ), // output wire [2 : 0] m_axi_awprot
      .m_axi_awqos  (awqos  ), // output wire [3 : 0] m_axi_awqos
      .m_axi_awvalid(awvalid), // output wire [0 : 0] m_axi_awvalid
      .m_axi_awready(awready), // input wire [0 : 0] m_axi_awready
      .m_axi_wid    (wid    ), // output wire [2 : 0] m_axi_wid
      .m_axi_wdata  (wdata  ), // output wire [31 : 0] m_axi_wdata
      .m_axi_wstrb  (wstrb  ), // output wire [3 : 0] m_axi_wstrb
      .m_axi_wlast  (wlast  ), // output wire [0 : 0] m_axi_wlast
      .m_axi_wvalid (wvalid ), // output wire [0 : 0] m_axi_wvalid
      .m_axi_wready (wready ), // input wire [0 : 0] m_axi_wready
      .m_axi_bid    (bid    ), // input wire [2 : 0] m_axi_bid
      .m_axi_bresp  (bresp  ), // input wire [1 : 0] m_axi_bresp
      .m_axi_bvalid (bvalid ), // input wire [0 : 0] m_axi_bvalid
      .m_axi_bready (bready )  // output wire [0 : 0] m_axi_bready
    );
    
    integer mem1,mem2;
    initial begin
        mem1 = $fopen("mem_task.txt", "w"); 
        mem2 = $fopen("mem_rdata.txt", "w");
    end

    always @(posedge clk) begin
        if (~d_stall) begin
            if (d_ren) begin
                $fdisplay(mem1, "%h =>", {3'b000,d_addr[28:0]});
            end
            else if (d_wen) begin
                $fdisplay(mem1, "%h <= %h", {3'b000,d_addr[28:0]}, d_wdata);
            end
        end
        if (d_valid) begin
            $fdisplay(mem2, "%h", d_rdata);
        end
    end
endmodule
