`timescale 1ns / 1ps
`include "cache_defs.vh"
`include "signal_defs.svh"

module CacheDataRam#(
    parameter WORD_WIDTH = `WORD_WIDTH,
    parameter BYTE_WIDTH = `BYTE_WIDTH,
    parameter LINE_WIDTH = `LINE_WIDTH,
    parameter SET_ASSOC = `SET_ASSOC,
    parameter CACHE_SIZE = `CACHE_SIZE,
    parameter LINE_NUM = `LINE_NUM,
    parameter GROUP_NUM = `GROUP_NUM,
    parameter DATA_PER_LINE = `DATA_PER_LINE,
    parameter DATA_BYTE_OFFSET = `DATA_BYTE_OFFSET,
    parameter LINE_BYTE_OFFSET = `LINE_BYTE_OFFSET,
    parameter INDEX_WIDTH = `INDEX_WIDTH,
    parameter TAG_WIDTH = `TAG_WIDTH
)(
    input clk,
    input reset,
    input [WORD_WIDTH/BYTE_WIDTH - 1:0] sig_be0,
    input [WORD_WIDTH/BYTE_WIDTH - 1:0] sig_be1,
    input [INDEX_WIDTH-1:0] src_index0,
    input [INDEX_WIDTH-1:0] src_index1,
    input [WORD_WIDTH-1:0] src_wdata0,
    input [WORD_WIDTH-1:0] src_wdata1,
    output busy,
    output logic [WORD_WIDTH - 1:0] src_rdata0,
    output logic [WORD_WIDTH - 1:0] src_rdata1
    );
    
    logic rsta_busy,rstb_busy;
    assign busy = rsta_busy | rstb_busy;
    CacheDataBram _CacheDataBram (
        .clka(clk),            // input wire clka
        .rsta(reset),            // input wire rsta
        .wea(sig_be0),              // input wire [3 : 0] wea
        .addra(src_index0),          // input wire [5 : 0] addra
        .dina(src_wdata0),            // input wire [31 : 0] dina
        .douta(src_rdata0),          // output wire [31 : 0] douta
        .clkb(clk),            // input wire clkb
        .rstb(reset),            // input wire rstb
        .web(sig_be1),              // input wire [3 : 0] web
        .addrb(src_index1),          // input wire [5 : 0] addrb
        .dinb(src_wdata1),            // input wire [31 : 0] dinb
        .doutb(src_rdata1),          // output wire [31 : 0] doutb
        .rsta_busy(rsta_busy),  // output wire rsta_busy
        .rstb_busy(rstb_busy)  // output wire rstb_busy
    );
endmodule
