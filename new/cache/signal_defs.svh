`ifndef SIGNAL_STRUCTS_SVH
`define SIGNAL_STRUCTS_SVH

typedef struct packed {
    logic [10:0] empty;
    logic dirty;
    logic valid;
    logic [18:0] tag;
}cache_inf_t;

typedef struct{
	// ar
	logic [3 :0] arid;
    logic [31:0] araddr;
    logic [3 :0] arlen;
    logic [2 :0] arsize;
    logic [1 :0] arburst;
    logic [1 :0] arlock;
    logic [3 :0] arcache;
    logic [2 :0] arprot;
    logic [3 :0] arqos;
    logic        arvalid;
    
	// r
    logic        rready;
	// aw
	logic [3 :0] awid;
    logic [31:0] awaddr;
    logic [3 :0] awlen;
    logic [2 :0] awsize;
    logic [1 :0] awburst;
    logic [1 :0] awlock;
    logic [3 :0] awcache;
    logic [2 :0] awprot;
    logic [3 :0] awqos;
    logic        awvalid;
	// w
	logic [3 :0] wid;
    logic [31:0] wdata;
    logic [3 :0] wstrb;
    logic        wlast;
    logic        wvalid;
    
	// b
    logic        bready;
} axi_req_t;

typedef struct{
	// ar
	logic        arready;
	// r
	logic [3 :0] rid;
	logic [31:0] rdata;
	logic [1 :0] rresp;
	logic        rlast;
	logic        rvalid;
	
	// aw
	logic        awready;
	// w
	logic        wready;
	// b
	logic [3 :0] bid;
	logic [1 :0] bresp;
	logic        bvalid;
} axi_resp_t;

typedef enum logic [2:0] {
	IDLE,
	WAIT_ARREADY,
	RECEIVING,
	REFILL
} axi_rd_state_t;

typedef enum logic [2:0] {
	WB_IDLE,
	WB_READ_PRE,
	WB_WAIT_AWREADY,
	WB_WRITE,
	WB_WAIT_BVALID
} axi_wb_state_t;

`endif