`timescale 1ns / 1ps
`include "cache_defs.vh"
`include "signal_defs.svh"
module Uncache#(
    parameter WORD_WIDTH = `WORD_WIDTH,
    parameter BYTE_WIDTH = `BYTE_WIDTH,
    parameter FIFO_DEPTH = `FIFO_DEPTH,
    parameter FIFO_WIDTH = `FIFO_WIDTH
)(
    input clk,
    input reset,
    // signals
    input ren,
    input wen,
    input [WORD_WIDTH/BYTE_WIDTH - 1:0] ben,
    input [FIFO_WIDTH - 1:0] idin,
    input [WORD_WIDTH - 1:0] addr,
    input [WORD_WIDTH - 1:0] wdata,
    // fifo
    output logic stall,
    output logic valid,
    output logic [FIFO_WIDTH - 1:0] idout,
    output logic [WORD_WIDTH - 1:0] rdata,
    // axi
    input axi_resp_t axi_resp,
    output axi_req_t axi_req
    );
    
    logic [5:0] r_count;
    logic [WORD_WIDTH-1:0] r_addrs[3:0];
    logic [FIFO_WIDTH - 1:0] r_ids[3:0];
    logic r_status[3:0];
    logic r_full;
    
    logic [5:0] w_count;
    logic [1:0] w_p0,w_p1;
    logic [1:0] w_ids[3:0];
    logic [WORD_WIDTH/BYTE_WIDTH - 1:0] w_bens[3:0];
    logic [WORD_WIDTH-1:0] w_addrs[3:0];
    logic [WORD_WIDTH-1:0] w_datas[3:0];
    logic w_status[3:0];
    logic w_full;
    
    logic rw_conflict,wr_conflict;
    logic stall_1,stall_2;
    
    logic [1:0] arid,awid,_awid,_arid;
    
    logic ren_t1,wen_t1;
    logic [WORD_WIDTH/BYTE_WIDTH - 1:0] ben_t1;
    logic [FIFO_WIDTH - 1:0] idin_t1;
    logic [WORD_WIDTH - 1:0] addr_t1;
    logic [WORD_WIDTH - 1:0] wdata_t1;
    
    always_ff @(posedge clk) begin
        if (reset) begin
            ren_t1 <= '0;
            wen_t1 <= '0;
            ben_t1 <= '0;
            idin_t1 <= '0;
            addr_t1 <= '0;
            wdata_t1 <= '0;
        end
        else begin
            if (!stall) begin
                ren_t1 <= ren;
                wen_t1 <= wen;
                ben_t1 <= ben;
                idin_t1 <= idin;
                addr_t1 <= addr;
                wdata_t1 <= wdata;
            end
        end
    end
    
    integer i,j;
    
    always_ff @(posedge clk) begin
        if (reset) begin
            r_count <= '0;
            for(i=0;i<4;i++) begin
                r_addrs[i] <= '0;
                r_ids[i] <= '0;
                r_status[i] <= '0;
            end
        end
        else begin
            if (axi_req.arvalid & axi_resp.arready) begin
                r_addrs[axi_req.arid] <= axi_req.araddr;
                r_ids[axi_req.arid] <= stall? idin_t1:idin;
                r_status[axi_req.arid] <= 1;
            end
            if (axi_resp.rvalid & axi_req.rready) begin
                r_addrs[axi_resp.rid] <= '0;
                r_ids[axi_resp.rid] <= '0;
                r_status[axi_resp.rid] <= '0;
            end
            r_count <= r_count + (axi_req.arvalid & axi_resp.arready) - (axi_resp.rvalid & axi_req.rready);
        end
    end
    
    always_ff @(posedge clk) begin
        if (reset) begin
            w_count <= '0;
            for(j=0;j<4;j++) begin
                w_ids[j] <= '0;
                w_bens[j] <= '0;
                w_addrs[j] <= '0;
                w_datas[j] <= '0;
                w_status[j] <= '0;
                w_p0 <= '0;
                w_p1 <= '0;
            end
        end
        else begin
            if (axi_req.awvalid & axi_resp.awready) begin
                w_addrs[axi_req.awid] <= axi_req.awaddr;
                w_status[axi_req.awid] <= 1'b1;
                w_datas[w_p0] <= stall? wdata_t1:wdata;
                w_ids[w_p0] <= axi_req.awid;
                w_bens[w_p0] <= stall? ben_t1:ben; 
                w_p0 <= w_p0 + 1;
            end
            if (axi_req.wvalid & axi_resp.wready) begin
                w_datas[w_p1] <= '0;
                w_ids[w_p1] <= '0;
                w_bens[w_p1] <= '0;
                w_p1 <= w_p1 + 1;
            end
            if (axi_resp.bvalid & axi_req.bready) begin
                w_addrs[axi_resp.bid] <= '0;
                w_status[axi_resp.bid] <= '0;
            end
            w_count <= w_count + (axi_req.awvalid & axi_resp.awready) - (axi_resp.bvalid & axi_req.bready);
        end
    end
    
    assign r_full = r_count == 4;
    assign w_full = w_count == 4;
    
    always_comb begin
        if (!r_status[0]) _arid = 0;
        else if (!r_status[1]) _arid = 1;
        else if (!r_status[2]) _arid = 2;
        else if (!r_status[3]) _arid = 3;
        if (!w_status[0]) _awid = 0;
        else if (!w_status[1]) _awid = 1;
        else if (!w_status[2]) _awid = 2;
        else if (!w_status[3]) _awid = 3;
    end
    
    always_ff @(posedge clk) begin
        if (reset) begin
            arid <= '0;
            awid <= '0;
//            rflag <= '0;
//            wflag <= '0;
        end
        else begin
            if (!stall) begin
                if (ren) arid <= _arid;
                else if (wen) awid <= _awid;
            end
//            if (!stall) begin
//                if (ren) rflag <= '1;
//                else rflag <= '0;
//                if (wen) wflag <= '1;
//                else wflag <= '0;
//            end
        end
    end
    
    always_comb begin
        //common axi signals
        axi_req.araddr  = stall_2? addr_t1:addr;
        axi_req.arid = stall_2? arid:_arid;
        axi_req.arlock  = '0;
        axi_req.arcache = '0;
        axi_req.arprot  = '0;
        axi_req.arqos = '0;
        axi_req.arlen   = '0;
	    axi_req.arsize  = 3'b010; // 4 bytes
	    axi_req.arburst = 2'b01;  // INCR         
	    axi_req.arvalid = stall_2? ren_t1 : (ren & !r_full & !wr_conflict);
	    
	    axi_req.rready  = 1'b1; 
    end
    
    always_comb begin
        axi_req.awlock  = '0;
        axi_req.awprot  = '0;
        axi_req.awqos = '0;
        axi_req.awcache = '0;
        axi_req.awid = stall_2? awid:_awid;
        axi_req.awsize  = 2'b010;
        axi_req.awlen   = '0;
        axi_req.awburst = 2'b01;
        axi_req.awaddr  = stall_2? addr_t1:addr;
        axi_req.awvalid = stall_2? wen_t1 : (wen & !w_full & !rw_conflict);
        
        axi_req.wid     = w_ids[w_p1];
        axi_req.wvalid  = w_count != 0;
        axi_req.wdata   = w_datas[w_p1];
        axi_req.wlast   = w_count != 0;
        axi_req.wstrb   = w_bens[w_p1];   
        
        axi_req.bready  = 1'b1;
    end
    
    
    assign rw_conflict = wen & ((addr == r_addrs[0] & r_status[0])|
                                (addr == r_addrs[1] & r_status[1])|
                                (addr == r_addrs[2] & r_status[2])|
                                (addr == r_addrs[3] & r_status[3]));
    assign wr_conflict = ren & ((addr == w_addrs[0] & w_status[0])|
                                (addr == w_addrs[1] & w_status[1])|
                                (addr == w_addrs[2] & w_status[2])|
                                (addr == w_addrs[3] & w_status[3]));
    assign stall_1 = (ren & r_full) | (wen & w_full) | rw_conflict | wr_conflict;
    always_ff @(posedge clk) begin
        if (reset) begin
            stall_2 <= '0;
        end
        else begin
            if (axi_resp.arready | axi_resp.awready) stall_2 <= 1'b0;
            else if (axi_req.arvalid | axi_req.awvalid) stall_2 <= 1'b1;
        end
    end
    //assign stall_2 = (axi_req.arvalid & !axi_resp.arready) | (axi_req.awvalid & !axi_resp.awready);
    assign stall = stall_1 | stall_2;
    assign valid = axi_resp.rvalid & axi_req.rready;
    assign rdata = axi_resp.rdata;
    assign idout = r_ids[axi_resp.rid];
endmodule
