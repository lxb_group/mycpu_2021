`timescale 1ns / 1ps
`include "cache_defs.vh"
`include "signal_defs.svh"


module CacheDatas#(
    parameter WORD_WIDTH = `WORD_WIDTH,
    parameter BYTE_WIDTH = `BYTE_WIDTH,
    parameter FIFO_DEPTH = `FIFO_DEPTH,
    parameter FIFO_WIDTH = `FIFO_WIDTH,
    parameter SET_ASSOC = `SET_ASSOC,
    parameter CACHE_SIZE = `CACHE_SIZE,
    parameter LINE_NUM = `LINE_NUM,
    parameter GROUP_NUM = `GROUP_NUM,
    parameter WORD_PER_LINE = `WORD_PER_LINE,
    parameter LINE_WORD_OFFSET = `LINE_WORD_OFFSET,
    parameter LINE_BYTE_OFFSET = `LINE_BYTE_OFFSET,
    parameter INDEX_WIDTH = `INDEX_WIDTH,
    parameter TAG_WIDTH = `TAG_WIDTH
)(
    input clk,
    input reset,
    input [SET_ASSOC-1:0] sig_we0,
    input [SET_ASSOC-1:0] sig_we1,
    input [WORD_PER_LINE - 1:0][WORD_WIDTH/BYTE_WIDTH - 1:0] sig_be0,
    input [WORD_PER_LINE - 1:0][WORD_WIDTH/BYTE_WIDTH - 1:0] sig_be1,
    input [INDEX_WIDTH-1:0] src_index0,
    input [INDEX_WIDTH-1:0] src_index1,
    input [WORD_WIDTH-1:0] src_wdata0,
    input [WORD_WIDTH-1:0] src_wdata1,
    output busy,
    output [WORD_PER_LINE - 1:0][WORD_WIDTH - 1:0] src_rdata[SET_ASSOC-1:0]
    );
    logic [SET_ASSOC - 1:0] _busy;
    assign busy = |_busy;
    
    genvar i;
    generate
        for(i=0;i<SET_ASSOC;i++) begin:_Cacheline
            Cacheline _Cacheline(
                .clk(clk),
                .reset(reset),
                .sig_we0(sig_we0[i]),
                .sig_we1(sig_we1[i]),
                .sig_be0(sig_be0),
                .sig_be1(sig_be1),
                .src_index0(src_index0),
                .src_index1(src_index1),
                .src_wdata0(src_wdata0),
                .src_wdata1(src_wdata1),
                .busy(_busy[i]),
                .src_rdata(src_rdata[i])
            );
        end
    endgenerate
endmodule
