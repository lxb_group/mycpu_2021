// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Wed Mar 10 10:42:45 2021
// Host        : DESKTOP-0MTHLTL running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               e:/WorkSpace/LXB/perf_update2/soc_axi_perf/rtl/myCPU/ip/CacheDataBram/CacheDataBram_sim_netlist.v
// Design      : CacheDataBram
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a200tfbg676-2
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "CacheDataBram,blk_mem_gen_v8_4_4,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "blk_mem_gen_v8_4_4,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module CacheDataBram
   (clka,
    rsta,
    wea,
    addra,
    dina,
    douta,
    clkb,
    rstb,
    web,
    addrb,
    dinb,
    doutb,
    rsta_busy,
    rstb_busy);
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clka;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA RST" *) input rsta;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA WE" *) input [3:0]wea;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [6:0]addra;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DIN" *) input [31:0]dina;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [31:0]douta;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTB, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clkb;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB RST" *) input rstb;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB WE" *) input [3:0]web;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB ADDR" *) input [6:0]addrb;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB DIN" *) input [31:0]dinb;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB DOUT" *) output [31:0]doutb;
  output rsta_busy;
  output rstb_busy;

  wire [6:0]addra;
  wire [6:0]addrb;
  wire clka;
  wire clkb;
  wire [31:0]dina;
  wire [31:0]dinb;
  wire [31:0]douta;
  wire [31:0]doutb;
  wire rsta;
  wire rsta_busy;
  wire rstb;
  wire rstb_busy;
  wire [3:0]wea;
  wire [3:0]web;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [6:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [6:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [31:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "7" *) 
  (* C_ADDRB_WIDTH = "7" *) 
  (* C_ALGORITHM = "1" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "8" *) 
  (* C_COMMON_CLK = "0" *) 
  (* C_COUNT_18K_BRAM = "0" *) 
  (* C_COUNT_36K_BRAM = "1" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "1" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "1" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "1" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "0" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     5.9299 mW" *) 
  (* C_FAMILY = "artix7" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "0" *) 
  (* C_HAS_ENB = "0" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "1" *) 
  (* C_HAS_RSTB = "1" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "CacheDataBram.mem" *) 
  (* C_INIT_FILE_NAME = "no_coe_file_loaded" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "0" *) 
  (* C_MEM_TYPE = "2" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "1" *) 
  (* C_READ_DEPTH_A = "128" *) 
  (* C_READ_DEPTH_B = "128" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "32" *) 
  (* C_READ_WIDTH_B = "32" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "1" *) 
  (* C_USE_BYTE_WEB = "1" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "4" *) 
  (* C_WEB_WIDTH = "4" *) 
  (* C_WRITE_DEPTH_A = "128" *) 
  (* C_WRITE_DEPTH_B = "128" *) 
  (* C_WRITE_MODE_A = "WRITE_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "32" *) 
  (* C_WRITE_WIDTH_B = "32" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  CacheDataBram_blk_mem_gen_v8_4_4 U0
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina(dina),
        .dinb(dinb),
        .douta(douta),
        .doutb(doutb),
        .eccpipece(1'b0),
        .ena(1'b0),
        .enb(1'b0),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[6:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(rsta),
        .rsta_busy(rsta_busy),
        .rstb(rstb),
        .rstb_busy(rstb_busy),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[6:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[31:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(1'b0),
        .wea(wea),
        .web(web));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
QGLtnqZzRetDH6gCWT4Js6wuLlZfrNx/VJp3sfR2NF+cxypO5AxN0oDKLJJtmdrtE/ueNDg+Qf7Z
TqBNRojORA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
B6Ger3hRvfjHkaJ+W8639Kl3TzC9TogLuklOXEiMNdc4Im+DjEUzxb3DKlzu0VW3zxZqjJ3+wsW/
LnRmPCESi5Y9eRJaLFXg79EMfoj4X+nTdHAP6yCfltBADKegZ12gpnB/8ey5yn2KA74LUtPC7jna
iyjqSfsWLGnz6UdXzwk=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
BX+DxgMPRyZbYojCUR9Sk8Lq+3ZigBz4yMFHQkmurfdfDzyTPJCE827eGiPyTenK1QPVhEtf9g06
0BFXq/0COPuU1BWJwdkz1c4dE6/exDwhvEh+hPx3vRY6z8fDEf6aGVIXrHDvrmddehe7yMSIpo+k
aXHR06EEdfHCFY4TggYwhcJVXjkE+ApsVuyfmEfPmYjo8hCWyQyBsUWIOY03q1+MvUjjsmTwgs9g
fh5MY9ToaLfoJxPKdCpsqrBX4LJ+VDGFlAqIcqHTE2jCmPiToZAFXB7fzf1wDjFCBlJyFVDBGi0i
m+CouLSb7X1mvVhdDZgNrZDJMV688Bu3o54vew==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DaIU/Ddc8USbZ2mURzujJDWDH1JbHl5tFVOOQ2aVaUPIA71yyE38OXVLEtF8rNmujYH30nEeQ+FV
LVJ16aaHw+iiuaqorTM3K5KLohVlN+WlcEtSXHuPNHjw8ddqtzpaX7pH1zqZH+YmfCL5oaNLqDH4
rkBnUl0/Gm/hzSwKjYhXGQFYQ+gGP99OjXakzrAqZzp/Iq4gt+Z5902/JV9thd/isHQImJ0QyK8M
EKM579iPAfXGes2mbiNYHcvDmSPYmW1zlhOE++N1EKeea7j/msnKeyhlC+hGE4Xfn4TVvqgQexCT
rp/wS/MosY6WH1aKFQlFH2hEppA7KXUaQlvG+w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XmWoAt4X8hrCJ5yTyug4ajJW5UhfkLNibzjihWzZ4Cr9hQSvWZoTc8rjGsLPbz6Le+/9iI5KxecS
eR0wiAO+G2IkwhZgVBeZdKoFnlnTVAyLjk9wMAFXNyJZM6b1NDbfXlPcUsC6JePvPlwwdWknkSsC
r3KvgkWAS+O3xvRmaNw=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Hw3Y+rShKrXiUViyNU1/O2qv6TgheLHBnFMj1i9MUGrHYqh9pLfLYUgWR7S2vj4jv4S+Ks0BpP4p
dKEqVAFmTCfQNEUHaVcFPkOHgig6L4mhLY6HUUKJoRgiQepgLi/W3V+ZZPQSQFkB3CU4MsJzhXvR
yLcpDriZy8cnAHD87Zi5DrNGBzj3kigJeM0du6lCQbxtF5aEdoaNP+YTnIFtcqYhoYnswQlYt0sV
HKgFA8VzqzL5WYnpH7+1IKmFkJBHkyqHCa9wPK0qCKnxkuDj70YzPVqQ+cocdKU+/gNdpCOdZlci
F2HTxrgfrXndJru3TiDqu4UavqAe0MNuFp3t0w==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
XPVggoWL6aXz+MpODTOZhEUQDa0vfEnUDaYeEHXm2vGyqKJujN2c/FFAFBeBYdJATLsIsQ+BqoPc
pBbcFYXDBfOtFIW2dH6Y1OoD65KyJ/hAq8coa21kFgq4hFat5vzZ2iIfkCpTUr4vDZO7Xne8cZO9
WsHffoTCt5rS59wWm2b8I5R8Eh2TUbQg3RCyrcnD66cvcEnlXe1CNMQ4/loVJpA4IBinBf820Wjc
vw2fZbGI0jXC+ACSHOviH63Xwmn+aRV5Ppkup7IYoon/ieKapRQeASu3TTY37xSBXiInSdtMTzJ6
+4GfO4eSHVriCk/sWbuTBzfRzoSShrnHjzz5LA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
L78XuiswVcgO2gtebzL7SA9BC/jJGAM0v6S9pzmyqL+QYzRneiYeGyDmsW33jEVVSTuNjTXkBLY7
yTOKQruatwe4V0OLi6174saSAmPgerSV1GyLP7KhmusLV/N61avC9TPam+tekhKeE0tds4EnJ3et
4JdLh+SE4Z4pcuqCjB5MFneIYKKWDx7siU6oesAQtoSJOesfMchX63MhOjOHFP/ch+1gHv3T45hg
IGF7V7TrdREVE4f9631tlVJ1o2Dypsmo/76Itz5WCGlTMjAnWXN8IXxKN+PZ3dyt1wjrZm2P/td+
xiGszFnSLrRvw/HferwtSmRx8q0fiHZ88roGTw==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
kDX5kq2QEe25429T6vQqBCFvV1McKTJRYfK99ymVNK2GGvGLXSzgwJHwB2fj9rM0wme3zYYY0vQR
x+9F4L7KLlOVY6qY3LB59uDzyXBI3mMZaS905HXHJkdZHWtQWpfHhl27LqL+8FSluaD6F+KFfYOV
CwIOVuCIp/XjxFXpNBik7YiPt4kHOlDA97IXNLnYUn/g1csGqeNWce4UTne50ggWvLYGbTFGmTjT
N67TpUiGRVRCSv8Tax72GWFIMFZk3Tlp68ZUSQEybZMWX1U9XdMdtxfvNGhf8mi5jQJ2SupSzKu4
T/+53IN9T8aLePAiGBKKG1ZBj4y1ZyYA7XYvjw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 34560)
`pragma protect data_block
hkoFvScCEnxtbWjddoLDNKv8hxLxVCEFO7+MuJ15uOFexXPshdOLRXd5q+2hLUh9HgzwTRyNyQ3T
jpHAbroCZaP19MmXi9FNvvBl4d/8BUXWKGc407rHUfURL2XkLth/1Hmn7n2H6QzuSMQiS+WWv5Ie
HMeUqOzMlv46MabhQTCllmgcbU1zqSRk8lQE39g8cbj2sRY/usTi/pe8plNDmFDMwLvgQtYDET20
FBZbjLsjcu5UB9EhYs0MD5wNncxKv9I7VL5zoVoAjWIQD32bARfLTtn9apA0TbJazgO/BFzGl0hF
EZELpdX8sVv1IxYhhGdy5FTjobsorLHDZr3wyYhVKFXSedJW8zqjUwCk6Jw5GIXcT6hhv5FsrKaQ
MPOl/jJsPSsRqBcqJ78T3iQWO2IJzgN6e28siqCty1ldkx5juTUXfHZOKrDVPDv2cxMHeO4jH9lG
PXnYgTmYXDvrKWoti5QS08Rid1AWZaaQhMl+2S2C7X7KhFVAtGLRAlfbrUpUmiu82oNPrg42tsup
AqP6QtVYiuvT9OVhyL0uezbN0IUyziAh0eZjw64kJOc+a2DHhY9+L67gs/xyT+DZa+YX95M4KZzl
EhJbSRjmwFq/oXTCpqlHRsz12ZKMqve7AsljwGaLlilfMpSocZV3bGAz02W0yWuegFIbbHgKmQVs
Ie3IUme1Lf2K7dtl1mAzcAofiGdg2q8LSWKXSxAcktpUY/bctm8CZu4zZwWB5igGIb/10E/FdVse
TzUw7IOjQ7SkU8QZdUYIzSdMmhXETVHl/CWVxA+kqiqFhKkGk5Dxo4MXXOgfQe9XZbljQD6Hbm+f
yJ7JTR/YLPzOPT06cBOLfXfrHBlJOWMaXLIUUcJefvDToVL0C9zekrHDsIHTFwPqoPKS3BXyDiEx
xTm5E5Ry1TkyOzNUUWy76cjRjJX4eUQIOzj9vzZ+uyLiNnRkTUFwcNYGTRwuIieNKdskgZUY6CUe
6qrE5lVfkJklVuHqa/eklJiRtuA2Q6C02o4lqOhQ1vG4RWHJcWPxmyu/CAwHY6ED26cthJv2N1sy
Jpsv65b7e+jw/rdxOH7MhY8dI0pOhcFPgYur2BNMKRvopOVBNrDUHaO+w0OOfm4sXZ0fkr26SORs
P3XBo8AACh6E0ScauRhLH8owTGDZqwzPrBnVsQwYbKt2Gp9ViPqXpj6FQ6OAtIbEWfy2kvbs8yLE
KEDKA+RQHbbbcSWoqjrgT/2e7XQtSeO05cym7e4pbp/768OZuE+op2Kf4RVibE3nBRPrl12ZNB/P
S7PeDykrx6W6815pFyw1/0+DZfY7jXZpPAslqmZG5H6KJ5n9rm65wFPjAUt5Ge/EHjWDVIgHxgBm
nRlpn6BGCqo6UVFXdhcXBklYPRlVGlVWFZMaPbGAYW+hOb42y6hmwoQmfanHXvNMBjXbdfP157Rs
ESeKCQe+oNjXqdfL3kBn7gj3O2rE5GWxQ7IHSCXAZxANxsAg9lsGJE3FXpIxtlNNp6gKPmDg+rOg
ewfUCpIDR+0YU163urYYzgQLU9xRZsGex/fDW8wyRPLvuMn3M9Ih4xUk7YSFYAxg/n87S0WzmcEK
XjP/F3FFqxpDt2DwQiTqUS1rMsnQGhlaZWFniatlXDq7jrLy1y7Z+P3QfOa6fsANQF5YFjoWtCJz
p6KFxpEne9K2J2K9zxYfMJ2bdaFWnTncLjRfZdaGKdEB3GafTrFYN4YdR2HMF0KID/NSZPpJUVk1
5VB/MabfnQo9gIWuk+A1FCvYs/SZBnr1Ipc0w1nFVE4qxcKK6pqqUaPJZsnfdYHuGm5IsKxUSTl0
g2s63l/v/HhDVUkdYGfyieoZ+9ZmHzBgif5CTBoBqD8TGE5K/2LnNEDA0CiuaWm66seMqwDosESA
rEu/lyhVG18RxPi7saIWcizRFWgyyBysNZQTyVSPUYhIbg4GUfVk4y8u+MqKCT2Y52bgpFGeAC0Y
UETk4VzmMWLQfo7IgnO3BBgF6k6xMRjk2kdoMICDOmwB0VXTg257M0s7QkvKyN3fjcT7Flx0COjh
83XnlkuhDtIndjKq5uGV15mzqctoEvtYj5JNd271PIBgtnsvHgKPykm+EcfsphI3w9LM6TPdToje
hgXxI/laCasl1I8yapgQ1qze8Hwc+ObJKxpIVlncCXkH81Rk+2To1eqmOT/UNvq2Aa4Wwqk166Yb
9FgUq47WVOO1uap8dVOPtARRiuhYF/s/+WXf+xt10MBqqrmitfrdswAQMBQMsZ3tzvXlnPpW9/AY
Ne+TxcB5MN7oRROJ28SEJZ+sK47jBgnIlVwUYmNpV7JB3yc80Z2Ufr8yVM0X7B/flt3Ji82zGiZV
7XKpbF9LvbSiJ4fQuJl7xKuHosC8NiTFqOI6zsaim9GHYYrIklKJUcceYfNDox9SUELU7gTKDiNQ
IRmPoIz5MLTi4oPn1gyc05+wnMS9RA/zRhopO65qFQTrtl83oSy1l1MyWQPBWauWBS3AvVOnIz/3
8zbX6bljpJvET59f2wgRKsvOV6Rrrk0Dq/l+ww7JIbmJcnxCGJTuK35u6k8d+wf0+APnx98o42am
9uEnL+p628zV52sfSCvr5Ssg6wR12JG5S8NvlwiNf7sD+/HzuZpy5Z7WA99/wbvDuKRiOt5iQHYe
XIiFCvi5o3iFG7qdgR7ZcdFCdRHrLkzZaVvLmW530h8FzBFTBSp8dJLJpbGj92hYCzign8GAlpAE
5qqT1yccXuf0bhUJKnR1BuKhP1XWdC/UGXCtD8DHjKLkG78wttBG924DBcxfH9w963l6K7oW7hAJ
n7AjdKRuB58FEitkl1RkYldX62Fqfnq5t62bbMddC7epK2ZKB88inkHEgtw5/w2Zz67XXPZN0BBB
naPWxPq5Iw52umYcO/rlkmRkZZHJ0eVPM1Ho/bg+DM773eq6pa0iQ1IE+woN/O5VWk++9kFKvEFf
q8kXyMWbrUkcX9U6q1PRm7AEt1QzIU9kpugdLTeFCqAazrn/E71wIWqW4F+52Av8L5nX2IUyyZnO
fAmF1j7wkVzjQFv+5QXa5D8CAAqKH46cEJRuaLJwXXTsUgbpRl9XVXEohBr5h/pRmqlhODvIHOxE
/Pha/hZkZfyN4e7yu+7aAnhtUfmpMF7eM1aVf1ND7+4QND+65D+ubCJk7is+AeZF+S0Q6AzRMbpK
fJ8j3KUexibtrj2aG5g7F4+UiF2ILYKqR6ydbhP/r8SrKZPMe0JF2KEO5m5Je38pR0rfqV6od8QF
rEk0doCtQ58BOLYo2xDXs8ZGF9mFn3xWtr3s9bfirm4Mr7EstrNMlgAyU4oXwQLJ7/9okh8ek+Ps
38f88YgvGhnhqq0xytlTIKxkjUoPQKsTlJuLMI6rMcUVgDEsVtUZjmICvXcNkrOHXe/FJSpX2wXP
5kKhItlUGUggr7zbphr7L5Mvp3O5eZHLfPl7BnebmH1z6jn5Yiw93jMZDEbjutjk29cenjnNtQtu
G1vDGMR+9/Ldbv5rcGhYpPYy7R7mJCjOHpFxJWFYTukNyI9gy+/MnBJVbUd19FtRACB+fnexxSyL
Bcv/P+X1xRZ7D+7cr+IicybZbz/GmXLuGatf1SX6aB4516NDGrfGshuuYVOHwEHy1KXghK6FPx2g
X6yTdKixGzdKiOpvi35fd39VN3aWZjyLPQGgIDoZ7OTOmpX8WmPSbNC/dCChS137k7Nq7ztQvouG
S5J7k9goFbqY9I+pV1y54/3QOwshxBZojzwGSHvWNl6tQ+SFpmvKAyMGBJK41xZxIECDgmIq0Y5/
Yctvq8n6ORuHZYmZHkv7gJrSWNALB/3Gd2wyQT08i7Cadqpg2TGyBWZkBMaPVNuW5cSDocAa/dGB
Z4HSnNLPaDE3s6mZhy1YzWqz9DOG+mxSqSYEn5enpdAPHNB5ciEC1/xjO7gmR9gKYaiwr/rymUBh
gGso6T90ojQaKsiWQ/LFdvxQgXxeZM4iViGipEWncdeJ1+4e4rdkKJ2KZlHqZ2XUFNZloQ8Q4Zqb
GikuvBypLlEnAiEmSFS/TdC7kT1wB9+u7NggAXX4s9610DkkVgpTYmvm9l2JGK8x41NcPmVQZ0XH
NZpQl4At7i4xpteldnchan6RHn1NZfsKo1Jh/+rQFZj+djzCje1oKmabc/5yKiflziGJRZq+eFL5
OzkJKq0SZYpb7M2T75y0Iz639U34mK4FO9qqgqoPp73jaqEQRooInUwieq8ItaMx6XK4JeaR85nQ
6SYsDWG972wuSh66QOn/M5fYNAKnR/x6CUWZsWnX6JXLvgYxgzKB7CudSeoOdhdEgEWxLX0YpeJS
l9wJx3ArXOv9GHKbQPeJCXu9wAzr796uPnAkZOzAZ0F5qrAwXFpgL8vn9U/7E+dBX3Q7DtcBNj3f
4K0s6IY7bBqNNdF5keEVyNKhQuZxWyPzQ7zQcm0jb61u2CM0jjVycF3Nzmdgz3yofhei40++WugR
Me4i+Ao+dTabu391XOI6KsZzwFYlbi/vaLghdCGGNeAKRFwkCE1xxCh926s6Ej0HTm+3oSCkH0AD
42/h57ttZ75scAskZCkoP/w+gLVc4JbaWTK9NMU9Ue69lQn4c4FRxk73OTyQqOO72h4ahBt34ADk
aDt3Zmvf1/KuLQifgB9AwmFvUbb2eQrmbhrXOUvoneurKCW4ykejVbbDFovEKgg2lYnqpT27dzRJ
wyFRFjPqYuQY7IbT4s/awbSIaonN4tChzM7a5mS29+yuoGB2LiaoeYnMuYiXXYPNZWU0blHOLn3H
r9YdFIubYlRYvsHDSfECuV282EKU0UmG/qC6Wk5uCCle5izVYWtOpD5g1QKJGrzTZ1dPrP6zvmPf
mOsAI4X4IyCHBcAvj8Gs6t2frghnpS/ZsM8aztdqQy4NmIi3V2fvjlHgg6JRgTSkmfS4unDXl1q9
qAd8Hol1OhdRBl/Q3zAHKKKlrAtazWmaCZDwVXyw/pts9xAqUEKNfXUBM6uN3XGQsF7+MIznkvUN
5yHxfsLRYArRf6ErXL2gi4gYvdefQH4flv/tINY5K/iEy8ZwEp1JTcrBeK5iYfkRp66wCLWWzKfU
lIJ0c+QDlF9jjUyjJS7iQkSiyIdnq10uiySiB/tkO+ud+Yyz+QC/zsVm4SZRAGaLEFbyFoMxmmTf
M0rr1lqc28uqlbY/5y3ql4F4t3WfbyFXwjzaQE88psq4pcqPYfzp8voYsDuuJsPfupIzR2V8AMD+
e4de/RPuMjKpU4qpaMqN64PxahqxrzT8iFRymvM5H2bM0G5nNVUYBYiZ6Xip8tYyY3CnL0bTq0l9
6hqreAuB/qIGqoPrDK+30KP0MvZqutyzXXyHtVLKZR6cAtNbXzMnV47i39eWNZWDBj/drwe4Ft/Y
4dcoUHKBEfHEkmeI9OCxuaZnQKGkIFdZUq6ini8+H+AiZqBxd1B8Fkn3gYx8y9oxJeJrVvHDr3Xj
azASnV0lGzB9niuUCsgZWa0Z4VQakVNcXQ1LyQv0OXL/74JVQy5Mek2cJ0l11FUsRoxgFotvjNa+
pXpwx4k7VZMmp85nlXQpViGugqOU+BihjCeRMkLUZFDw+4yQ4nZobmzXXHi8wVkn4Vzg4MKWiNvw
/PXNsqrVullpnO9dMjhMIPFahpRT1DOEyDkizk2kQ6foQNtwPNThx+6ya+2c51ICvvu1ImnhzF23
d7gbshmGbAtua3u/7K9vt386c3V3lpQf8QM7xMCwc1msNaN0F7K4B0dGKcCN9dteU5b3mwW8i0Ei
BhJ6qWLURQ1jkwx6mlv8ZHlSb4j4DPXY9x5JX+zFDndnA4nV6GnWCQRsQuB2cAjfUtFxjNTqRVPh
N7HV5NTUGI8ZuxnqxA+e4Y4dWEn9tQQj3S9r6R7vJtz/nOtreYOZ74V3toozveMpTIisqJDnNcuS
H+Bat4vkyqPzzA0jRvC4kS4JlrzQTYSJkYNWnh+x+XoHITikM0SSDFB4YkDDcg8E+1pFls/NHpaj
MjxPguUADjTEQSDDDWo/aVBR72+N5dYV/2XKliAy1a5CNnLwY7YjvN9qfoTmXj+oukEPsNZd3Ifj
ErSucx5B793TV/KF1w2dPUQeinbd4K/DJZnDyEXLlOjhmKq4QBhTAjuDqeLHI9y7RWlD43nHmHdb
7HctcnoB9N9J+ZQjlZHCpD1iE7QvyyNMHDObcIGDGDPP52zgCFLMaVt73Ae2GibCkDeYa6/W7TN4
o5M6EKsNkKd9LTwSNnGJwTkceHRbxvitY1xW9tYhIsfPtWw5y5fkx2szPizW/utwVoKpc91yUZkZ
nwOP0EdeCh2QffyYGUYUmRMrBqKi0dCJqJixmd53jLiCb1Fj8ZDtOxc1iWkVGjO2/kb8LkLX2I6F
QAD5pueVz2Z/CBuSakktJEDDfCNs/7e9JJMYDQnI/77EttL53XoqJbdOzDcTqW3uOywjCKED6NFu
hapsown5gftNubT704MAG5zgRWjrWMD1lJlA7WJevms0f4vqNg2NhQuDZJbNEne7diC9Erf3AGJq
bq61opmvVasxJueQp7ysLnFSFYqZ9XxiTtqZe3RYOjQCs5GRLF0HORPe5PUAHHXLGdnwxEVWdQ6T
BvNWAsOl1Yo+MCiu2MgpdOIXcIVP+nU7hqdijQ20GjKLPisQtb4iV+f7LwwHd9lEkYGBc/GU1cbM
Q40IvOnaMxeNAt1mdVx7b4JH2bsIedPJtJ3+c/XzisFbCnn9f2J5P31wMHOp7468eohbVj6EEgM4
+VXvWgl7bW9g20yv7gdwS8iids1tRspRqjIKy4X7krcVZpRFg6I7VLRh/OcQP++5ge7j4iVM3Zma
PvQh2R+d52atOZ77265rVeVGqcPIVv1dmezzPR35jMU7CLhNavzPImS1aSXXbHYlJMN1ehJmxtDz
0IYQSUNBRxwIAcRzIl3X/D5qsRh3S3NHbcBCVuqIiK53tB5sD21WbCantOe8g2XhkZPsXUkT80Ek
4oQ9oqtqsoqG5+nm3Wj2ZE584QLRACcjhaM7KpA4aB4H4idlrdALNIlMhgH/XhtG3I/BLb3vGJtf
fAHcGa4pYypmrpQjMkAWQ1T2MIxnPGb0u+tnQkRRKyEPv1WcxsIe5spbSh8wTfJ4PFDBB55Dy34D
7MnY8tEJwhECQUt2w1R1RWGMxdAa5uKugGcWCrzDJhVDKQIq0icxQneJye1aKGmPgvnruOr+hhY+
oEwyQ5rT3tcW3nHJYrV1clvg/BnFJyQmBnNO3F/fpYUQTEZK/ky4wbOvPaeFke5V4v0LOdVpKKEz
/ZdZa16yulUzb5ziReicRZA3mW5wBito/K6+Ux4RTsjeIcFHbg5PwLpSbFp/5tzqMBAjWsCnnQBC
xOSS1VVt69iMAOVek9+SyrQDeY9G5BLN9dYQfwkvKCMbYGtzQnb5TDKzZd5m+ABXRRdgh875wlQT
8Z7eK1EehxH3tLoKALgMTaQOD+TP7t4/nvfPxI2tTFEJVGsoHumRHwIGz3IuKwCzY75fIDvZHtLj
/Iw910o8/Z3mV9GWuaoVjya1HhltJm5W8jOXbo9H4qFuN+ByF/wQvM6OEeDiuaGgOBns3FH3jGmP
kSqQKEYMT3VeyWg49OJOY7z5uwvglN9r94b/l6uMK9vexe+XsWlVkZroAZ0MqpMlekoC5Jxz48OD
dXtP2DJYVGdU2TNDWwrJeWtyL5kZpOHNXkiYIxjfutNLhE8/vQFfExpd1vFi6qst0bgbZJSOXZa7
+3CSoKDOb10QSfApUJUqPlSkWpOUKQwW70xLfN3P41p4Cw/76/5Ur38CDaSjqFzr9YvMuck3FsSd
fiuC2mLsYv40rJUmNFU5nfua5nzXOSa01tiakH3Z+8t2fgTrbQg9aenkuVxMRt5dzRQyd0Sqnng5
nz/jxJkzkEfbwyQyyFyBXYxgymR6jFpuyrBtGF6a/5WOWV60vne+niHTbE3W6cGswzcJXrHmcsRP
JmemOK3kkigkM6eiqaSKxSUetFcMEaMvy0I7sfIj/G9iWeBF74BfsVYN8kLHZ5rBCpghvHupP6vW
DoY1D0LXLE9pDMutfjiOfdsPIPBDCCdw+6EE800ABzS/x4d2qonq91mvJOdHyWv1dz+utPS4qDXL
7ZixMaSU2nms/TPbhdiFu4kvweNkfhfWx3QfjRkLiUcxVdfQZRa/OWO7kclo4Ss3ebXEDbMiyGhB
U35LxZH3PFvl2Iyt0kBudOLhQaRIGGea2s0ftynmL8WL56kYilEts9jSSHWUg1PULk9pKAbLKO3Z
pnzVSiV2kO6bi0Jrb+k/MSuGBhX0IFRx77yAateIbKIjMG8XZM9AngTAYjjseG+T/qWax3nINiY2
V0IA8YFd11UhJURbLvVAp6L1/DMYJemJ+74weTjDWN2oQaMc6IV+YcCUaJ1j1CgCOqe9pkt9MweU
x0bV/yQxGXK9Yuxz8AXLks0jdvAM8MfMcYk4MV4uSExjrZiDOfwcuDy50NzaEnaV+J3DUL1uK4s6
9PElLU1GBj5BCvQmhWXGZhq0apzVi8c1sLQ4LhdEZ+dQq1KK2T4aB2/eUPT3lKuNlZWmkLZEmcxD
Wc99r2+HTJtYeEQ/wBbcL0hMLBX1uYt9CpBdLYe8asoeA81+BWhEwkW0VT6SQZfOEpzQ0+pXizbu
YVJjByD5z7/pAD+CGuw9hPfyBXjpfD90uE8nuwDPRpPI+thaKPv/A1vpp/Y9TkV+cH9A4SosHpOP
/DyEInyYByGFJN0U7I6RYsF1mPnj4T0A+YSPE82jdURVp7HCqpx6Mj27txaxGAQgO2yriFUXUNHY
Ly32No6JHmiYNfzPchTkhdRWf+2I57CDWBAuVtID7IRI8GH/HztZhELIE56a7roCHUVze1RAjgxn
gx03twSvl6wsw8ku0c/s2109gKwkEhlw3qwUKqSBIp6WTOu+UGQHpz29IcC5h+tX4gUsc2owOiAa
ZR3CzubccrEXe/kVnJXuzNZBLZ6AlpBWJOude61mRTXReckPuMxGGZTotojEZ8riKYkj9OVKIyst
JyE5ilzzMpRo8Fo9NDwDFmYLR62TWb1WJL+LPOD3u951VmOMspOb8Qb6qfryJeLNKh9iK9Kr0jk5
az3D94OAGGcaLMAWq6o9tzSqRPuYrXc5tdyxo3tT5cIr6223XKp0pwQ5sNmk4uq1zrskljpQwxPJ
dpaZDe/9ei9rSgQa/yR4YalHpjMWlJ5pSfHqmlQv2UBj/+p4Pv2mj0BCGPMBZJbO/1UBS1+uPAn0
s9KAUE4VJ0IYek4pcb+Gj4PeKua/D+VZR4bDKJHEVkYPea9gbl9W7rfaiivImRT171YM13GII89Q
jJZW6qA+xUTWeHG5lYKbntAEKW0Qx74fOYwDLuXemKo2l+7X4UmAS57CNXPqHK60OQdPLluMGTls
g0MEScxKLnl/hSIa/ypP/FsiX6w4s69qXUzUUDt+YatCz1vtoVNxpZm8xuw/ZUrgT9nBUrRtO2p/
9T46RfMw0E1w2IBwgCnTBGpVp99kQWRbI8l4I0zNEy2Gu6Q2DzbAFMjRTehjzaJTj1LIWAHf3oGI
gAC2c8UyIxvaAF8j4vJLv1REqqB+9tUYv7uiK7/1TUPHit2ecCQMcEVuBkRAjf2oPREoyE3HTNvn
XGy/C0AOf8yi91AZ/laT5IfKwVMmQXN43ht1PhPSIwz4FEoFvxfYsLtz17b3+3rF/PTqcNUrudrw
QcghOKWr+aD1gXFkwkaJKq3SI51FZw59YlpFZXVwcEQVJShZaMO5uLD/gE+2Hr8idG6fq7Wb6Ril
FuMf2tw6Lwv6Em5kHljU4cS/cxR3uv7BiRVNWWh852uTKcnepB83F0is2s58PCIYw1+mcd00MKQB
2P+AMdk/B3k13/hsHxN+mZY1Adp29ZN8lgC7QNUIFPRHS2c2lYeXQhu9sBLpbIF62xL6KLoF88cz
U1UUhM7hre0hR+T8sZt8acevMlQLbCP0dFoWJzeiSOSLXObWtmqAMwEb1tK3Fm9ie0InYsfbdLd+
BC1JaIG7PBIVjm9l3/hZ2dPxJed9K78dXXITdIoLy9xeawtqXJO4e4N71mD4yzMo/E3xXJ9iDAMX
gN+sV5lVFZd9Ge3RUCE20NPJzahaPuPGTG0UR9GHU/X8dHLGEg0ROixeZ+8rdreC2tHkPCtEOAqI
s31vT09v+Nw8qlIv4M171NVCqbdmFu1MGnBexexuCbSlpdX9nkZUT88x2iYBroPB0ViaN0//F3iz
FAh5aUnv75+TOIvBNXoKzbyGg3FSvXbItyrnuq+7OG8bPpPV0EchZBoD55jmdqI4pwDwqiaG7kef
rXqFMMuJ0fZHRXRhn3HWgOir/HVnkY4blwLA2naTjO7z92lHox2MIRrCw0A/p5ZHW9unb073zCjs
D1AHyBwWaCe1q4ezpdJkRjcB0yR82Bh7kmH3ha/EVp9tYHpo1UPWnM3uTpYZfOHyTWQ3a46LsTG+
enHAza7mAVdKbyblgEORP3kFpVco8Wf9aWrOF+VK6shnditmEtpScWe2d66k7fTtg7gZl2F3BAy5
dXyX5gcoNteWupKuqjWVN3T0bcoz7pYEFRLAPtvUf+tFFyFaXebcrmx0TYwwCGgm7W0FJUielGKz
wQtc+AbS8rlzZdqzOXrdQ13o3s5lK8qnmtZJm/WwQgANYIIyxb3hhiELfLGkqmj40y4k804kF96i
UGtzIyQkT5kyau8joG7qBfV4TYKxgSnseu2687alr5W/+sSwXksbPN8zIMRY+M5jZMkM9tpPHtYV
Pc5/kpW391TZdBXhjkL5rKpU3lJ+fdQ0hDrJH7jlgs853iO2Q5YSJgVq36qAIbZwzMpNr/XoUWG8
eNkISaqT3wRLgz4/ENwnv1OwqWewo+ZIXw2aE0v/WbWFiJMr2gCPRo04iFYgV8Y5aF8Sv30DvU1o
dBDiO1vxKn5o+oTlX3A0Sxi7VSaugKWeSZDhuFGwxxAhxM/7/I0mc3hHBaPjMBkejRMevvU0M/Ad
etS7V5hIH6OdEQtROMLskdoSuOVSgUyg0C/F+H/ZQ+T+5D91mvwafXQGpjyHrsz7jt0gJrjjhWES
vQsOdRV/JTUlsGM7B8qcSfli1pL7Y6cZ1u5LLQGuFINbheug7yWi0GhR5pxW7PEFvCI4F9Z3xrA4
2vf568NR4tBjVa4Fp4iDOm/Wy1nwvJMzwAsLkfKnuR1mNa6REFx5O7XDLCL5wUgJjZ8wRUBUDIum
gHtt1F/hXzzpqgQMyN9f+AQ3tbT9GIsT4Jqnh67QHdVPZGrq2koUeyg8wXmo9H0YE2IHSBsASwQn
OuaiEr1T2uEufneHIwsuHnN5eG6iNH2NTPUdD/VsdFhuXat4sjuukg4n87/pfgVNqsHYs+afB5Ao
Iewd7yIJu39HYmx1vDOJU0eHSWivkLQH6h0/n5QHYvf8O0JH4IUr19Xf6fmPAXoOQf7BBLOm8Jwi
Kc29OhASc5wrhv+d0tJlxJkLOdxP58lh0h6LaKvFNTf1brzWu3gwN4dqWYw/Clof6tTz3oDzBWDn
M0x1jokf+ATS78fiyO73YB213WS1Spmt/JKgCKNcTqhB0u7eYGDz7UpcbARqiGn+kto0OpGkVo4Y
7FhocSYqWwOLRzZRflrQtWVO5LI5ML2O5CktR1C/AhOgWR+UeWpRnlZTCo+vNdI/nnSt/e46StKd
dGbHYmbtmIj5EIuOhzDPUIApdFTXUf3HvDs75yO6IrIK6cqqCAxRQTs5Fn/nAEXnK2V93o2HVmw/
0T6oJufxV9IRzEuh0RQcQAOyjMUEsjnpCh8bQqXLYP/SqOdQ/LxC2f/xj76s78UMLIApeU8xowu/
iVt5x0wUBZzK/miIRrGId9TVRjC2ZcmVaouHpRl1IvFiPKC8Jqh+GcR0vkdsBeT1bRciVIEMVy3h
pTjL7gbmAdJcRDgdp7qS261eZpNaoe8T0kG3nq69IoPbWun2xlaLvRr/l/wFsIOv0W6QbBM5BLJ+
U/XJoBp5FEDbYcyL4I2o2+yglo3ZPmeQyH1MIjF/DD1w3pPZZNqyPaiS6+aQ55ZRASn9pnswxLTK
ctM3jjQvjrtKKgIsbhONke9JDkVabnF4RF10WKOSbfQyVpFokd0MIt+hW17u+pmkk77xSLfXqSwq
Q9aQcF0W+WHCwF+uYEZ4jWLO/0NGXCv4E6HLu0MLUjvSWMnuFhBwKZqnYAsH8Tg0cX4DfZI0uJw7
d7oeRBMC76BUWNI8O3S8R1H81Q8u7qUbUp9dGPVxcFj6FZUwfRI114+9hFlWkZmIR3d5WtiMjZg/
w+fgUuuVyFUR1NGZ8B3DZRLpARxyL6mw5Q4n9kaDgowxnQNsw/Zyh6QpKkKFaYGbwA4+TSmwtWZV
dH1UqNfpJSINBG0FWGJbW68+NV29aPYLH2lDUkKSdVnx0PlXjU8vEzXkkQ3hO6dU9cC/O+Hlw0O4
ftRDDl4MYHxFHy71sgt4MX2tTz7M/+YGlTek1pn/+AcLizv9mKFq02iVwyc76ffaZZ94aXI6F1Ru
4imlfZzoeAAWm/j1KMte8b1+qV2xX2vidkGG4zl8tI+lunscUu3xwz2mIXqRS9AECJfO+creUz7C
QHp8sWa6fIEK+QOySF+KiTwvXcNrXaW1ViribGKqrxTuyXdNP/ebxIZ0lU9B4FxB/ckQ+SMqh8Ab
67kMPJ2HEwEpg22Rq5FZbkP1mk0slJzXmtdW5NK96I/OqrJFlZVyEY6KDVoHPRxyole+HNH2acaW
dLYy8ypxY9WGkc0fmmz1kncn0lYxQqD+iZZJPRisSwibagOirw2MNcVt4Zwhd5gSXJRzQCAFKMRl
p+bQHs2qQLRqHcGR34YDfDFjauQZJND/1IT5ruTH2P4L322UBJgZHFTq2hyubrb3abjJQPANs/Tw
Q87fnZM0k7EOzEvR+WaFlrIoJXrw7uOOp9BkFZe7MDQKvBHjB8VPbzIl9ChXJH+VcnzLADd8+VeT
gEPDqqbQHFIA+KFHwYx0+XcIs2+ARj0CqgqMu6wZ+m+2b91XyX/qnjc+L+sfJJGq/b14koW9oqtz
yLiE0lLRYVmPV6iZoz/Af4Y94Es2B/JeTja7e6WU8WyiHq7kyZXHTwOW77XOmiZHf4sWT8ypvdq7
sKOQXF1iRd3KiI8grNe86aQ9Kx6UEgWeXWAPUb3sgVNN8AWxwp+lM0K4cvOXul2h6UUI8B+GZohD
anDTiO9Q8vAFUosc+V5snhuVAe9zxRg0xMtAFU2sZfUXrSpaZFrrXB09Sx7T7VuM6Iga9ig4k4f5
IPbWT+0uyAb4OwuMSnWSzM8z9LwVKeOQSIYpyOUDsl67KBXqZPUNG/0Jlk7ElWijCwa6p+jQj612
uVUGA0FybkGUBamFQrqSHev8vmMxhAxuaLEjUKqH15CNceNPfAZG1GFNiO2XH0HydKDs17+5900k
krAa20p4DGaf2yED1pR1WAtNzXwzY8iccNdUYZkiAtlvIaMss1Y0WThw0W6KDSvmr8eGfhxVROQt
AOAC8nTBYW8g8Vtk5ys2dWKZPxU9sNRaUYeqF/ovo3UzmIUuo6HIX7SABAFXFaITnVZ/qmOPGfDb
anTwyW62LXlGwJP9JZ/mWUubXiVe35Pi0E0LOvIAh8q08ylK/5bEcqoisFu+hi6juophQN54rwmy
wyISqxfCjFk/HjS5C2JOCiJNV6TAyErgWA/tMndwxVscz2wOPDEcFeuiosKQVhOcduvjsqd7pVcj
PGJJHzCROg6a/3mCVY2NznpPifhB4kEMjOPMyIClN8+FehHrOR0TXACT8dAHsrPXEWEcB6vpgnTC
9VU3G809Qc7OXOjNG7kTLYkSR5XyDejbUOdCk79L+UbhjIu3CQpZdKun0iK4iUsoasvtICYNc/93
rrFf0SBIQULtpevG66H8yI7W8WCa5F5dq9uyjwBdE87zHJbbEA+YXVBG/wz+7kEFdq24xX6vVaqh
LITfchhzS/rJkvVT3Lv1MBzUB754wvL28u/+9dl0apOiivy9yBJO3aTXygt7Atenu5YgzSocPZh7
/SU8Ho3lT4PJiMX6KXrIxiDemBe69ZsAyIvrUKK+rrF5eku/6VTka0RYJTWAbJ3fa+p3UrJ6f8zm
25pE4f0FGMzQfj1D+cN6DN/HvPnkiVuR5D9FuvGlACK/VmiBhdMJKVs21zCdH1Qvv0tJh7J/fwFh
N4bZRHEYrn8BbB8kHR2ctqEQOXH/LqHVnIReCUbwBLmSPqBP4lYAo18+cd17/0yjPSWnRSGNubMe
zvdXdWxlKdGo7keMRiWywB6C5XeLOHFnOjVMdDfDyDQlcMPdRN9mHNPRKiAQFgAhSC/KeTXpNp7B
m+aBMZ8YlivjG/gj+eICaPfsxmt5/ecsLwrM2BktdXykdSHC3p+fEcv14k3Q40lZ+cVG0KxrcKuo
5e+oyAAbJD2XxalDd6vrb3zx0NkzRwDxAqdHXRlesf+1LAZkeH8vorGhBwosT1nZMy+l7nqPt2rx
3ezVC2CeYBZmodl3tBwaTPQSDGFd7+USEhCEFADMHYukywvDqqryBmtgKEVXSd72gEOVtARYkMll
m7+Uf8oK00Czhqdz4ckRzaNtSR9ydZAdWcWmGdYcniRoYmNhuZID65C8o2Ej54b6vtSH6yVnuCwg
LzjuF8ee8og/WS5UIUYPriuye052mP8Fiac///gnzrGRM0cUsRClkcmjSB9lUprDtDx7yk+4bTDp
VKTRMoKyipQH8UlpccraaNFA2TuaSYRYERI4DGWAhtDHGavz+xHPwduLOQDXy5LZn2tr2v9MHovT
Wl7XZ33e+Uk7DxY3w7s94GwFOEDezqeoCH6xkRN38FjM+GT/UyPLrUayjhQ+qr56ntfLUGhEppwd
U7HRvkKxaxCMHPx+TOZzudfL4H6C6XOSBgfCkxTkGbkwspo9dpkO9geAARnYdL+pVvaoEz30EWh7
B8wfyZ47Buq86EQe7Ra/oK+oNCEk4jG3DftyqovgejLgwu++wlSXRBK0qZsmeLMnVAm4WphGmHEO
5vvRwpKpuwOL9k6fJ076pKk3P0ityXHqhTZXpX0z22aUyJR1httHfhm3zRKlgfyBRDG72p5fYsD1
8hAsN8aJa8aSdfhtPAsceS6SHrf+Asfbf0h7vcs+98FmgcsN1Y/Q6uWGirGUf8fb8UGb6/ynY8oQ
ok194aniOIy74GOIgxLtEtk4WxO8khsSrdCgyCqgOajOH/IMKa2ksZSAyBXU000D7ijtkhWvAtP7
c5NWaEwaF9+rgLOQIQ2xSG92sRErAz93MXCQzQj2SMkQy+Sm7WmPH7yO+EcquNWbeyfvd2azGgop
akW1JNDcLXL/ABbfAhj4rI31AMKfDUOX40zQfiZJZbi56RqVZxXzGffGavpuJd4immlEYezmMcou
xR1yeLLEXRikPqDNUPbD/luL9NPtHZHKiJgqu/U4fJnSRA6ccOrhm8NdM5FQpETgzVVc5G0bbScx
c0e/somjm8nNN/avYshvcKrrREBp7J7cSblBpZc3ws9SGselMsgn95PUkz3ImoQzLV1g3k8qSWU8
yc8jduSZE5Nxx3vN3wYnaE81THOLuOtf2oXAaF+l8aDfN5Fv+SIEICTWxbSzQwZtF7CTWhjujXpW
YHc/dYpYgPZz6U2M8XKliyU+VjQraqDy7BKuBU5QCp5uNipn9FghVp78HmlGuJNqYjQLYHo8bS/R
m/MLTN/kUPEjOhwpSM0DXkUgp9OQ9narlWjUNCdw2T6awqisVY7OruEbt+nmYdeTdnF1pURtaPKi
Z32vAj/DqZ4o6uJ7GDXP/7Bq41gDwZk6Giayt107uTZGm7POd2pbSIRdz1H8kSaCWlmtZG46WIop
wWGsbh7f9qQynhWTzLO4RLvUuoTKaNc9NnVs/dw+2Y11I/eV2IDuRMUt1ID0qa4GxDJnfG9gS1qm
EO1b5wzPBAVhAJb+GbGX5CVXip9NJUfxHaAup3udriHRlKO35Gttfap/Iu0qQ1v5x+GrkKlbzHX1
+EDyWjmfObICpu3oM0grchmFsfz9gEdm+RE/CAd16dL493kVi7OZSQCYKUdshVg3/NeMCtMnnomi
iycIjM0XwEgJUEDy0mCMYm4dbOL3+Km/wbSXDfOm7pUx4vMLLO1PAHrg7HVVt/QKFfUAF0x39kKn
XneWHoKbTrhofNwgbTQGRQZe6jlPh4O2KlpvUNwhhhOcZX24gPfKawUFxj3+6+BmsOmUIu43bdPV
zx+2HCw6+HxJYO7/UJzuNIHvNH13WW6QRj5igCz65r01Z8VUW9jpHLJmTZsTenjU0ZsVe/CXq2nP
73YZ7GxMyMqZOkxQBDegbnSBT0dRb5reE1lkxMuNYPedU2hH3teBOKaU+dJNOO6wmWpzBfXSTXwc
ftMV9Ce2IQ/FomHE1UWGFCQk2w7pmaDtKZ/8TRVNzCFeyrN/o+o+IHPjjXHu56N+z8MUFckdlkW0
DC78XnQ7GZaGapYohYkBX4/CfoN/8EHnDbzTfFLBlPl2LUmOIsV2KMw4yMq0BVWIB9Z3nnc0IT3o
+W/y81qejqKW0z+8x+MJKbtAbiFUwGsU5FW2mYlvqGC8pLlJw+xl4/64MDitMd3oKaRCo3sFnyAv
bWYTn83ybWfyF8WHDMVhBRsYtHJtqmGIx95lcJfH80X8mpXyvMbznP29c0KM/PAkZ8IDBiiPFeUr
MHuJ4f69oxpvq32yjF8nXUIVn11+wkudeQ7pag9IYDB1VXJ1zec9lg+XLxBmMWFsMiHKxuA1OD5L
0udk9aIf7IHJUMl47wkHmXLH3j6qPau8obYyeiYa2EfT5RV40aghdIVl9dzYEwjhRb/E8SRq9fpc
2/QfzET67bPn1govFkTvFoM5fa+gFqGjJcE7WD61wpiqgnJOMRYy0quttxwZT+zRy7/hXMpqCgDO
MVyLwB4YHd3l3flCAQebeEDuiUIoQn/5jO8uBb1ZeEXopZiI2Dj1e8023g4mn96ijIkZyVrnGNc8
UWq6fKMiAt2UTHImNEe1r6WHqqpl88DyseNEORnPPgyhEtnQM6ryon2pvCnnfGOP333TSMvp1aZk
WXn2tmIKMAhrCmaqO9LJRnUINzJ1jphxQIufpN62NZJ2YIfwmUEbTLwPIuFtQhDkGZ8sK43Qdu5A
39yRLmMd87ZZV9QV08itkAanY6loBsbe0SEvV+9wTLYZaM+srqgqQfxV7YxVTqdRuYpUcl2dOhSt
UkiqdROuUCFVxtn5K6Xz/r/H2EF+S4KthzvUSVHiWf6CfjZDa0Q9QlJu9ECBWYY6ZACmncmsZCTu
6nUv4DJrVKHKEgVp8QU/INUD1a9FZKqN9KyyB6T93Um5Wesgm6H0RbKy5VW/z9gvXFlWMOuaK4nU
8kT9aZM0aW4e/K2I8DX8Hkau7iDCiVO95tz7IF4X75TGXUq0E63FavICTbplHu6E2Dl03J+wM/Ge
uGFYHmiNu42aL2POoC0uzjEwQEJkJI3bNYC035uh5V7MB1cfePtFaZU0bjjqHgKjiXV1QlVfhYlW
TfMSJE4uA5TzZX/x+5KLk0bcnVpc+metcyhBeq3uwZ6CmazxEVmVHcZwZrPoUSTATNCi45mO47DY
2EoW743H3sDc0fiLCW6IlaCN15se68qDor4K9SiDeoKB/v9pDL26yc0Ovbb0zhch0x4hjbP8lsCj
+npgPJVgzevJNaS8np5hRGZnaus6d4ar5KbQu0zOUUh5dk5Fw646XiLHNRKhOif1BWpHxw/4Tb00
lW+KFnvnmu9l5VPa+awvi3+Mna7aZP2v9s5Bi0BGPneUQ+PY+twDKAe1BjfCGZT+Ees2fDZzuvG0
7nA2TjHSATDfu3c7xQjEQ4+r1nZXIQpCM7wD1fM2TOBjxgeqpagHxS4fxLSHECRILUCko68Ulthv
VSiigWbntvx40vexw89h1+cItk+plZZLNXPe+nYn+EJ1pg83F/WfKxXtQc8hNN/Dz51An/z8lGYu
1vrjoYprGHov+/j/XjBYpywUPUBN0A2K9ZYcveqYlz+C7zVrbVTgMpZNcltp7X+kTC7i3FRZnb4F
pv399fCIa2YH5U3u1huCNYEq5AF+fGZNNhBMzmMgSruQpbNPWOJF69DjIWdxNCveiloMr6Tai3KI
t/P6+mvU4x6k6882F5PJADjuPCdWk2Hs8cN0fD0KzZfdgDzU9+Oa+TwmAoE4l6yjDI90EyTJXn7o
iSxyWWbojAE5eEEiGI6VdNwt8imiQEO2xh8XxakjqMqfBDELGIaYhKciuEwY2zydTCatrkzDBGTE
PskHTYrkiU2i2ABfHubkzf3Rvuj4Q888DrZT8CuileMvwwDl/ocyYN00+3qH+vA35WSVsdBEu6yk
2N6VmBJhNV3fL/Fvz37yt+ankOqNJcE7+e7LDlQrBiwVE/DXPmn9kqSzQ/hA1DBzmy5FfNRbGODW
/2TStg/mpBW1weEhV160tG4+GXFhMNrCC9v23ZYDY/k0TYplMVNLuVA5nKTms6VtsS3EetiNi3k1
Rl2bAb4chgJiu8/WtdaitADxCK9ngaeRXTSkg5B7PdsUr6fMXT+z1mfRPD5Hbs9jzZzUNKYE1m0W
Sm+Fk/Fp8V4RK/cu5Rq+bvYQ0tinPwKXJaJtbeV9/cPlnHtI8LKW7YK0MI9A+RN6EPBtjkjUK7uV
ruthlJKdpmgZz7caAW2vQuQ3sEIUgej2Sc1aCOWtSH26DfgIgaxs25iK0ndZO1n3wr0dL2AEJv+v
sl9oOIfZCj1cpJ7waqdep/yM0+I+Vhllsb2fp3IDcwTmE8/ou94NC11Tp1BnoUj03Pmd1TVfuTHM
/Uy7V/U2LPJ3qCGeVtSMe/D62tDrEHpI6JR498tUygthcGLOAGSBdU3RUyLH/JFFZlHnPLZaHcs/
Lby7MXhpG0l9oRIUsCnE/Sr6ChbMVugsxd/5U1N+bMklQCNxL9XywK0w9seKu64KwamDuJpXkaLg
iNR/5x2Ybq1RFpDogOB1w+1fv24XSOYqmBKq6RDbzksNUw8pmARAphrZR5vfFOttiP3Xiq7EjtKI
9XNNxl0SmZHS8i8OW4oTjDhH5eGYyQgIXVSSLTIigqoUoJ7Ho72ASmdzfAF+gOcct6yNngOTLWHn
jSusc8Xs8lLlBSKTsbJclgV6lAuRY88IoKtXsJhAyaLEnNYkDTV/w0nhcW8lQL9zWc2eH9Sw6bn1
CRYOcrwtLmhB3qTw8DGCRpbDqfv24MOGlnF3JRg6yHKOVHQVfERWIV6iGREJOlGkhlCIWlAmb83c
pdVzHpPoh7cPqGb3h3BPTWWT8OLaRlYLklB0h06dcNDOmm/hc3gOQsEpw84SDsTUXihe3ryWNOz7
5n8actYQHSScHX7pokiF1X64dAT344Rd5pSAZ3Qydb5IsbZdjG++1qO9cPVOhGECIbuc3adzK8U/
+d1e/UBUzwuOuaRWx9JhC+6Hbt3HywbW+Kjl/tZq2xAPqCuQe22ciRcm+iSBLF4ZC+wMcpdLeFeZ
u3qjt+WqE333gEWNCohGTzA7vg7wzHanEYA+axwjyKzA6DTIMegLe3b+J1tZ2Ccfe77+P7d+kA1x
DQLF29cUUkgAN2k0SCEOaZ72Lyc/8EdQ32sPXv4PbaShSdyklGPQ5E5sIFtRDHRPFnBAAU9XGLMy
MZIjPYT2oxe8rmw1JT1oCA5xjwaavbl4Z4sW4G4BuQao6iV/ATIYmywGMg3hE5FkeLWvIVyjX21z
QrE5/399+nM7k5umzhrQ6MqRAlrum71FxoQx/1ZkzCQLUxi3m5VQ3vnZBGba5EeR05FEJfvIAd/h
/LsJnQ33g5M/1Yq/k/3QARi7Ptl43/YX+geE6OObeynDSsZ5odei4nDw59RoFOpFmsCjA5Gq+YF5
Ro203rIMBkW57dWGE1I7eyfaCOKD6cn2Q5GVyK/s/KtDTsc8O2T8gR7TshcbvT9Yp3ns2r70TRYe
ukvH5CtLgcZ8RESUIgopTxInv88vkHBVC5RQ+y00+oJZc6j9gAWyBuA8TkMfr37XFc6jQmyYEfpY
8PzV9HeHLkT4ndJ8qNdCAaGhbOiidosHRfoWy42WCUMYWD/2XwsbxnKFaFm5y9EXs87zreX9U1Me
BfKnKsmlD52TIlCIg+DxzOrMhE9r7KT/XyBJi6dT46GKHSfYmRBr7fgZps2zegPilJoKReRpAbji
vHf+y6hcIeIaPhdNrfLzVG62nEuETtOxac3/shZPXZbFLFgPI9csMIoSaUVwdmEhnVR3Sk/5KP3P
XHtaRVpolkgQP802XdTRJ1A/LJROj2+tB2hA4dfPysVS3vyg96v76s+NRB2R0Y74t9bCV2S9it64
BJp1xgLZnAHRK3D/JY2HEQMATsE9B38+MMfrgQBSJCwR5WeE765MZ7f4VFCnmrLvhoaLu9YJWrHK
WdbKGujgKkdTyEr5f5pIECIkyQZkaLK0uElIcqM6uv8xWdCwpwcK/Wbky/MX9dMSwF8Pl9EUXV6V
uW0Bnt2h4phB7b/MYqFvXecYDPhH08Q7rNWjwp5KMEYx3kS+8rByHs5ot+9pRxgBSnATkrJrXMBs
8CMZ6i+TXHRlpIAA1odc5KocyGdTaFjeose1GlfMjz2QNB6wWMzhIZ37C+m2dM9s2+Y5Nj7JyoCC
sHwyQ0L9Xtz60/V1oaBXIuSdFwCjNOIStIcFB3FOw0JUFtBoR90Fu141V4x9Llc3p0e/g0SKGQv2
rXw8JOv0x2qrVlmzamAWZkyHjbc6h04OpMaOaNACoBD/66dkC6RIcOpDyagoZId1G2iEJUG1NL08
26gtlU+jOu5WhHDq5CS4FXeRnzYT3wTfQk9ldqI49GDPDfbPyfRP0FyhH1MfAimrCoQS89FBpHAs
w4T8F/P42DWRuP7bW+XMHqwmfKR4m7EDdLrI3r8YK4JLmBY7kp6CDi3JhbrORsF/xsPnyEKBR/cx
UAKTvMeqj20gkU8by8HEk0rQXoZdD18DR68d2gdxn3ijQxGHYdEd1QTaQhGz31ofaAvYxDTBv4Sm
B+U7XRgpRXTr7a1wAG0Hl+Y6TqSwy6QCbemr9K6TbSagP/qMfZZJ384sosAdZ+RiyfWbom5asM4h
C5+/kq12r6+Egw7Cj2M9X8Va7CL3I1nQbE4MBEs5hEOiPmzd6PnQ8zn3ZYcmIQ12JYq3awb2suOp
maWBo0uUeBlZPBFoZ9qQ/DgYtI9VLRrxc3GZVrA/qTaGV5T3KO14ZZ8KWL3OfWHOSWpTOle1wAc6
LAI4a5az8I2GGAj90TdDTVVlJd7+ZthTnLDDdaDVIk4rvICSxaPwWpLrtK8IsZzGn3eTZ76LRO2T
pe1SwMZ3NEKdGXKtcB8WpTT+d6g2TzS+v17IJwXzlAKC/tWg+Ek3PG/j0fNq8Wb3vlVkq6iysyCm
Ub7Y1q058V7VEXi0KCioNfgrCXvSUDfKeodLt7GQ4NFBaWHhkWOIjaQPTnBboOy5aQ/HHvRTmV3e
ajrdPXh9CikM2bAYfAmwlZm7HTsYutUw9wlmAiFigPT7WhWCynQRyIkCAGIbo8gvJW8zarIV+O5r
R+aR20Ld52IscDrQf8MtoLCyhrQR6UZ54VZXDyoNIGmh/R828Sn/ytLrMS789oHmWswyD4aUEA6l
UHsTzSmGi6JjipwJM9De0WQLQq09fGrHlGMcSLpxg4gGu0DBAWsfEtKkmiPyjJMGTWd0JLrnWsxK
Teqa9+/po0SttlEa1JQSj4dUXkMT2nq1/gDffsBsPO6qNLX+T7K5SiJdR3Lget4F2+4BgVhoPWfc
J9IOO5VLQRLvtfuUWvrWaBGjxPFIjp+khLnwMaiw+Cn2jP1MB+uXci/ECwp3CqByQ+HY/Rf57do/
m7NdvflSNZIYtMDZMrTJBPyKs6DQC4HZwPIcLyTCcGDxtlt7cBHu1j8j0HVvboNClOMHHz/zZr/y
eReJfzThcQBw06aepZ1JfoCB6Rh6ojMKGNz7tZjzPBjGwKT/Fqe+8kROykLqvrgjdSne4ej9LS5D
4PUtPHB46xE9Jnqizb/9PWZfsxK0wrJePIOCJw9Rb5oBr/x4ujUdxFwlFw9IftAS3ddrUNpr7yKQ
B0nve2y1VWe4ue1D5sH33YuXCIkz/x3IteUORcIkt9p852sjZiGfAX8FBSvPI5B5frOrRcZBjMkV
eMv+QrRaL0EKXbCatrK8xco6tiCS6uIoTS/X6sSwk4sfzYCpkvZaltRyjyuWf39whEwX1vCVH0Oe
VZ5ADb7g2tcPsVo2PCRLFMEBYU8mYUoe8T65TPRLkdZp4sMsduZ4gMol+wbFvc8gInjp2i7a3aK7
NfB9//arxYgn3xJol7N96L+G231NDBVy0d82A/Z/y6N0CbKuvZ9MTGPG9J90LEex2gwIW9vC2+rc
caAEu9hKUbyzvquZcUh2RCRYbTi033aQSgjWJgJd/3Rsx5NMr4RFimP7OrnhK4Kcklm3yAF0hboF
P4kvyiUZkFhVasXu4PpgmQW9WkcZ4tTl+qT2aQR9CRjGnrPLDj2OgalqAmJzC87CaTusgHpVEAzd
uUrRICqDirNkkx3VeBxj6nEe9e15+W/2OYKApZAoYhv0DbbeVSpVEeU/E2lpX20nGbod7BNImC3r
oFxOUddEN874BZbNLN+giTMA+Xin1OUJbhsS6CPnDgeo5sWdCe9AaAjS4CEVDpX31ycF+Kp2CxY0
ByoJtdV1BjDYicE0XqUJZdP+GNRjqA99TDja55HqSnxtH6A739PeUqUwSkcD3vMe9QA2RAnQl4B7
NflAG7KkqXNWo2qP6YkD73iMrqzsAjKuBdXIZDV3IwFxOzr7IdGkTgqMUuYSXjP2EnjPoEYmrrcj
1KCutzxgDDfpk+9N6TcccDjxRr8iOdWV+PgqqEilvDAtyQcOq5qrv7vs+sZRz0TTwsJhChPS4ov4
n7hZ0XKgzwupgqr6v/T06MJ1AuK+jRmKQ14LDpHjv0aQgm456T892FL5ehFDzInebl9FVLopKkma
dvMR1W8ldW6l+sCVWqioJgn46vQbyxVs29gIRA4XNM+G4Q9XcaPQathfbyXorGhn4cN7R8msBgCd
i9vOyGIo/Aoa9OydcQktOL5+oSWHH3rAkODNLp+JxAqQlw9mxtwtztXPiytGJbjo51OdrMWgj1JO
pwtVXEiQspAzO4k3fI8Ow+dyFi/NyTjZST/J7BDfVF/yIpzCWJYuzaFSIfZSBh+3mFSAl9wO3zN+
SB13i9Lk5dkYBlc7xY+TeRm+0bhRo+KSL3/HGSGhr/nykL1mgw9UlnBRWTaqMAXfByM1UB0cN0a+
dWeBpOMXDoq4Ee3nFePHXMliq5ETgod9xAxazWw+cA5DA2CU+eemMyk6YBSKvJrQDkIYHiWG70uz
Vp0DEnb0BGbKRblBJUc8MMyVzfyIthsDauphIuN2YsKdHdzzLnQYmUiAWKuSO0eH4fTMIe/Kkofu
7zPQftNCSWugiIhv8UgEORkq1onJ/NVWxvsOBTXP6iPBrvXHKNYEbGqKpz6VU2SFFRHeMv21+Yf8
hHKQQiLHIH5ZgjkRD8XHj7Ux8wnQLmlHsFDtMRk8nmSKL7UuIF7aUaUfHHUUyxmNkEAvZtmLavaw
Q9hyZ3tZaA2yL2x38hfq+IPbfcubnLPtjTqPg2yvgUsJMRd+9k6a0Pmi8aDQeP6thBgwg3J7NFp6
j2dhOqU6xSytI9cKOqw0jXzJHccNcqG9/Ih4WI+mSnNQTMKyh2CNi+DliPYhV6G+3HgYFmJ91hBk
NWmt+ZRKfN9+DXMfNNCy3MXyIXXXcGajFkBVH+LCPJlEurNUvj0VaISju5CGizUxZVvMvIfFYK/f
7Gb6A518mSrFl0Sv6LdOrY5zMsBXb4i96HXRN0MFHo6VIwdK376q6dDaaNT5P/lj/zJXkh8XCh1I
CCaiR8aYOSt/EMqvjx1yGo0roBtmeGii1zs6KsE2U/P2W+TecfTL+Ge/4Ce3YYV9cUoVPQ8UopVl
KXuY5VHWqMHdB6Io3gP3muKZy3b9nrgHNF90kQpdIGEBPtI8mX/Xu+cVRv3A+fAWEXtnBxLSMQZO
oiAWYaK2LUFzZrWGmPEB28WZjcXnfhoaU6d6yv3KTq10rechO7J3oWoEO7gcQ6tsq+//qOGvlZbu
EIK0fo+w8vMjHfK1mjLfXp35DEj4phOMdbZjPboMa9X7DNFzILftqdcF27kelP5EF/gA/HRsV7GW
vB305WCzW7MuUM+i25Oe7HShwJqTHKBFYDKatQXYi2GCqZN9ChvUIJFosFTqQjTB/YPLPwqtiq1Y
7/nAKVLyg2P6b99DlJ10A6jYyAzWdE9mkHnyQn958Scv6NRLJkx0QdzdaYuTmA5j4wsM1v1LFKbl
PpReV5GmoAeHTdc/SIk0O8kwuq/ehzZbWyUaDex/STwbVwUbmx6qcsHUvR6Y2mQlHWvpf+rVHe3h
Amn8Nh7GqE32LAexVgElmJHmStbaRSkUqKakyBO8/xKBN6YSHrNZF6xTHmv0uXyedmKqOf082VhF
QIefiTXIKE35vKBvO5MogbGkTWLKp+1YlQVLpxt+MJ1Ki2eal4IRkOl/jP6N3VXXqes1i5HzYtwO
F+NnfspzXY9Kz+y+q2ogVI9HiWoh1MihwdujKYh4TYxrNj4cQMfBECd6RBhy7JWNXJBOQFCVCe2a
gn30yQyID1cJinx1t4dJKvZhR2i9xZpks7e8Y2B/wKEd9PH1INXyGUdRuxh9KrJ4NGy2ydvZ31hL
WKB3kMxmIvnpDVQN2MID5+SAXRFXlPNM4MGr2e7FWeirwRgJ2KwO+84UMTyrNQ2uL4gDg10GaxoP
48I4jMtEBm7JcdJqmSpAjn3aX8BOnCjbbARDn4SidNe9ojU+/x/LhNfSXfXrf0JaGejuNK06OREK
A3ePnumj/Q0r1gcm5/WSFTBU6KAhRA3pYhmqHpHjA7iABX3QSFk3/WDY9hEv5rfDwlfyZve9P+ZZ
Fkh7hlLe4pKE7RULkWdUTYQDqu9zvGlJYc/SAOqvd1sXNokr6SXltSjIzlf58H1i2+9jDlVNKC0w
OwAeBxPmUWLHCoqdOp5SHKFY4AF0uSHeClbBXwfpccuR4p/bMDk0dAPo72285Hs2W2Tc9MepbEhd
IuYF22kKNrf7mrg+CCHMzyuxW996cjCfJbQE8G7JxKxdCa5hgTkfV8aMv4GfTxsc+ine4vbni8hR
3mG/L7A7VwQl9z/SFJ78Olxxp3QHHLC9mMkovF/s7gcsDyF9ZCMviDcFx0UETxORFnpuh+cWeym1
ZIUsasPOQu5tDwZRovf/7GEufHswijq1bjzrd3KL8jjeYH6VrI3GpBoROZhnQiT9/cNB+VGbZu9k
QEoIosA4kqiUp2eQ9sTJM0fziIxunPO4yalP45lvbBQsgSwLfBOlz9s8tP1FAlZzJ6P8hSLASbCC
zsXtlFUVxodEJqx1DP3RIsr3qssV2o7Eklxw3xBLJoz2trgyLwEd6Tavh3hOy+IQEtj7Xr0m2NQ6
3FeNjhj8U60cU+/sb3XhikCmpI4H5Wa/UM+Q4lq08V5nFU+OmWROvpJP9qJiVb6ZoDGQAqtTHnBj
lyBH3V8WlJlIXCMmPJah/pZQNk81wKbQTvf3ADkoJI+dR33C/gOYfT9aoVWkAhckVR6X9DyrjQ5V
eqXMlg3XkiPdJe178VcWYAWxk53jTzx0H5ORweXuNa3xSstw9k9u3zC1hHQYZTQ+a0i5DRHsI4lk
E9o7XeV8swtgejUyCf+RjeP5WW2oMem82iScj+e9S/jnliahiO9IahEJ3JUJTSCBK8kxC+XWITO0
vA22PmXbUH6Zz+qpdHXfXARHIKNluUUepqy6g5w0aTYlvq9w505Xd8V6eu03HMHVCYDt0OhDj4R0
xXYIgjTBAWwwb6222gQ5krhx6qPBZHrF6zsJM2lXxAeb8v1z5GJcgz8yEDBEC3mtks4ROVpAu2Ko
ZNqSudxEUFxYb9sRxE2r1xSj4jje1ED+ygnf85CtkKonv7uF/fgTCCeYRQWby1KOpYX8uKFNOlCu
IkpBwDa3XCJeL2JolcNosyYoGZoQ8dwpDtqumjZd47E4thQSeScjXFzSXxy6dosA1o1vHjv9fzvD
plaX65f550ovQzYIoRbPTdS6k0bY9ZmWsuGhjLL/kCQ/OasTdZSLhtZRn/veqG0IcLm8ZGrjBXCj
zRItK4SvoQ9mS3vLXrXnPpy+pGSvxaC6x5qSKc9HaqxzFm0S6+rzZckZVZLMf8i1J6ZKRqMnvhz+
I0xJP/HeDhalkigofS7Jr5Jfd9T9U/m7TbQHQ340mvudeNxcpCmW8Sm4K6F1J8XsvNvelPce9RtT
g95vqFC6Z0HV0AipIgANwp8nbMHnWgQ16Egovv6F6TimfgsFWBeoyGzhq8Yw3m9GYXcGVVmEot+C
nn0+5v9O4i2Uc4wYedIuTvn9GiuQ9QGsWnLURPj9zdWcfA0nq9ZYeN+RWE6+tXcCmrwRmPoN63Ra
UHrhfYlyRJ3Uc6tN9KhCXMIBui1npGF3Uu8eiVj+LJR2PkhSc6HanFdDmOTsjpgZYSlWfugXx2Sj
ank9RHCQsUfVHIRl5i3jmcwWTo6e7tcPqs4kjuIR3xgR2SJ9vFD2t8xPjzXHx2vdqgtcZvcrsWzd
v9nVN7JMTBxkGPMQGk5lJ16O017K/VAaB1qRLDNW3cz58nBeQU+LS74GtpOy2KEEjhfSU/5ngZQS
hbWHFE6rQqebUtcRpiY2ewzcw6rG0wnG/5H+pHAbLYGDYzi1z6NSkooCdfxZsCJlYlUQ+ypLqinm
q7fN3QgrBL+BczZWC3NpLtHJZqkAjtOADnGqt2ycMtyDo6r0JOr83OK5li/pLVn3pfnL9n73AY5w
bpF8lJ+DY31XIW78FrDTlq/G7s8TyDvC49zc4AVUbiD9KcjlhS/hFIkBSyW5QWUwEuQNuCI+mLKu
pFA24+dZdx+d9KShqs7vU0IxaFsKAlybTbGtkgfdZHRukBPuFRdv+gqrrhMVEncOKC3zPuUQZXck
rXJZTLsGK2qlGl8iWN9D4aDizBqisgZOIQXiGNpiA6IbjU6Z+gazMu2pZkypzpq2bpGLytdMR0rX
QYoUgxYg+V/M/NiIouSvHSMVZ7VxrCeNz6AMCSqFFpKKxCbz6leQeJQZSIHSygMYSe3iQPu4xNa1
a1Rk49t7EHvUcS0imcvRcGORUStIdgSSbnr8CDA5IQQcTn0gX7bfXr162n6QOpGfczPgHzRG4sOZ
sgcT+XF/G2e2kYHIu/jv2DQT75Ejyx6p5S7mH5QiPbx6rdTpNDvRd6ectNms1aObV7TnISFpuv77
Rur1A+/W6wTZce5NFVEbUaDw7oW+uNAmiIFgz6dKcFw5EvVWGbD081u21ntzYpaaXEGa6uSpOrAs
4Ft6W7oLMaGnaV95esTQUc/XZHJ+bAVoye7oKmdSqpLFQQ3fIoVz0hIykJvIaQqLJIWg9OuMgSn2
DofU+aUtrkZL590Y0bnkhoZe5r+T5QkVzcohCe9P7Pe52849e58v5Se+y5YKQlymUbzN32aRxNdC
op2DKGhkx63jZVTsV1TD6IReMIjFz5RPr7qb2hwdjJWsM700Hjcad8dOY8Gri0ILlXZTOgXowqYu
nSGbphGBnvifU7YSn/kl2abGeGk221SLmKW3jrioopGAldAQabZ4vj0hnYzojTqUDhg2OL8s/oUo
lZROnu4C2l8JFTZpLXnkof7p6vp4jWUxS0AxNl+g4VfJukoAoCCrJSciIxZdMu1UusELvoDfVsAu
FH1I1IemAaUnLFHF/P1mSGdxpFULXqYT7zSEkg/llXBKQiorIxywHxwf9QPYfNWgSUTFk1SR5FaM
Sc/kAlS/luWVJCmRXFDUc8mR8qHra/B0BA9SKlqNi2aJss4D7HExbVnuHSNCCi89fRXX0v82PWpW
Ur9KUPZw46VoA3A6q99976H/hpSBtlVEYsrotbV4BywFZ3f1ou9jRD8K/2lVMpewNh2RaMmy/yPX
Y96bjuiEnVKGJkxYoYrmU9+iRTjk687WpxyZzhhBHG5Va0ZhogfL0CYWhHx0QtuTDmYMlVZGkL/M
sLjH+6L59oz5zw89unGJaRSqeXzSrhVydYhy9TQNSnvDgeWw1brYnIe++iZPWa/KfFxbY28IJYXF
RMw8hWG3tcRajnI+VEaNyNOBx5wlmloq9EyFueLyi7K9pvGR1tVuiQX3ZegIE1HR+fRAFeghHPTC
GMQ8wj7Jhx3Iu5RmBLdMj9+gBlbU/dflIHTgF99yv324IvTf1TxriG95HIGUVacN0VX8edrj6LBu
dThzVg9+K4d04iUlabM00qo1w293FE3kDe8yuTnRhcxFN8nOKgzckBjNfcqX1lhDgdbJVwRnDGxR
nnlRsMve8/3k80NsUZBmY5ARTX11Ni0GMmma+5mrzBC0a/V+G43g0CvVEeJl+P+dgWnP8HGaQi20
9mJWGNJ/WT6rA70YXIYYBNNi6cvK2NcEhAcXB4EEsNvDH2Wyphk7ZsMWs5quQVKhnehe0rxyBLJe
Q1+iDObgWJlJyGAHBnTnOLchDCaBNWQdhidNJrUkUxjVq+CWSYL1atjQI693nwKxqHskKK6nLi1X
5SPfHQZhganX3iv/MpskaTfQpNjaj8OE/a/gD8u3cFlE6hz563KAzRGUmpf1elDYhNoZtc0r8Wx0
x4xzWJhZkwzxcvoTL5VkJHZghswkW4tFUNR0J2Gw5mu1+RYoLhrCVjqCvvJFCORyJh6mPzvApC8n
qqO2xLBRYgAla2amSKL8a23w4cEjdGYjM1wfTAOQ70He5DcJcP61tw+gdrVlYxb8u35qFrCt8bAx
cTglr4e/SDkfll3HuGhaaTSLuyk+NG4fHlOJJL3wJkmufjd3sIZ5BhR8ozGrEHuQV28qDWuRto+K
GclpWGKB5CzKAcM6QntOZjWtTaQdFp2mzgAU5sq7bf6KimZu4XYtjVjvL79NO8TogDxbxiEbxdRf
Hf1fq6BnnUG8YaexHPFG70yQ8hNLgVhserC2JzonyGlmk0rSx1/KKS2UKP9olADi6HMPwwolOOed
oglOnfrRV5SFA9R0dxli6taFJyVTNqepejyrSfliOsDAsLtxBsGnDnjB8l0k4U2zfxZhURyR2dAF
SaTgygaQvJEeF3YUE790i7at4NTm+GIIJ4RYB+c9ZiyHdMZeKYdX+wmHSrCn5JriOPmDHeJzh2+Y
/cje/HKboFMfAGAEkHAfdsBu61mgnS7hQW30xLDFVEaRoCRlCubKLNGXC7lRXjnKwSD4C80BbmaH
yOWf9ZtrG28mk84whlO4mbkgUYnRPycl51v4UeA44DvZF84xpya0mDlcE+Bl+rWhsptwOkRwtK41
mLxMtCDi0htuxiriPHt2sDT0j05yHjVZcvArlJTGmiYl7ww4/r41lqAVpL6ga1i8o4jgk1sUrjnU
Udacag2uMY35Jw3HvseqnHQ4LEockxDu6tOUFe6onNzq/qRvJvhykwRbW0xjetlocTrbUG53RIeX
0H4dexCuvH1riueB+4ytzn6mR6iHNHangwuDu6qCNKBgAaevh9Kj+Csh+ouOPsUaiGC15grGRxtb
Xvp4AFRc5ZVhQoNUTzCqLlILx6Rb2G45FSuoNYk3WCZRSXK3Lu3yxLM+8/YXaw3g/eRmhMsBJsOt
HcAwmCVke3nPabF2vUetE894nGlpVNIJPop/MCvK+f3mSnCOQkp6N7py/DRWtNJxFOxPF0APX75x
s+LYEQMbD5RZvUIqKxnLmXhcU4f9ee2LxC98gsP6lRF6/1Gn1NVCq3gASUUn6HIm8Ym5ms4KTnSH
uX7JjZ9E032b/Qf+RwpMoJ+HtuTK2rnYO/16SLMF9z7tInY+vzPmKraRP0MgvM42vo/vk9d68iT4
vBuY0QusKV9QNILjc2mXy/JwqFeMW2hgH5vePo4S0hqXG6QYNAIutciqHt8+A0QDMq4qXVxNquQ8
fm4CYU3e+7obUpu7EQ/fghq8rU5GuolIzURLXmW2nbLKFIPc0rjv4TMzVKdPn96E2veUbSWmKct1
CVJp7zANolUB4HQ2w+TVRj+l1ythiK76whVoewK8TeH2s5Ss1dXRF6yDsIcl43O1diS41YHjAs9T
TRICXViGalPRIvvNtuValCw7uTaIceyJNC4vua3lm7LMmjpwIl7tPT5BrghntsVcYyI2WwCPLT9A
bZRG2N4+Ehn79SJidxYtbJWl9kN2xCQguzk1HcG2Li892vcKTiQO1W9i5LXoy/4RZBGjfKouQXzj
noK3HWHrUmHwG41bQe+QI4ynNoNQKLCupIPKZU46A9FUF0pQXHH9IREsnblVGh0glCVBdIm/hPDQ
ZU5qXSBPKVhjWDfu2x/KwUnubJHCOY2tSyoJYPHxFOn/we+J0C4MHS8oYx6Zh9KFa799NTfQSvYN
dQtfki/TX0FdujBXXSVOP2lYATE9tLQ2qurNNC/g4VLucO0A9kzzTfb9R8xx3YpjDgssqEtRKqUw
+l1WCkY3lj2vmG4xLmuRC6nCTM4a7i86J2chU5i1aezqMwEtS9yp/8kl3UQR7BG3yKle5GAed5oo
smdi9K29y40+PeTuXZQfeRo8yt+bd5/TPV1w50a6D6v3Ui5f3Cng3w/I6eXxZiMFqgCZywDVvn2s
sJU33ke1TxpMuSgOtBQmMRNYE6/16D7plCbDbG/wmQw9Za0huMxApn4X26JZkTxKow1+dTJ6t/y5
5EMxgnfGUar1g9JnqN7FQy7TgDIh+8QO1UEhJmcxZhFtXQuUwwwgUr8dGHKPqmS7RaQyHottE+84
BYJRVu27f2zN42lmpSqQKLjXZDbm7ygh2fVvtnhVGMOsvDUomABaJd9xiZrn7L8sB4LqaBmJQ7hE
ZyBmebPcsM2WTTacACClJDTB51FEZyF81I/xRPIAl2yS+Um+W2BGVcOLtAGELTKHl1r0kiW0lEzG
YmM19n95c06/N9s7j9cT3SEwnRGHGvIV4n2cxkBKt4gZ+4jTwLxgMsdHSz5MWlYGW0GDwsDEBn9K
PzrtQnru8JPxamEFh5AfS5NA1SZLQNKlL3hgsrdcjlt2udhMBx4GX1XdFfOnRefy1767IZi8pZVp
L2vGxsvd9NPEKS3HWEWNGibofIdegZ1yrndSbbIevbqeA4AEpQ50fT9sGU+BwmV+4/VYGZFzWWFo
CGbhTl2O79KP2S79/4D0SLbxSyOJAdfAxPMuEIdhTu7yjXkbBRjo52EY3FKtmfoRYRQMv1sNepJ9
ZIJz9xsqNMrTrTjP6a3f6ZuhhbwMO40HcquHrttjqzw586ClM/wYY1VQqE/Z7YLrXVHejcOlnByI
LCtdsBJC0ACTEMXam9U4BQLMBsbmvYkXfIbSG4+G0b2kILAhKE8LNa2T+LbEItSBiLSKwfgH8f+O
s1jAAo+Yk2OK147DNe5LHPi+zdPJP6mkzlQJ0pV5QPYWtqZbXW/qgscWBszjhVpTtPF44mLl7qfR
qVl7Ihj45hfuxB5tObQGGRcRbO8zZcplsLaL16FwlnMQ2Fh8jpVsN6j04xfWN12jZAqf21MmYKzi
Bf1IYI8jHOFnxDPgcahp+x73oS5J/TeshivFzgeiifnCbu8X1MEt8Al+eLJgewGgpLKR2a3xhK7+
cuaJfqRcF5/B/DuIrRyqlx+fepoWRcWN/WMiFuiOwHdqKaXBJCRLxOaSjbjbqoBEl90LZShhc8su
RGgUDgnLemZSPRwi9MqP/4E0LyBgkw8fkknCC/HYaXqkPBOyU1NeqdFspg26yDgKpyJmzTGAHPoM
inID7f2F7+JVuWkFQ+ck2OM9yu2Tn6TzRD837aZaAk7JrEmJugl0yRSNPWEk59o+1H1Pbx/KMeVD
Ca9eqIq9sYhso14D0aAoBkCdIWk7QWPukmhWD/V2msF718bNNqsXBCp0zKWs+CoGC9D/nQC7dbXw
BwHN5AcCNsFaDE7d5l7hlf+BRLoIjMcvwq0SXKwgRGeMqL8EIrDfjYwdWB6vbLA7N9LsAqYtiANf
ua5NLls87t6k31Cpil49D00+Vj6hcWfh6uutYyJQnbTMO9cXCZs8DloqaXmvXj1NrrQpkBfPKKhD
/4ltI0ol2nBo2MwpgI1rg4Xl729wEtP4KuXxY1vN09esXn8E7uj0p3Dd6V8wbVzl7rXw5BHFdTCi
bzJPJQRLUoA7XvGCxTMYjoqhG0tGQipLYpyrIUvHx+yAgiJR7oUZijAD7bZVh4yArdu/gbecZl/7
eY2+US60xBV3eyIkXYgiS261Hk0/m7AIdcXHE755cSQhmh9XyTMg3Lnq1KZ5JOM9w7shU0uwvsPM
7sv2FrNXtbWCpAP0TDSM1NO4qVTkWj4/jjHnryZcWDiMLnCmP/JPvV4tR+XJaoqOb8RmdKI7mjNA
wv0QJpaPCiAYAMrMdyLAXWibI0VG9/HuTcfIhUPTRMVRaEvmAO+yet0AXR+WhNNTsOxJAjH0NQcm
jckZbD9oEDs3gBwCej6BDrtvg1oEzY1EHzjlhhnr3UfettOuQvfb0r5i5UfdQhkDtfl7bi94kUlj
t5iGpDd9BjEXr56Ro1E4IoWPN2lUl63AI/Ke0aLKQvCYyI+DBnY+xGNDBTXohKFbqvoK0LEZ+7sc
eYBTZmE7jms0qO9bmab59JxeI46mr2PzIpO1llQJDQyK3o2Mm088GGvzuxRf5cRmup1wrptujsT1
2xoK3Jd+7p5uRIchsvper4fRiJdpBCAhQadQBVGXc5XzrsdmzW7ZsLTceWgkH8faymqkfBts3PaS
VUbX+KK4J/zb/9pTc3pWrhuwtCmAPTtW7Nhm7XmsRNUCCGTjFwaLeazWqF9CK3gXybQOvX0ZCOm+
MocJKVHHJ+A/YVp/SietepJ5bSSe15LEOEQsfQ/UXDCyfrI3JYCw7DHfa46njya9jUqHZ0oIy2Mt
pUx0D1m1a8XSgmaX919wcN6jrgfDCyP7S0qRtriemPisX7vARvkbmbw5IJbX738cNo5mgD7j5ee3
KwpPm6C0HES02IuzpRUJ2e4FnlAlpc7J3Pa6Hdpn57TKppJPdoAagtFp0ntIMWvc4opyWxIG5Cz+
7h2Rn1ShOqcSmFaQLRjHFOaggW5a7wkCB80XDFXa+Z12hwPy+IO7iqZgtTJ06oQH8XoyzK0BWKjE
U6HSBaGU2OCemaX+s+G66lMaDbU0U2aD+r/Aapu/yR3Tcgq2pAq9oY2sSY5X3q4J9QvY/cCDf3tJ
RB3klbWDkbsi27P2A0a+tNDybTSGQkGovx0YmoliBzdL5WtvA1yw5n5fI9DaB+0lPDHpaVAIl9tm
wpW+4gt81GWXUazNqwzregpAQvG1IdJj857hRAXL+VQjqiEwsYAHeh2521YcfotAbpp5Othj0FV3
JOa3bG70B0fOr8wTDgPoqvkOk1gbfM1toBhIRVSOM5yTqgAyV1TcXCY/fo7/+Nis2ZkQkdttHbkO
X8keCXM8AM7QCqSVh0u2umCKlDKAY8qt0iB4+xuUAB5Ve1OVhdnoatwGHtCefYDsVjXA3O3Elzrz
inXfsYe/C8MDZ+91KBbFJFPnpjDBojj16n7wY7MT6LhcirjduNhFGkjiHWOe2e557hdGFIESAGiT
2LCP50exSJxTyqpXFUR6MScilv41wOUVxopBaND2Q5ztQBganSrrW2DNS7rOGKxCVqaKC9hTpT8a
Xqn4lxkH3OIfBcWLJRc10dvXd5CXeCzgJFf/S0oFS8Y/cbNhZ5q/e5lHjEujW8N8o163a/9eW5fq
cj6/9AtphdiTGVHrEva1qQPb3bgF9nIJNbGe14eWWBzhsCaQZw80Cx2yCn/wjyhQFavEVlxHtQuu
YzZ9B+G267tvy3EacHcch1kyWgud7qTkwNN1lfnZtm9JyzqqYrpOg+Sig0wfNOIBuAECFAArnQE+
mU+1R/MwE/O/Y3cFufo8qg8gyQ9UcZgaFhH/c2FNy1sx2PcPdef8IaKZa4rizyweF2T7uVzyGS2R
QEjIMCXE0ZCZwCoYdRee9y5elDu1c4yjF0Dln/4qSK+m6g8tKW9C3ZgLyy4pvH3+WOBUNjncaWUb
w4ldWM5qFj0ZWw7ZNWcsRdbbCBRDhFYsJiCEwkXn4ZHtk8BWrDYefR3N9tyPeYv/eaRwx6OgX/HM
Bj0JrMo6KWWABuY21v0tEwZM+9qy4LRaqwrKr3/zLQO4UAf0UzkS15UJniAd8NNW15VX3kH8O0mz
vqrmGgaSkP5C13HLslLXEPCdUm6D8oOZvJ4D3P6LwYA6OMEV6ueF7zWTKTq6BFAc6F2Tigbj7Iz0
lYEE8LCPdre2nSXqfwQZkziHS8zAiTv3NyKApp7839HhCKa3k9nAr0pZAeEPKN73mw2B3F0nG9Ue
+KTWtdCY1a1CcCaMFh8IrbvICahi73VRfw9/OMLLVvo4gY/ZEIcQik9h2k2VKUf2t1q1STLWrFvv
B/NfY+3uJ0Ek0lmKQY83rKCKu1xaP8gidKhAr3dTbtZxuttlEW2BzqL8dtSgqZWArmgbo1y5kQ4V
pU/iOCOmwyDVsQ+m2oUnB78tE08et2Cx8Dco3J0W8coPm+DsgrMgDh00dRefGlR2hGr+4PMHJp6T
1Xsmks9/k9hmIh1ZeoDFOrzZLiaLtDQ5oVlCLwGefLfx5m4Iz+sG943OSnk4Hk/m5zx9AwlXHXyY
EhA7iu74gSzMtDasIDPhdFaRYOdibu3I7NxUgR1NFnEbMfarisZl+U67lT8dQew/W4Of2m9OtHSA
iB5B24R351PrNNV+/YgEjlG6lESlsDyezHJbCv7kqctMU2VuMjzMZ88xsXmWOIAOAjfZNnonzVQ/
E8HFdO+cE+83AQi2VXM/Mt2gy6EpBtZluqZuggLfrjRV77Q7bWn/q6T133UUqYwcnn6QMBOfkt0r
tYzGNDiIEF9h+3TJvVMshueVP7FqdIIsQNZbalngxdE/9y0QkU8lNfeSQCHHND3hPnYmg+9c6Xe/
RTMUFu/od1cGHVmNT8jD5FWuFpDtCpyMu8M8oTOGquXZDemPBQwP0ecr9ZXo3tVz0bO2HNiYnocS
SbdiKuq2BsAETWekLRwyGHLsCWrvUVLh+D31lKs5/l1WhtimADNQSE7oXHNHxHF/PI545/Dk/UNI
6OJaphD3IlsCq73oxcveyYyQS9pUKBi2ENmzqAGRLgJcjz5FbJenCTZdLw6kohXFLyC9P0B90AZS
8iZXJmdWXoPb/IxFtnL7IDHK6/Ub8gV+SF3FzJZ+a9eg0xQKTlqANQszaVy1ScCkLblM2FpVzwxu
yiYaGnnKUYQ8HJ2GT3B8kFw7qjcjX1oCvNLayloyvC2l0O8QikXJW6ukQ//VhxlKIB3MFtZLz/vo
18+es03Ol7q0YOn5ulynCI7lONHumCsiG/GY7+A4oSbJ7TBpCTajdDr3IxrkFn0UNGAjPq3Vmwqx
oQ+exs3BLUEemXg5COxqN0vR6ubpleKF3D9TIKcIRT6N0W5szQsPcPh+kGjYiYRJHMNoU+OG6vCH
NhVQvbDc5l0oWPCnujh5XeGKmG24mO8+ccpvcgUcAYfVHvKqxZcPyZYBG110jW56BpQjsgTxy94j
LE5AD55HmUHUoPBOyNoJ9ObYbEtFmqDxiHhkTh2CS+1FmhSoarflYgAOOmO085ZzZpo7a6uDmk7H
ikM3hsyrcrGc3W6oyqNAPouEaX6GKYDzPlZ97ggoUGoxeY24raQ8CPuhgIXDrUqm/5EvTfbr+lqx
9Kqwvem64j+g+aFebrhR3M9yW6X6o0ftpEDpuzesyE+wtr4BwF5D4FsXpk4TBeTLfu8Ni7SMHMgO
gl7CZ2G0amrZWqcwSefYPsQahhYulvbBItOCDWC4GtHmQ5FW9pMcLKeKQs5sT2kutf+2t65K35Ek
B63kDboAigXUWyn3u7TvbhuDi38+3MorAKNAGH+XJx865Jdowf4Rx+Yq87/5FG8xIqcVZyK2ZEvP
7Tr3WaQcjSyGmLn67daJHBNNi93N4oCoaqmIm/2XHNNXBU9y+TCJ39LpEIDt9lYNffyW+xLEWlgF
/y5uILaM2k0r6dx/Z7+PVnzPw4koRMUauBpGAPcGvvKO6Pe1Qpt7aXdGjtV7h1Iou0eIUhTycKc7
L5MRjg9z330zp4/6McPbYxmZ4on7dG1CYVvlxj5vrzNUYgqO6QolzEuLjuBRKwP1rA8LGwG9hBhS
ogr8arBtwR2KxTjPj4dUOXUKOdR121320qmOUI5ytVbl/Y4PuAyUKDU4a2zyBf78XSnRBSTjRHBl
yRsDtx7Z4TuBs+lzHQHjtMYFM1C7JIC3EWzVtGedTXvCTeozLLPJpj3xWO3BHN43jB8ASF1WKR8Z
3j5jlkipR6fU1GCQYodvYne2ZHaR+0K+rYEXowDT/s1lD9Wss/ShNN/VPS9w9O0C05QuxBR4seFC
snJ/JNB4uxev4D8mTESn1M/0Ywmaz1Dqv140RJ59xJzGhIKskigOsNRd42yZe4bFdPSjEGFh3eCN
epznmTngfFQDEvICqwjb19dEV6hXYwVZkFuPhadiTIntn3gdvyMclV6KrmgrGz40g7MpD+kFZCKs
0rZTDSGnDBWI+/QITnh6J6FkRDY8UpcG+TsSVZg4Dc14txBO4w9+Sf3zGTbCX4SFMft69tVtdt7g
fQYa4ug5N+a4+cRszEiSab0oABLyA9D1cFFmmCAF/rn/zztlOT+Knb6KvN3gyVC6TtbVtBsRPI9C
x9nMnHn5jNoR00gdVpGpsmvZ/OBxahftF3cEvR8aWqGrfZXFp+boaif4Cuh34AdVYjzgfvmMVs36
AnD/HMJVrwGL2wUFLswBS3JztI++/vPm2x/aDRdbGKdmQabL1378QkDt7/kCpZyx+hri+bI+Yc5a
2ylQcRiUuKOt2KAODryfHr3jbWAHxAvW73kfpYrucJF29OfHVc/stHx3oh8GNNDIHD3H2cV/2Oft
kIoMxxVYp9gyMKlxidKyYj+GWaVIAi7VGZSbu2HaQ5DObQtJDYtFi8HnfhHkuE8scjefUFUr9W9l
+M+lkkxaoVe9b1BwDRRBXNDbRbMpX6YQqSntb17+F+EbfNZaRIcMQUAtyGzU0t3HBCglfkolYmsU
4NfNBN/5Yh3Rd6Vnj1HE4Za7tZocd2sSMej5NWLOqRnFfABw7LbCE8NLXg6v4CPKYIm04QtxcGY9
4IIS0f8YdN53NEwHw/FmHmPRShcR+x4xf+MUWugc4bl7nZlVtM2/FClhfPtA2H3QDpwIjkKIwQ5a
ZdbtU9tqG42R5rPHdLR1QfFYcX88+wEGrAg6vMUFXRvw085aeiwBvgCrVebowDZ8gP75RH6mEWKQ
KrCUhxyXMO8BQse1BAo7zqgtoL5vXwAjNj2FRNfzYPkX0B2Zj0Hhx0qCu/p8tAnBGwxq4c7r+87R
XHXZk5UJg0pTyE5574YltxeHcb/+OEXyQf+Pv124C4AhNWd5D32PBf8szGGM/rEy09ABAUGWNEL+
PBm9FWeSQa8G2FkLbnaOyKYthWof/RwPShbuI/adSlciCCr7yPKnN/n3ug/HT4+HhRSOwCuexngS
phQe98DyOnTNsZPsH4bIYFKmljIux6iAI26Vi2AXntkrtQwLFFiQTGA+jtQY8jcftxA2w3DQmmkd
Ml3HUzNP3gyBRbL2vAQVzQwCDh2NJDlp1zpFTPL5Rwu+ahnLtA7JltYa3HqoKsieHUC3TYwHkVze
5ohgLYq21uflby9Pi5hqIvKJuHKiBwAJ6xjHQkq2ysfzqH9Lr3wV5cjEKrVJ4M1WBSEYVJMrqNNV
QOGoVG07AoPnArRwqWP/59NrBN5dHz95xAl0tOqWPBhzzSIhw+PooQl6E60JJukrzVyNVPuWSEvH
gPGF0XonNz9cB4v8wExJEBv0cc9mXgNuYwz/3iOFImkZTAAgr9TGuHzYa6WIPG+UVjw271Z3AWxo
fjH5i3OVNPrFaddBuAC3/GGdXbJDnqktfBPimWTiGYuWh0gIQy7cNAAf1UUbr2sYuvwtSp/O/Wls
4RPcO59Te/OE1eMvQnZUV/nO3k5G5O/K8y7OWZ2gVsB8UiCjYxjG4SomLbkXJNsxmHStUUFQYq6Q
dETFbA3afn+/r9onvNcbB2fNpfhovl9snS16mY2sOe1uFdGGnDIs8pQf8nsxm/ho8r5V5cBgQZoJ
7BbhvzzWD/SWG91WA9hif/XIsrdf2zL3GhSD2eAfXxeg092h4vO/NyjRupZMOBNNCkLgZTqqspSO
VJxRfQrxWIR3ZPH7D1QUHtdw62ULprZAubjuHrOnTFWx8UHWHqN6aPEK8RaUkeruY8rvmxCU2s/y
dSqT6XLX21ElGZN08maQfsRnbf2o+M8ZNo6IWkOkfQrO+j/lmAza9JPSFSDInXZi4xDmNHIZpZmh
OWm2sCEWa9FBpx06Nx8iryD2isEWOlFKrtssIjMWRfvbhwRFJucHxdQpjpOBRB+5vhDpN7+i5BvD
wv3kIh88vJEfYBG8uR2zc+iDjinS4AeKGAlM0BLRnwWhkuuowK1zdxp6t5tLxaV7i5PZuTmUIgJe
YHC3SpSN1rCci15pqndcy3f2K0q3TEnKhTDFpSfmoVsral7rCHx22RDFnH4KDAl3rN8/UXlFH8SE
cJfo/PaB9QrJ99ozV8PX2D0zl4M37fEcPTticq+vTBwtUOlhQhCP5ZAyAnwiMmlsVCruOBdnu9EG
2X6jMJ/cTl/ecJ25WBIkSD1f7lhEpheurx41yCQ33TRc+XU0Qx7f4d7y1Uj3qorZpncqvhjzwrGT
hKB1qSCmHCvY13HSTR2M5X5Mk3iQ8QbjPstTT03+mzX2LGFuqlUFk3iWnToCgiDJOyeAKPM4QPsK
kotnbnoq++L7ksnaW1nWnIzd+7qyZp60LcMJuhz25+k6KQG6tvfq/E9c+ZelbVj35tT3epj3AO3n
a5JdnyLMvOtyfm0OSBa4LPVAPaSRb4PEReYYHfWWrms8QmfKuobCR+5EkX0sLDCsPxRJTbqfE+x4
PpgyYztvdoRxE/LYSRMYHKmIeIMYZ4ziFS1fjOMbjdpx8QzjehGesAp9tt0FDJK4oW/LnQdojmH+
MhKfRK0RwhXB4+qe4BEikWt0Wa5AaOAwnjklV0CCf1jFkUOKn6tKghFx6Ff9iY5iW1onsxjVT1Iu
l2GLZeVWWZmjw/u/mpedHZ6UTIs4Gs+wkGdJ0YeuGc+Kem9rGj+6dJ9CncLIEHMfHkajEnqNVebf
58xlZtMen+RWW6t/MZ0WdtzBdA458VXpbd7lpXiKWVIY58d7jWn9FtrHVioxuzRc7WU9f1VFtugG
DUm9XhiMcKOgWvSj/ysXpRONPFpcvAEE+/dnBoepLJmiBzdqiX3665Sidcq5jDNCMCRueUI/fP/Q
+8+DBMaVH/UIumCRZc/a8ySB2pKKm30FTOl+vI80X84Snlps+1jlUEu28cy1ZVGUnFfrigGqEMjf
x5Pl+esN85EoUQ3Vd+1I9BzDH3BTXrcsBqDKDHlrrC5AJ/sZbvjwTLAjbA1UzyEl9tbjj7hGYte9
K0Wi5Ev6RFxYaGkelus6Oqqekad3ECafn369njtXwjhFAiYmXMzOtKbwlg7HW3ncWkgU7xO6LB3Y
oVwGCtFNVs17P84mv2G+xdyczDhtdYEoFcdRAicIvwqphoSJ59zcxbQFO42bCFv3SG9vFyS4SSxn
XAdu3bGwubm7l+BqYzSj++IwVKcdSDyD/6V5KkCTK6WHVpOMi/6868i/ZOb4N/9KBLjzvgH639uO
rGghdA/DILSEdo5iHL1Kgy13MtfrngGdfv6cFLNxQZebWqcN3WtbX7Atl8EOYCneAErxXzFyuB+6
ueOX/WJkkz6VHKfSDy1a4IJyJTEw5riYSF6nny5BI81Y7iiuAsV+LrYrPZtubqA93c0facIBBYI1
2w9WIP/EJ3bvANHT4ViSI0mDg1KV3wLimhll2N8uXd1ZOSRgXixdIkyNDh3VgJsaBMjRP7cVzX0O
0ljsjoWsstK9YepdjwN/+LJO+O2AHyuOjdcBq/rpt841WT+Rqo1Hl8oGbIHBfIwr6SHj9dP5KG+o
1bsOavd7YmUVYobAbHnOfiB8025E5TAv9kvk2+6s4dX+wFmfeqgGpulvqcfb2cNLAR4D57PiITxt
0JifR5XKmDM0V62LADAeneblu0tMLPfUMsStbcaMXWH/K5HqIhO+b2DiT1kft4hFxhTJek0yMUDt
B4qzBxJVvUHtGGTN9vTiBaFsYDNDaKENmEriQIlDUmtzlQ8FWXYL68ROKxfGNXGG7QGMogpjJQUt
FfmLP/7EqLyX7YHRWVd2ek3YnSpUuvA2VOZyqpSy6Se0aVcTjtr7QrA+6k0lZP4q9AALMGoTtlXS
nIURyzWNe45hnsU5xjcxqWXutTQBooaGj/guPEaabcN6V4HObTq8dvmXwS/4uhumZECWDWTeu/3c
zqUDOya4of48SsUSwqMyFwqStL7R0t2lhl5jGZ5mWUmnjhI0tUfpICIyvIZWVZpuhqkX2H6OuOkW
K0iXflmJboa/pfl0lX9ZRoyvzt7GmNRM8HOLTBQxtVSkDmAlQF2GjyEvqH2aKvAPPVVj2vG+mywJ
S0Cy88QJx5u0EOKL/oRMBB4CRPMsKcNuIKpITN8vJd8jhD+b5KWS4Zz8zURezFNMsg5FEl4wykRs
DrtKc8zFXnKqUzBT6vEhki18/ivCStSUKWXaunD5zWJUPELt3zLJdoe4aubn4uRrRjTWnPW4Jt5u
/rfhosQHz9IZe9CsbIq+ZKnn5USKBYaIybrEHPgiDDddx1W4KqDYgyxqJde4ZzwaYXEWHYpIulJY
mkz8mDh0rqR8hjSxrwGuwAJxC20hdyCF+NUQ1J8kvJDIttQ38/6EQ/y/uE+Mj/gxEt9/cqE0owL3
geax8ztqLYA40XKegufjfiidCJc7+KK79KRq/I+RcPoGk8RFi+0Ut0AYveQIF+T7xpZFOYEGcHGX
wGZAVbuV337mtgdQp+8g969C20tzguj8u7T+G8v/2zuq/HiALHozPyIIH6AUNNDvhfTeIZk51X7Q
7r/fjoe0pHsH/eu0a70CjoMhMPS30oygo8P9V2UDeTz2CG155MxZr2kEAIYCwMl97hv09zTMUXTk
dhoMI8m4djp4rT4+5JtlJiFzG5t4JfA13LanWLk2fE2Lp/8L5bl6XSAThLVOdDGXT7ANYJX6Bnlu
ctyvBsAL2/qUa3Iez9bjLnHOvE4+/I1/jy3gHqHmbD3NDca7DZfUhh0HI5mNd5QR1HR/kntEehFI
6UeG+v8vLgpe5tg/nYHeXz6JZMYRYxXtjC1xk3QvlMiDpIf9vnzqN6TvTme3k+rD58A0hIsCUJyc
CTuHDuX62IPFSwF+kEkWrLdnSZSptDeEgBmF3VyGu5zisYlHBgpdcBGMlo2g6hwdTzn7089XK2X4
H9MD7+GRTiHDzW+rTzggb54e3tdJy/89EAkKJjxN+CsMmdwrcWBD/5Jw708y1vIh2ERgYB6gp97g
jqGDjBRQABnC0CG7SxQkWj+WE2qXVSyeZFQlK6KRWPYsD+OJHEMI7GTVhboef+9tUiq81j+4vyke
yaOK8KVQDvtzMaP7kDBx1a185zXVfFwd1nd2P80tNWyiuxyCbSZL6iVsYBV0Yh0ij0JIL/jQlyYv
qo3lTTBODwupF39qrfy9P5xWscMoF9USY3S0xCgPsRsaxBuX+Z4b9XqlJJ098vXN6vVayupiJyAS
Kp5UL7xnME+mSbVDMxqcXL6HegM/ewbhCiOftP75lOPsNxcGeemweaRbGUo5A+D+5s9UaX0EpsPX
rkcM79EXsZbPQIvYrIjKPC44HolQ1WcHFLNKwQfNwyvKPlJiYdAIYztSSKw4iQlkWP/n88GeHDCK
Sb+yEknhPdzTii5S75gccXadnqqL9CcpzE2kRGjtyaB+MfNQ7N728RiNpAHQ53rO3mADm86Bys71
pN7erPXAEOc8pxTvmWp5yRB3fADFTgpvQ2fYLYeRSxdDbEkaeT+RWfgvLDIgOykeldCQC7C7tBa/
AyVwmrXUhy9N262mMn97iY0FAEKyr1b5oC1gje9b8QrA70+g2CZqrA8cF4UWuQ92TOrJ57CwsI9W
XBy3IrVdJf7QHAupdSNBN+HRnj+5qMDa3+NdhzgRsjE6ai1wmqXMS2hoWzZ4ZAz+kbbhbCHvWWFh
iqSzd2kXjfImmvOntw9AF1jrTXaGwAK6UwzplULh+B+KaqRn1NDiu+95hyCLZG+gbK/jMUJBTEDC
8/mvqHTy27CGQrWQD3SVxtmkC75sNMCYANiLHVAOFJKKIYLeiXBedyG1Vf1h7gCoYoNCij4b3mb4
MCePL2zv20+ai/6sK9++yWANCf1Ytj8T6T8nCIoaECBMlf9GZkjby0E7VqggriREvb2LP8cy5lkv
WnNlOhoy/UXM4igdJ/qrpHwpituegiJfZjKlpT5nQhMmOPrkHMxWiCPRA74PYgZWSzVF154O+4fD
5OVIjZAJ2IKbRKJKmZi510BaTg7XrRkJQo8GzL+ha2mWqthJToz96hW0D7PDDh2kUuUU8fyW8MMS
EBaV0Ldaq+ZMjcI3PtamvKteXswRKeFb5cHtaZoSjI5iAZZbmOvf2ce0wC0kJXKCaW12kJQz/UrH
QKSMJC1PUjHhLsUUq7l2YJQjXRAQzuwzleaab7pvkIUOPw6tn6d3IRvmHe6PhaEmZ7MHCebB0SgS
H+rHFD/A2UGouXuoAoCEQRtKieFvduCDd4m02+3ri7og2RFNSyduw+BpFAT619ICFheFRdnuwmUG
TNntXYQB1F5jUo+FBVKUizu2Y3mH1sl1dWAiUMB6OvxOgxhIMz4ibFGQRguQLz96P5pDCNRFNnPa
rE/JLRVspmrVMZaZ6LLgLv20U6Bak/SpX0Y/xGOiZVlUu2+KBZPNeJmo9VlmEMdmq89cxp7hORfo
WPhyrHR3yMCy8+lFYVA390Vh00YRMoywGV6e6+J7iAeFCQKV0WzgA7fHg8b7nl+0hIY7bX9VlbAx
F0dSbSaDXMYxu5BpcbULQ59eYxrio7RT1WAB3WHUYqpyaikvqZbtM+/DHMBiAshYPXjcBa7ZZbte
8yO+b7U/08nybyrLzcSWgThCAfAm25ENs6iAlRAmKMUpPYv1Rily27zWpR8ZImjh2Tvw99Y4bDRM
6hnDNl7ra/B+VeuckJyZpKB2QLhQD/Rg9Zc0UmZ2PF4aqEYq6xg1tF0txjBOgZyuu9DGuJXk5Xju
6/QA8R/LA99OlpCKbgBPiaMU5wxlpmgWpuSqFa50MhtVTD3LgVP1D2pp1rgdS1kKiFi5L6kaalBG
gw/aq2W5yZnuBC3QGYdiSPhY63wgBdTLKSp57N8xjqjPEFfGhK3IxLOclMCQA/ufwOD+X7r3HOaR
FjtAg7/q3fEAUdsqrxxlD0vzw+iDdj7zpTS7pMfiWyz7ydkB5DWVI+KuhAL8BHyN4+fGQsCZ81qd
c8bHrXIvtPekEd5LxigYkh/ezpQqrPRB9d9xpT2ixwJXdHLAsuTYJkzkqxFSgx1UrYOcqlSa8rqJ
SlBohPfnxVuHyHPz9XLHCCt323EiYsUU/z6Smr6nwmytxCHqi2qm1yz4UJTQMkTvVzAQ3eGrpojh
Z1u2b8cTEdmiziH4em5lzQbgluxYi8xusyQxNNVewWfThqVnZkEiB3XUxQCniT0a8BoVDy0ng9hz
ggP9lRRzX7tNyVJzlSVUqDrWJiQuAI6+WksXyHNrVbQcyxyy2YEPRlGT4LbTv9xFfNube669EYye
2CjYGCFNKqXPWD5b3Wt1hgFc5DfG1PBu+lHQaY9yLkohdcP+DSYJI3gCCxtJ33RI7s/ZwwZT9GQi
+MbYoazTW37bMtOVHdfg7gEoNG1hqZB+Ef76I78WzjSBNAxF7JDTuQNRTxUHhAsDBAt1TtDPfUPC
70sQK+hJYB+VfGb4xjQ4HaCUlTgVDj+80z9d9eFnW6skrS6QMx+sSqIJbIPycDlT1dyq6JqtPpEn
xmJWme3m++FLnBg87RfISyDQfQwr1iTwP1hItBq6YpvgRVFeJXBkyeMgJue5fiwo2jxrvyww+U15
NglqbmKFRWsD0lFLmWEAjtctHZuljorQC+fPb/Yi8bp/Onb9z1EDQPlW/3wz2GLPEcO8V4tPnYr8
OGWtGLsKqiRYGRVABoCsppXuNy7LeUja3jjbOQWC4pKSz+82kQ2lDYGuHcPX3Y8TbJEh8/veiEwE
kmMiTTcdnYNiuZmd8UAQUmeWMaNjHGs1ZSZvhchr8Jgp1FKssVqm0rtpYRO+iSyxGB9Rm5avYrhH
gsb+dJMoMwXMhYJIs1vkrZ1thz/lFlwiITG7yOaXGfig+LsW4qUxB31vnfEMcJkrqYqM9TKuMQnd
kj4XfUo88Kj3MMWGSvv/GcNAAbegzEV7TbOfxgDORBwRohh0A8XDfzeEMsV5+gkSXL+9AKWkHh8V
PC3ngwTq3+OSsoS52MjTUvEFoECrqKDowPGkdJbo43Yle0WTs+fOFc5QIu6I2/jf+NWXPMsYvk3u
jXvqwdqhSJLMeUPLNXdmtHcm5A0vhJ5Yk2NCWvAfxjytwdn5EdqVyOUQaWWt3oX2t5Pid04xVorf
xcldtwGT6bc4bPIxTSK+n7RxjoiiDeo1dQb4naCSozhgq7SAJWCHv+l+9Hj3y3/PXQmIlNS/OZNi
sYq7UUaVTWLGSM1mx4hZTK5aDfY5txt4m0gWEBlbqHF1jsXvPAHHjcRTzLBFKkZUHyI+lS9pklyH
3ViRLpydaOypz8rLVv5kSZQGDQ0vpeNS6WVLAq6Wqp/8T/GzsShb5rqaxt3k006T4o6Xe9jZSq1D
wtEJyb90HsQOuXvqCT2G1RZBEcSR6vP2FFspmAKMVDAgz3hraXBFBcWc3MiaWrTE2M9ISZM0IEDm
xMfet49QkflCW9MBmH8wyqXdx5JDw0m+KxU8thPbY7tJnpvR32lgdVpF0LbrQfr/sPzUV58Mv8dF
5XDG2TWdMedQqHdHcJvT7DkrGhJikJCY2yKd7/SZy27OYqaJY1Q81ySCbzBV3o47frcmccMpdfpx
MnXVLsLfiCdO1x11pjGWN10sdNfB5L+/68L3cJlM782psuzSRB67TebR2FECB9rMdcroNwJgw76W
BH1TLVyEJdHglRNR5hID1+bAb0CyHZ7cgEYoT732j2a3w1KCw5xcv2FnlH+8VlfI4T3oXnO4gFH4
MSfGNeZZSACmE2hqTHpVq5HDWfWJ/7C5/ICHT9LgV85H/3h/FEnagDSKsqNQYZL5g9ucFI9Y0byn
ENy4vjbsqnZYcAf0tsT4LKQfoED4FBgGm4o7c1jzamuQgJGlx+s9dOojNHCsG/6QNjPv5j3mcfak
cUCe7zwi5vYPFfr4owv+vDI8SRyAGSMB2hMCngtO+JkCa13M0n0v+gCnHlEyapHG9Ku3WZL9cTRF
xeG1rqHQTPwKV/dW3VJREdC23KRz8wTNPr1LyrQYPVuZbrGgn1zvvjN4IgySgbzu3Ct6Nfc0zZOC
FzDuz6L2FsrEarSNjdXobynCLQpF5YIsm/OljRllnzt6hc5sirEvuEEFg8gUr0Om+iud6YkqNJKr
sghsnrJOUdRWCnz4GWTLTcd0ZrCtwveOArRIEZnYp3emgazuNbqakTQnu6lFgPylmSzpqj5Tbflw
6ffrAcPwH/vdm49W/Squa8PF9WmaaNbqIb70WpbHaQ++HN/+iV1ws4c4JRyE2chIpEuJijDRGwCT
09cIMhxY8D5O2bH5AcoituGvslX53SFgU33o8tmb4+ETIenczpkXfDyO22tYXTT8vnTgovluhvg4
JxlmgMww6PFLwC07KuTFHsv5FYd+MtmbZ4T7fwTQq+0gonze1ch4ttEmakZFwUJafeNHL9AVKhTo
Cd5D2IPNq2Bh6u2Bjbzd2v1wpMRvhypXo7TQlVvZ5wypHBkt3P1gA+GC3X0s6gVSE0zedL6AN2we
yThzztPW+TutK72lPPkO6iY6
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
