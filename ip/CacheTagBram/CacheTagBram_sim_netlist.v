// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Wed Mar 10 10:42:23 2021
// Host        : DESKTOP-0MTHLTL running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               e:/WorkSpace/LXB/perf_update2/soc_axi_perf/rtl/myCPU/ip/CacheTagBram/CacheTagBram_sim_netlist.v
// Design      : CacheTagBram
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a200tfbg676-2
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "CacheTagBram,blk_mem_gen_v8_4_4,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "blk_mem_gen_v8_4_4,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module CacheTagBram
   (clka,
    rsta,
    wea,
    addra,
    dina,
    douta,
    clkb,
    rstb,
    web,
    addrb,
    dinb,
    doutb,
    rsta_busy,
    rstb_busy);
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clka;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA RST" *) input rsta;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA WE" *) input [0:0]wea;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [6:0]addra;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DIN" *) input [31:0]dina;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [31:0]douta;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTB, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clkb;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB RST" *) input rstb;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB WE" *) input [0:0]web;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB ADDR" *) input [6:0]addrb;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB DIN" *) input [31:0]dinb;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB DOUT" *) output [31:0]doutb;
  output rsta_busy;
  output rstb_busy;

  wire [6:0]addra;
  wire [6:0]addrb;
  wire clka;
  wire clkb;
  wire [31:0]dina;
  wire [31:0]dinb;
  wire [31:0]douta;
  wire [31:0]doutb;
  wire rsta;
  wire rsta_busy;
  wire rstb;
  wire rstb_busy;
  wire [0:0]wea;
  wire [0:0]web;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [6:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [6:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [31:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "7" *) 
  (* C_ADDRB_WIDTH = "7" *) 
  (* C_ALGORITHM = "1" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "0" *) 
  (* C_COUNT_18K_BRAM = "0" *) 
  (* C_COUNT_36K_BRAM = "1" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "1" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "1" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "1" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "0" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     5.9043 mW" *) 
  (* C_FAMILY = "artix7" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "0" *) 
  (* C_HAS_ENB = "0" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "1" *) 
  (* C_HAS_RSTB = "1" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "CacheTagBram.mem" *) 
  (* C_INIT_FILE_NAME = "no_coe_file_loaded" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "0" *) 
  (* C_MEM_TYPE = "2" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "1" *) 
  (* C_READ_DEPTH_A = "128" *) 
  (* C_READ_DEPTH_B = "128" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "32" *) 
  (* C_READ_WIDTH_B = "32" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "128" *) 
  (* C_WRITE_DEPTH_B = "128" *) 
  (* C_WRITE_MODE_A = "WRITE_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "32" *) 
  (* C_WRITE_WIDTH_B = "32" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  CacheTagBram_blk_mem_gen_v8_4_4 U0
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina(dina),
        .dinb(dinb),
        .douta(douta),
        .doutb(doutb),
        .eccpipece(1'b0),
        .ena(1'b0),
        .enb(1'b0),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[6:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(rsta),
        .rsta_busy(rsta_busy),
        .rstb(rstb),
        .rstb_busy(rstb_busy),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[6:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[31:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(1'b0),
        .wea(wea),
        .web(web));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
QGLtnqZzRetDH6gCWT4Js6wuLlZfrNx/VJp3sfR2NF+cxypO5AxN0oDKLJJtmdrtE/ueNDg+Qf7Z
TqBNRojORA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
B6Ger3hRvfjHkaJ+W8639Kl3TzC9TogLuklOXEiMNdc4Im+DjEUzxb3DKlzu0VW3zxZqjJ3+wsW/
LnRmPCESi5Y9eRJaLFXg79EMfoj4X+nTdHAP6yCfltBADKegZ12gpnB/8ey5yn2KA74LUtPC7jna
iyjqSfsWLGnz6UdXzwk=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
BX+DxgMPRyZbYojCUR9Sk8Lq+3ZigBz4yMFHQkmurfdfDzyTPJCE827eGiPyTenK1QPVhEtf9g06
0BFXq/0COPuU1BWJwdkz1c4dE6/exDwhvEh+hPx3vRY6z8fDEf6aGVIXrHDvrmddehe7yMSIpo+k
aXHR06EEdfHCFY4TggYwhcJVXjkE+ApsVuyfmEfPmYjo8hCWyQyBsUWIOY03q1+MvUjjsmTwgs9g
fh5MY9ToaLfoJxPKdCpsqrBX4LJ+VDGFlAqIcqHTE2jCmPiToZAFXB7fzf1wDjFCBlJyFVDBGi0i
m+CouLSb7X1mvVhdDZgNrZDJMV688Bu3o54vew==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DaIU/Ddc8USbZ2mURzujJDWDH1JbHl5tFVOOQ2aVaUPIA71yyE38OXVLEtF8rNmujYH30nEeQ+FV
LVJ16aaHw+iiuaqorTM3K5KLohVlN+WlcEtSXHuPNHjw8ddqtzpaX7pH1zqZH+YmfCL5oaNLqDH4
rkBnUl0/Gm/hzSwKjYhXGQFYQ+gGP99OjXakzrAqZzp/Iq4gt+Z5902/JV9thd/isHQImJ0QyK8M
EKM579iPAfXGes2mbiNYHcvDmSPYmW1zlhOE++N1EKeea7j/msnKeyhlC+hGE4Xfn4TVvqgQexCT
rp/wS/MosY6WH1aKFQlFH2hEppA7KXUaQlvG+w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XmWoAt4X8hrCJ5yTyug4ajJW5UhfkLNibzjihWzZ4Cr9hQSvWZoTc8rjGsLPbz6Le+/9iI5KxecS
eR0wiAO+G2IkwhZgVBeZdKoFnlnTVAyLjk9wMAFXNyJZM6b1NDbfXlPcUsC6JePvPlwwdWknkSsC
r3KvgkWAS+O3xvRmaNw=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Hw3Y+rShKrXiUViyNU1/O2qv6TgheLHBnFMj1i9MUGrHYqh9pLfLYUgWR7S2vj4jv4S+Ks0BpP4p
dKEqVAFmTCfQNEUHaVcFPkOHgig6L4mhLY6HUUKJoRgiQepgLi/W3V+ZZPQSQFkB3CU4MsJzhXvR
yLcpDriZy8cnAHD87Zi5DrNGBzj3kigJeM0du6lCQbxtF5aEdoaNP+YTnIFtcqYhoYnswQlYt0sV
HKgFA8VzqzL5WYnpH7+1IKmFkJBHkyqHCa9wPK0qCKnxkuDj70YzPVqQ+cocdKU+/gNdpCOdZlci
F2HTxrgfrXndJru3TiDqu4UavqAe0MNuFp3t0w==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
XPVggoWL6aXz+MpODTOZhEUQDa0vfEnUDaYeEHXm2vGyqKJujN2c/FFAFBeBYdJATLsIsQ+BqoPc
pBbcFYXDBfOtFIW2dH6Y1OoD65KyJ/hAq8coa21kFgq4hFat5vzZ2iIfkCpTUr4vDZO7Xne8cZO9
WsHffoTCt5rS59wWm2b8I5R8Eh2TUbQg3RCyrcnD66cvcEnlXe1CNMQ4/loVJpA4IBinBf820Wjc
vw2fZbGI0jXC+ACSHOviH63Xwmn+aRV5Ppkup7IYoon/ieKapRQeASu3TTY37xSBXiInSdtMTzJ6
+4GfO4eSHVriCk/sWbuTBzfRzoSShrnHjzz5LA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
L78XuiswVcgO2gtebzL7SA9BC/jJGAM0v6S9pzmyqL+QYzRneiYeGyDmsW33jEVVSTuNjTXkBLY7
yTOKQruatwe4V0OLi6174saSAmPgerSV1GyLP7KhmusLV/N61avC9TPam+tekhKeE0tds4EnJ3et
4JdLh+SE4Z4pcuqCjB5MFneIYKKWDx7siU6oesAQtoSJOesfMchX63MhOjOHFP/ch+1gHv3T45hg
IGF7V7TrdREVE4f9631tlVJ1o2Dypsmo/76Itz5WCGlTMjAnWXN8IXxKN+PZ3dyt1wjrZm2P/td+
xiGszFnSLrRvw/HferwtSmRx8q0fiHZ88roGTw==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
kDX5kq2QEe25429T6vQqBCFvV1McKTJRYfK99ymVNK2GGvGLXSzgwJHwB2fj9rM0wme3zYYY0vQR
x+9F4L7KLlOVY6qY3LB59uDzyXBI3mMZaS905HXHJkdZHWtQWpfHhl27LqL+8FSluaD6F+KFfYOV
CwIOVuCIp/XjxFXpNBik7YiPt4kHOlDA97IXNLnYUn/g1csGqeNWce4UTne50ggWvLYGbTFGmTjT
N67TpUiGRVRCSv8Tax72GWFIMFZk3Tlp68ZUSQEybZMWX1U9XdMdtxfvNGhf8mi5jQJ2SupSzKu4
T/+53IN9T8aLePAiGBKKG1ZBj4y1ZyYA7XYvjw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 34560)
`pragma protect data_block
YxAsLA9M+M8qz+AMQjNEyPqrTc5ZTjhD9VYUDd9mA4XbBTBA+6jdaYykZQaZ+sDqlnHKQSdn8qZq
PDlNbhQ2E0ph+vZgQlBYn8m5lvieZcGA9p9LISbHh7aoU9Xe9YVgVHW4oaKYnwI6V5PmUkq9qUy6
KJxFsZoyp8aX9bzBvZqHFi9H0R98NxxHvM1ERj7msvnaaKlhKxwaJ868CZnL9YS1KSCbOnuWQYkK
s7xCHvP/018u6HoO3IL9R7cq3oJeyPmJC/kf3EMY/1HRJH4o+Rz2Ir78CjaHOr7o5bdQvALAyT10
I4AIhKumAod+SNcd+cnek0mVi+0P3GYZ/AKwumi+PCpnxGmFwotiE+lPp1uA9c/kLDHxy0r8O8MV
TVhDsddG0X1IYKCPhTnxmxNH9g64hMeBvztyO5YvK4SI6rFx/pHouFUiEwuu87iiSxRADLkxH6vj
J8R3FZWYkr8kNvpryO3/ZmifXpkywYiFOKSDu+AsXBrV26P/goUnpiz+swiIo87apMGcm42y8rIO
mv/1+rvrvCTizwsvVnLrjce5BxSRqsRlTtB7dKXWdKdah5toHLVqcnuuDx8U0rwGf4FPVw2LwOK+
P90sHJTgb1UTQditMPTnzXRDfb6zPhRFcS+9KTA+UIzwB/Vhv8u/OuO/FLwmLw1iKjt8tZm9f3r3
7u+M4QzJWGz0wUSWqwxHJVTh7/9f0NlOociYioiv84pa1aQt087IniVCcjnFfjy2PS9MzYYg7yZ/
RCLGYAQfhV5A4QeHAh6nBN80AvgrdGpzVdpm8D+HelVkv1bcQYUOeWDn+FycUPhg8RF2FT2UZZ/X
3ee1TQBqQT/VF/RDP2n8mVBAoV8pO+8eecJJYF8uRaoO6ywAma33z0+C25CbYWoj/VKPVIoMORvC
8nK4w1rbib7s21OxlNZ2g1fFqGuhnwrJDQDAWeeINCjeVimL+COTzpNEqYgap1Ee6hq5dg3Zbnvy
z/p3cc2hOJitAFDs6VKP217ihsXUWxu6Qx9ZgY26AYkB20N+J1182WJi5yFWgjkhqGtthUMlJ7O1
D3xLiuXKjhFHUbiJGMT9YBjlZpJ0Oti9QgFyI+tT2PR8UFQJ2xsyr0TAW60OkT/+5zeuzPB+YQKc
qFItY1jVBIUqt+r0nLJKxBEX6v11Q6c1YNojfi0k18urLV1ZESuVz44IBk4+fs9epeF7gEGQJhe8
73mCEM1tKi/Fhy2n2jKlvDB4Mj1hOkQNgtw1iIlXdBkNBddNomTnGw73JmkjHhCB5oj6kLY8rvaX
7xoxfh4X9PVBmxIT36AgVsJq171O5eFSiZO6UraxOahwX6bWKBTTSGUu2UhhYjXdo2nUwZ5GMj/A
OxqIPxMZUeTgejQURiD8jhBcxv7N5mLpFOn0GYQ9yaJWBZ7lUtjYlcC80IO/5Z6udWRfO/sDsU+H
uCBs8yzWhOfKdTmTho3h0rt9fkIGBs03ur92l2DobTfzDNzcZc8G9iS6qZFJtZWyJQybSO0exLbM
D8bnvLe7k/OUeHVV51ryK7QhJnY6YVxPkcNlKq7svxqRtMqB8yKe7UnRwhL0cbdwgZSyu2Do8ZLl
0GW8LsYF1WQfq3imvjuzgWxuiG0MAIrKJ3IROd4rhvcLsZU2Zt0Ya5X2io/g0vfs3lA7FqKCxcYH
9dt5fHQvgGEqrQy7UzBB9/lZnolL6xzt0SA1TfT2Ujzkuc+DwbhNClgXJ7nohbFUu7KnEu5ZN2WD
Z94RW9bp9TC+fJVNvTUzthmKGn2YRtMRM+e30xjkxlkgJJ+TUi6yjP1MCrKPRfKX6jzKEZeNhjSk
t+oDUbaBgdaxv6laU3sD3o1jmz4CtaFrwdc2AXRDjZGN/YLbJt2wslatpJ1i3rLXenkyeR29jG3Q
F3hSwI1VP29NPk0qgh57J8NAIwjEmGhLKpY2NDngODE1ab3OHp8ve1qb0EUtmwRdmCdOViSUjUUy
j9GyCf94yER7MWqmQY8q70wE+Qi3ev3SYJl0x4W71FhPke4H29RlhnAEprXlNoh1k9KEjUFrpww1
MfeEanp69kGaDzbX+En8Zr1ciSLiHuLr+Ip5yv6GeeYtKiqslUwgDq170PJbc8v6QE4PjoNfEEyA
G7PCbe7/IiqQk4RFIZm5LC7fUq7qQaREHDPn+Y74GWNUuFKFsbAf3rjfM++k6X2Cqvy5XWW9CCTF
dT5LSTlVMXKgb640Jv+xrk3uKVBG2mr+psLIIBa7GWfft6scQbSjiVaw2WE/QRHgBNx1MQ/I+ln1
2m17+YzIPQkdepgZFRM90sDZVdWJ03/uJK/l5nxCxsk6FzAtal2wqHZP5KsiE8ocm840oHpT/ADN
OFGIXj1t7V7XeQpuAJpGRvN9N4M+BOIPoCpcaBE5RxY5a6W6SA3oZyLWjst6CKrGHTfyCU8uYNRR
MfMSAfFXSXq/scvQXI+jgyWZhXX+60Qp6O46dpyjE8zu2f8V26SZz9n1ypZgKHGz2DwySBBPPSjO
/MN8nyUdveE8sajI5EnSbzG62afgkSEszWmf+ujzHoinwTjl3GBP+0jcPDNu+Oxk9YFxoCL0fXLv
KeRjz9UvdETz+mgqd2PczwpTJDPipCxZ9XPVAxCXHW/llDU4QDBgOsVoOet7JPadAx3AO33gYXPU
xVSZOkkHlZjxBuJDEpDsQg2pdlguBQhLhzq0sJEe8l31I82f8LtfgeIXs1/fjflGzq04QIX+cyVq
FWFUtk+EWruNjjK+54SUltjP5Kt/kzQV1eaMZN8olTm3IAC1OhB4U8r3oDTnpF+rKT6gRywnplG5
Ux2HcoKsB70i1oof2gnN2StvWp0I5ep2aYmGYBjKA/uR9jFv9HotkjG/RzOBdBtmfbVrdCl6OL0h
QQLLXhrf61EeDe0zAHX1ANZJgQ+4sQ2c/I70qnNXnjhoZ8vPJ6ZICmgeS5dB1/MsuhO43pVTF37F
M/oThUYRyhCFLKGf2OGTpPUU9SDGYS3B+1Mp7AYQ1yMDXmBJ23upkIsNPLe+yNw1zQwh5XTzk+3G
B2QfAAKiYzq6XBobi7I+omWKbSz/nN30gjkBksxEGKJOXiPA/hH29HmgrWluJ31ju+HOq0EQwjoz
ew9ue/Sd6LbHltXEcjG7qUCIBdyqent6oSldXXaIDLdIXvyhWBAcNlkqTh/UbdTRbScF6Fx+Io0x
Om1nz+Vg289JfEZS3lWRVSKNUGTbej7SsxecrxXh0DNaFH8lzHZPAoaYC1zrFMizX6qytV6qtSpP
b6C5+Ik/mxdU18+tUs65eYmSBc6IcEtOMBZ249Q6nF1e43o0NAX+B2miNJn/eEOCfyb/AnB/7Y4u
CrTfrGs3W63+PVCHphvJ/qp30CKrHHYX7PoCiHXEQoFHTJBOLIKIZKWjKAhZEngk/DLUy/mIJVFP
V6hrBVf9fKaJcktp7h84cdIPRpexRXDSPcsxxu3iHnfHMYWW9sGpZ5VosIn4jqdIczkxMFjfWfwF
kV2EOehh8Fn8wP6sC8sCiyqE/UQCFtjQuOMqUs9+2X/bzaUBzO/XgpaYjvO+ziCqPEAB/iL1Fw68
VOaUSaVFEMwfWe/6TLoGmUsyELMmz4HDmWbTYC3cidoIaQsfW/Bq7A3ZRWYrs+MU/SpYTXAotsHB
yB1Gqsuu4wazthkdSsi4+Z+8hPumluZH0eVMmTB9tfwLlJ/N7O61s0ulTSb1CDxGwq+u0XNWUcn1
scpx3+Apovb7wjYYfr+wMro8EBmJDs1DzoZEAaGhRUFG5ETCo8Bzd40svgtNCitM4HJNwYUrSAeT
ma/WiOEkwhnW2m11gvoblMK5J/o3RpJPBZbzhQXdxrGG/rU6/kSTHz9vZN16AheRdPE2GQLIE5c8
HWStowYRpOh5aMqtNxk3Qrg0ZAMPvzA1/AbVAHkRjKIT6tce7MkkvNj1n9+mnsf23ywWoddc2Kwq
wRofOdI44lQt43eDdre+FrbqvsaBo4oCq+qPdBI7MDaP6k/X9oeqlBiQawm0ZRejF5uTdncf80Ja
PtwNuIf3O7mjVbxrzaGejHVUodBqJ4203j8lja9GM8yqF/Ss57qqbXmjCtKeAePKOIRXnKS46IKA
xf7lc8+8jTUHgBGLrR9+nh0kam3FgSfHPOLsYwk0z9EKLvZIWv+rrAkAuli5Y9XlJYV6ZjCrMhpi
t//eej1VHPZOqd7Z74NMY0DLTQPelTSl27TIDa1k55SgIn+Ht1uYFJ1DqeoxTpQvCOzybdMdlnvy
F2HNyxUiVhKlMjr6HoqxD9v22IIQ3OV8XtpN3d7xMvTeMJL4LH4Ej7pU2POpEUxwBMGo6JktFDTm
UoWFkA4ChP1H+u39pRUtV/3sy0rEIOoo5DKWmTyxJ896NqGO/HdTbSoyyOn8a3l1up9RpHTHb2Oe
OiuPeJ6RYk72zfTIJV/Vvl9NjqGxHBfFFj4vfdNH3lnk8XEMnDSshXHbMywtr29Z2QAgWDKQK/ac
t/m8BYNiTFGKMq9Rr9AKC8h3+2j2Fv8+3eNJ3MTAUSaZt34NwNPF0IWxqdeliJ723H61yGK31TaW
JP0VoN+pSulwl1hvKtoQbh5mTvsbh8YqDhACOK4JR1ZHpSI70jShraHP9pW+7FNsKHHyEnlLcrmH
3N6j+ieAAZgVWZaKtLxvktpww6PIvxC5dM99T732Zlq1rtgDn1usbYaGLDPG2N9u3JObZMfOOY/R
9S9noqRqkOAI0YcqFPKHlagvNcaHm+bouytDbx5OigGJy6pNapqpfO6NfiaczbKF1mX2DCjq87fJ
KtSYgJ8d0Y79E9jtXlmGeUI9qWqSMErHNnMVJrKG1n2l0eSUeTU1reINXQfZijCELxe4ahMhpzPW
PPqe2X5I7WNavQ5ok7VR7KYlXtnPpH5re/P1buSGXiS1FHwyBaMtUuWI/m1VMDOH16KWLpivk35b
uAOtx3AN+GgkBtofe5oeq7JdkpO16q+SETKiNbseGP82WBRFTHFdVq4oitwku3goT+XQKc1PsyUH
jcJhgFhcic2S2MOzZR403u7BttVn/16S1N1S93c4aQpSf0nJEn7bWF4/+M0ka2tcLliSBX5l2zAR
GCY49N1QKYj8w3/3kJzjnByunG8l5hoMeMwUTp1cfVz6rA3eYflGvYyu5gn6SEdVty4QfxDaOuPt
B9KQp4IoX7zpAR+kYzoLKLL+Ag9Wtjj1XVIAsBXm3miyMyNUy4+4gjRoE0KW61HwZ3kW9F90VxeP
q+nJXJt0d9MsZrC0woN8PXs2RPDPOD+1i1WcZtd8bgcm9cvq2r6fAamUElswP/cogDxPiEvIau4K
loxrjIGMbPaYZBztX1Z+4bm07AU7iz8ro8JQ8ggEB1BS7PXz+5PvZVfJ8mcjKIBo6bXgClGchzy0
39cDrXBB2Yhdc44cGn1UwD8ZqhcJffR60qg/oKnMJW+xC4qHaz83nPM9QKSeifJ1PT7rRID9Aaob
CMTwsItJn7yjIkqji9Q7giePr09BrGeeX8TpHXEOTQK6D95udOm478ExN9ZnCeperRDrVnEVm4KQ
pXRewIdF+khJOwLf+Apj27sCI6aj2MXAOl96FoLkOrEiA3M/0qQsljxcIieyhADIshfIfMLxpRC9
ReKjEnicuOcLi3PJqd0rIojmdg2qKguEN+n2ttP6Dk/1iPaQ35j3KSGI1h/q+gVc60XIcP5tQ/vk
u3bZk65BLjNCaGUk0nmkhcdlJPMHCiMCGaVwdMJFSGrG9Zfh7nnG77KhccuOApCtivgLX1F2fnkA
qpzZc/+4G5dr1N0f541v4Hg9PtfXkBYXtW16nrbFX7Nnt1KggedQSHbSKspsPap2kqL7qku5Km9U
eZYBzw+fBTRTAMo+RHw0gC1Er7gaP5Im9F47bqHYVWkpYSVXIAXgFi0ud7JvxXN/6a6q9Xg/iIGk
ZblgzJ6AWFKuDLDavZR1+d+X8iz7L2xLw/ZYv3YjF7gT7aSxvL4Q9IBBYxKBOyVD+Thrv2OLnncM
TzP9bA9uQdeYvyZDQdx1nTUqRC2Ba9HmGJPE+11I7pRK3IXgjZiG2vFJJnaGAW2ck/UKHcxr60FR
KfsDiKOS24x9gkMxrLQ66JQYDo1tLOpzINgnyydLh6cue6fUwAoSF9Wp0EI13M+9g6ivVnMwspVx
wbDc9JbwkkUtErizbJwm3zbAuWOf+VOrDrkEN7QLV36HS+ntG+iw2tOfbmBOIblh7dhzFnDyHThM
jW8AaDvVfkUY9i1x5iSvvk+QcE3d9rTHZpVwDR0ssJRQZQQ86RHAxN1jLupYn43NdgTNuoMaMQ3r
Z7fpo2zNlC6NRQrdWj1FozfWJD0IzWA6KQkSyYTSYV1Fu69aVMMIpXxnpZmobLxg5/dCGrm4gDWJ
ra9QJ/BHB7213yyYzs+FlKXbxgnF+UFRewN+XKbx2JhUgt85tQohNHJQ9TgGilDbXOUY+4d5Ca2f
LNq56L/Si4ry+bfD+uahQUhSjgro4o/592dFNX27Fs/7ZU4ZB5lkUxBTSAZvs+pOVeaANwXVUEV9
OniC2hu+FeFVrnxSsxqNlPMYbr2qX6LtX+SJzT5rf9sLfLEei5GzBOj5BovZbs9dwMzLFmYedeMd
xB6erwhNooh466LtWtnRHgYrWF2isBacRL/83yQIRtRDVNw4xv4k7E7q+CM/wxjSmTv1swgALA5s
a6vUyhE0H3+IZMHIK76N7dqiHKDmhtDgOb19cE8qaFoO7VIfWRQO8WBoEroTjPpeu37+K8gY7XDz
jkimE2wrVd/pE7zRi0vVKg444nDx7WEcnFeDELrXs6td5DtqgfclSuakylFWSkMgaZKXQv6Ulaie
jfupNBa00qIFtwOgoxbS9U+wIBfb0vC9QkY1ZH6BblWWaRXufpnLldcdiVpvo5nuhKbkv5uTu7r6
rbEu8c72/7XQ0uvJ551z0Fa99a7cT1/UU9yHQNbd0I8dt02d45g7X2acbm39dtq/99JyQNdP9cNA
x55zGAdHL3l05epbkv8kK8Zbcz7oGsG1mLPRqsDBvhsAH24OywNWDyPvVqON31QRxzVBMevG5ehW
P2bstchfnhaDOTx14vQr6CPmVtQkOJC+owUP9JfbjhfziJFYNA+EYigsSsOmc7fCZ8cLdtHe5z2y
dVu3xUZUzMxY+rxuha/Y/UIRC2sTS+i1ltyuRNvsNBl+a6u8mYHYI94Og66XD9n5kB+4xl2bHu7v
JM0DEQkuxLNiFEiwvdSdGA+Kns8efow+HCoaiFMZtTTyoe9g/MSg9BbTYODg1mGABgrbDyJaP0M7
69YA2a/W973m8FQJDbIVgFyMbj/zX0XPfRQtVYKLS+uHcxL33czBP+zGLmdz6yFkXjnax1piNrb0
0ywg1B/7yHMErHMpc0TzEHluJLP/PFnz3C6/74qLeY9e670VkVyQ8heG1RtQhEVof+PtwBZ9ujxu
okAE1yFtPT8vmftDBz9r8cfMNWoFcSWVWMOqYJXWz+eJFaNDmlTrVT+b+LokKcmwuzt/Ktj62si2
nDg6DVVRVUt9EJWtq0tq6nPz6FGcr4UieDCQ1Qn9zyTfc0/Y8GjgPd+uYmQugEfCOFtSEb0suLkZ
KJ1n7ria3CiPqmiHQhExawFXpQ7lu4Knz4MmApDIxb8xcshyhy/Yg8c1X8M7NbUisHdxk7S4tCbI
jE/UfyoGCGyRyOlHC1fxBJnxvGyJnBFhm7l8ggHf4nh+TTS9YaaTD8H93/YUB+fft5VFIWjpzajn
i245FW0HYu+OdIcuMbchOMHGI6U2Bfpgb7IlZB9nPVCLck90jXH446r+iGt3bCbDkLFZPwxupcKe
XvEzdo3KhNmJVwUDhkGb2pxj+c7Wmf8RkHdVZfCoelM13T8R2EEukI5nydcYX434M3uWWcu7lSj5
6nAzEUxL31gFeOxy8kGjiKghDeB20TS48+JGvIPwQ+yWLZWA7a9A77le59PVMuj5VDgSTSNioB1f
33FioRVenkLCuSkv3h0qMsGznedoLZfM1s30wHcTR9pcOfROJ348xupRbTsI6DmCLZRe2fNt6/lT
/sCaCZ3rEEfzshq41xvO7gNcDB83M1XSK1p3ImHgTLJlNFhtIYslVx0MgPIOa7usHftw+VrAg4pG
VIfJlvVSQS7nIHYCIMvYAe5fzcWFrmv1eMVGTJxapYROMwcRNISc9P2IZs7lxoVYvD+5qMcPEiQV
fZ2mNq6uX95T3PeaWM7GZ/vFjUQg2tcWjlv/fHVaGstNEtChWEzKOQuoHsgiTi6KuI61qzKIVoBN
jJFY98gq0mWPpDwOZqaWYd+sxgFIe+xckHpiCY7ARCEFo3cvhXbki7E8j6z1CzBIFfYBF72JwyAU
3vmNWmh6RaG0F40BcpA/gO8iaStO/gbMj+eprGPCOUKz9kix5uISu+Z1JKXo4KFpIb+R0W5Apqm+
4bKTF5vTXLC/1qh1UGjYzssOXK9TqnIZp4tYv2WjR1wUuhfewtE+LN9WYo4YZfu9mwk3YgVX94C5
ngPwWcRG6SkCvS7kuYq+z1Gdr4yISDX+2b8Ky2l7BIKgx8Nmg1uV+Nkd2oIRr5Pdd/PC/fxtuSPh
05qFeiKJ7GAfi/jvJ2mSkU9A/TOU3xgUIzLBzuOUjbRbaBUSHqPuJHv4IhBuJdsn2P7I22b5gHgA
SshgeubYuUQAMzIu4bkf/LIgLOPJlDndHzPGxY1i3IwOWmqxMmrDZonR2QoH1/A9YnZpOncde2ZY
n5ufvPZ9fO1YgqeY/vWbDC3lqo31LaT7f0v27Sl5f7tAvdhqvYyMYI7IYGsBDSgYRKQuyjkB+mbM
d01sM2mnOyt3MZc1LZqqRdFz17pkxQ3k3gHeHoa0X7EDfUYRxtFlQkhSwicWnydYypeZCtylXwCb
VamgVNsYRairJ5lBg5Ln9y8P69JK61AmmbsKfUtp+Pv9AMW0FSndCkNG6/i1RqHuY0tRg09NeOPq
MNa+S2o26LSkEJUIhUhhTIMuPFVyxHmnOtrWOCL2Vxt8MfbmW+rqST/iuObldNLJjrh4lnsElkot
9nHpUtmX1r2m2QuT6buGNgu3Hyo6Nh6f+z13LSgWUkZ4CoCXLdWwR1adoNlago2RThLp3hX1ZKvu
ZMpdPJzzcHEUgm91jnUMEvYwkwkcL1HdtvOQgnTgX6YazVqhjEr1PTHmEffibsQdyMNAVSyCOMMZ
fnzJtZaRCyMhzZWjy1MwVG0fvST6sBnntOv3yb0amcEtokdf4qmnuv9nxXuYI7TtMn4/ptuHLrS5
fIvqr3X1Rqc/oypVND77t5pI097LiLy6AGssuvsUpOrDp1HjCcNtPkwCyP1O494VZ91p4MREYcVE
Tr2tbcLRDhX+bvV7KasyWvV1zYoSsf7K9lvJeX+civhjSZxBDK3EBTD+ar8rq70Uj4I8D3Mg30At
uZvEWo1CJeRpywfAM5QRLznuvNqt5S6O6b27P2Nd4VpRNjlWWRFXFyVDkUzi67l5H+gLmatTsddf
cu9w1OeSgITIBw3WnppetJFBJ+YH7HKVP8rVNEpNSDAsKNXxWi7gMQsNsJiGOBROlolw3mLL3yo0
AVkUTUtqJm2ZjF8QtGaKKGmfAsaZD7rIfToal+9MuXnjQB1t4YcAu5lLYeTw4keKfL7fYKkMM646
OK8ZSW4fAUKum1OuauOKB7sQY7jig1+cmQdJFc3llPhMZuC5wfpxuVvhv7NiOBELZ3oo6zvIm1us
CdOj0zrjCzWppg4wnPP2wILQxXMXoiAntakvwOnbBGgJfgH9hzzSAGZfqytRIT0z9EQxuW9VRcKn
lBzVZRX5qMIVaEYh5crLqgWxIaaWZZfmqJpIc3xrdkFW9xovx9iaB6vKdOtYITYKCZVA5czg7Lo5
UJcb83WlXUlMpvNmLqOR8dsmGjxCsQ1fDj7Uh19YzFD/019LMS97+CX0Et2Pgb69AFjJQnedV9Mc
nKbUaPoj4K5Ylf2tZT8Xxi0iyxIo2snfrmlRbeWoIBAfQCSHJL3tnpWK+nNpJddXQLd/vGiL6iXX
5JqhwbHSkTi+AYL1IDyH0K187BhL2xrgZy6kvqlYUNdbRIFxxDegxXvvAdGSNMJwyOoO+Pxdp2yH
bTYOojycNGP5dJR++iAhPjy2xUpy8+dJplrue0+Av8mQ/sEhzfCZXWdjMFyJv5Hrp87E0/87xbmh
oK7OA4ICYv09OxDvsFfNPWMCv/zvE/zhMzpBcyNkHp4JOV1hi+Z8yQRM8O3sSxNCRIFBPFDPlJzR
jS2G+NvUgWR1RirEKbdEGAZSr4TFvs7jtAD754iFPLaRturvsn+N9e2daQoyy+1iinPxxoeaXMpL
JD7ppvC5Oqn8JL7TqAkp06O9IsF9O3KQrRQcIVojGPcgZEsDBRp/QIM0t1dQqPcMacUS1CmFg6Jp
T4WciBDOPZClTkwZ/WkR9Mz3q3fUNgW1XD93VZJieO6BXqsCF1mkDwDzLq2JfNcz2ULk3hI5lE1U
vuXrKKGySCnZYFu6qdp//lsYYs7xpBHM3kvek23fz3s0Ti0HBrdvroAXt3VE53ZYbd2sLerGLpb8
CRSLCwHkh47pmDJcVPemPyV4UrITQHk5sO/VNm3V+ax3/RJZRh+isSlRuJE4IOxW73K3uV/yPvQO
y1KI5DjnGhuiu/HrtYudtV6OSbsPbWiBWQw5i3+HX7vu9yFww+YeIIijyrEzKbPvjeYBFhQh94Sz
ubGxtkiOCJI28NwIuj6UGn8c3/oOy5UDno6z/lVtKtY+IvuXhRcDSu2+j6aOQ1+G/7pNhf12qZOV
5MMCWEXgwnkMxNmzAiOT3pqRsLALie7ZxGiNh+itUHOdAeBuFQqIE8W5pbcJKvEoncGW+hUckCKt
ObtjJ/2u/vZDEjpeX09WoYrCn9hEPx7Cs7y3y9zwCF6GXcxlGrEtlWoPw/kbm6vIh+CdUezgqlrk
SRBXDiSa+99txN327K9W+yMcAuBOw6H67UMWGtDsVTfEjUovOi6+nZigK/ZnhPMiP/Nv1h0cGjJA
EFffzk3eE4h96W8CJff5DQCHFOgCliw30rdphY8kpLfZJQC+FRsEWmVWEZL0jUqtf+c0RKDt/u0o
WLx52CfAGlLJUj4+H2Pe9ZW0Moa/+4vLAhpvonNWvXvlsAQolgGAjE45Y1KysO+w2E2Lg6YwxjYZ
Rw6KWYB86lAuvSeHKB4CUmlk4YXufzr/8ChJoL5+TlmIDkJjJG07Fb5qM4bPLtnadBh4WYYvMCAG
CPjh3g8wjdTgQK7wvN87SJ+3yIIZiL2vYZsZn2TnyJqgHYTke3OcahqwNLLUrvHqYQPbg+ifbGzj
QxJAfrKYXoHzeC0xTZVu+nY39iITHyAAx8IR72Xp2g7kST41vbMAvp3Bp5n2AxMoTtOThldiCfOd
o9vrxlc+H3pHRrhS6GzCFzYZmB5TXaKmGJ+B1ekV1eN3QhC9f4z6pqFpaOrRedJ0OIpKBrtE/Pv4
9vkUPFWF3CW8++KZLChqYKXVP/PCon1UhU+jM/3WbtKW+NJfaxqMdfgD9FBGk2yxy7w8UczWKbsN
zySwM+dZfMPTz3uhjlXxDhLiEtng20UH0lZs0CJsxUpwDjZfrn1EWRvJ67kAnF7nEkV6SwMOCLKT
BjTGfbSNDBqls3Xi2HmAUz3OnFIY/yAblr1PtnU+axl+thaZnp1pH7vkyVN1z3bVT2d2HQpvqUjT
GGwlidd4rRkHGyxS25Dpl1NXI9AZz+fFpT60znDop8Y19WQ6eu+GkgTOZ/q6IMzYA6J/2auFRQ6i
t6aAchWPTkBZ38Scj2yr50LHUBwYaD0APFs2pGN8or7MQ7oACa/YF/pV48dAxsQRg1uzkMY0vE6A
CD82nWL3uhETSwgw3RNxqPSc5aAhGJsAtjesdZOlA+U9ZfIHbS+mAw0XJO5b3CzDGaHgJPB+5yMJ
obN8eNW3uoRqnqK8bY9xUaJgY313jakvAnfxems3ii+GUkdHtvbtQAXTyOs7aKimdeUQZmZL+MQg
vdlBvIfIk0PWeoiLZeiNKOIGFVBmYtrnEPBSKCnk4kdy+xqkbRhpIlTFBBKwmPSDqqBHy1gb3Kdu
/dnpsAponU/r1//HjVafSPDKaL710j3VfL2qpjM0oIwnl/Rg/P08Q2/xyFPrqLBGbtCU+EbTUB5q
/Kbj8i8To9DDiLKSj5h/jAWvS73KhRo55ZC2OKLUF/EXadSX70LenLxePDPTTM3Jtu5OlBbonT+8
z6Vnix8R8LAneodcPtlwXtFngQJRAeth1VjMQk1cdHSisrzIZexmUfG1L7RYOQ/ItpTw2I9NGfO5
2xPRwzBFYD2Op4FTSeJ1DIHlyOoD0CpmSVg1Yn7D/5DDgf9SVdfUQDcdB0Jl7dJaoaiacMTDiSC1
uAjPcMgJXfdbVJ0XwyCpIoDeNp8SnaSxNbpE6T7aLsU7cubqgTIo/lIiHpuFCY6FW3zfwnu575De
uWG1QWvKxAFIYR9W8T5Wz2GPnwuPOaVBsKVUQb/Mns0BF3UBYMFK9XZ+wJiMNIxfEbr3GBB4QkFj
fzClNez04+AEcw3IWqe/KM6iBXbklCUO7tJ0Oy4QdXFcUM6Yy87SMLLDwJN5lFk4RmNSEo27nYMZ
17fKGu4T1Kvt7kBPGfgBpJy4j/q8BVkvuNuM8LOjMz+233NzsmGHhGs48cheLPwWaIP8IkxM3Y4z
bM1USTTf8bhc6jXb9t3PdDY45IzNkBVXHMziQQ9a67FFc/VqfNpwa/mioHvni73St7Mzc17V27Mf
peyKm2ABPqYkYOQdpuCXOZcvvUmDwa23ZiUf2Iq4blHrco7ND008EDgWjG5hNx9d9GVz4e1RP/4s
eikjdjWrZYkaSKiMcnuBnHOefdIkEKnedoaJcr3t4+YYkutLS+WoonjNlCIeMv299p8VWqfQ92B6
j9HM++/KWyeweaLOg/CSkmsKmIGJpazDYriwbXP2HHNZBszcMcZSnNP7tsmlycOgrT0sz9mU2+Lo
Vk5bIBxM10VQclPHhLKviptTxStYNkPRZczGEOWksUS+PnCWC+sOCf7ZnzlGgxwKnqvjWMkO8mC4
MBurYx8qCDSA/nuH02rhdQt2etQpYzY/+pIbi4DyhOIT4upajJ4E0826fSSM8dB7ZkSI/MOJQ8gA
5c6CYnhG72/8dc+pioUzpFcqK1rs0LEgT0OP8PjlPBxyOwajP2xGLCNNcOxqqw824x0ZrcTeCieo
d13HwdoMwzwfemiKEgiIKkoU9lAt76f2/MmgRqcEBG01rDzCH6OoCacO6WOLS5rXjkWejaprE4bd
hUEYniX/JXAF6XzmoazWiM1TF7HkwAmaQBd1uNmJ7eqER++t537x1U2x0Wk6CIYyZH5sp7EhBekn
jVMfQOEtgJGWGakls5KQGeS448NGBBNLPz0AeQupH1v+x0HWT0uDRIKUyW2ahc1IiI3fQQ4gm2S8
72+4Qt6R5tEeM+ceT6vZtWrw5cdsyvdC7d50SzTapmCh5GXW+UrF/pg0i85lQxGIZIgEzFfVm2Rr
vED3P0TiapVXHPjEV0L0zTMxURFXaiGH5eOOQJdhG1U6LMSQATAL0Q8tCS+fvRjFDfq9z0Gqn+tN
lUnPsGvCTH4cFfEEEQdDSlxBEBw09+jZeyW+3OYaCKFLmDS9AEiDFT1Kc/96fMCihbrQnYTLcCV3
YdU/WNawCuveQG8Ls5QlaKTZxsTHiehlB9d5X2wUlQZ8V1tBcf+mz3tFsAvLtHfWlDEjQhRlK/Ss
lwi8NcG/xatOcSwHzveC33UTbs5KawKSMtFVn7JayOtjUnwifjc1M80KBJFxidjeUIeiyZDr1SX0
KHt42LBB4eYgwOFNbo4/S/LycITrBrsxXccYjFxRdit7sluzauqxQSW370oZ7i9zX2w1LD9+esDj
IslevyWMUfTb0e+jTtr7xDHh3IRk8vchNWKgzig6EnRuibdcs6/2tFxpcWqZRh9DdE6OWNwiw8dA
5o5suN4+jpnizin1YJQAi+/dz9gAMKXQXMEc+zBxP8f04c22yjuneHMMkAh0vzs+nz0KbEVIM59j
r2WgiXwR+mwk6aMCCMAQnMo2BHNSsvBU8AhkiQgYQAFlM8fN/vhm7PQe83N/A9+aCcmsUyij8s1s
ShHUNnZEcw+bkImeIaRn1P6d6CV2/Wit4e+5QkayvL+stqxUYXAfSoRTOrRiNiLsDVqspBXnjqtt
xFmnGuvbKFpDZMW0+hCDcYfZC8QKq9f0Fmw53HF591G5+jsQli1yElGwoMJk5hkly1uk/NlFBIMc
/g8b6XWptwaeN03VCz1qxCIeNCwl1otc/vul46zGnXQIH6yewOVM44JYJI+geqJ6yGbMZfEEqxzV
4neLbCUnZuhoqZuweeu1umXSJxjgQc1rl6GiXFRyug25PRi/zhxYX43Llu12NFaMfgC5u7Ox6ayW
k+ya3jGR6Ox38bkPwfK8DGeH/6/NnIDRcMlNbNaQg7qO+ZfVdvS7SB9stWDmopVidETyEOSGwmov
nouGLk21BJfM0nnibb2Cgq2DSYWHyHFTdWRMCBa72MOd7nkk8eXE62i5CUkWio5Y3bo0rmissNrY
6u2cZ9d/HCewfN2NRze5ywGQp8MFD2eg9nFzF4w5xd0dD5nQgV5ywPiGT4YDGv/46uUh3UKgtdhK
DFWDnjfmtR3M7yZ9pjPvab3Nn2tkVKjYKKVSyEAWItM9ukEHFdFc5sxRe7gfdXzsBKxtGQvV5x5m
gTjcEgbGq4p9LhcufnFxTLdBljYOi0f+52h+KJS7T0mjkHp3wxhWmoO4LCm70FQzL+UMABheCjC2
L31Cq3HoqpgYx7RjLWbM1TInB55x8mclwznbcuf/Om6jMyJBbU9CL8mE5EkKDx3UK4RbPtCNNLQQ
t1nG+VAm3Kwdz/Cr5HoQ6Nz+0jhef+k3v+uZKjHfBND0nmpURziwCTmG5dm01VVMrG9MweaVLnDh
S+20rj0LHnbYPu4+DHSDPgncLc0F938inVVkAdTZ7v9gBjF38COJX/yP9G0z+eFkLzgWc0uebiMD
ixs8r3vFRsnHOxtQZDuhk01QhSIP4yAzmnMXQW1jiorKLDOHOQHP+iLNEiClzMys35NV0PDrC0Vl
f98Z8mSFrr7sWgXD4I97MfJcq/uWNZ72LKXDsvFgnXztKoDJUF6F0R4kNaeqgk3pGpPyNGEeCTCF
RTSWT1jYm84aQhdoyR/3E4Syb+q6JnX6ttPzDVLFS1n1SAloMFMH7F1vExbKhFvyBtyDNdySFYQO
DZHvImm7+XJ9pMlzFYqv8VABRqBhzqM7FxgjldghbdqkUSTnNDlU+0ffBHVvsjgXYGvpQgpgQ15H
DuMOh5HZakhoInXqWRfS4ngratxaooVQT9N/Wf2omrdASymUxFn+ZAcfHaReWTJLvrHptkYjmjvm
VwRHelrBYNnLC5ONmO1352JnzVC1UcaB+LZ8WJEhtd0sWHkGrvTlsNpTkY0lnOWlYwwS4d0ftU/x
rRbfTv/ZmSGVTVWncBZFzGPgDfc3tLvDjqf0sd2c7KFDoFMDxKhgyfe/kaLhzh7KL89d5EHdBYEp
aIneOp7lT963z2g+z79J5XlgbwYaP7wRIylZMVV8c7bHhla7C9z9uou2XciRz8WKjRwwSAperJcM
KQdTGqhxpFc3852fyonm/W0kRpofOO3tHrTGF+u5dbXI8wufri8YFPLOBa711ChpUfqhI4ZC5OUZ
WoTq69ioHpOwCfD7vAHXzvo+JHrB3JpJPMEK0+cqOQbpJ26MTK91R59SKUqd37OGlXkAFADADIJK
nL5yt3eZBlgMMdla01PbH5C3Rxfa1x41/wtY13IWhaMXN5zo44u1pBZqK9Hd4B3xFUbZJPsbiNh5
GmSf3itISK8tDRKhY4FlQlIGmaTaP+7j+czLQ9VtkehjFvcXLCNCQc0R7boN1b7s1ACfr2fSoBfS
g/qmv+PCXq+agL+qTov52u24t6NPaABGtGGbGfAnlJ4AlmPb4sRGXBbYuZzIgAePYwkHDCIRninU
PIFrFQDrejvdhcmtt0327kgPPg2WOocoTedd6rTLZRd00zoN/51mOgSEXNWYtpeW+8otpmCM52oa
cotiT4OBD5D5mO04N43ph03F3f6wsCjJQzCbUGhZ75z70co2aEX4WDBnrVJe1TO/LwGoOYX7GdDP
/A0p3B7v/Gdfofo0tdRRKMQA7JOogIV+24nDYykjDEpME/uVjgNGkEAIiib1e/QZItOdHVG5Yjd+
LuYIAW3QuWZms3EQT2F2YuAIdbBN80Ulm8V8CcMd4B3tk15ilVuj5OfIgzQm+0pQs9rTDmHvjNVZ
ihabRcKi91Hcp5+yQ4ZwyrkCorCNAbM8/TOjD1EH5I/Sx8fka/bw29169hdsJSKXb5nezD34BrQe
vybIqr81e+twSGrw8SUg/YLgwrzzNlFR5lTaclnV1tYlUfWZI7hfOrvCYYehMgB3yNzYLuI9gvYu
VegOvDFtjqrg7SOlmMovyiLFfr/IkmqluP2Iz+c2N5+C4SbaPf8VlrciL5qGJWSnTwVujShBLnkR
9ZNz2X2+El7U/qgdU3LGL/0ny16f8PToJMyBZN1dyjKT97kitC3gT3x+o0IDEyiOZnoLUWcNAS2Q
ftaBmzQYELrOYIMpqBthWoF24QRF0ySukpr0mN6MgDAwB7yqU5esDI9ylnwsQwzPsbX10w9LMhmb
3sZ6HwAhl8JFSeEmf7cZAXAXOLYZiwqeEmi8bmt/Jer9nDBfR+n1JMOty0OCXK/jfIbFp1gn/cPH
m/eGJtG9YODB4gQkxkAga19hzo9FDLgIgBXQyaIZIizaPVeh2r3zZAbYw5ixzn5tYDpj9nkMRihy
/BCDMepZSePtmwKFC1+AgyLAPg/RaKnAQSNeDVWNmCg1g3tr28T5sh3L+nBaZuFmsSzAQNVHSoIg
1RQQwg/6Nw+4vhI7TktkRa0YNy3lbpxWh5PONZ36dhu0U5sqE6qfLgpVDJsEa+6DO1XeZAWq+K0B
9hFvo8n35Bi0QSx3nyw5TLxrjEcU9nAQToNuIHgopUj2hOuegp/0Nx+Gfxcf9Sc50/cBp6Y1MN5y
f4nbFzQoReOZBET+BmHlZfhmqqaV6BvyicK5SmEAsFTZ2is+DZPkb8lzzEff8JBbk8wt3VuutOIF
yereNnziDnkTBNqVm3UFTWf+V/V6XzOScB36UPCkjgHGAthzodDSseSRbyo8WRVnJDbwFHmaDcz5
M/RgU/q7bbupPxPtXcfgU1Krfll2ZZ6MeyNYwxyLvA9qMOumu0RAkijorTMtQLwk+LtCzCWRK3vg
dbfZqr42wLDmNI0JyaLyIZ054zCJOOwtkIUp8Hy7I70oofV4gsndipRvj3mUVWJ7pbLAh4ZgmqMX
Tt1YKZ21S6ZCVO7S6xxGqlcyw2YC0r5Y4RuNm6U8L7Jmrxb7sGvGg0wAoVnlXyv/HBeDALl1rekZ
SuIB0MW+VzsLjf8tTcWuieztNMBb2dbTJbOxY6RouOiF2vZXGUsKwmKPBz8e764TAMY3fSzIMTmr
kZj9MZDjieJO+CN4XRapjuumsoIskgnM/PJNmfyzJP5M58B8L0htRnD43fPzn8/wM2ZFThpuWU2H
6R18lRAlprjk1tXi6PJ21iIMy4purUr+QN+qOd7k9EtyHxkT2f1Z48VQlsvty1Ik4zuA195TItkf
HdTnRmaZN84YIWYk82OQGdIFtlvOWse28hYAZgHWzz0YBsWFdc02V5kK33bTP8ZMRch7RHZs4ohH
b8/A58h8hYEsbEkH1N96b3GU0/Wf3/HytsxJXNfonQ19Bm3Lnl0E+mAwuxQU7umHrEE2cipQRabO
yKRiJzhB6DMRyrNxyXHv8GyzR0dpdnxISoBZwz7UJAyjGJi9BIcdElmDjBclsCmSgwKJkWwXkJBo
h1B8eDt9/ZgoZI8wL1AoL7kYtpFWw5Pgyq75d76FM8enIeIXl6JqeB4jIwgKo49F68kutkYWj27m
6sdBrT3simeQtr7mkTLmq4aDuOazd1jt+WaKEDTnyDDz2xk9oiJ6/JrmqyBuozmFN1/CnleEGvRS
4tVz2YqfuzMpiIHjM1O9joFRMCahGwMSGHY11qSE4X4G9pjOYBp7gqp3XLWgy/2+7iXqV9iPTBnq
VQv6TOAuTlsjtLHXz5l5okVNZxHDkfpKhdUD7j6ZpfgVBt79ti9lgA+diRlwMpSTl+edDz+BVftJ
h90stTXALhQIHNGB/IetFILBQvHsbyG1UszufJybGFo0gU+EbykghOG0RUuaix6712YzOosBzbyt
Kw2jRW44vIuKtyEYwOERPCE8f/m6H5/P7JSwTLNVc5Hk+6rTV22qiDiQGDZfY1lY78+rusGoHWn7
v8a5njCRMvkKrLe6nHmYnhZUfZgknCrkROeDkUmBvJffXPbVI2rzlUgDbuLTKqZ7t3lpgi/fjF+6
kHXJ/OOxRSF6QuTHX+mQRMfkFYVFvX1NuhDCJ1mzQC51UwTxSKWntQTdTxsVTtqGwKmvLwaeNAJ3
HseueauTzCLv2eelyy/fnsffdd38wVTz2JQNGphERCz7EryOffK04kJ3IG7NnCd9OdCz++dHJ328
5+4EuZMr82bwaWztdFrl0nA+50PNoYOu2UwTlFxSZ+UIEppHXneP9b/xB09AU1WTWIsf0urkhgf5
ycYrV64bYQysblGT0AOiwEcm69nABAlApHILaFLURZDSQzTIAJrQKlagcTAn15cdzL6+4bUBwgj9
B42z/fOOg4F9OCkvam1DPvtAGOktIlYgTg76Yrtqh5m360kvv2Dbx9EIP2eG8z+e1ZQjtawcxpzw
vJMJGLatjmCYAom0N8XavPHuGE0XFVCHAPN6T6s72dvHzA6VNxGeZ7s1UG0TaIK3aWcM0H+Wzj/i
DIrZAWo6/oQjBaLkw9eDX4pCJ3Y+E1T8zpmjf1BYEkG+70B96p+WhIqH4k/QX6v7mYXNDU8hN3KZ
b9/JiESmQkxCXOAoFvEJLpRtfugNo6vqZ74I1aeoBgn4Uo0CnQEGGmqgBOeaUPIph2AXPVvJGlQu
OC+KWyucUALmcZqvuhPhOyX8FxCOILEpyDqYBLLEkO0Brr2x3KEevI/GaE2Q703Pre4RwuMZ6toF
ffqZ14/0ovvdZflZ+u8tDKUw6ChWb0olGcQtPh5DMSPpIKOwtcfDdDq8NkvFlz2Keee+X0/ir1+v
XFFSc3jWXKVXobCqCyV+R1RehyjK5z1GgKDVKnvE0+fVm31j+7PhgsT0hjGTzqyrc4VJSCOla0ql
n7QnBXsnOziqx9iwN4uP3+x+vF+YMFPoy6s5NPol8Ch6Z0uy/wSlIlUee237SwHmXWjDyTrDzN4+
kIiwvf83ncXVBJRXDGUyB5piBS3S5ACQYhQd95qAuZnBvMlzUPo/z7mjuszAG5po274Ntz0LVlNf
LGCEGa/qSU4PdcRe0oLIO/QI1EceR7kSff5uySuQnPBU9yUVb/4oqLLvxjrqDkCYOkqum0BvGKGc
kkFTlwYwS2KWY4CoadsxqVl+HTURX+Gnk42I3X9A6ieeSMore5OX9aF6LtiPvy0dP4BhzXp9nyAb
3OO0BtxvA3C8e4hAb0z4hlvJJPVDJpX1mwgyNs6D/bTb3qxGN4NqwdkmINF/i/90ZyxukFKagT8R
szGRtujxC8+OTdrv8K1wmuwJQLQNgxuV8zXj9XMmUSPPcwtomsabcWKGP+VwTHX226cmlkGwtcGd
9u6U1cGZOAjC9TLB8Hwgh+pYFJ3ifoIhNFuPL8tXmTg+kr3fqX70pL1Z1X0pxSRShnzBfOLytBdH
YFUOeZCfiIp/TkjZ+f9YC8QSOYpf9Pl0XzMmM1CJyPcjP8o8s2QHyb8baA+2UCJZVJ68wqWwra8M
S+GlYMxT8IhjHJC9sFd5e9shjWrO2qK0jW3U1i9peF0RMSplJ0SoKkGjLRJ9fxQ6JCcXwaI03whO
W62ho4l8h03eLUu+UZtDBUtlmB852hRZKT9UOrOF09T9g6jMgGADllrlAWzEedEUW8JWvseOaH6q
38a+RKWnCgd+m5iZPIMgRhTN22e56FkHFVU49t8/rA+Z9SF5o8iCa0rFbR22dwVZ8HOk9d3STmvb
yhJ8281BNKaIr725rKBPVIAEDRxw8zx37Q8sC+6xEl6CEKl5NcTbcbVpsEPj0CQRpi0FcFkuj+nf
4ymWD5tI7+60Pjyg2urMU3Xi6LG93Kxf8kaY29eW0BZgcPgUZYpjg92UdT0i8ARuG9y+nMaw/xm5
VtJbsWSVHX+bHypVy+11cZ5RBjZjqO8E6xT/jofI/RNh6gSk9atQNEFrCHye7o96l3FfYuzLrc2t
Y+LiOw7JvREzA0L2hxI24pr8DFeh0bWV9Wplkw33ZN7pI/F7femKd4EycDGhqcImhJ+Au/A38t9V
OxwlvRkzpQCD2fVPg/WunO9bumxL4CfUwG0VO5knaNHITu46/45XsRcC1fhUn3sJxLLriszuqwJD
m1ZFp82xMg4V5WyA/+LnGeZxBIZmBGGdzhXCquibZ9VzzkpDpn34vk0A6+sUNW+SP7uofLO1cULK
BotNFX+wKLIqnKduYDRwF7sXdt+3sZy1kShV6Y1oxM4+sg/K9GlGfFet5+TqzV3kgUL0tVJOedAi
qrcfh7/Lj02/pEqvYYEmUAsdgOpqRXwA2QRhFEziXT4wOmCvU9TgL5fL6QTvUa/gDwb3KDlN0IBm
bNcElzDdMF6mNipvR8DvuU1vtFuA+AQtd/2NjLVpD9D6xMJD+bWEEgmK3twPfjGw7b7JBHtQoRcS
Ll5F9Rj82zlMq1gpQSQOFHi1jTtb+wvj9WOESDCWxDRyXbRYIl1adyRX/VqQiYl2Z/CpQHwq+nUi
ZnhDJ/xRu2+DVGCUejxdtMsNLORFdBSMiytqlWwYKLjVE1x5ARAD8V0SpYWoAkF6wzXIv9OZ2m0d
v7waHCLDu8Ca4bq6yqe65/1m1Hb8e4CdJkR+wwx1pzzKSTu66FNtDKVA1rs6CaC3AJ1BzIqw0Th/
lWvoaIJUFpE+mZ/F0c7AI7eHWhQcycXEcZ/BDfCoRKsBIEl+iPfmu92grkXfNt9ke5lij+0fD0QS
gRFo3i4LBpbXBoZ179MvdI2eKvf4WscEapIIqN5kcXmX9Cjz7DaF1dLnsu3XAZd0EOoBAICSgfEk
TcLG6+P3nIOMk45BF/G/MUZbxn2f9aqaD+UZ2ZtIV59OvQKTp/igF9DlU12mlQodubF6xUyGT7Vv
k/IeASQPPwozEKQOOT0FtdNvHoDZUVmBRKzcUgIsiop0o1hUYfxV5ywYlWIYj3kbXtCWf1AUPc65
pjhxrlZzT/VZOK0NnfXRXTnJjtUHRa3QK4tqGxh6qbQvFOfnJqmDDnPqLphviblPN013OU3fX1ln
XqOQkgBytabKF3D4zb0djnERJJgxIRki2louKKNjQybfjk+XfIZSdC/clj9r9P+iXDmFwMf5/rJ0
b7fNvqkEio19FIPAJ7t9kaEvhwqI1/sGMaaZCPG7DrTicIYH32YNRF03iorBU2JXJNK5nAxHb0I/
qbN9sB+c6BKuNaMtvFJfounwgb7oPZwgLc8cLWRJfI65/517fp3qo1LESHGMXKw9+5mhjuJjPIK1
UYrJbQqNXsG1axB6DVawcDAcsUWmZXmoLs9BTTyoI+sJQUjZiw4ajRV8WhDHqsVTDxjfqCYnQfhg
0wZMXKZVFIYNwua3xhWQI+9jHNUCb3FI9At7nE5x8UP88PIuAz/jQ4Gph+Zk+2EdzP+jzVw6hK6m
S6P4K/+IKoRLlHoSX6G2e5td8qyfE4vrpUsgB4V6AXMYUDQjgIIDUipZrcgTqBy0yVwMas389mab
JD7RkmLDygvcGk/+3Tw0JZEYfkFGJfVspY43bUAuTnf/y3iV14F987ycQeTtqOLWEeuqLnRdQDWm
hGQ/W847+wx4+SMJTXjdkrxhNZKGBp+FESdtS06pVPiODV70S1iXWa52aGqQCm8cyAyPRt0bhpnE
b8e7gN/3Ks5bciW+82MFEZqJu/gqIbPURvJiBl3rb9xDqJccAZ6Jd5E0QfgFGPcVJZyGeZKqotOh
+IfjMGJ9VLOUbqf5CCEoX5UPEVAaFKw+3rJlpdPeY6e6eTINbDUrolh9pep8kyc7gtGTqbSGvUTt
SXoIT+GGjavNPfNCU22X+c3JCqWItZZRFnW5K/9RH+K+WL2j9PZyZT7n0yL1cMlBjh/c0m7gP2ba
eG3+QjyU0HsFHkXQHFMFJRnPr7v9RiuuLni0vEynuFA8z5t13MI90ZXf/mJJHVX9DMmlIw4mvZS7
9bbCL/S4jCgEWX8ovCmlgtWjOadqP6YNwRNt2qH7KjiQ6vCyzco7h+4QqGXCGXiYRiLkO1U0ZsON
EGH5VrP2K6KC57HQACh2huCfHLRT9U28Xr0grIjYScPGBsBjyN9sg9tC3ekSRtqogIwJXuuMtGzw
OF4iMGNpczDaWHdbZhhgmnASWFwkB7gLWvQEAjXdk1EAlXvRP1ZALWQvaeP6jj9YskA+N0OXf9hN
nilUfuh96bz3A5kaoWW2+e2LlUKgGZwWqJBkI+fie4lMQqtSq2fdKdHZjCbTAbuhgc4U2xE/odzy
SH9lHgLXtmmBBGpsuWNCnLqrlfuasbck/7B1Rs+llMLBQIE4KdwaIn39FHtoGZECubWfDuTEB/nV
RxRMgUH8lMaVWBdUKKZXAbqprThN1lSZgfDZM3rd8htr8Z2qowTwbrbdCwBZBLaFYDlWYnXwLHmA
UkQdE7LCfT5WDOcIzo5NmwVpoQqzmoxdSKlz48GfxgcmJfWEqtjqJARATo83pbltPpoirygKyOwS
5f8ahKkUxdt4AGIzaW7JRZQg9/tL84sgglys+srgz5imjp1K913ynjMta24dCWi6xauKj6QXYIAV
yEwnhTlfS90/My/3c7S49emBr6BbWXL6Ds8T4CObzyJAdLxEieFW7pN42YswSntY14ippFp5gPtz
tgL3VUShuBo2wHgt7X0d+c/6Dnip8wGTgq+xYBY26hwtTTfFkSxDqWdDixvQ0mb9ZXKr5+83ojd5
cMo1eqg6SdhA4JcKhsK6ZSet+FWSe+HWqysIxq9XtSwVM5UIBf/3uCdFEe2cL4MGHUOXDawpzBhf
H90tuMIT/eszDg+OrUhrfwSyBB6zDmFjJRm4KZvkKVeZ5MeqO0fM2tjxa9v5qISIzCbDNNTVVKId
z6YnWuIoDaK10iyA/MiuTjYmTMmYsy8lpYZPgRv+9egCeb7DTln7eNZ0k2LxlSD3SlSJx+gKkMqv
/I/ecZ7Gp/WcVCMt2y9SCxWAyEk2OqBAKXT/+c/4rXXmtWKLXqvDoNy9u2XW89vILuUfx8XozhHI
YxHAv3/u52ejn3MBgCtuJo4MIBCtxjFgtu9HapY5t51kZ8Kb1AgJt1PdW8Cl6MU4oS4h/woRdOBG
yaLeSQud1nsqvvsN/ZpveUnE2HRo86Pmkplr/bXB24gF6wDE4cRjSZrnw5x/PxwwrPFaQ65n7ytE
AvODtfwyohDn0boTG3yfg5xA8fpFrn8Y8q7PXxPvDryE/BKBCCp9R0HHTY5jjlYoa2bGYgXuSG8l
3j5rKJDM0Y2+aceD2hhqMaR2dJjT/ipqTTBEJjPkwnIuusoAbB0cAxbNc8wTJ7eL3ShxiVoRsCfL
tHVxhDknZOyvFyhQZpS4gYhvcTaZNQuQumeK0bQBubbfWO4GvGNmWmrgF9MKDAiMryJtoYQdnJgl
WfUcLrOLl2p0hCEdf5sst3K8pOzyrdyteHcm65VWSA9hswNqQNuWwYWL0lv4DIDmNefh5BNF1KyK
QJszCSlYvaIeTIHkUz5HTtU2EACaRx6lWU41EJhlQI74blwqBIrD+OzpaowDJowAF7RaJnCnXdhN
hQS8Z9ZuAvLAssRJAJRt7gzA77FWMzrD0TlLHZ3sR9BztGbaLiQlodounSOqSuFZgM+DYoDAL86N
qEgKCfhJvOF5TAo0hVWvXWqCpf6lekdmVK8kl9GlkTBKHQftGz1tYeamwD5H2+8rPofRNypGy2Jj
whlRRZzqZoeiFpo4+QFeEoir4aHKqysKPZ3rUQYEZdekYZ9J13Np0awKpNe3GCPHVYDMtCnVdLzw
dXAmVQMZTIrSKqVPjCIORfIR7fUYbhQ+6IYujcJGniJFZ7uH+Eal+fFJw4hmYTEMwkZm1Hx9RH04
rSqKgMOFgD0253lcVZs5NLHRQNAXjQb3zNyJ+u6fOL7cIMbiQcvxF5NpvUiPQrZKd7ZduORAW//Z
gcqo2UPwoBFzvdFc55WSTyplW5wZM/5YlW2+Id3YXgqrnGN93kZNNR9DBYXFdGJkEjh/0zVac6bs
CWI/+IF8Bdo1UGcC0Ms+IEhk4gaNPTKk5uTOyKC+LIRx/6mjUZlYNU1nIp2ImMyfyDtQ/MEqgoj0
D0GCGODAESYFeka/RN43Uj1wIFNV0++b6+aOZFvscqBc7P/0DO+wlC6aNbJDya6LkDd0OCAiBsrO
WLS3/0C1nqhkkfEdzkCP4VnAP2oWZdF2MAr0RKq2Q8in6eiaPMWN7ZY1qZVFtsgTgctHAQ57lzGR
72waGZl7zZqUGmlpUFPoBitt6g4S9ek82881m5IlIWIV5UARFtd79QiVY3OLsTK60cMlXj9WFpoG
eSnFWEV+wYLZS1SOhf7I90JswfCwufoNE/wxpPvdDsfOHjyCtIlkB2hLdMi0/OdnvG9bgV/hr+Qy
CrOH4q5JFsCI/I+qcZRhT9LadPUsxEIeeKOdXV0FR/41J52fg73D6fgtpxbxQ+YfBVV/sY4intPO
ODeJ8Ze4jgxaC5Wz3Pz45IFt7dnGQBwPKGWcdHne85aIYrLeNBkwbQPDRJOOCrZmrXoQcqmwwRP4
0KRGQNpu5BfAlIXn+3SB5d3UQ0/EScf48JIs4cf8kzqNPbdYZlU9g/jVe2QNO3NyVv9NiZ2ZOU9n
e7kaBJVBdNOo5kj9pxsq/vXRNZsqwsYTaMW2xOmWJQwEJcHRgXw76l6u/5kd66CnK82QAQqp/wBY
NmQxSLH4gJpyFRw21bJT3mnd4be7HOgjiP5gJM7UaTHTkME7jCfEV8bxRHXIU7h0vTicDrGiunFq
Urh4mik0IL3pQXeGx5gWvCl/W+DR7ZwmeFF1pT979I3tYRK2hzBMJGeWaCjfHKVGe6i0ZbB1yNEH
RtL6fTiYuMORDaU7nNCJDvNpBcBpETMnHlC1GkL2vjkt+6Gh/v32cBSc00nR2Sq2IVTQpEmDa37G
TJYtAmRzyhD6N0UhXjQ8B8ZCFhpxjsN4f6v4yLs/gGHDOoll1uXXD5Vq8fEiyNl5/zFhlRPGPC7B
dJKnRiS/sxtU4npX9s4qTLaixubpG0DCX/tDQE+/4dONHbjqtqIgYSp9Sulha9yNA5nUvWellhZs
JCPx+Jf1698smYbi9cNtOmbShm9LmrE8iDKPmhepdYpMy39cTDD7xzrQBF15QEOKNGPB7JiADQFs
etAI5EXOvMELMEaoXIQ8I7rOk7OgsGJHEkNep06sN3MmIjtgIqhqQ5IT99FmCv7qnPNX7achuci3
OW+bNn9EW4WXmBso9rM1g72Bx/+XwWfN20BemEMvpR1LmZhqHrq3sVaGXloQm2YqMQ1wCj/5+735
Ek694heXm2HBiRj0wPfWoNjHSVEXjHsdvbOdJonCtV+qOVRL9pY4uaK72AOrjwpjuBOFhHkKhhUr
/o3EGb3DdinVWxQYlbdtYB2bymt1EnJdWKCxPANgE9U4xqb0n7wPXgqNE+RlfbqVN2KyyM7aATgT
6AgbGJjP6iweo+682AlaLtOkw1S5dI6ZhFLQyLmAyWDhW29mJPyjhJSaqhMUDYpWvakNmYQEkEbk
Oe9rRqj9IPLlOFV73EAbFFzDRlKXzFMkm4DXG7gOsPMBOhxYjSwZ9aqKW9E2QkXhLEkaMEvkFgWF
zdhUcEeETwpDrCrFAAil/fcvjkWUtjvgFmcVpSGDSVonx9nt2c/odkiXMF7oLsNxstOFZAIUoFq6
Twd5CJtvbAo5ZmUHegzQbvLhMW3b6jhQxZsfF8bL9llRQcyNUmQ5uvGnqe83SAlQiB/EW6AEI7sf
d16VOFWc5e/tjNiYnjOTSKTJFQX0tOOXinxRUDJm/d+MKDOajhfL6cF2jRGmJT/HM7nNAGkcET/i
ol/LSUnyPIojSqjPQ/z7PePEv/I5DSKA8v+l4XlYE1pb6Vq1vRppop1Sh07MH5ynETez5cVwbhtU
p2uf1Orn6Ovt572o7oQ5Q1OnvHoP0g7U2t+whd8SPJzbrkmU0xYjOulZvtl4HgEh0t0J9937ZlW7
BZ9F9xiYyFuUPWVnU6eX2LQsIyC2kWy2q93jZzEWpb6lYstRx2prbbMBq15mMGDG/CnQxD+6842H
VsSE/E7UWBEYihpzA/aWOHkG+pPgvTrEwuCxH5ZaYIPT+3cHy11ohnhrEcDXWqG0bWndcdtGItU8
rqmYgU/NtK6dCBw+seSbn2Qx3HPo/7ZEqEAJmcLL+xAHYzOfYoHyyZbsR9QFpwGoV7Cs4BIHQuUN
y9CC/GtrDujAjX7MKaLyZdpO+1mcs6Tk9zIn6luT0GhJusXTdbzUW8DqdCKZ9oMnlYselJJyX+Zf
BPTtK3mvk447V90favHiFRVC4NZqoz2uCbnxxB48WsgIhE5c/UgVOaItQisJOJ2zJgwIyOJmZGfh
cwVTxgp6A9u4FRpUjCQE07B6e9eSbhColl3uke2WSWlDJ3fvtZidfnYDp5N2Wp1NtyaBs5ixyER4
hiDLCk0gby/8RZWrLrWLftID+KJZjh5QgFuYYCOSgH3dX/sZ/Lysy8scIPjDE25LJjgoUVbq1YW5
iZFedCP4laGJrGZGpmEMjZDMulC30MyfaDD9VU4XZuXrU1ZBm9JR3t9d2dqjueVS1C2qO9Cmr+/9
3GiMJrKxoO+BJu8fmh4UvZgvKtAsMxjywk+wO9LjXcFo0hm9Sb7QlQ1Op92pnozCJvBCnq59Gt14
eympxxxCDlPt2NUAjuDMeZSDhOTLGmF6vMwzaU0m9N0PoQBlz6mGYj4ans7X9U31pM1yhkNJ9tmr
aA0+FqxVai09VdbGwtJw360TOCeF/5A7h301eduYnifPJhKD9lDpp7VcDc72aHlmaK9R34NYLDTE
LgHJp8oyucHyuSQJ22tiGPS86SFeuw/gPWqz9A8tLOvtjhmW8D06+AfyVhcxyZ7YWU65Uw08Pmne
uztCzFdM+/hLAThhjYRSlRgY2ke2cmHwTEEdavBS4S7Bwh5kCiiDFw8/ijvsOOUwHm26OmtkPyRs
MNiWmZf9yMRwlIWScYdike6XHOVJWD7kzk3Q9M5UVsqWkWk/T+aH6vtb+/7qcKfaO1wxpOElK39I
qV4PZ+MyvJUYz9oYJm1JbmPTnq7lz0qzGYekV9d1Vo0SjDUJ6QhnZl0lhMzhQtdmNLYyQRXCFCKE
/3hynAaz/0iAAZIfLX4SW6In7CUkxJIanxY2GndeTk/VjlO9N6XiEVy47qiXxvZ+Z9b39EYley2H
y9gmZ1pN2NhWetnO/7SqfUlsm2R/OEavQt1xVPXzz89KCxJaKYx/QnL7Hk/m8PtljK/KaKtQGhLe
KodbvKYhmhNVigzecVQEATraeDVxF9UUwPnRO/qx1izxOxnw3eFnbfvSB1r/EYU2b+6x7fNXcJTn
JcVPpY1IxZRpM00x8xngDascZwd+9xozu5m8HzmjR201mf1UpLHPifungD0OSENfMpeNFAx34YJH
4x8R8Y0uHQkRTLF9wiPAeJrniYpL3GdZ6i54Cc0ldSXODJ32hd2yr/tB0x9aRcwgL3VcqDYTrrR9
FWrYGgT9weZ8SpRxzhwZmuEU//WubNjgJJJztQx2B79rFPI2PobMJSXLtSa2G/vBXE3KfCo7f/3V
nfT6ct8vEW+kHE8Hf5ZQeMzHluXYT2Sh/6bCqLIPr1U0lEYJtswzPvar4vo263vGDCg+jx4KAtry
n2Z7CQhNDdqnI4Es1TGEGr4WK09ENm/dBMb97fUzU3IdLJtZSdHmVOusydaFrQ5MJuEpboFlpc7V
1B5hOZ8pcRjTK1CPHRCuq9+gmE1aRIGdIt2/wFNAKcPmeSeW3kSJ401LnmzuAlQVV+xBJS1VgeLZ
9RIGsPo9WKHm5tU25fe5nwofoLd18EKRoaIzE3/Z26GsZzjgpGOiNyr5rrZ1torc7alXqJVWVGiU
YsFcKE/CbsHWV3w/hQQbmOs0rLhEsxbE1p/sj54SS7T8T1qLrnkxNAriI7LrvrbtP5QKYR2FPiCr
b7Gf2g+43cZcDlqpQMgWzUJbmbtdKrUYeVZH3hRLglOE4uQh/Qt14LA+LGMj/1pPfkEPx/3aEbt3
phyywCfFft565iRzz3Js/yZMSxWfmatPsZJ4PZOrGSithUW2Awqb64tI98LLpInOrC8McQSF7Loa
bszSppbd1YBmRxGbJI3UsivpPP3yJ0FC/pDPIPuGAc2YMk+rKZHd2wrdNWi2AmQlqHF7CxRGXDIp
XKZXAMI2url69krBtdwd8Kr6EkfjgPGbXNsM/aSIZb8E2dgB8WeDhNPpSUfB9ItJAomtOdobZ81i
x4/A+HkXZ+SLt9kCYEkbJx3nnl3VRCt/54tkglaFrHXbFQjDlqhJmcGC+POqbdYkP8N+L+OVjLfG
dXjFd4kTFtyL0LdEqoEHlbmUfCG7Ac5Px5sY4S8QoILULkdbzz8acYrl04lSB9LHlFJCn+247YDQ
gqWipNLSaejoFtNyRX1zVz/qHV+Xgw8CSf6Y2RFeE1/uzzLkih11Ug2/JYno/AtXL8poZemfAnDN
psCV7QlqcLIz1L8YaV0u6RiP8FbpYyn2itQt+mkpNE++Q8g4ciVE4uWWaO9JBHelFh9rkfa0bJea
G4lsYmkD/ERu+hh9SXt/KoPf6fQBv/pKo8Faxq7m3vLDMThnsqLZ+Zp47Ugqde+jHpa0vf/H9Kuh
Zf/WBas3GaNE6XP8qAqEG9z9VoGwmZoo4aub4xpdkDUJ1yGVxFUqo5PSbluMP7BcVfR81AGusnxX
LBofH3snKK/YjDVyALR4vHaqIAqRUew5XwT9a3kFcRVE6bFrpogoDs8YXZcV8boehfA1tXgdEM8M
+rW+XMgNkkaAKIRZ4yju8ZJQmn6Ek+K+aEF/dAtomtwA56rxfSX42hNKVud9A/V1kOnaQLp3/fKd
/PzaWXBFH+h8Nn1xvDWV4Lamqj1l0SSPixuWP6Vy0LjF6MpTFsmK4lpABlFAu0OFBZ4sYyYv4xTN
DbpAdfhdhElkYfHmyEhuZoI1yuIzRSSMZtbZ5SfVI7j+nTpopqdRszvIxY/aYfcpeFobHJYK8JXG
33mmquBTrJ/FbJwL2sbJuDIttRySjvoTDE8vwhndIV1+lzdig1EkgnzBJm73FyTPSjaun0MEwSDR
tHKIowZIg/2WP53NyHY0Q6zXeW6sZ+hGQyQEkWkazSKcjnvi5K2GJ94uKyWl9YyvZFVaaKraLXbU
3h42JcDLoNrdJfVH/2zMT7/fF+vZT8JYmz+yLQDsUYzhX/i7GetlZAu+gLwAz77IjMYCPmql6nkM
9vr/PFBvarLPX0JnM2UfXITOUwgFx423hkGH46GOMtuXF/3AuEzzvedRtLMjVvQzLZJp0eiItvU+
Jt4DsfISBk8HcnLjVbbIWovKfXR+QUsXcSOVzF6Q7cqk7Pd/+XtY+JcC9Y4r8uScq9GohnHZm2RS
niljoESJNUYwog6/gYhzsioSkyWwRN8p19Qy9Sy3zQzc3iziBAdwFR/s9DryCeAH5IqJxzeGs7de
Y19atAgAEaLanqatlMBz2QMiYfhNoPwxM5LtiwPzbX5b6IR9qsrT5e2ghV4dO6jb3hjSn/2f05uN
mrz/3nd12aNRflgDlnXaspvYekbsy0r7Szpgcc00rFxYfT6Sdr/lXDi/gR88iEcfTL4JgFaEAusA
A6lgNYgs6uc7MmUpAC8iHJCnX79Oq1CcKqisr70G+wQbu3YanOmVOjdf7iSn9XH3g6cWNO94RI2o
5S03BWOsLdvHExh6/TGjgFTZWEtDJVF4BS0ehxxbKnaw8uiObtN8+i2cZU3AIhKbunGL9N1VCSQy
zVFJb3XsH5n2l1+uibBryWJxUKyk5UyUokVBL7OCOrNJdKO9scrQz4m6JjQawuuXCryWY311L+mr
GPQi7+rUrPKqlLK+F31ys4c+bzYjBNlUAhc3yq6DvRr3/TbfZCr8sJNdFkwfbTWRR6oaWO4qh31o
Ovrfrn/d38XHKrYKhkSlInfMd22AWoORAZio3DSi3eAauiTd9JRR35fxt/CJyvhc1Re0SgjVtb1s
LW7I9i7/M3HWUH8+1P3pGT2cIXiQGNoZd3+EYxWm/DU2VF+Di4qQdWYg5TnHgm3jn9PL7+eeSY7T
OGAOL7tymUx8Y6/0F7v3nmJ6cJK+bHAAnnnRGyuhkjFUKyEYXMFJXsTUXO4p+GLogALBndsC9uSn
eL2b3X0z1N88Gx1vbRtqoLRuNhOgOxVhHEmtkgggzVptO/OntTU0XfNKKCGdSRdwPwuyYQQxVrNx
cnFnsYs3n+IlXfTPzdSbJXVvgXN2gtsMhDYHiLSTrfHhY4coESXs4vUy6cbe8mlPC5HhuUhHsSbI
aRxZRD5TEryG8EmaDFz7Z9rCjw+h33ohiKI9NeTDRTocXQ5TfETh991lF0HpZLFwpuhyaNnUpvUH
BRd2tlyX9wF4uNrRQwRTM9syt19YI3xdTKsN6J7IBxnsvDi3sS5HG73lTXRoRYg+2GedkxOkyDjJ
AwxPICdWet/WAPXH418Bax5w6VhIzYkjk6434DY8sczVNFLQXMTBRow5dW9j1XxhPOzxx2HG8CWJ
2VQasttaOD9m87kLbutFLKrXT0G/G64ry40jbsxZTOnmbsG6+BeoSatSu+mOwu31ruJy8csNkw0e
yMcOJ4L6kvH5agensqJwjwa6mw4Hm8OLIZ3rHooararIFanFn+cqtEfGj0vxByRtkBcjf/bbXi/M
GOCcNGOkwrJ8siVcHFJn0EiVX3l7txgOqT8IhYN8d5uGw/kEEscNw0uNkOabCCPRW9GGRz7tCn8J
wdryNdyaoImtYMV+zClWNRc9hPbYHi4WixXRF2IucbcUXsYpF1LRe3m836Tnuq4sbvqZ9TgBOivW
rt/RpESI7HBsfQOpiFzVk4ZawOoS0xxupz6qMUHsZHZ+VH+sY7/iZQjev6dTtwujNv1L5JMR5Hiv
Qk6q/WvfaZ3cEbMesg0mSP4RnCnGsmWknrgz4LeaSnmzgBgtznSCFpIuTW1OaeGcMzsyNWyrxbER
bPSoAymdd5nolOHLVNYy2167w1JFh8GKbcB9DdtF70z5ndmV/SeBvhFSZ/6xV6qXs+HHXR9rgVmv
3qbPmvpA3absmvycQLJNtPFLU5Zf+xXos6qpkIH4ITT2TmFeQ6RCWSq6pByjTnsP4k3ZwYnF7F+g
b+/etnpjKdn8ECrdfZSdTDqEfWf451yJ88T5cfCJRLfM9jsS8oLr8xHjLeJTz54BulUePMUux4t4
Iq1LdTfz2yl/oQWV4BcE382VwvyHSOaTRqFkfsv0Gqr77mzHQ+33O3rcWCkpQZRlXs1OOYoPjLvV
gTBx7XLGYnFzF02Pyc3TrwXh9fms0adslgOz3JCivrcBfh1Jx1qO2mGFuTTJ0ZutA3/QlkhB46KQ
Knr7vNk4kpNxUuC4pSZsuKIxXYna3etb1vPcvP7CshMGeDmUEWX5ubezoup+zDJv/l7dzqVsxyi7
S1mMYGjxVnRfjf/PGqryzCgSPg2HJ9FDQEj2B6J/CekU+Dd6w9lHXrxhBnzyizrpRkPI9ay0agJ8
lgDS/4n30EyD2X5ONpLiz8iCNfo7BaYWefVh4+C4anMuHhZ6Oxfx3DndK4Yw3NFtSQjS+rdQZtyr
07PxkBfVDMpgWvpNx6QbdY9gOkQR444bw8G+7lKBxzBcznaXUHQGQxiG+zMmcMP9P9/76Z6pNfdx
r20rrUBkMiaKzuRyTQyPqagEStYUsFeX7cGAOzfUuoc9DnvjB3oemCYt4Q3giE5IQBhFBWV0Qnni
F4BYHIFMxhsGxz2c3DkKvHcugYl26VPM/GI8agukVGNjxigxc9ZYdz4MGHXRy7UkLxRTpAwteFng
YyumPCoqZugWMLYpZGFImu62imztDdxul4XpB956kYGp4YWysmMXREBSuyuTY5w2AE35go2/Tqdk
gtfIGR4KG17JTDaCUy0fLe0ouf7QTbOzCPbo2XxmGN14Kc2/+QBMSBBW2G9tKRPSV8KlANPJd8EJ
MdvKxP+WcCzpSKNvaclfrSsclUphFkPDT/UJ5f1nIo6WJe91cshKDxvcqyHYBXs+E27isRLXviq8
3gCJX8ek9pOCM/0H2Q18MKdtHsxaCd9hZYxZl9chFlYdHcDX8M/EkKymyjfEQAe0EQsfBjUcrYM8
wLmOxE0rtGlcuRyquUfKonspq5XC+klEMibMxB0IT+rIu4yTgluFSYkBeQsizZYG7qXeaCvvn6bI
ZUCJReppOqqbdzL2W0tb78d/DWXXJBLO8nR8IqN7mZOQYJqqWeXWOIY2bAK9AkLTGhGNDioV+CZ+
JXUMfCq+nWTlMBRaLp+6dY7k3ku9F/u0/wwgI57clxKKE9Ssz1+Z+nESrwZ441wnClqjvUE7UqWx
T7mdTLJ9wSrpaZehHzZ1/0B9NQayA5XPm/dEfEpGkdRYB5XHk60BYwwGHP8m0iHZ2I7khxT5zCNI
l+ESX4eDUj/tVd1YxRUj6/Jjvg6revaqOB28Pki+fuWewfyNIaC8zMyDIyGUmucLbEmGIMQeZshs
lJm0SmZ/kP+C0wf0RPK1YUbJW7adxuLk2Cf7tmtFFoTp0LwurO2RjstX4CGbsUEaPqOtefxYHlkV
x+4pTPAu9KYKDZV2XwECv/CSEOY3JGL6cHSAXdM/OE6XI0UlrbmlNFoQrVte3ARr3RCgGrI1rw5U
8J20dYBwMNoOpPqk2lAh4/9oB7+suCa6Rn57EkDkxFD5d6d7zr0GOLqKjbmDehjgG8sI9RzWahGf
+zopS/tRzkUl2zWUfA24AlaBbW6DfUwmVKVSNHtAPc4Uik1mg37uqtZQAQUEf2thrNdbagtObQhj
sXQ9RC2Sqnw1m3BzSc7QX1n6+PeSXmwuUrohK1b15CZDB8FXTnSr348g0J6QOEsOGirylSWZhuOn
zTLh/oT4MONtqAEigYfMNcxjDCZxEOisCxoyOb2SDENK3uyvdp94mbrG24QFQpE+JagfmvzR+nB7
Tmxbz3971vD8Iewfw1X98CzRrqtbb0RwQKJcdYV1pPuV6A0kOMkA0VrLLuZYrjJYC+FBa3ok99DU
Qod8HVFyZC0B0fTW+CcxH/53xJx247NzM2Dm4c7PgrUmmvkYg9cdUzeLrxLH6zFxdLey8RosUntr
8PLmkmza7qDP2JONFq0uw/i9mTQfugwSG8rIfgCxC85tmN4REEMHOT4nxhYb2mzOLVTD2VITB1VE
ku/9L2NVIe8I/4bWbqBm5fAQWrqJByq6cT5KLpzM/16zD/klt0E5ykvvyBwO5ILDSVlFYiDsth7I
prAons6IeQR0hQa9IiWiZYdIRTzc+XCkkaX+asCkg6J7U6rYHdY10F8XsKg7fATgXP6/+ZzHAGQ+
47IKqzyAEsCOnv+SBbvsBDrEhsiCvmVbHxJPJdOdJPopI/dAi+WL1NUm/lpmyt3PLbeYO0/7EUZF
9d0+FcJFISUphwFJIgZZh6x2oNBfj1/sFCifFNc2Zeo9p/Wtyy0/CuN/52xII3OGut1N6sfEHsbX
LbVJDOh/vuzaL6jeYI4pRXzTthVA49s1h6OYIGRtagVoEQe4/U/xnWq5MGzstA3cOTgv3234T76B
/dBWM5eUO4hQeBZ/cnnp9ljs+QAHzCVdAlSkzbzXPaowgRXcpkSH1MqOoUR2UwPQJtLKYapTJ9f5
h0iaaEXPqMrk7+U7TALhIQ5XizEHAJazWzJB7hGxd4mdGJb7ILcSAcw0i+qMcEaJBBvPtlq49KG3
duUAneTAJLtYKYxWBUipiWc0YiyhXaPMrFcAwSMsKMQQdNhmPWu6RhsQTSHzoqCFGSOLgStrho6H
Df0UPqFT58ru5YzVMcw3A/wE5Idw9bVgAFHtQwrfeZb1oveBhezRhdFNDd/E6TVRy16GEBTnEWWf
j6dQyn8oSeGaWY9mznL2XUNjVTFRGJKeC8GobcDi6HKe20zKRDL1hHy1/7u+PWl4SIVtjkU7G2gK
C9HRan+jwvrdHGEckXcFwKCVuN2fBJ6ZHnrYPnGqpQYvijjxeD7L1i/vV/f3uh4BiF1Feq2YVlrh
0WcIcE+b5ptjbFzvPhFtfoHHJ47O1oKdPOYy+i2M0Z2Ywwk20uiSM0Ls8VgzUCt2rE5Su3W11KtC
GoXo3WmtPbfF1+7o6hVlOEMsMZoaPudkjCoDMQRx9ey8CzDjWhxYClhrl3PP88tt7wCUYTuBsidN
6KJS7Jg6tDec3B9KDQU2soWTL04LZiAvXeE3GLK9QXXb65otQZNHptcpztI7nnFrMOlJfeMMRL13
pxPap+DMrxNPbcLU1Tbd7MeptWwCdAcAq1a+WxhMv6kHOmZGzAny1rdUpZPXt3GgF9EDL8Z0f5sC
LtlGqjW7iGpMq5QFOQaTKtvkjj1YrsjvKmACTnTFc2QnCZgFW/VPGG2Yepvg0FkcsSZJ0GifXWnP
60yDrSjttul3gsSK6tmWtOuSamzvAaPlJeX+cN6rsSG1hCX5FPQea9Alx5B2PXYOmL0GcXKsQN+7
C5iPqvZ/Rl5RmWPon+w2T58btQflod7ZRiNNb+ju9Pd5qZGFCMhpzn106RDByfSdhTXCqRriew3f
JS1jwWvgTdk72ppbV5gixIAwe8PTQnO51CER36YVGbwtc/nJJkp2Xi0nqoCtUCYuwJZsOPyrkYDF
4X0OlklHUeAa9NuXNp2CczfWdT3je7cjiQTSFCCrjHCChA+7pTAc5zg/FFlFmnvMu094eE75TdbI
+D19ZU/Dis2SYXKXCIFliS/aXHJgQSlHZv9Rp60C9pcpNiYo8YMWu4PTBHpwq1wwATwDYk1ov1VY
5wXITVJwZn96WGec5tXP9JoPJiNy2r3OvK3K3bmIJKSQKGakirGj9uhXMcuU+noPhLTXZAIxUsTu
po30B4D8Z5cTiqGsW0PoXqMi3gXpoALVggxyqeoetlvfBSz36YWdokdctjjUgmXUvRcbNAtIdqx9
QdVJAxPBgSHTfinD+sJZ/zz6bLwQ9R7u4EZJFYKfg/4L23nohamfzqm46mZ+8xO18bT3t+qIk1rF
NGuSHz1WaF7HvWkhRLJGsNRZeDJhNiIDAWkP7cnHU5fUGsrY16rLUEzOsWqRAj3PYTy+WwKRj4nH
sd1aSzE3mS9vV9z0/SLRBkBtCP2b3BXhdjbhEwvRUZAtvBmrQ4epYpU3QwkeLP+Wgex1g4b3Vu6P
lt90CXyjIOZfmrIYiiPHtz6bom1ndjGc/z+vxxQ3Oj5UKaX4+ydibmE07Sr5pSLMLb7FD0Iyzd1X
elyEg8rde5zZjHOzLiJ2j1Misf+BgTaxkA29rsIyPpKcMnbDaKzP7p9hNhEhmSu4d0I0E/1gjVZ7
6SOwJbVrvXhcLSpUU+mu2gxQzmS+4InDtAXxH36spjWsb0TOG6tzXxxVyx/AwZD2gAsDvcX3FUHQ
Tn4CvkAQtZU+OV1BrRQQlDe2JLucT2iZ00cGb1LxBNv+vJ6CNt9TIR/gxB10rMiXJm0K3Zd4dmIS
vkyoH4dCmWs+uttX+TTr5Fp89eRukoR6M5l5aifvX/4uQCv9yZkskzGd5BNyafppgyoGZDCO9DPE
7ag61/Cd48nClQcU0vxKkl1xZq7qL1sJHu7GtjpsXP+3n7XbruxXWUcEd74buSZlNKyUkvpZ7qpM
++qI9g7oZpZtSHQ6lP8PokosPuShA6IDoi5ANss0kF8te0ENpjC/FZ/3pXQ0dRRP7lOKcvMZ6wI9
/1/ayq7gBZLAlNMcuYaW6jYYTxFRu+1ZWBOr/qjPh8UDqMNkWfPM7Uon9++Kb/V+CY/O5Jdj3BiW
RGh4Y+hrRG0BkJfbszND0bPzX034c2hv9YHa99btr0UBd51CUT33I+WzeZ6Xsxzi3nbXDYqKxMlS
vydVfJ12nVLVg5PASOs7lACevv6NLS5rRg9J7INfscWCqG4Dr3aubne0iI0oweGxE+FR/H69tibN
cIf4iftrRdFtnGLGBU0Xp5S+gCXUbYT333pbYHfFshtwcmHvXZyMGk0vtPOY0o86re5aFxl+bfzi
WdY+i0XRFZe91SPknBLMQKymNjtLvh4n4o6CsKRFTDm82cWOi3740W9Ziv3wOkxLdoLTvdR4Yvg7
GJlatbDo5KU2NT7DEdMsa7I2DqWrhgyEnAONjPBfhQF94HSpD24rUviE35eNAupxTAuGfqcFIdGS
umiukymOKJoUntWW1BeZaJZzYnq1YaDEEMLZoWzdLziiSkIxE2fHxr/yXcVtGKLC6tM0xxvV/1ZI
NixHhhmEj9y7lIpdF99HUa6EVqyPGxS3Uw/CH6ZbZ1Fg2ifI+6ZrfBhxO5wCppnf9NRJuMzQqB5T
Qxpx5deHT+P5N2QjjOCXuBWb46OUwF8m2Um/4J5ModXSdTgQ1yhwJkXjDh50+sXVdWXPcRWs5AzN
Lgkj7BzDhtXZfmEH4r3GfhZsPszx6XF+zpCSW5SkskU7MuriwsN22RSZJWnW7vzHD06uiNw49m/7
kSSsl1N1iCstKzJSewYCNUMhkM4NTKiaaJMayOPgOrPhbKpfGHOYCP25mYj0BovO30rnaNfOcfc3
81lALw4usxAKlZA1D9XD6rvTU5oBqwuzU7MA/RnEzFOwZtPx4d6J83qcnM4PtpJDfrgsjxKdsarl
sQ1c9fd6W8R3yIDZFTEFcYB1EGBptAAuMGqadacc95C0eLlaUXYbNKoggyts8Bgbhv3BV2/P1ldP
cKprwJFIFxjAHprrUlUnMRH0ehtu3u72SOMKn8DKEHnzHf10UNmZhB55SNcjq0xpXH2APmeikiSy
HFaoMnTzBtF5YLbXViVVngv64muRCY5iXXIBuC/FH5NHWs106u5EbR7/0Rfnf8rQgFBVCxj6yq+5
kmUSuvQIxHQPS7tppMAYBuFEJo0hXftkwdEWrCInXqIpMJzM7T88nsENlejlDBX8Q1eV8fU7wI3I
uox58K2nHRhLJJYXnL99COnoFtejOJ6oYRsEfyVmH7gYyuAb6Glwk5AdST29s6rRRRmyj1xl6Nqz
b4wswmfmpEiMTcftdq7oOoOBB08fgZBUf2SENMjHtybHZ/sehY7uiOXB21PtyGlu7ZNZ31XcxNAt
wUznCPIQGeU1/uEgYq8HaleDP6O3nbzwWd+j8ucYiOaghUTOxNRCkWW4pn9+v7DSG9ufaPKoQg8w
b68RnGJhK/NSsS6Z7JEcL89QdHjoy2R6PxEyl6w3pT5JVTE6ghtKz2CapdPhKXuXyaZMS1UHrok7
IpTedc4ZFxEBsOwmxaU1Xwam/Bc6jK6UcyqMhgkEgoqLX5sANd0FwscmbeWR0HtP2oU4d9IzGwJq
i10O3oeYpfpMmF2IjfrTPHE2zjvvUg/jEhSVrbaq8jgRTPHwzrpjpcrqrcSWAqtr4n4AtLxTaMfk
lM5ynQva24OZYKkyFqu5oD+AVGPxk0NtxmQIX/kjt5N+yQHxiltgLj4ask3RCKeLCZ7sJRPejSIh
DgGywv6Y0KjQNkrh5J9ZWkEEZWdwfYdXur4+PHLBktaD8OrUl3Ry81L1thtcreffcN7A/JptgrOu
4jdCXoSnsvwgbZaym2FsXHiM+doSVzSElJUTOcnlA1yBVgSC056SnpC7XeedIWPuljyFVwY3y5nq
GfraTYnMBJQidfIKYqOzgHWAd80rRbQtGfsDelA6VwA0Y2pmA+p9GgfKgktHkO3MzWSMxru5HFHW
JDdeqSj0iDrvEJmLV+D1SZoiI1CD/3kWX00ndGyghH7ndiZcY/gZZYd0BBlUoVjUT4rGiJEL4cXl
mZIkFoUaF9BZts6BOBSPQzWFWIi5HPR+o1cUog10WL0tUgdzEHxRwAiSwZHxQwmyuqRrqwiufZQQ
hzB5BF3IAIlp6KgaUDMc3HSrdtOZfqo0IB1ow9fwAEqqrdAVK9Qe76d0bfviQZN8nX8eNxatzg2p
eNWlCA5LkO/P5Hc7sr6k2tHq5mwykv6E8x9z8htUGZLB/U+UlFvBmATw2ZBP5U734Fn5lX/X/zQ2
HALw2hgOCcHzqAdo7zUPz1+KdUdIi4GfIt/svktskBcsQP4PlNu6HGf+xBaosVKx1j6XSI3DcS1G
2bl9ImOrs7kl+ti8FZeRXPJbdbcfkjR5n4WVTiYmNv4vPO73cKXsvTchVsJ6TnVqwDu0GpXHdKix
nDK4CmAThPmoDYohk+hRf1DxaMFJmMKO8U/OIbx2/pckSYkc4Dmc0tmLsfDmLyIoIxdUwhVE72aF
cgn8UbqBxEwLlK7gihXPcKmr73S4Ab4bIziLtMQHeCV6MRjYgl8C5ljm727xeEmsPcMXqPW2k81Y
oIs25+J9t5vOk/ZjysAJOzwTpUMxlP9oTkyeQSshiZ2LSy9peUoUtIGTMEO4s/Qc4cdlZyRSggSz
AqEHF1rd2vpqtvO8hg12u3E888VCSsBizWPZHSi7rL1lN64Stm+NjTl3l/XwzjJg7UPawKl3VCvy
gyZnn0fy4VtFfxN9O4cWnyqBeQE/QQs1NvE1Fh7afvfyRgthD+EbTRGled2TbUEK4gwV8bA+SPRH
O7b7XcSde9utRPo2sxypFbE88qaHUrxCWpe6oK/bPJ0ryz9gJT2S15L3y1GJAKsf2TaLzJgiu2t0
mXtQVCo8prDDlPWMIL9R4oj/fhKUH5ltl8clmxadl6Vmad7B3gbdhpHR6XLymlk/pOOcFHZJW78j
31tBAsWxV5FoR+NaY9wjPTjkWZHHvbXdAXYFww5E6VmgqPd9V+8djhu9gYTjAXxhHTBM+770qC27
Tws8+lOPWp9+KvEJqsf9Yfv4Hn7f0KKq4GmKHEg15NP03INiIR+gJmG8ziEnvzl+rLHYiEUhOtnV
Ajb3czyQxwwvV2LeoUfD1JdxaRRbgSZDuOubHzU1CrQji/af+/Va2xXuPEGINu+RoeovTvXHDWPS
jahEqQrQoO9FneOB1k9sOr9lzgslzVmgINUgw4qr+JGTyRw1PBppxvz+8EN6NZ00thyTmXV7YAOR
cRiiJ59r5poS9J1PdjEo0ezTlMiCHQFvQiLgldYzVDyrSdy41QltjxAQpqclfEbXrTUtS9qly3xo
+p8OkFnl2WxtMFUqPUq1YY0kv3lzcOKPlmuy3mnyeItyVdcizMAammOqy6r7xoO+5t2w+0TdiGxg
xx7NZITm7VOFbg0cyIWy3P5GpqZqxP07Mbwg3eOxKPV1GMm8HYeB47lJre+UvV3lWvOIlRpZOtbA
eqjxHTSoQs6J+/zi+j56pv2VrdzLBtordCMFsit1/nImWbaVBgI7R9o08AmHL2GrKb116R1P3JT1
5rq+HtIiNkc7HfSv9Rb1kSKXmcAHPUnfqDQwXAVI+Om3Hj8rWjmCC0onkzRHDoSl1a3ox4HxXoEn
eHHDVsPKuMFPjMzjkFSMcac9dSZdu0OO7lIzaUcsIBVCNq6CT6C94hSwP6uGQu3/XCJtsVRdFbvE
DqvgGRH1wvAJYIa8rsBhDO54Hr0Ge5pDoErEg3+G5oVKFIdl86M8L6rV7sLf0ts4SP+YFRvkTrbe
y/Vvy0QtE0dl9qfNxT3i9zQIGYeV8PM6CqzHr7pr49h3238/DecAymrOE9wBYcChIrKMtElYSuJZ
ClNI72xdTd+1G7kPskoH+csE3LnjUOS4ciJ/y1m1ic2xFubYkv+71PWwRHgqPOddKT7GnP1dnaHA
kBYsly8P0jjFX59AemT2fOYeliLt7gy8uLR0BDu7pyEqT4lDy+DbupKhN0nPTI334Pdolmbj546d
XYAGcA3q/ljNoVJ1cI5kMb84vHjZofszQ5bRGKFhR/1qY8QA9r0lAXi4Z0oD760UghmRxjWROmuy
2xSU9cTNfqDb5IHlYFG/MJHfubAh9lAkiX9lRTBQ1bsHf8oxaxJKewlrRNGSVOzkTnyzseeUf8Dq
dxFyGueYMi0puVFjYfMKyEIkefS8rAUgfl2pwtxp8f3s0dW+Ikzn/L/dwt5GMUOElDUQojrdIhUK
CMb59FqpZ5CMJleFExxqBFDWQHyqCagXAaqCB1T4X1EDiNBlifOh/EdZs38Skl+Tw+vta6pAUIrT
SRWzcoTnk4h8qrVZZNOC/dZecC+I5lB1QyuHSUPJKss7n82jzCY0fs/jNjA34HLzvK+EToVu6cId
ebccJ33Po7fxSrYnYkree4XLiYqkC0kLBkn9fmXCM/Cz9BBDTpLID1W64ErgivHtTn4BkNwkc66+
G8RXGo7/0A7RAarroQObT5ytXpub2M/o5thP5cxszyJFUVuV9mWS6yUmEzUNP26aopRrOMXSan+2
iPZmWoybR8oCggptRwV7z0ldYaw+abGsNiOHxw/LwFDaKr/y6+3BY9SFs6qxhI5KLusHubf85MRK
UeKtz40mAUXwpfWBAz7JuBQZkTtVloTigBpr5pA54ZPKdkUygH0KiKIfHq9qMft2YwzDlOEeE5aJ
oMMQSOPrrlGoqM9IFVE+Gta49XaZDgAmPh0F0+o3Uh+YcbuDo29l1/T7ZdZ8VXrQqzwvQ1C8NLqT
fgVWOkPpzEI7GXC53B/u9gPkmpSy6Zoz5+JX5wJXTWFB2DZu3ip8jG+Cv/vKLkCY+Ra2eiVdoPts
ZPkXt6RmEiZ143P5GGcc8qFMnHErYhlDG8y3+uC+kEcNszxsLD2XDBzPK7TnOTsygjX4UprYQ8ph
7vR0zrJC1vPK9iMC+3hxNYbwQUlHG/JZrnRQ9iDdCQSOdMLX9n7G4q3hbLqG0urif/RSFr3PdODc
xO5gB+RzKuEhqtFNYDrAbn9m9nc6e6rrB3eaUCEqr9mczq2qQ8NRuGzAyk3V8YohciMsomDmkGoI
mVnvOXz0xDTGsaJupIJZ6iZQsAavxOH92x42P5Kx2yTIIqiRKUTBAHhZ54ZMm7WT/DrGG6/NzgmZ
F/TWe8xZRoKorYMhVKf2LOuXEMD5BqDQS0TBAKESP2/LY7eE53pstAzbLx0t+l4A1Vomi0mEECyr
qkOKL9Ff+s29xakXkGzVNfrcVV9O+VS3qOXZ1+c/4Xv860wbsV2I802VZ5SyJ4ywTCtrbLcm9pL6
6oa7F2jRvwqCfSQxzlUKN4c+Cy3+rmtLIMtoDwf5ogu6GVZM6NTYtXVJsa0/QTfwaETDPvcJ1axm
3lYFPWKa36QwTK6azWaH0hVsiJ1y03gxk9kdZpYdmeG7xxbJptmXT0jTAzHOUUK2YT65cNdUunXd
WuELZxHBr7uEOn8ptqFS1ldqSJYGUmpeXEWuket1vJmYdG8VN/iAJmJqefUFlCOtA3KpLeKDSkGO
uJoFYc5kb6RoN2Z0O4CGMps8w1masM37nR+8dHJXNxHvjzlIEhQuDQLzVL8h5PquYhQAhxrAPQpr
rmX3A3YMNyXQ4RLHaThQ/cQKgBitg6/IoBCwkFgTuwXuuaCvFOlXLn/5w5UJVB1HXs0vhctCvkPA
tTzNAtwNOWA4dwaG+vmz5udrwqDwlr+rxoQTc+Hj2GOzobb0D989vx8PHRH3XO3iKMmDyW4UPHvc
+7H6fN0m87z6mVx3ZeRLfA79O7q7+UMwi9mgVgOypE7deZdbh1iJWLpK2WSSNMMuwbXDEVN5+qC6
9n7ICy8Ww224aqYrVbT3hYZy0GZWaQmB3WxefcVMYPXrCQyBFLNij6Q45mqP3s4D+VnsXMrbQg4l
lkLJdWrlE//4CX7Msq1xDkm+3OPGeAFT3iemRdUFxaODuYXKceC3orITIKAk6qTtuB35mFrXHeCZ
NANw3yqy0jmjjSPHHhyuiNjeZi+LKJLqgrGlpXCIdyvrv3gMHvEHFM37CRZb3dgrd7Ckb74SV6LS
FopLaKoV6fMBie8JHttGmld/zK74S4nyoLxHLW0T7SgEGOJld73SnMJztg7uio++ex/vPXk6J0S1
Goiq4woDkWnPmK3EvAP+D9zq1dl64iltbYtFDb7fqWYE196jmaTbVkcWrYzeq+8WOJqeohvjykTx
NoU0NTMdjJU+NpEr5W539kqYyoXpse6iU25xuzxUWiBbnZ7xXwOrDT3uug89LNXWl2WCibONKpbq
5b+Z3RVTTyjyKYJbaEEwWULfQ/djxyyb+GSsXaODQ8WSWgL7y7HTnBdeZFN6TH1d9WbmRoYxf2Qc
RYGh2d/NIzbTnA4AQ5c9VgAEGE1XOZjnurV2tyqOCdZzoY7xqruciTPFWIU8OSGjKC1AT6GFqwp6
A7xkiiUZbtyaXlcQ8Tudme60vHa5xsgKgMVT472dlLRuNLIfxNKMuClGM3BJHa5p9ULdFGNdff81
jbZt7kK8G6U8f6Is6zTuOpvAWNpTT98MrLbNsjU7KaiKWuvtOVNxfiQ9qlZe+Oxdamgt/K8RtUua
ZndEky+6mzzHQytdtqZwvzkaNs/dLdEDD13Vttj16956UZ7jnQGav+RrSKnycVt1m0FRU9Pw9naP
7MBFCojQTN3oZoZ64L8PSTEOpPwUTuVbN8lmGUIz1dVR4K5n10wbP2GM3fbZj8PPNBNT77QAwDM2
/U25+LFG7o1l+FCF6Yu2K8JO8G8Nw3Hsrb7wAHPS+ZbjGhHxy4itghTj7jWSsT6DPFsTWRPVnjiL
/vLyEDsOl5NkRw3iLawCP1/WIOJS6TFASY+k/ahpM9gPkDYtlHgxikMaRFqqGLNUSxV1KNi61RDt
ZLiirWcXQYT7HfgrwPv9NX+4fuDGDsbXnK2P5Ps6JkQTANYJRbgruz1GLB/4kmHkuqqGUuY9v3Z7
ydEZIyH5aJxurKYe4jQpX+xrt+hp29V6L9Mn2XmbT5Ns8MqFQ/Uxqag5VgwqyKqmS+JHSqsjwPc0
2Ch/zAV7VU7lHBbTf67wLe4LrpjpuwJtRN8GTE+ptige5U8i86Zc8FcH6+D9Tva2Z5R5qQm4D22h
znF5E9i7J4NnpZWN4yfSLl9SkZL1H6M2OMaaKTUQInKfyHPxyZRly8V1LsZOByiJ8ZofIDeT0SHV
Ikc9ziA/fQHekAKWu/3J6631uGDZxliS159z7FzJAPdulh5sA7jRFUweCrHP+YCc3aRL1CS+wQh6
wu4eryEVaY5YcbWsJP1RDQJ2HubHlUHW207TUimACZnbjOW5ybAYD6dyPg4+srky5IVFknM2b0Zr
lib6n9q73a0kWKVyDoGnFM8hVk7USriHtXSmXg1HSIotuAT2k1l3+EpaKDhlHvaVwkz7CfXM5GaF
wSPe/yis7sCpaBCRgyefeRBrOGnqQqORPe+XXEzKrs4j5Pb4nZgAaH9UbxR+7Geb5BZq95XzSueM
JzIgKnWbsNFq685LwT+P+xQJkmv4cVZbRZ7H1N8tcwOTQEqGOGBnQ9TSIvcOZBauHGvmAPQcCLj7
UV1kBs+14jGqdFgThwTpSNsEx48g5OAFNEeiyIhqk0YMK97+WgvcLWO6SLdlugfa0kegwvokCwDO
RTRQQP7pyK68gBaHlpnbCVw+/3LkNzpjf4R9SN5CtpEsXba1aNJBvykzUxXeX+uFBrIlaXNEEZPe
4uXf99I+9XnUDZqisVDE6/VEINSECQhUucE0dix2u3K+JAoq+Nxp85hFV3nXZFX1wG4LkF7KcZlZ
+5IFYMtIvbXrP0Md9ZyVcA0McpT08YtbhpIwAQKy7qIOOO+X8J5UURc+CK5XnSmsTvSxacGV6ovv
SZNQ2O3BhD8PCMRN0JUOa+MORrCZ2ZgR7Wpu0YjjfwLTFL46gQAbGdlIvgZ7/4LTrW9Om8Dct5Ot
yRDovY8H3SIBDhiwyaZlc9RPGJM2YvM1DOFGhTOsm9FY9QGwG0lm9ojzmznTrAle4w65iF2GCkaM
Ji6ZBV2fTBSWvtL76bouTRjHl0m7P3yHBs6ONYheeRfdpgncqJveoCDtXny2ixHlThCbWUcwqHfQ
yFAzJGTMB2c+S5TANYoWLxU1XX9wQADZ3ayKSA8gNWpSgUmGdy788Hr1r8ZbstqK7uyiJt3gJUTQ
wrKVAEI9tK8xZG99gLr6DqbfwzJ7607OxB9hcrrFZKqoYFCwKe1ulrfmWt8psT+kuOvtTpuOCPTR
0y8UqFvvjWpNrPrXzlGSAdBwd+dNCE0rTFNFHneyC4Lr598ERk82wLqKtYnzFdHqFh8g+r/MU2U9
3UfpIoPjF4os8Ek+qegyaTjNAeZGoH46YiGYm0Nc936Vrc3ipqszZhczU00XvjHQ0C/t19qgqeXc
MhdQWsw1zXRhMNtsP0H195Y1ldKynjODdXgCLRbYbkUn2RX91jeNg16vCwYslH4ciCP4BWVl7m/F
us080hp6senm5pC6FRehBmL0NUerMrkz5dQmDvhx8V7MsnFnX1++l8a8I16R0dtrxsOkFKpCKzF0
w2hwidSOje9sso6ycg0SystqeNpsGU8UUvA587Qdbr4431vppmU1fAtK83AhyFzzqGW5NDQsGMjN
9QfXOSKcAheSTyy8fvsYAt/KzYOgFiZfe0jVPpHGV8UFEY5nno+BzzUS9lwleOS5DXiL9pPKu3ck
1RpHLCMm40wDw5H8M2AQo91XyHjqrZCPBIfqxFYs4Qi9k2fNO3Jg9zFliJnScPpN7T6LtPBZmpoV
GBOTL+3JktLqwKUUiIp/mTOPz/aZSK6dXKDwViWehTofs8FhSg1HnwylHBBtw4wP/d7h4hD8Z0W1
GibcyKCkj7bQ7CD4Qj1hIdn+gaiAUGfUoctf/U7GLgNuZYSirOXd0LEfkPy71CX90qDC+X0MMD9k
1e1CYP+MDe2TH7qfGbz+wkt7jhhNzJPpvaV+UT80huzv75KTu8rIKwLC/mlDZGLt6bi/+DFOHy1E
CDJiyg+EUunyZeyoNBwFjPcm/UL+aZxrFtVpA7Zs4s/Z5fr5LExgfennty55YbtUv7rUTMWGjqB7
GDx3HPgg//1T6trOfQlJZ3C/9cXXN+SgX//DISk1115gQpWPEtmxInLNaxOpft8hQP9T2pKJGuMr
fbHnTCbzM7pBRLH7ZP8cxS7qna4+cgES28QShh+75rtpF5PknVXGicp0/3xu93rZOSfRQHoxgca/
9EyxD7L3kx+ZkxGdI8fgu1FxtS1nt6Vgop61JAWVjZ2y4EevowkJsA0a5rF0sgKC8CgLep+9jsrZ
cHPDFBESHQ8ZlRk50spmQB0JB/juKXJGSCYzyIvnOJHvPmSCE1ibXhY1oBK8U7NJSgjAAvScxhjT
RYCA3d2mojEjVMQHlRFh3Fn0K6LafMUugjIIr7QtopHU+4DUd5d2W0g0VdcPfUbxr8rGzlgLY9sy
5yJEv//eaHmQGrOkV811LGvH+GL6u1oBC7Qkxg+0iDzB2ZJTpQsEb4UR1fFWtfEroyyRYG6MsUJ/
+ldMSCO5FV4Tx5EfSB9m8g5L5TdVqbvDxuHocOEWVitZdaR0n4C4MPIfPYKiI+QZGcvWmoegp39q
x1m0wUWZeJBzn26rdAfqYk3SzvOBF6vTBtQhEbhcnRNiwuZBPOGyRfSeqMkJaII9ouTc5BA5Fh3C
JKzwbkdII8eHgNY0UkQiJUtx/lznL04ejxDTVf6YfeQd1z+ZuytjVTpfjrTImp1RoHG0nAt//a5z
kpuApclDqi7hSytkBT8pLMkczdhgiFjS+J1Gb+8e62UB9b1ypyJZDo1T/nutPqUbYhHY9Z5OqIU7
7dVrPm0ggBA8Wm01aGZXfuiA0u/bpsw/zaC2Pl6nXZXnlqD41WGM8sZT2xlx496MIGj8WfyF6dlo
Fw01Tw5/UDb3Lbmun3jCn0bS
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
